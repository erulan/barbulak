package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.barbulak.generics.CommonVO;

public class BottlePlaceVO extends CommonVO {

	/**
	 * Azamatshekin
	 */

	private PlaceVO place;

	private Integer bottleAmount;

	private Integer emptyBottleAmount;

	

	public Integer getBottleAmount() {
		return bottleAmount;
	}

	public void setBottleAmount(Integer bottleAmount) {
		this.bottleAmount = bottleAmount;
	}

	public Integer getEmptyBottleAmount() {
		return emptyBottleAmount;
	}

	public void setEmptyBottleAmount(Integer emptyBottleAmount) {
		this.emptyBottleAmount = emptyBottleAmount;
	}

	public PlaceVO getPlace() {
		return place;
	}

	public void setPlace(PlaceVO place) {
		this.place = place;
	}


}
