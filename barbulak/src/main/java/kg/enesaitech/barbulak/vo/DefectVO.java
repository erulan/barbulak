package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.barbulak.generics.CommonVO;

public class DefectVO extends CommonVO{

	private PlaceVO place;
	private ObjectTypeVO objectType;
	private Date date;
	private Double amount;
	private String description;
	private Boolean approved;
	private int objectId;

	public DefectVO() {
	}

	public DefectVO(PlaceVO place, ObjectTypeVO objectType, Date date, int objectId) {
		this.place = place;
		this.objectType = objectType;
		this.date = date;
		this.objectId = objectId;
	}

	public DefectVO(PlaceVO place, ObjectTypeVO objectType, Date date, Double amount,
			String description, Boolean approved, int objectId) {
		this.place = place;
		this.objectType = objectType;
		this.date = date;
		this.amount = amount;
		this.description = description;
		this.approved = approved;
		this.objectId = objectId;
	}

	public PlaceVO getPlace() {
		return this.place;
	}

	public void setPlace(PlaceVO place) {
		this.place = place;
	}


	public ObjectTypeVO getObjectType() {
		return this.objectType;
	}

	public void setObjectType(ObjectTypeVO objectType) {
		this.objectType = objectType;
	}


	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getApproved() {
		return this.approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}


	public int getObjectId() {
		return this.objectId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

}
