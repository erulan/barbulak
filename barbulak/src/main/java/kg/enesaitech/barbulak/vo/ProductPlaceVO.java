package kg.enesaitech.barbulak.vo;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1



public class ProductPlaceVO extends CommonVO{ 

	
	private PlaceVO place;
	private ProductVO product;
	private Double amount;

	public ProductPlaceVO() {
	}

	public ProductPlaceVO(PlaceVO place, ProductVO product) {
		this.place = place;
		this.product = product;
	}

	public ProductPlaceVO(PlaceVO place, ProductVO product, Double amount) {
		this.place = place;
		this.product = product;
		this.amount = amount;
	}

	public PlaceVO getPlace() {
		return this.place;
	}

	public void setPlace(PlaceVO place) {
		this.place = place;
	}

	public ProductVO getProduct() {
		return this.product;
	}

	public void setProduct(ProductVO product) {
		this.product = product;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
