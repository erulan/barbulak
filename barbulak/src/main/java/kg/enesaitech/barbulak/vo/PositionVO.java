package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class PositionVO extends CommonVO{

	private String name;
	private Set<StaffVO> staffs = new HashSet<StaffVO>();
	
	
	public PositionVO() {
	}

	public PositionVO(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<StaffVO> getStaffs() {
		return staffs;
	}

	public void setStaffs(Set<StaffVO> staffs) {
		this.staffs = staffs;
	}

}
