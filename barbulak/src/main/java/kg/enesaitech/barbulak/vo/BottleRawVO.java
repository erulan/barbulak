package kg.enesaitech.barbulak.vo;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1


public class BottleRawVO extends CommonVO{

	private RawVO raw;
	private Double amount;

	public BottleRawVO() {
	}

	public BottleRawVO(RawVO raw, Double amount) {
		this.raw = raw;
		this.amount = amount;
	}


	public RawVO getRaw() {
		return this.raw;
	}

	public void setRaw(RawVO raw) {
		this.raw = raw;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
