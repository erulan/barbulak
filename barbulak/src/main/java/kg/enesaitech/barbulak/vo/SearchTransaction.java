package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.barbulak.generics.CommonVO;

public class SearchTransaction extends CommonVO{
	
	private Date fromDate;
	private Date toDate;
	private Integer transactionCategoryId;
	private Integer cashboxId;
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Integer getTransactionCategoryId() {
		return transactionCategoryId;
	}
	public void setTransactionCategoryId(Integer transactionCategoryId) {
		this.transactionCategoryId = transactionCategoryId;
	}
	public Integer getCashboxId() {
		return cashboxId;
	}
	public void setCashboxId(Integer cashboxId) {
		this.cashboxId = cashboxId;
	}
}
