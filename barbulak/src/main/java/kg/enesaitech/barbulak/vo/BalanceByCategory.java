package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import kg.enesaitech.barbulak.generics.CommonVO;

public class BalanceByCategory extends CommonVO{
	
	private TransactionCategoryVO transactionCategoryVO;
	private List<DebitCreditBalance> debitCreditBalanceList;
	
	
	
	public List<DebitCreditBalance> getDebitCreditBalanceList() {
		return debitCreditBalanceList;
	}
	public void setDebitCreditBalanceList(
			List<DebitCreditBalance> debitCreditBalanceList) {
		this.debitCreditBalanceList = debitCreditBalanceList;
	}
	
	public TransactionCategoryVO getTransactionCategoryVO() {
		return transactionCategoryVO;
	}
	public void setTransactionCategoryVO(TransactionCategoryVO transactionCategoryVO) {
		this.transactionCategoryVO = transactionCategoryVO;
	}
	
	
	
}
