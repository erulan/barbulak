package kg.enesaitech.barbulak.vo;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1


public class RawPlaceVO extends CommonVO{

	private PlaceVO place;
	private RawVO raw;
	private Double amount;

	public RawPlaceVO() {
	}

	public RawPlaceVO(PlaceVO place, RawVO raw) {
		this.place = place;
		this.raw = raw;
	}

	public RawPlaceVO(PlaceVO place, RawVO raw, Double amount) {
		this.place = place;
		this.raw = raw;
		this.amount = amount;
	}

	public PlaceVO getPlace() {
		return this.place;
	}

	public void setPlace(PlaceVO place) {
		this.place = place;
	}

	public RawVO getRaw() {
		return this.raw;
	}

	public void setRaw(RawVO raw) {
		this.raw = raw;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
