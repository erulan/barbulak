package kg.enesaitech.barbulak.vo;

import java.util.List;

public class ShippingByProductBy19 {
	private ShippingVO shipping;
	private List<ShippingByProductVO> shippingByProducts;
	private Shipping19VO shipping19;

	public ShippingVO getShipping() {
		return shipping;
	}

	public void setShipping(ShippingVO shipping) {
		this.shipping = shipping;
	}

	public List<ShippingByProductVO> getShippingByProducts() {
		return shippingByProducts;
	}

	public void setShippingByProducts(List<ShippingByProductVO> shippingByProducts) {
		this.shippingByProducts = shippingByProducts;
	}

	public Shipping19VO getShipping19() {
		return shipping19;
	}

	public void setShipping19(Shipping19VO shipping19) {
		this.shipping19 = shipping19;
	}


}
