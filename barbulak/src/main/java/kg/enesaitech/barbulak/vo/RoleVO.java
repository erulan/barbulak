package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class RoleVO extends CommonVO{

	
	private String name;
	private Set<UsersRoleVO> usersRoles = new HashSet<UsersRoleVO>(0);

	public RoleVO() {
	}

	public RoleVO(String name) {
		this.name = name;
	}

	public RoleVO(String name, Set<UsersRoleVO> usersRoles) {
		this.name = name;
		this.usersRoles = usersRoles;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<UsersRoleVO> getUsersRoles() {
		return this.usersRoles;
	}

	public void setUsersRoles(Set<UsersRoleVO> usersRoles) {
		this.usersRoles = usersRoles;
	}

}
