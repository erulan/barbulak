package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class ProductTypeVO extends CommonVO{

	private ProductCategoryVO productCategory;
	private String name;
	private Boolean fizzy;
	private Set<ProductVO> products = new HashSet<ProductVO>(0);

	public ProductTypeVO() {
	}

	public ProductTypeVO(ProductCategoryVO productCategory, String name) {
		this.productCategory = productCategory;
		this.name = name;
	}

	public ProductTypeVO(ProductCategoryVO productCategory, String name,
			Boolean fizzy, Set<ProductVO> products) {
		this.productCategory = productCategory;
		this.name = name;
		this.fizzy = fizzy;
		this.products = products;
	}

	public ProductCategoryVO getProductCategory() {
		return this.productCategory;
	}

	public void setProductCategory(ProductCategoryVO productCategory) {
		this.productCategory = productCategory;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getFizzy() {
		return this.fizzy;
	}

	public void setFizzy(Boolean fizzy) {
		this.fizzy = fizzy;
	}

	public Set<ProductVO> getProducts() {
		return this.products;
	}

	public void setProducts(Set<ProductVO> products) {
		this.products = products;
	}

}
