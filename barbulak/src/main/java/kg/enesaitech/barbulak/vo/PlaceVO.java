package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class PlaceVO extends CommonVO{

	private StaffVO staff;
	private PlaceTypeVO placeType;
	private String name;
	private String address;
	private Set<InventoryVO> inventories = new HashSet<InventoryVO>(0);
	private Set<DefectVO> defects = new HashSet<DefectVO>(0);
	private Set<ProductPlaceVO> productPlaces = new HashSet<ProductPlaceVO>(0);
	
//	private Set<> shippingsForFromPlaceId = new HashSet(0);
	
	private Set<RawPlaceVO> rawPlaces = new HashSet<RawPlaceVO>(0);
	
//	private Set shippingsForToPlaceId = new HashSet(0);

	public PlaceVO() {
	}

	public PlaceVO(StaffVO staff, PlaceTypeVO placeType) {
		this.staff = staff;
		this.placeType = placeType;
	}

	public PlaceVO(StaffVO owner, PlaceTypeVO placeType, String name, String address,
			Set<InventoryVO> inventories, Set<DefectVO> defects, Set<ProductPlaceVO> productPlaces,
//			Set shippingsForFromPlaceId, 
			Set<RawPlaceVO> rawPlaces
//			, Set shippingsForToPlaceId
			) {
		this.staff = owner;
		this.placeType = placeType;
		this.name = name;
		this.address = address;
		this.inventories = inventories;
		this.defects = defects;
		this.productPlaces = productPlaces;
//		this.shippingsForFromPlaceId = shippingsForFromPlaceId;
		this.rawPlaces = rawPlaces;
//		this.shippingsForToPlaceId = shippingsForToPlaceId;
	}


	public StaffVO getStaff() {
		return this.staff;
	}

	public void setStaff(StaffVO staff) {
		this.staff = staff;
	}


	public PlaceTypeVO getPlaceType() {
		return this.placeType;
	}

	public void setPlaceType(PlaceTypeVO placeType) {
		this.placeType = placeType;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}


	public Set<InventoryVO> getInventories() {
		return this.inventories;
	}

	public void setInventories(Set<InventoryVO> inventories) {
		this.inventories = inventories;
	}


	public Set<DefectVO> getDefects() {
		return this.defects;
	}

	public void setDefects(Set<DefectVO> defects) {
		this.defects = defects;
	}


	public Set<ProductPlaceVO> getProductPlaces() {
		return this.productPlaces;
	}

	public void setProductPlaces(Set<ProductPlaceVO> productPlaces) {
		this.productPlaces = productPlaces;
	}


//	public Set getShippingsForFromPlaceId() {
//		return this.shippingsForFromPlaceId;
//	}
//
//	public void setShippingsForFromPlaceId(Set shippingsForFromPlaceId) {
//		this.shippingsForFromPlaceId = shippingsForFromPlaceId;
//	}

	
	public Set<RawPlaceVO> getRawPlaces() {
		return this.rawPlaces;
	}

	public void setRawPlaces(Set<RawPlaceVO> rawPlaces) {
		this.rawPlaces = rawPlaces;
	}


//	public Set getShippingsForToPlaceId() {
//		return this.shippingsForToPlaceId;
//	}
//
//	public void setShippingsForToPlaceId(Set shippingsForToPlaceId) {
//		this.shippingsForToPlaceId = shippingsForToPlaceId;
//	}

}
