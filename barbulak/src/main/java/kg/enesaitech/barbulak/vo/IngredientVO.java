package kg.enesaitech.barbulak.vo;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1


public class IngredientVO extends CommonVO{

	private ProductVO product;
	private RawVO raw;
	private Double amount;

	public IngredientVO() {
	}

	public IngredientVO(ProductVO product, RawVO raw, Double amount) {
		this.product = product;
		this.raw = raw;
		this.amount = amount;
	}

	public RawVO getRaw() {
		return this.raw;
	}

	public void setRaw(RawVO raw) {
		this.raw = raw;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public ProductVO getProduct() {
		return product;
	}

	public void setProduct(ProductVO product) {
		this.product = product;
	}

}
