package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class RawVO extends CommonVO{

	private UnitVO unit;
	private String name;
	private Integer minStockAmount;
	private Set<RawPlaceVO> rawPlaces = new HashSet<RawPlaceVO>(0);

	public RawVO() {
	}

	public RawVO(UnitVO unit, String name) {
		this.unit = unit;
		this.name = name;
	}

	public RawVO(UnitVO unit, String name, Integer minStockAmount, Set<RawPlaceVO> rawPlaces) {
		this.unit = unit;
		this.name = name;
		this.rawPlaces = rawPlaces;
		this.minStockAmount = minStockAmount;
	}

	public UnitVO getUnit() {
		return this.unit;
	}

	public void setUnit(UnitVO unit) {
		this.unit = unit;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<RawPlaceVO> getRawPlaces() {
		return this.rawPlaces;
	}

	public void setRawPlaces(Set<RawPlaceVO> rawPlaces) {
		this.rawPlaces = rawPlaces;
	}

	public Integer getMinStockAmount() {
		return minStockAmount;
	}

	public void setMinStockAmount(Integer minStockAmount) {
		this.minStockAmount = minStockAmount;
	}

}
