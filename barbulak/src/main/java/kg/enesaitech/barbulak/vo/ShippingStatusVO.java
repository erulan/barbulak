package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class ShippingStatusVO extends CommonVO{

	private String name;
	private Set<ShippingVO> shippings = new HashSet<ShippingVO>(0);

	public ShippingStatusVO() {
	}

	public ShippingStatusVO(String name) {
		this.name = name;
	}

	public ShippingStatusVO(String name, Set<ShippingVO> shippings) {
		this.name = name;
		this.shippings = shippings;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ShippingVO> getShippings() {
		return this.shippings;
	}

	public void setShippings(Set<ShippingVO> shippings) {
		this.shippings = shippings;
	}

}
