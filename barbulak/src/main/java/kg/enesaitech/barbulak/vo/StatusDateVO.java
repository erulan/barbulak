package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.barbulak.generics.CommonVO;

public class StatusDateVO extends CommonVO {

	/**
	 * author azamatsekin
	 */
	private Date created;
	private Date approvedAdmin;
	private Date rejectedAdmin;
	private Date approvedFromPlace;
	private Date rejectedFromPlace;
	private Date approvedToPlace;
	private Date rejectedToPlace;

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getApprovedAdmin() {
		return approvedAdmin;
	}

	public void setApprovedAdmin(Date approvedAdmin) {
		this.approvedAdmin = approvedAdmin;
	}

	public Date getRejectedAdmin() {
		return rejectedAdmin;
	}

	public void setRejectedAdmin(Date rejectedAdmin) {
		this.rejectedAdmin = rejectedAdmin;
	}

	public Date getApprovedFromPlace() {
		return approvedFromPlace;
	}

	public void setApprovedFromPlace(Date approvedFromPlace) {
		this.approvedFromPlace = approvedFromPlace;
	}

	public Date getRejectedFromPlace() {
		return rejectedFromPlace;
	}

	public void setRejectedFromPlace(Date rejectedFromPlace) {
		this.rejectedFromPlace = rejectedFromPlace;
	}

	public Date getApprovedToPlace() {
		return approvedToPlace;
	}

	public void setApprovedToPlace(Date approvedToPlace) {
		this.approvedToPlace = approvedToPlace;
	}

	public Date getRejectedToPlace() {
		return rejectedToPlace;
	}

	public void setRejectedToPlace(Date rejectedToPlace) {
		this.rejectedToPlace = rejectedToPlace;
	}

}
