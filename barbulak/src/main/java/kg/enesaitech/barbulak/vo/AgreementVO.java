package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.barbulak.generics.CommonVO;

/**
 * author Azamatshekin
 */
public class AgreementVO extends CommonVO {

	
	private String agreementNumber;
	private String description;
	private Date startDate;
	private Date endDate;
	private ClientVO client;
	private Integer bottleAmount;
	private Integer confidenceBottleAmount;
	private Boolean virtualAgreement;	
	private Double pledgeFee;
	private Double unitPriceForBottle;
	private Integer dispanserAmount;
	private String dispanserDescription;
	private Date lastDispanserWashDate;
	private Date lastPurchaseDate;
	private Integer lostBottleNumber;
	private Integer minBottleForMonth;
	private String note;

	
	public String getAgreementNumber() {
		return agreementNumber;
	}

	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public ClientVO getClient() {
		return client;
	}

	public void setClient(ClientVO client) {
		this.client = client;
	}

	public Integer getBottleAmount() {
		return bottleAmount;
	}

	public void setBottleAmount(Integer bottleAmount) {
		this.bottleAmount = bottleAmount;
	}

	public Integer getConfidenceBottleAmount() {
		return confidenceBottleAmount;
	}

	public void setConfidenceBottleAmount(Integer confidenceBottleAmount) {
		this.confidenceBottleAmount = confidenceBottleAmount;
	}

	public Boolean getVirtualAgreement() {
		return virtualAgreement;
	}

	public void setVirtualAgreement(Boolean virtualAgreement) {
		this.virtualAgreement = virtualAgreement;
	}

	public Double getPledgeFee() {
		return pledgeFee;
	}

	public void setPledgeFee(Double pledgeFee) {
		this.pledgeFee = pledgeFee;
	}

	public Double getUnitPriceForBottle() {
		return unitPriceForBottle;
	}

	public void setUnitPriceForBottle(Double unitPriceForBottle) {
		this.unitPriceForBottle = unitPriceForBottle;
	}

	public Integer getDispanserAmount() {
		return dispanserAmount;
	}

	public void setDispanserAmount(Integer dispanserAmount) {
		this.dispanserAmount = dispanserAmount;
	}

	public String getDispanserDescription() {
		return dispanserDescription;
	}

	public void setDispanserDescription(String dispanserDescription) {
		this.dispanserDescription = dispanserDescription;
	}

	public Date getLastDispanserWashDate() {
		return lastDispanserWashDate;
	}

	public void setLastDispanserWashDate(Date lastDispanserWashDate) {
		this.lastDispanserWashDate = lastDispanserWashDate;
	}

	public Date getLastPurchaseDate() {
		return lastPurchaseDate;
	}

	public void setLastPurchaseDate(Date lastPurchaseDate) {
		this.lastPurchaseDate = lastPurchaseDate;
	}

	public Integer getLostBottleNumber() {
		return lostBottleNumber;
	}

	public void setLostBottleNumber(Integer lostBottleNumber) {
		this.lostBottleNumber = lostBottleNumber;
	}

	public Integer getMinBottleForMonth() {
		return minBottleForMonth;
	}

	public void setMinBottleForMonth(Integer minBottleForMonth) {
		this.minBottleForMonth = minBottleForMonth;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
