package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.barbulak.generics.CommonVO;

public class RawPurchaseVO extends CommonVO{

	private PurchaseVO purchase;
	private RawVO raw;
	private Double amount;
	private Double unitPrice;

	public RawPurchaseVO() {
	}



	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public PurchaseVO getPurchase() {
		return purchase;
	}

	public void setPurchase(PurchaseVO purchase) {
		this.purchase = purchase;
	}



	public RawVO getRaw() {
		return raw;
	}



	public void setRaw(RawVO raw) {
		this.raw = raw;
	}




	public Double getUnitPrice() {
		return unitPrice;
	}



	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

}
