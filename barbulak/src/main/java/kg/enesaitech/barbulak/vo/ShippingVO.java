package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;

/**
 * Shipping generated by hbm2java
 */
public class ShippingVO extends CommonVO{

	/**
	 * 
	 */
	
	private PlaceVO placeByFromPlaceId;
	private ShippingStatusVO shippingStatus;
	private PlaceVO placeByToPlaceId;
	private String description;
	private StatusDateVO statusDate;
	private Boolean sent;
	private Set<ShippingByProductVO> shippingByProducts = new HashSet<ShippingByProductVO>(0);

	public ShippingVO() {
	}

	public ShippingVO(PlaceVO placeByFromPlaceId, ShippingStatusVO shippingStatus,
			PlaceVO placeByToPlaceId) {
		this.placeByFromPlaceId = placeByFromPlaceId;
		this.shippingStatus = shippingStatus;
		this.placeByToPlaceId = placeByToPlaceId;
	}

	public ShippingVO(PlaceVO placeByFromPlaceId, ShippingStatusVO shippingStatus,
			PlaceVO placeByToPlaceId, String description, Date date,
			Set<ShippingByProductVO> shippingByProducts, Set<Shipping19VO> shippingByRaws) {
		this.placeByFromPlaceId = placeByFromPlaceId;
		this.shippingStatus = shippingStatus;
		this.placeByToPlaceId = placeByToPlaceId;
		this.description = description;
		this.shippingByProducts = shippingByProducts;
	}


	public PlaceVO getPlaceByFromPlaceId() {
		return this.placeByFromPlaceId;
	}

	public void setPlaceByFromPlaceId(PlaceVO placeByFromPlaceId) {
		this.placeByFromPlaceId = placeByFromPlaceId;
	}

	public ShippingStatusVO getShippingStatus() {
		return this.shippingStatus;
	}

	public void setShippingStatus(ShippingStatusVO shippingStatus) {
		this.shippingStatus = shippingStatus;
	}

	public PlaceVO getPlaceByToPlaceId() {
		return this.placeByToPlaceId;
	}

	public void setPlaceByToPlaceId(PlaceVO placeByToPlaceId) {
		this.placeByToPlaceId = placeByToPlaceId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Set<ShippingByProductVO> getShippingByProducts() {
		return this.shippingByProducts;
	}

	public void setShippingByProducts(Set<ShippingByProductVO>shippingByProducts) {
		this.shippingByProducts = shippingByProducts;
	}

	public StatusDateVO getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(StatusDateVO statusDate) {
		this.statusDate = statusDate;
	}

	public Boolean getSent() {
		return sent;
	}

	public void setSent(Boolean sent) {
		this.sent = sent;
	}
	

}
