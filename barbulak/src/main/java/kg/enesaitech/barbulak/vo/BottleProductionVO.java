package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.barbulak.generics.CommonVO;

public class BottleProductionVO extends CommonVO {

	/**
	 * Azamatshekin
	 */

	private ShippingStatusVO status;
	private StatusDateVO statusDate;
	private Integer amount;
	
	public ShippingStatusVO getStatus() {
		return status;
	}
	public void setStatus(ShippingStatusVO status) {
		this.status = status;
	}
	public StatusDateVO getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(StatusDateVO statusDate) {
		this.statusDate = statusDate;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	


}
