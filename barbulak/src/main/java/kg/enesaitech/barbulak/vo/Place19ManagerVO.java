package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.barbulak.generics.CommonVO;

public class Place19ManagerVO extends CommonVO {

	private PlaceVO place;

	private StaffVO manager;

	public PlaceVO getPlace() {
		return place;
	}

	public void setPlace(PlaceVO place) {
		this.place = place;
	}

	public StaffVO getManager() {
		return manager;
	}

	public void setManager(StaffVO manager) {
		this.manager = manager;
	}


}
