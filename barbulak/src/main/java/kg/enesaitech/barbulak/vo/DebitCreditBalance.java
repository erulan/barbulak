package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.barbulak.generics.CommonVO;

public class DebitCreditBalance extends CommonVO{
	
	private String cashboxName;
	private Double debit;
	private Double credit;
	private Double balance;
	
	
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = debit;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getCashboxName() {
		return cashboxName;
	}
	public void setCashboxName(String cashboxName) {
		this.cashboxName = cashboxName;
	}
	
}
