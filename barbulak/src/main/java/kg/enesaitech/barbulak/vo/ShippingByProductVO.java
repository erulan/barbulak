package kg.enesaitech.barbulak.vo;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1



public class ShippingByProductVO extends CommonVO{

	private ProductVO product;
	private ShippingVO shipping;
	private Double amount;

	public ShippingByProductVO() {
	}

	public ShippingByProductVO(ProductVO product, ShippingVO shipping) {
		this.product = product;
		this.shipping = shipping;
	}

	public ShippingByProductVO(ProductVO product, ShippingVO shipping, Double amount) {
		this.product = product;
		this.shipping = shipping;
		this.amount = amount;
	}

	public ProductVO getProduct() {
		return this.product;
	}

	public void setProduct(ProductVO product) {
		this.product = product;
	}
	
	public ShippingVO getShipping() {
		return this.shipping;
	}

	public void setShipping(ShippingVO shipping) {
		this.shipping = shipping;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}
