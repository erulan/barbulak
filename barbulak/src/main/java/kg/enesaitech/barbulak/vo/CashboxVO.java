package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;

public class CashboxVO extends CommonVO{
	

	private String name;
	private double total;
	private String description;
	private Set<TransactionVO> transactions = new HashSet<TransactionVO>(0);
	public CashboxVO() {
	}

	public CashboxVO(String name, double total) {
		this.name = name;
		this.total = total;
	}

	public CashboxVO(String name, double total, String description, Set<TransactionVO> transactions) {
		this.name = name;
		this.total = total;
		this.description = description;
		this.transactions = transactions;
	}


	public double getTotal() {
		return this.total;
	}

	public void setTotal(double total) {
		this.total = total;
	}


	public Set<TransactionVO> getTransactions() {
		return this.transactions;
	}

	public void setTransactions(Set<TransactionVO> transactions) {
		this.transactions = transactions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
