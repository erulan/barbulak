package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class ObjectTypeVO extends CommonVO{

	
	private String name;
	private Set<DefectVO> defects = new HashSet<DefectVO>(0);

	public ObjectTypeVO() {
	}

	public ObjectTypeVO(String name) {
		this.name = name;
	}

	public ObjectTypeVO(String name, Set<DefectVO> defects) {
		this.name = name;
		this.defects = defects;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<DefectVO> getDefects() {
		return this.defects;
	}

	public void setDefects(Set<DefectVO> defects) {
		this.defects = defects;
	}

}
