package kg.enesaitech.barbulak.vo;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1


public class StaffPositionVO extends CommonVO{

	private StaffVO staff;
	private PositionVO position;

	public StaffPositionVO() {
	}

	public StaffVO getStaff() {
		return staff;
	}

	public void setStaff(StaffVO staff) {
		this.staff = staff;
	}

	public PositionVO getPosition() {
		return position;
	}

	public void setPosition(PositionVO position) {
		this.position = position;
	}
	
}
