package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class PlaceTypeVO extends CommonVO{

	
	private String name;
	private Set<PlaceVO> places = new HashSet<PlaceVO>(0);

	public PlaceTypeVO() {
	}

	public PlaceTypeVO(String name) {
		this.name = name;
	}

	public PlaceTypeVO(String name, Set<PlaceVO> places) {
		this.name = name;
		this.places = places;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<PlaceVO> getPlaces() {
		return this.places;
	}

	public void setPlaces(Set<PlaceVO> places) {
		this.places = places;
	}

}
