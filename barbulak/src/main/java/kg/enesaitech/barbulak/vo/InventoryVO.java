package kg.enesaitech.barbulak.vo;

import java.util.Date;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1


public class InventoryVO extends CommonVO{

	private PersonVO person;
	private PlaceVO place;
	private InventoryTypeVO inventoryType;
	private String inventoryNo;
	private InventoryStatusVO inventoryStatus;
	private String description;
	private Date startDate;
	private Date endDate;
	private Double price;

	public InventoryVO() {
	}

	public InventoryVO(PersonVO person, PlaceVO place, InventoryTypeVO inventoryType, InventoryStatusVO inventoryStatus) {
		this.person = person;
		this.place = place;
		this.inventoryType = inventoryType;
		this.inventoryStatus = inventoryStatus;
	}

	public InventoryVO(PersonVO person, PlaceVO place, InventoryTypeVO inventoryType,
			String inventoryNo, InventoryStatusVO inventoryStatus, String description,
			Date startDate, Date endDate) {
		this.person = person;
		this.place = place;
		this.inventoryType = inventoryType;
		this.inventoryNo = inventoryNo;
		this.inventoryStatus = inventoryStatus;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
	}


	public PlaceVO getPlace() {
		return this.place;
	}

	public void setPlace(PlaceVO place) {
		this.place = place;
	}


	public InventoryTypeVO getInventoryType() {
		return this.inventoryType;
	}

	public void setInventoryType(InventoryTypeVO inventoryType) {
		this.inventoryType = inventoryType;
	}


	public String getInventoryNo() {
		return this.inventoryNo;
	}

	public void setInventoryNo(String inventoryNo) {
		this.inventoryNo = inventoryNo;
	}

	public PersonVO getPerson() {
		return person;
	}

	public void setPerson(PersonVO person) {
		this.person = person;
	}

	public InventoryStatusVO getInventoryStatus() {
		return inventoryStatus;
	}

	public void setInventoryStatus(InventoryStatusVO inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
