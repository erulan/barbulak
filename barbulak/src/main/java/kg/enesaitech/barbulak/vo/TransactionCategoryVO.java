package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class TransactionCategoryVO extends CommonVO{

	private String name;
	private Boolean income;
	private Set<TransactionVO> transactions = new HashSet<TransactionVO>(0);

	public TransactionCategoryVO() {
	}

	public TransactionCategoryVO(String name, Boolean income) {
		this.income = income;
		this.name = name;
	}

	public TransactionCategoryVO(String name, Boolean income,
			Boolean fizzy, Set<TransactionVO> transactions) {
		this.income = income;
		this.name = name;
		this.transactions = transactions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIncome() {
		return income;
	}

	public void setIncome(Boolean income) {
		this.income = income;
	}

	public Set<TransactionVO> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<TransactionVO> transactions) {
		this.transactions = transactions;
	}
}
