package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.barbulak.generics.CommonVO;

/**
 * Azamatshekin
 */
public class Reminder19VO extends CommonVO {
	
	private AgreementVO agreement;
	private String description;
	private Date reminderDate;

	public AgreementVO getAgreement() {
		return agreement;
	}

	public void setAgreement(AgreementVO agreement) {
		this.agreement = agreement;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Date getReminderDate() {
		return reminderDate;
	}

	public void setReminderDate(Date reminderDate) {
		this.reminderDate = reminderDate;
	}
	

}
