package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class PersonVO extends CommonVO{

	private String name;
	private String surname;
	private String personNumber;
	private CashboxVO cashbox;

	private Set<PersonalBalanceVO> personalBalances = new HashSet<PersonalBalanceVO>(0);
	private Set<InventoryVO> inventories = new HashSet<InventoryVO>(0);
	private Set<TransactionVO> transactions = new HashSet<TransactionVO>(0);

	public PersonVO() {
	}

	public PersonVO( String name, String personNumber) {
		this.name = name;
		this.personNumber = personNumber;
	}

	public PersonVO( String name, String surname, String personNumber,
			Set<PersonalBalanceVO> personalBalances, Set<InventoryVO> inventories, 
			Set<TransactionVO> transactions, CashboxVO cashbox) {
		this.name = name;
		this.surname = surname;
		this.cashbox = cashbox;
		this.personalBalances = personalBalances;
		this.inventories = inventories;
		this.transactions = transactions;
		this.personNumber = personNumber;
	}

	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}


	public Set<InventoryVO> getInventories() {
		return this.inventories;
	}

	public void setInventories(Set<InventoryVO> inventories) {
		this.inventories = inventories;
	}
	
	public Set<TransactionVO> getTransactions() {
		return this.transactions;
	}

	public void setTransactions(Set<TransactionVO> transactions) {
		this.transactions = transactions;
	}

	public Set<PersonalBalanceVO> getPersonalBalances() {
		return personalBalances;
	}

	public void setPersonalBalances(Set<PersonalBalanceVO> personalBalances) {
		this.personalBalances = personalBalances;
	}

	public String getPersonNumber() {
		return personNumber;
	}

	public void setPersonNumber(String personNumber) {
		this.personNumber = personNumber;
	}

	public CashboxVO getCashbox() {
		return cashbox;
	}

	public void setCashbox(CashboxVO cashbox) {
		this.cashbox = cashbox;
	}

}
