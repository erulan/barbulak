package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

public class ClientVO extends PersonVO{

	private String passport;
	private Date birthDate;
	private String address;
	private String deliveryAddress;
	private String phone;
	private String email;
	private Boolean clientFor19Bottle;

	public ClientVO() {
	}

	public ClientVO(String phone, String deliveryAddress) {
		this.phone = phone;
		this.deliveryAddress = deliveryAddress;
	}

	public ClientVO(String passport, String deliveryAddress, Date birthDate,
			String address, String phone, String email) {
		this.passport = passport;
		this.birthDate = birthDate;
		this.address = address;
		this.phone = phone;
		this.email = email;
		this.deliveryAddress = deliveryAddress;
	}

	public String getPassport() {
		return this.passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}


	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}


	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}



	public Boolean getClientFor19Bottle() {
		return clientFor19Bottle;
	}

	public void setClientFor19Bottle(Boolean clientFor19Bottle) {
		this.clientFor19Bottle = clientFor19Bottle;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

}
