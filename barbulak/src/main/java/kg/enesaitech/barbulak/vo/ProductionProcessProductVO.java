package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.barbulak.generics.CommonVO;

public class ProductionProcessProductVO extends CommonVO{

	private ProductionProcessVO productionProcess;
	private ProductVO product;
	private Double plannedAmount;
	private Double productionAmount;
	private String description;

	public ProductionProcessProductVO() {
	}



	public ProductionProcessVO getProductionProcess() {
		return productionProcess;
	}



	public void setProductionProcess(ProductionProcessVO productionProcess) {
		this.productionProcess = productionProcess;
	}



	public ProductVO getProduct() {
		return product;
	}



	public void setProduct(ProductVO product) {
		this.product = product;
	}



	public Double getPlannedAmount() {
		return plannedAmount;
	}



	public void setPlannedAmount(Double plannedAmount) {
		this.plannedAmount = plannedAmount;
	}



	public Double getProductionAmount() {
		return productionAmount;
	}



	public void setProductionAmount(Double productionAmount) {
		this.productionAmount = productionAmount;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}


}
