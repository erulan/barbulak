package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;

/**
 * author Azamatshekin
 */
public class DeliveryVO extends CommonVO {

	private DeliveryStatusVO deliveryStatus;
	private StaffVO agent;
	private Date plannedDate;
	private Date sentDate;
	private Date completedDate;
	private Set<DeliveryClientVO> deliveryClients = new HashSet<DeliveryClientVO>(0);

	public DeliveryStatusVO getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(DeliveryStatusVO deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public StaffVO getAgent() {
		return agent;
	}

	public void setAgent(StaffVO agent) {
		this.agent = agent;
	}

	public Date getPlannedDate() {
		return plannedDate;
	}

	public void setPlannedDate(Date plannedDate) {
		this.plannedDate = plannedDate;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public Date getCompletedDate() {
		return completedDate;
	}

	public void setCompletedDate(Date completedDate) {
		this.completedDate = completedDate;
	}

	public Set<DeliveryClientVO> getDeliveryClients() {
		return deliveryClients;
	}

	public void setDeliveryClients(Set<DeliveryClientVO> deliveryClients) {
		this.deliveryClients = deliveryClients;
	}

}
