package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class CapacityVO extends CommonVO{

	/**
	 * 
	 */
	
	private UnitVO unit;
	private String name;
	private Double capacity;
	
	private Set<ProductVO> products = new HashSet<ProductVO>(0);

	public CapacityVO() {
	}

	public CapacityVO(UnitVO unit) {
		this.unit = unit;
	}

	public CapacityVO(UnitVO unit, String name, Double capacity, Set<ProductVO> products) {
		this.unit = unit;
		this.name = name;
		this.capacity = capacity;
		this.products = products;
	}
	
	public UnitVO getUnit() {
		return this.unit;
	}

	public void setUnit(UnitVO unit) {
		this.unit = unit;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Double getCapacity() {
		return this.capacity;
	}

	public void setCapacity(Double capacity) {
		this.capacity = capacity;
	}


	public Set<ProductVO> getProducts() {
		return this.products;
	}

	public void setProducts(Set<ProductVO> products) {
		this.products = products;
	}

}
