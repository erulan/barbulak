package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;

public class InventoryCategoryVO extends CommonVO{

	private String name;
	private Set<InventoryTypeVO> inventoryTypes = new HashSet<InventoryTypeVO>(0);

	public InventoryCategoryVO() {
	}

	public InventoryCategoryVO(String name) {
		this.name = name;
	}

	public InventoryCategoryVO(String name, Set<InventoryTypeVO> inventoryTypes) {
		this.name = name;
		this.inventoryTypes = inventoryTypes;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<InventoryTypeVO> getInventoryTypes() {
		return this.inventoryTypes;
	}

	public void setInventoryTypes(Set<InventoryTypeVO> inventoryTypes) {
		this.inventoryTypes = inventoryTypes;
	}

}
