package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.barbulak.generics.CommonVO;


public class PersonalTransactionCategoryVO extends CommonVO{

	private String name;

	public PersonalTransactionCategoryVO() {
	}

	public PersonalTransactionCategoryVO(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
