package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class ProductCategoryVO extends CommonVO{

	
	private String name;
	private Set<ProductTypeVO> productTypes = new HashSet<ProductTypeVO>(0);

	public ProductCategoryVO() {
	}

	public ProductCategoryVO(String name) {
		this.name = name;
	}

	public ProductCategoryVO(String name, Set<ProductTypeVO> productTypes) {
		this.name = name;
		this.productTypes = productTypes;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ProductTypeVO> getProductTypes() {
		return this.productTypes;
	}

	public void setProductTypes(Set<ProductTypeVO> productTypes) {
		this.productTypes = productTypes;
	}

}
