package kg.enesaitech.barbulak.vo;

import java.util.Map;

public class IngredientRaw {
	private ProductVO product;
	private Map<Integer, Double> rawIdAmount;
	public ProductVO getProduct() {
		return product;
	}
	public void setProduct(ProductVO product) {
		this.product = product;
	}
	public Map<Integer, Double> getRawIdAmount() {
		return rawIdAmount;
	}
	public void setRawIdAmount(Map<Integer, Double> rawIdAmount) {
		this.rawIdAmount = rawIdAmount;
	}
	
}
