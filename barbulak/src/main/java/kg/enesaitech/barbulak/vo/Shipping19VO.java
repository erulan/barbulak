package kg.enesaitech.barbulak.vo;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1



public class Shipping19VO extends CommonVO{

	
	private ShippingVO shipping;
	private Integer bottleAmount;
	private Integer emptyBottleAmount;

	public Shipping19VO() {
	}

	public Shipping19VO(ShippingVO shipping) {
		this.shipping = shipping;
	}

	public ShippingVO getShipping() {
		return this.shipping;
	}

	public void setShipping(ShippingVO shipping) {
		this.shipping = shipping;
	}

	public Integer getBottleAmount() {
		return bottleAmount;
	}

	public void setBottleAmount(Integer bottleAmount) {
		this.bottleAmount = bottleAmount;
	}

	public Integer getEmptyBottleAmount() {
		return emptyBottleAmount;
	}

	public void setEmptyBottleAmount(Integer emptyBottleAmount) {
		this.emptyBottleAmount = emptyBottleAmount;
	}


}
