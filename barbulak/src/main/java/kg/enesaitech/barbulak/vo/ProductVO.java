package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class ProductVO extends CommonVO{


	private CapacityVO capacity;
	private ProductTypeVO productType;
	private String name;
	private Double price;
	private Double cost;
	
	private Set<ProductPlaceVO> productPlaces = new HashSet<ProductPlaceVO>(0);
	private Set<ShippingByProductVO> shippingByProducts = new HashSet<ShippingByProductVO>(0);
	private Set<WareDistProductVO> wareDistProducts = new HashSet<WareDistProductVO>(0);

	public ProductVO() {
	}

	public ProductVO(CapacityVO capacity, ProductTypeVO productType, String name) {
		this.capacity = capacity;
		this.productType = productType;
		this.name = name;
	}

	public ProductVO(CapacityVO capacity, ProductTypeVO productType, String name,
			Double price, Double cost, Set<ProductPlaceVO> productPlaces, Set<ShippingByProductVO> shippingByProducts,
			Set<WareDistProductVO> wareDistProducts) {
		this.capacity = capacity;
		this.productType = productType;
		this.name = name;
		this.price = price;
		this.cost = cost;
		this.productPlaces = productPlaces;
		this.shippingByProducts = shippingByProducts;
		this.wareDistProducts = wareDistProducts;
	}

	public CapacityVO getCapacity() {
		return this.capacity;
	}

	public void setCapacity(CapacityVO capacity) {
		this.capacity = capacity;
	}

	public ProductTypeVO getProductType() {
		return this.productType;
	}

	public void setProductType(ProductTypeVO productType) {
		this.productType = productType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ProductPlaceVO> getProductPlaces() {
		return this.productPlaces;
	}

	public void setProductPlaces(Set<ProductPlaceVO> productPlaces) {
		this.productPlaces = productPlaces;
	}

	public Set<ShippingByProductVO> getShippingByProducts() {
		return this.shippingByProducts;
	}

	public void setShippingByProducts(Set<ShippingByProductVO> shippingByProducts) {
		this.shippingByProducts = shippingByProducts;
	}

	public Set<WareDistProductVO> getWareDistProducts() {
		return this.wareDistProducts;
	}

	public void setWareDistProducts(Set<WareDistProductVO> wareDistProducts) {
		this.wareDistProducts = wareDistProducts;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

}
