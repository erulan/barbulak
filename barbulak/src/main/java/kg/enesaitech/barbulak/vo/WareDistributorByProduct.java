package kg.enesaitech.barbulak.vo;

import java.util.List;

public class WareDistributorByProduct {
	private WareDistributorVO wareDistributor;
	private List<WareDistProductVO> wareDistProducts;


	public WareDistributorVO getWareDistributor() {
		return wareDistributor;
	}

	public void setWareDistributor(WareDistributorVO wareDistributor) {
		this.wareDistributor = wareDistributor;
	}

	public List<WareDistProductVO> getWareDistProducts() {
		return wareDistProducts;
	}

	public void setWareDistProducts(List<WareDistProductVO> wareDistProducts) {
		this.wareDistProducts = wareDistProducts;
	}

}
