package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;

public class WareDistributorVO extends CommonVO {

	/**
	 * 
	 */
	private StaffVO staff;
	private String description;
	private Date date;
	private PlaceVO placeByFromPlaceId;
	private WareDistStatusVO wareDistStatus;
	private Date created;
	private Date approvedAdmin;
	private Date sentDate;
	private Set<WareDistProductVO> wareDistProducts = new HashSet<WareDistProductVO>(0);

	public WareDistributorVO() {
	}

	public WareDistributorVO(StaffVO staff, PlaceVO placeByFromPlaceId) {
		this.staff = staff;
		this.placeByFromPlaceId = placeByFromPlaceId;
	}

	public WareDistributorVO(StaffVO staff, String description, Date date, PlaceVO placeByFromPlaceId,
			Set<WareDistProductVO> wareDistProducts) {
		this.staff = staff;
		this.description = description;
		this.date = date;
		this.wareDistProducts = wareDistProducts;
		this.placeByFromPlaceId = placeByFromPlaceId;
	}

	public StaffVO getStaff() {
		return this.staff;
	}

	public void setStaff(StaffVO staff) {
		this.staff = staff;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Set<WareDistProductVO> getWareDistProducts() {
		return this.wareDistProducts;
	}

	public void setWareDistProducts(Set<WareDistProductVO> wareDistProducts) {
		this.wareDistProducts = wareDistProducts;
	}

	public PlaceVO getPlaceByFromPlaceId() {
		return placeByFromPlaceId;
	}

	public void setPlaceByFromPlaceId(PlaceVO placeByFromPlaceId) {
		this.placeByFromPlaceId = placeByFromPlaceId;
	}

	public WareDistStatusVO getWareDistStatus() {
		return wareDistStatus;
	}

	public void setWareDistStatus(WareDistStatusVO wareDistStatus) {
		this.wareDistStatus = wareDistStatus;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getApprovedAdmin() {
		return approvedAdmin;
	}

	public void setApprovedAdmin(Date approvedAdmin) {
		this.approvedAdmin = approvedAdmin;
	}

}
