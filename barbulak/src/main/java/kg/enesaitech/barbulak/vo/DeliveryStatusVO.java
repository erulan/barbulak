package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import kg.enesaitech.barbulak.generics.CommonVO;


public class DeliveryStatusVO extends CommonVO{

	private String name;
	private Set<DeliveryVO> deliveries = new HashSet<DeliveryVO>(0);

	public DeliveryStatusVO() {
	}

	public DeliveryStatusVO(String name) {
		this.name = name;
	}

	public DeliveryStatusVO(String name, Set<DeliveryVO> deliveries) {
		this.name = name;
		this.deliveries = deliveries;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<DeliveryVO> getDeliveries() {
		return this.deliveries;
	}

	public void setDeliveries(Set<DeliveryVO> deliveries) {
		this.deliveries = deliveries;
	}

}
