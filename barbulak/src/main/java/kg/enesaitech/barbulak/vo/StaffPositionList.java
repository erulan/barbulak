package kg.enesaitech.barbulak.vo;

import java.util.List;

public class StaffPositionList {
	private StaffVO staff;
	private List<Integer> positionIdList;
	
	
	public StaffVO getStaff() {
		return staff;
	}
	public void setStaff(StaffVO staff) {
		this.staff = staff;
	}
	public List<Integer> getPositionIdList() {
		return positionIdList;
	}
	public void setPositionIdList(List<Integer> positionIdList) {
		this.positionIdList = positionIdList;
	}
	
	
}
