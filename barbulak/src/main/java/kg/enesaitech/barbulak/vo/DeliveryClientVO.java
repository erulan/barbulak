package kg.enesaitech.barbulak.vo;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1


public class DeliveryClientVO extends CommonVO{

	private AgreementVO agreement;
	private DeliveryVO delivery;
	private Integer amount;
	private Integer returned;
	private Integer returnedEmpty;
	private Double pledgeNumberForBottle;
	private Double paid;
	
	
	


	public DeliveryVO getDelivery() {
		return this.delivery;
	}

	public void setDelivery(DeliveryVO delivery) {
		this.delivery = delivery;
	}

	

	public Integer getReturned() {
		return this.returned;
	}

	public void setReturned(Integer returned) {
		this.returned = returned;
	}


	public Integer getReturnedEmpty() {
		return this.returnedEmpty;
	}

	public void setReturnedEmpty(Integer returnedEmpty) {
		this.returnedEmpty = returnedEmpty;
	}

	public AgreementVO getAgreement() {
		return agreement;
	}

	public void setAgreement(AgreementVO agreement) {
		this.agreement = agreement;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Double getPledgeNumberForBottle() {
		return pledgeNumberForBottle;
	}

	public void setPledgeNumberForBottle(Double pledgeNumberForBottle) {
		this.pledgeNumberForBottle = pledgeNumberForBottle;
	}

	public Double getPaid() {
		return paid;
	}

	public void setPaid(Double paid) {
		this.paid = paid;
	}

}
