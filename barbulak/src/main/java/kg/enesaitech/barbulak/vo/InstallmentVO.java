package kg.enesaitech.barbulak.vo;

import java.util.Date;

import kg.enesaitech.barbulak.generics.CommonVO;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1



public class InstallmentVO extends CommonVO{
	
	private StaffVO staff;
	private Double leftAmount;
	private Double total;
	private Integer installmentPeriod;
	private Date startDate;

	public InstallmentVO() {
	}

	public InstallmentVO(StaffVO staff, Date startDate) {
		this.staff = staff;
		this.startDate = startDate;
	}

	public InstallmentVO(StaffVO staff, Double leftAmount, Double total,
			Integer installmentPeriod, Date startDate) {
		this.staff = staff;
		this.leftAmount = leftAmount;
		this.total = total;
		this.installmentPeriod = installmentPeriod;
		this.startDate = startDate;
	}


	public StaffVO getStaff() {
		return this.staff;
	}

	public void setStaff(StaffVO staff) {
		this.staff = staff;
	}


	public Double getLeftAmount() {
		return this.leftAmount;
	}

	public void setLeftAmount(Double leftAmount) {
		this.leftAmount = leftAmount;
	}


	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}


	public Integer getInstallmentPeriod() {
		return this.installmentPeriod;
	}

	public void setInstallmentPeriod(Integer installmentPeriod) {
		this.installmentPeriod = installmentPeriod;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}
