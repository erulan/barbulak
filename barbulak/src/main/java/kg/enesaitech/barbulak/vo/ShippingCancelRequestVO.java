package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import kg.enesaitech.barbulak.generics.CommonVO;

public class ShippingCancelRequestVO extends CommonVO {
	
	/**
	 * 
	 */
	private StaffVO staff;
	private Date date;
	private ShippingVO shipping;

	public ShippingCancelRequestVO() {
	}

	public ShippingCancelRequestVO(StaffVO staff, Date date, ShippingVO shipping) {
		this.staff = staff;
		this.date = date;
		this.shipping = shipping;
	}

	public StaffVO getStaff() {
		return staff;
	}

	public void setStaff(StaffVO staff) {
		this.staff = staff;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ShippingVO getShipping() {
		return shipping;
	}

	public void setShipping(ShippingVO shipping) {
		this.shipping = shipping;
	}

}
