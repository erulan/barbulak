package kg.enesaitech.barbulak.vo;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import kg.enesaitech.barbulak.generics.CommonVO;


public class PersonalBalanceVO extends CommonVO{

	
	private PersonVO person;
	private Double balance;

	public PersonalBalanceVO() {
	}

	public PersonalBalanceVO(PersonVO person, Double balance) {
		this.person = person;
		this.balance = balance;
	}


	public Double getBalance() {
		return this.balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public PersonVO getPerson() {
		return person;
	}

	public void setPerson(PersonVO person) {
		this.person = person;
	}

}
