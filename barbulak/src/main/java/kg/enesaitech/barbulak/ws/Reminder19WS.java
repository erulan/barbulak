
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Reminder19;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.Reminder19VO;

@Path("/reminder_19")
public class Reminder19WS extends GenericWS<Reminder19, Reminder19VO> {

	public Reminder19WS() {
		super(Reminder19.class, Reminder19VO.class);
	}
}