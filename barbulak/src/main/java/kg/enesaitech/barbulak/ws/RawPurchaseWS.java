package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.RawPurchase;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.RawPurchaseVO;

@Path("/raw_purchase")
public class RawPurchaseWS extends GenericWS<RawPurchase, RawPurchaseVO> {

	public RawPurchaseWS() {
		super(RawPurchase.class, RawPurchaseVO.class);
	}

}
