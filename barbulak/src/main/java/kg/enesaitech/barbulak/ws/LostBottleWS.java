
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.LostBottle;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.LostBottleVO;
;

@Path("/lost_bottle")
public class LostBottleWS extends GenericWS<LostBottle, LostBottleVO> {

	public LostBottleWS() {
		super(LostBottle.class, LostBottleVO.class);
	}
}