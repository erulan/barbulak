package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.ProductCategory;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.ProductCategoryVO;

import org.springframework.stereotype.Component;

@Path("/product_category")
public class ProductCategoryWS extends GenericWS<ProductCategory, ProductCategoryVO>{

	public ProductCategoryWS() {
		super(ProductCategory.class, ProductCategoryVO.class);
		// TODO Auto-generated constructor stub
	}

}
