package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.InventoryStatus;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.InventoryStatusVO;

@Path("/inventory_status")
public class InventoryStatusWS extends GenericWS<InventoryStatus, InventoryStatusVO> {

	public InventoryStatusWS() {
		super(InventoryStatus.class, InventoryStatusVO.class);
	}
}