package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.UsersRole;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.UsersRoleService;
import kg.enesaitech.barbulak.vo.UsersRoleVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/users_role")
public class UsersRoleWS extends GenericWS<UsersRole, UsersRoleVO>{
	
	public UsersRoleWS(){
		super(UsersRole.class, UsersRoleVO.class);
	}
	
	@Autowired
	UsersRoleService usersRoleService;
	
	@GET
	@Path("/delete_with_role_user_id/{role_id}/{user_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteByRoleId(@PathParam("role_id") int roleId, @PathParam("user_id") int userId) {
		return usersRoleService.deleteByRoleId(roleId, userId) ? Response.ok().build() : Response.serverError().build();
	}
}
