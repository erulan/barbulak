
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Shipping19;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.Shipping19VO;

@Path("/shipping_19")
public class Shipping19WS extends GenericWS<Shipping19, Shipping19VO> {

	public Shipping19WS() {
		super(Shipping19.class, Shipping19VO.class);
	}
}