package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.PersonalTransactionCategory;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.PersonalTransactionCategoryVO;

@Path("/personal_transaction_category")
public class PersonalTransactionCategoryWS extends GenericWS<PersonalTransactionCategory, PersonalTransactionCategoryVO> {

	public PersonalTransactionCategoryWS() {
		super(PersonalTransactionCategory.class, PersonalTransactionCategoryVO.class);
	}

}
