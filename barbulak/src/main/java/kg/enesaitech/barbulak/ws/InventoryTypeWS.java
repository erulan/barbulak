package kg.enesaitech.barbulak.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import kg.enesaitech.barbulak.entity.InventoryType;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.InventoryTypeService;
import kg.enesaitech.barbulak.vo.InventoryTypeVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/inventory_type")
public class InventoryTypeWS extends GenericWS<InventoryType, InventoryTypeVO> {

	public InventoryTypeWS() {
		super(InventoryType.class, InventoryTypeVO.class);
	}

	@Autowired
	InventoryTypeService inventoryTypeService;
	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<InventoryTypeVO> search(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<InventoryTypeVO> searchResultVO = inventoryTypeService.getList(searchParameters);
				

		return searchResultVO;

	}
}