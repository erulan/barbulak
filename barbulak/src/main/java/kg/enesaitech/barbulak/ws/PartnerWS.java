package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Partner;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.PartnerVO;

@Path("/partner")
public class PartnerWS extends GenericWS<Partner, PartnerVO> {

	public PartnerWS() {
		super(Partner.class, PartnerVO.class);
	}

}
