package kg.enesaitech.barbulak.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import kg.enesaitech.barbulak.entity.RawPlace;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.RawService;
import kg.enesaitech.barbulak.vo.RawPlaceVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/raw_place")
public class RawPlaceWS extends GenericWS<RawPlace, RawPlaceVO> {

	public RawPlaceWS() {
		super(RawPlace.class, RawPlaceVO.class);
	}
	@Autowired
	RawService rawService;
	
	@POST
	@Path("/alert_raw")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<RawPlaceVO> searchAlert(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<RawPlace> searchResult = rawService.getMinStockAmount(searchParameters);
		
		SearchResult<RawPlaceVO> searchResultVO = new SearchResult<RawPlaceVO>();
		
		mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}	
}