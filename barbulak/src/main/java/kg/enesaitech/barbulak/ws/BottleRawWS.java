
package kg.enesaitech.barbulak.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import kg.enesaitech.barbulak.entity.BottleRaw;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.BottleRawService;
import kg.enesaitech.barbulak.vo.BottleRawVO;

@Path("/bottle_raw")
public class BottleRawWS extends GenericWS<BottleRaw, BottleRawVO> {

	@Autowired
	BottleRawService bottleRawService;
	
	public BottleRawWS() {
		super(BottleRaw.class, BottleRawVO.class);
	}
	
	
	@POST
	@Path("/create_list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createList(List<BottleRawVO> bollteRawVOs) {
		
		List<BottleRaw> bottleRaws = new ArrayList<BottleRaw>();
		for(BottleRawVO bottleRawVO : bollteRawVOs){
			bottleRaws.add(mapper.map(bottleRawVO, BottleRaw.class));
		}
		bottleRawService.createList(bottleRaws);
		
		return Response.ok().build();
	}
}