
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Raw;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.RawService;
import kg.enesaitech.barbulak.vo.RawVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/raw")
public class RawWS extends GenericWS<Raw, RawVO>{

	public RawWS() {
		super(Raw.class, RawVO.class);
	}
	@Autowired
	RawService rawService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(RawVO rawVO) {
		Raw raw = mapper.map(rawVO, Raw.class);		
		rawService.create(raw);		
		return Response.ok().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id) {
		
		rawService.delete(id);
		
		return Response.ok().build();
	}

}
