package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.PersonalBalance;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.PersonalBalanceService;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.PersonalBalanceVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/personal_balance")
public class PersonalBalanceWS extends GenericWS<PersonalBalance, PersonalBalanceVO> {

	public PersonalBalanceWS() {
		super(PersonalBalance.class, PersonalBalanceVO.class);
	}
	@Autowired
	private PersonalBalanceService personalBalanceService;
	

	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<PersonalBalanceVO> search(SearchParameters searchParameters) {
		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<PersonalBalance> searchResult = personalBalanceService.getList(searchParameters);
		
		SearchResult<PersonalBalanceVO> searchResultVO = new SearchResult<PersonalBalanceVO>();
		
		mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());		
		return searchResultVO;

	}

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(PersonalBalanceVO vo) {
		throw new BusinessException("Access Denied");
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(PersonalBalanceVO vo, @PathParam("id") int id) {
		throw new BusinessException("Access Denied");
	}
	

	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id) {
				

		throw new BusinessException("Access Denied");
	}
}