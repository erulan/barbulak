package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Defect;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.DefectVO;

@Path("/defect")
public class DefectWS extends GenericWS<Defect, DefectVO> {

	public DefectWS() {
		super(Defect.class, DefectVO.class);
	}
}