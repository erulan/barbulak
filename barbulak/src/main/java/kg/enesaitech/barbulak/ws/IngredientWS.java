package kg.enesaitech.barbulak.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Ingredient;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.IngredientService;
import kg.enesaitech.barbulak.vo.IngredientRaw;
import kg.enesaitech.barbulak.vo.IngredientVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/ingredient")
public class IngredientWS extends GenericWS<Ingredient, IngredientVO> {

	public IngredientWS() {
		super(Ingredient.class, IngredientVO.class);
	}

	@Autowired
	IngredientService ingredientService;
	
	@POST
	@Path("/create_list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createList(IngredientRaw ingredientRaw) {
		ingredientService.create(ingredientRaw);		
		return Response.ok().build();
	}
}