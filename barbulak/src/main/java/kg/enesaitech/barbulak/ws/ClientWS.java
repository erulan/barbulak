package kg.enesaitech.barbulak.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Client;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.ClientService;
import kg.enesaitech.barbulak.vo.ClientVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/client")
public class ClientWS extends GenericWS<Client, ClientVO> {

	public ClientWS() {
		super(Client.class, ClientVO.class);
	}
	@Autowired
	ClientService clientService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles("admin")
	public Response create(ClientVO clientVO) {
		Client client = mapper.map(clientVO, Client.class);		
		clientService.create(client);		
		return Response.ok().build();
	}
	


	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<ClientVO> search(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		SearchResult<Client> searchResult = genericService.getList(searchParameters, Client.class);
		SearchResult<ClientVO> searchResultVO = new SearchResult<ClientVO>();

		if(currentUserSubj.hasRole("admin")){			
			mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		} else{
			for(Client s : searchResult.getResultList()){
				ClientVO clientVO = mapper.map(s, ClientVO.class);
				clientVO.getCashbox().setTotal(0);
				searchResultVO.getResultList().add(clientVO);
			}
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}

}
