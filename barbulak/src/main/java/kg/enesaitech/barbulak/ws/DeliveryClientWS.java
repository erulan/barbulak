package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.DeliveryClient;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.DeliveryClientVO;

@Path("/delivery_client")
public class DeliveryClientWS extends GenericWS<DeliveryClient, DeliveryClientVO> {

	public DeliveryClientWS() {
		super(DeliveryClient.class, DeliveryClientVO.class);
	}
}