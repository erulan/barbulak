package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Shipping;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.others.WriteOffInventory;
import kg.enesaitech.barbulak.service.ShippingService;
import kg.enesaitech.barbulak.vo.ShippingByProductBy19;
import kg.enesaitech.barbulak.vo.ShippingVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/shipping")
public class ShippingWS extends GenericWS<Shipping, ShippingVO> {

	public ShippingWS() {
		super(Shipping.class, ShippingVO.class);
	}
	
	@Autowired
	ShippingService shippingService;
	
	@POST
	@Path("/create_request")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createRequest(ShippingByProductBy19 shippingByProductByRaw) {
		shippingService.createRequest(shippingByProductByRaw);		
		return Response.ok().build();
	}

	@POST
	@Path("/create_send")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createSend(ShippingByProductBy19 shippingByProductByRaw) {
		shippingService.sendShipping(shippingByProductByRaw);		
		return Response.ok().build();
	}

	@POST
	@Path("/create_empty_bottle_send")
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendEmptyBottleShipping(ShippingByProductBy19 shippingByProductByRaw) {
		shippingService.sendEmptyBottleShipping(shippingByProductByRaw);		
		return Response.ok().build();
	}

	@POST
	@Path("/create_empty_bottle_request")
	@Produces(MediaType.APPLICATION_JSON)
	public Response requestEmptyBottleShipping(ShippingByProductBy19 shippingByProductByRaw) {
		shippingService.requestEmptyBottleShipping(shippingByProductByRaw);		
		return Response.ok().build();
	}
	
	@GET
	@Path("/approve_by_proizvodstvo/{shipping_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveProizvodstvo(@PathParam("shipping_id") int shippingId) {
		shippingService.proizvodstvoApprove(shippingId);		
		return Response.ok().build();
	}
	
	@GET
	@Path("/approve_by_to_place/{shipping_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveToPlace(@PathParam("shipping_id") int shippingId) {
		shippingService.toPlaceApprove(shippingId);		
		return Response.ok().build();
	}
	
	@GET
	@Path("/approve_by_from_place/{shipping_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approvefromPlace(@PathParam("shipping_id") int shippingId) {
		shippingService.fromPlaceApprove(shippingId);		
		return Response.ok().build();
	}
	

//	@GET
//	@Path("/cancel/{shipping_id}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response cancelShipping(@PathParam("shipping_id") int shippingId) {
//		shippingService.cancelShippinig(shippingId, false);		
//		return Response.ok().build();
//	}
	
	
	@Autowired
	protected WriteOffInventory writeOffInventory;
	
	@GET
	@Path("/test")
	public Response test(){
		writeOffInventory.writeOff();
		return Response.ok().build();
	}
	@POST
	@Path("/update_list/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateList(ShippingByProductBy19 shippingByProductByRaw, @PathParam("id") int id) {
				
		Shipping e2 = genericService.get(Shipping.class, id);
		
		if(e2 == null || e2.getId() == null || (shippingByProductByRaw.getShipping().getId() != null && e2.getId() != shippingByProductByRaw.getShipping().getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		shippingService.update(shippingByProductByRaw);
		return Response.ok().build();
	}
	

	@GET
	@Path("/reject_by_admin/{shipping_id}/{description}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByAdmin(@PathParam("shipping_id") int shippingId, @PathParam("description") String description) {
		shippingService.adminReject(shippingId, description);		
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_admin/{shipping_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAdmin(@PathParam("shipping_id") int shippingId) {
		shippingService.adminApprove(shippingId);		
		return Response.ok().build();
	}

	@GET
	@Path("/reject_by_from_place/{shipping_id}/{description}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByFromPlace(@PathParam("shipping_id") int shippingId, @PathParam("description") String description) {
		shippingService.fromPlaceReject(shippingId, description);		
		return Response.ok().build();
	}

	@GET
	@Path("/reject_by_to_place/{shipping_id}/{description}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByToPlace(@PathParam("shipping_id") int shippingId, @PathParam("description") String description) {
		shippingService.toPlaceReject(shippingId, description);		
		return Response.ok().build();
	}

}