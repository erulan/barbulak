
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.ShippingByProduct;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.ShippingByProductVO;

@Path("/shipping_by_product")
public class ShippingByProductWS extends GenericWS<ShippingByProduct, ShippingByProductVO> {

	public ShippingByProductWS() {
		super(ShippingByProduct.class, ShippingByProductVO.class);
	}
}