package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Capacity;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.CapacityVO;

@Path("/capacity")
public class CapacityWS extends GenericWS<Capacity, CapacityVO> {

	public CapacityWS() {
		super(Capacity.class, CapacityVO.class);
	}

}
