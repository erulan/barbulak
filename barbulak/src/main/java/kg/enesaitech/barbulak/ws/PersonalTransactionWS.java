package kg.enesaitech.barbulak.ws;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.PersonalTransaction;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.PersonalTransactionService;
import kg.enesaitech.barbulak.vo.PersonalTransactionEvaluation;
import kg.enesaitech.barbulak.vo.PersonalTransactionVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/personal_transaction")
public class PersonalTransactionWS extends GenericWS<PersonalTransaction, PersonalTransactionVO> {

	public PersonalTransactionWS() {
		super(PersonalTransaction.class, PersonalTransactionVO.class);
	}

	@Autowired
	PersonalTransactionService personalTransactionService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(PersonalTransactionVO personalTransactionVO) {
		PersonalTransaction personalTransaction = mapper.map(personalTransactionVO, PersonalTransaction.class);
		
		personalTransactionService.create(personalTransaction);
		
		return Response.ok().build();
	}

	@POST
	@Path("/installment_evaluation")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createInstallmentEvaluation(List<PersonalTransactionEvaluation> personalTransactionEvaluations) {
		personalTransactionService.installmentEvaluation(personalTransactionEvaluations);		
		return Response.ok().build();
	}

	@POST
	@Path("/salary_evaluation")
	@Produces(MediaType.APPLICATION_JSON)
	public Response salaryEvaluation(List<PersonalTransactionEvaluation> personalTransactionEvaluations) {
		personalTransactionService.salaryEvaluation(personalTransactionEvaluations);		
		return Response.ok().build();
	}

	@POST
	@Path("/salary_payment")
	@Produces(MediaType.APPLICATION_JSON)
	public Response salaryPayment(List<PersonalTransactionEvaluation> personalTransactionEvaluations) {
		personalTransactionService.salaryPayment(personalTransactionEvaluations);		
		return Response.ok().build();
	}
	

	

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(PersonalTransactionVO vo, @PathParam("id") int id) {
				
		
		return Response.serverError().build();
	}
	

	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id) {
				
		personalTransactionService.delete(id);
		
		return Response.ok().build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public PersonalTransactionVO get(@PathParam("id") int id) {
		
		PersonalTransaction e = personalTransactionService.get(id);
		
		PersonalTransactionVO vo = mapper.map(e, PersonalTransactionVO.class);
		
		return vo;
	}
	
	
	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<PersonalTransactionVO> search(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<PersonalTransaction> searchResult = personalTransactionService.getList(searchParameters);
		
		SearchResult<PersonalTransactionVO> searchResultVO = new SearchResult<PersonalTransactionVO>();
		
		mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}	
}