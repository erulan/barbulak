
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.BottlePlace;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.BottlePlaceVO;

@Path("/bottle_place")
public class BottlePlaceWS extends GenericWS<BottlePlace, BottlePlaceVO> {

	public BottlePlaceWS() {
		super(BottlePlace.class, BottlePlaceVO.class);
	}
}