package kg.enesaitech.barbulak.ws;

import java.net.URI;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import kg.enesaitech.barbulak.dao.RoleHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.Staff;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.generics.GenericService;
import kg.enesaitech.barbulak.service.UsersService;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.StaffVO;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/security")
public class SecurityWS {
//	RoleService roleService = RoleService.getInstance();
	@Autowired
	RoleHome roleHome;

	@Autowired
	UsersService usersService;
	
	@Autowired
	UsersHome usersHome;
	
	@Autowired
	protected DozerBeanMapper mapper;
	
	@Autowired
	GenericService<Staff, GenericHome<Staff>> genericStaffService;
	
	Logger log = Logger.getLogger(SecurityWS.class);

	@GET
	@Path("/redirect")
	public Response authenticate() {
		Response response = Response.ok().build();
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			
			String username = currentUser.getPrincipal().toString();
			Set<String> rolesAsString = roleHome.getNameSetByUserName(username);
			log.warn("************************************************************");
			log.warn("Roles for User in SecurityWS: ["+currentUser.getPrincipal().toString()+"] : " + rolesAsString.toString());
			log.warn("************************************************************");

			if (rolesAsString.contains("admin")) {
				response = Response
						.seeOther(
								URI.create("/barbulak/apps/admin"))
						.build();
			} else if (rolesAsString.contains("manager")) {
				response = Response
						.seeOther(
								URI.create("/barbulak/apps/manager"))
						.build();
			} else if (rolesAsString.contains("store_manager")) {
				response = Response
						.seeOther(
								URI.create("/barbulak/apps/warehouse_manager"))
						.build();
			} else if (rolesAsString.contains("production_manager")) {
				response = Response
						.seeOther(
								URI.create("/barbulak/apps/production_manager"))
						.build();
			} else if (rolesAsString.contains("manager_for_bottle")) {
				response = Response
						.seeOther(
								URI.create("/barbulak/apps/manager_for_bottle"))
						.build();
			} else if (rolesAsString.contains("accountant")) {
				response = Response
						.seeOther(
								URI.create("/barbulak/apps/accountant"))
						.build();
			} else if (rolesAsString.contains("purchasing_agent")) {
				response = Response
						.seeOther(
								URI.create("/barbulak/apps/purchasing_agent"))
								.build();
			} else if (rolesAsString.contains("purchaser")) {
				response = Response
						.seeOther(
								URI.create("/barbulak/apps/purchaser"))
								.build();
			} else {
				response = Response.seeOther(URI.create("/unauthorized.html"))
						.build();
			}
		} else {
//			throw new UnauthorizedException();
			response = Response.seeOther(
					URI.create("/barbulak/login.html")).build();
		}
		return response;
	}

	@GET
	@Path("/current_user")
	@Produces("application/json")
	public StaffVO getCurrentUser() {
		Subject currentUser = SecurityUtils.getSubject();

		Users users = usersHome.getByUserName(currentUser.getPrincipal().toString());
		
		if(users.getStaff() != null){
			Staff staff = genericStaffService.get(Staff.class, users.getStaff().getId());
			StaffVO staffVO = mapper.map(staff, StaffVO.class);
			return staffVO;
		}else{
			return new StaffVO();
		}
	}
	
	@GET
	@Path("/roles_of_current_user")
	@Produces("application/json")
	public Set<String> getRolesOfCurrentUser() {
		return roleHome.getNameSetByUserName(SecurityUtils.getSubject().getPrincipal().toString());
	}
	
	@GET
	@Path("/get_username")
	@Produces("application/json")
	public Response getUsername() {
		Subject currentUser = SecurityUtils.getSubject();
		return Response.ok().entity("{\"username\": \"" + currentUser.getPrincipal().toString() + "\"}").build();
	}

//	this method changes password for current user and it needs 
//	userame, current_p, new_p, new_p_repeat;
	@POST
	@Path("/change_password")
	public Response changePassword(Map<String, String> passwordChange){
		Response response = Response.status(Status.OK).build();
		String username = passwordChange.get("username");
		String currentPass = passwordChange.get("current_p");
		String newPass = passwordChange.get("new_p");
		String newPassRepeat = passwordChange.get("new_p_repeat");
		if(!newPass.equals(newPassRepeat)){throw new BusinessException("Some text");}
		Users user = usersHome.getByUserName(username);
		Subject currentUser = SecurityUtils.getSubject();
		
		if(!currentUser.isAuthenticated() || !user.getUserName().equals(currentUser.getPrincipal())){
			throw new BusinessException("Access denied");
		}

		if(usersHome.getMD5(currentPass).equals(user.getUserPass())){
			user.setUserPass(newPass);
			usersService.update(user);
		}else{
			throw new BusinessException("Current password doesn't match ");
		}
			
		return response;
	}
}
