package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.PlaceService;
import kg.enesaitech.barbulak.vo.PlaceVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/place")
public class PlaceWS extends GenericWS<Place, PlaceVO> {

	public PlaceWS() {
		super(Place.class, PlaceVO.class);
	}
	@Autowired
	PlaceService placeService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(PlaceVO placeVO) {
		Place place = mapper.map(placeVO, Place.class);		
		placeService.create(place);		
		return Response.ok().build();
	}
	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(PlaceVO placeVO, @PathParam("id") int id) {
		Place place2 = placeService.get(Place.class, id);
		
		if(place2 == null || place2.getId() == null || (placeVO.getId() != null && place2.getId() != placeVO.getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		Place place = mapper.map(placeVO, Place.class);
		placeService.update(place);
		return Response.ok().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id) {
		
		placeService.delete(id);
		
		return Response.ok().build();
	}
	
}
