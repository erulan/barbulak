package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.ObjectType;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.ObjectTypeVO;

@Path("/object_type")
public class ObjectTypeWS extends GenericWS<ObjectType, ObjectTypeVO> {

	public ObjectTypeWS() {
		super(ObjectType.class, ObjectTypeVO.class);
	}
}