package kg.enesaitech.barbulak.ws;

import java.util.HashSet;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.StaffPosition;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.generics.GenericService;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.UsersService;
import kg.enesaitech.barbulak.vo.PositionVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;
import kg.enesaitech.barbulak.vo.UsersVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/users")
public class UsersWS extends GenericWS<Users, UsersVO>{
	
	public UsersWS(){
		super(Users.class, UsersVO.class);
	};
	
	@Autowired
	UsersService usersService;
	
	@Autowired
	GenericService<StaffPosition, GenericHome<StaffPosition>> genericStaffPositionService;
	
	@GET
	@Path("/create_user_by_staffid/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createByStaffId(@PathParam("id") int id) {
		
		
		usersService.createByStaffId(id);
		
		return Response.ok().build();
	};
	
	
	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<UsersVO> search(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<Users> searchResult = genericService.getList(searchParameters, Users.class);
		
		SearchResult<UsersVO> searchResultVO = new SearchResult<UsersVO>();
		
		
		SearchParameters searchParameters2 = new SearchParameters();
		for(Users user : searchResult.getResultList()){
			UsersVO userVO = mapper.map(user, UsersVO.class);
			
			searchParameters2.addParameter("staff.id", user.getStaff().getId().toString());
			SearchResult<StaffPosition> staffPositions = genericStaffPositionService.getList(searchParameters2, StaffPosition.class);
			userVO.getStaff().setPositions(new HashSet<PositionVO>());
			for(StaffPosition sp : staffPositions.getResultList()){
				userVO.getStaff().getPositions().add(mapper.map(sp.getPosition(), PositionVO.class));
			}
			searchResultVO.getResultList().add(userVO);
		}
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());
		

		return searchResultVO;

	}

}
