package kg.enesaitech.barbulak.ws;

import java.util.Collections;
import java.util.HashSet;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Staff;
import kg.enesaitech.barbulak.entity.StaffPosition;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.generics.GenericService;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.StaffService;
import kg.enesaitech.barbulak.vo.PositionVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;
import kg.enesaitech.barbulak.vo.StaffPositionList;
import kg.enesaitech.barbulak.vo.StaffVO;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/staff")
public class StaffWS extends GenericWS<Staff, StaffVO>{

	public StaffWS() {
		super(Staff.class, StaffVO.class);
	}
	@Autowired
	StaffService staffService;
	
	@Autowired
	GenericService<StaffPosition, GenericHome<StaffPosition>> genericStaffPositionService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles("admin")
	public Response create(StaffVO staffVO) {
		Staff staff = mapper.map(staffVO, Staff.class);		
		staffService.create(staff);		
		return Response.ok().build();
	}
	

	@POST
	@Path("/create_with_position")
	@Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles("admin")
	public Response createWithPosition(StaffPositionList staffPositionList) {
		staffService.createWithPositions(staffPositionList);		
		return Response.ok().build();
	}
	
	@POST
	@Path("/update_with_position/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RequiresRoles("admin")
	public Response updateWihtPosition(StaffPositionList staffPositionList, @PathParam("id") int id) {

		Staff staff2 = genericService.get(Staff.class, id);
		
		if(staff2 == null || staff2.getId() == null || (staffPositionList.getStaff().getId() != null && staff2.getId() != staffPositionList.getStaff().getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		staffService.updateWithPosition(staffPositionList);
		return Response.ok().build();
	}


	@POST
	@Path("/list_for_acc")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<StaffVO> searchList(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<Staff> searchResult = staffService.getListByCashBox(searchParameters);
		
		SearchResult<StaffVO> searchResultVO = new SearchResult<StaffVO>();
		
		mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}

	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<StaffVO> search(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		SearchResult<Staff> searchResult = genericService.getList(searchParameters, Staff.class);
		SearchResult<StaffVO> searchResultVO = new SearchResult<StaffVO>();

		SearchParameters searchParameters2 = new SearchParameters();
		
		if(currentUserSubj.hasRole("admin")){			
			for(Staff s : searchResult.getResultList()){
				StaffVO staffVO = mapper.map(s, StaffVO.class);
				
				searchParameters2.addParameter("staff.id", s.getId().toString());
				SearchResult<StaffPosition> staffPositions = genericStaffPositionService.getList(searchParameters2, StaffPosition.class);
				staffVO.setPositions(new HashSet<PositionVO>());
				for(StaffPosition sp : staffPositions.getResultList()){
					staffVO.getPositions().add(mapper.map(sp.getPosition(), PositionVO.class));
				}
				
				searchResultVO.getResultList().add(staffVO);
			}
		} else{
			for(Staff s : searchResult.getResultList()){
				StaffVO staffVO = mapper.map(s, StaffVO.class);
				
				searchParameters2.addParameter("staff.id", s.getId().toString());
				SearchResult<StaffPosition> staffPositions = genericStaffPositionService.getList(searchParameters2, StaffPosition.class);
				staffVO.setPositions(Collections.emptySet());
				for(StaffPosition sp : staffPositions.getResultList()){
					staffVO.getPositions().add(mapper.map(sp.getPosition(), PositionVO.class));
				}
				
				staffVO.getCashbox().setTotal(0);
				searchResultVO.getResultList().add(staffVO);
			}
		}
		
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}
	
	
}
