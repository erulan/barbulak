package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.ProductPlace;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.ProductPlaceVO;

@Path("/product_place")
public class ProductPlaceWS extends GenericWS<ProductPlace, ProductPlaceVO> {

	public ProductPlaceWS() {
		super(ProductPlace.class, ProductPlaceVO.class);
	}
}