package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Product;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.ProductService;
import kg.enesaitech.barbulak.vo.ProductVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/product")
public class ProductWS extends GenericWS<Product, ProductVO>{

	public ProductWS() {
		super(Product.class, ProductVO.class);
		// TODO Auto-generated constructor stub
	}
	@Autowired
	ProductService productService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(ProductVO productVO) {
		Product product = mapper.map(productVO, Product.class);		
		productService.create(product);		
		return Response.ok().build();
	}

	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id) {
		
		productService.delete(id);
		
		return Response.ok().build();
	}
}
