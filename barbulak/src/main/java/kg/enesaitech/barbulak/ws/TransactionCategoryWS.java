package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.TransactionCategory;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.TransactionCategoryVO;

@Path("/transaction_category")
public class TransactionCategoryWS extends GenericWS<TransactionCategory, TransactionCategoryVO> {

	public TransactionCategoryWS() {
		super(TransactionCategory.class, TransactionCategoryVO.class);
	}

}
