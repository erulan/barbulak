package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Delivery;
import kg.enesaitech.barbulak.entity.DeliveryClient;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.generics.GenericService;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.DeliveryService;
import kg.enesaitech.barbulak.vo.DeliveryClientVO;
import kg.enesaitech.barbulak.vo.DeliveryVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/delivery")
public class DeliveryWS extends GenericWS<Delivery, DeliveryVO> {

	public DeliveryWS() {
		super(Delivery.class, DeliveryVO.class);
	}

	@Autowired
	private DeliveryService deliveryService;

	@Autowired
	private GenericService<DeliveryClient, GenericHome<DeliveryClient>> genericDeliveryClientService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(DeliveryVO deliveryVO) {
		Delivery delivery = mapper.map(deliveryVO, Delivery.class);

		deliveryService.create(delivery);

		return Response.ok().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(DeliveryVO deliveryVO, @PathParam("id") int id) {
				
		Delivery e2 = genericService.get(Delivery.class, id);
		
		if(e2 == null || e2.getId() == null || (deliveryVO.getId() != null && e2.getId() != deliveryVO.getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		Delivery delivery = mapper.map(deliveryVO, Delivery.class);
		deliveryService.update(delivery);
		return Response.ok().build();
	}

	
	@POST
	@Path("/send")
	@Produces(MediaType.APPLICATION_JSON)
	public Response send(DeliveryVO deliveryVO) {
		Delivery delivery = mapper.map(deliveryVO, Delivery.class);

		deliveryService.send(delivery);

		return Response.ok().build();
	}
	
	@POST
	@Path("/complete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response complete(DeliveryVO deliveryVO) {
		Delivery delivery = mapper.map(deliveryVO, Delivery.class);

		deliveryService.complete(delivery);

		return Response.ok().build();
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public DeliveryVO get(@PathParam("id") int id) {
		
		Delivery delivery = genericService.get(Delivery.class, id);
		
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("delivery.id", String.valueOf(id));
		SearchResult<DeliveryClient> deliveryClients = genericDeliveryClientService.getList(searchParameters, DeliveryClient.class);
		
		DeliveryVO deliveryVO = mapper.map(delivery, DeliveryVO.class);
		for(DeliveryClient dc : deliveryClients.getResultList()){
			DeliveryClientVO dcVO = mapper.map(dc, DeliveryClientVO.class);
			dcVO.setDelivery(null);
			deliveryVO.getDeliveryClients().add(dcVO);
			
		}
		
		return deliveryVO;
	}
}