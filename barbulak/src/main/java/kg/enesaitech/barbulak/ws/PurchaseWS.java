package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Purchase;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.PurchaseService;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.PurchaseVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/purchase")
public class PurchaseWS extends GenericWS<Purchase, PurchaseVO> {

	public PurchaseWS() {
		super(Purchase.class, PurchaseVO.class);
	}
	@Autowired
	PurchaseService purchaseService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(PurchaseVO purchaseVO) {
		Purchase purchase = mapper.map(purchaseVO, Purchase.class);
		
		purchaseService.create(purchase);
		
		return Response.ok().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(PurchaseVO vo, @PathParam("id") int id) {
				
		Purchase e2 = genericService.get(Purchase.class, id);
		
		if(e2 == null || e2.getId() == null || (vo.getId() != null && e2.getId() != vo.getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		Purchase e = mapper.map(vo, Purchase.class);
		purchaseService.update(e);
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_to_place/{purchase_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approve(@PathParam("purchase_id") int purchaseId) {
		purchaseService.toPlaceApprove(purchaseId);		
		return Response.ok().build();
	}

	@GET
	@Path("/reject_by_to_place/{purchase_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByToPlace(@PathParam("purchase_id") int purchaseId) {
		purchaseService.toPlaceReject(purchaseId);		
		return Response.ok().build();
	}
	

	@GET
	@Path("/reject_by_admin/{purchase_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByAdmin(@PathParam("purchase_id") int purchaseId) {
		purchaseService.adminReject(purchaseId);		
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_admin/{purchase_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAdmin(@PathParam("purchase_id") int purchaseId) {
		purchaseService.adminApprove(purchaseId);		
		return Response.ok().build();
	}

	@POST
	@Path("/approve_by_acc/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAcc(PurchaseVO purchaseVO, @PathParam("id") int id) {
				
		Purchase e2 = genericService.get(Purchase.class, id);
		
		if(e2 == null || e2.getId() == null || (purchaseVO.getId() != null && e2.getId() != purchaseVO.getId()))
		{	
			log.error("Id sent in object doesn't match path id!!!");
			throw new BusinessException("Id sent in object doesn't match path id!!!");	
		}
		Purchase purchase = mapper.map(purchaseVO, Purchase.class);
		purchaseService.accountantApprove(purchase);
		return Response.ok().build();
	}
	
}
