package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.DeliveryStatus;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.DeliveryStatusVO;

@Path("/delivery_status")
public class DeliveryStatusWS extends GenericWS<DeliveryStatus, DeliveryStatusVO> {

	public DeliveryStatusWS() {
		super(DeliveryStatus.class, DeliveryStatusVO.class);
	}
}
