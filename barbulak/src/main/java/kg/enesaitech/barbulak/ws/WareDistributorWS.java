package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.WareDistributor;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.WareDistributorService;
import kg.enesaitech.barbulak.vo.WareDistributorByProduct;
import kg.enesaitech.barbulak.vo.WareDistributorVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/ware_distributor")
public class WareDistributorWS extends GenericWS<WareDistributor, WareDistributorVO> {

	public WareDistributorWS() {
		super(WareDistributor.class, WareDistributorVO.class);
	}
	@Autowired
	WareDistributorService wareDistributorService;
	
	@POST
	@Path("/create_list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createList(WareDistributorByProduct wareDistributorByProduct) {
		wareDistributorService.create(wareDistributorByProduct);		
		return Response.ok().build();
	}
	
	@GET
	@Path("/approve_by_from_place/{ware_dist_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approvefromPlace(@PathParam("ware_dist_id") int wareDistId) {
		wareDistributorService.fromPlaceApprove(wareDistId);		
		return Response.ok().build();
	}

	@POST
	@Path("/update_list/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateList(WareDistributorByProduct wareDistributorByProduct, @PathParam("id") int id) {
				
		WareDistributor e2 = genericService.get(WareDistributor.class, id);
		
		if(e2 == null || e2.getId() == null || (wareDistributorByProduct.getWareDistributor().getId() != null && e2.getId() != wareDistributorByProduct.getWareDistributor().getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		wareDistributorService.update(wareDistributorByProduct);
		return Response.ok().build();
	}
	

	@GET
	@Path("/reject_by_admin/{ware_dist_id}/{description}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByAdmin(@PathParam("ware_dist_id") int wareDistId, @PathParam("description") String description) {
		wareDistributorService.adminReject(wareDistId, description);		
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_admin/{ware_dist_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAdmin(@PathParam("ware_dist_id") int wareDistId) {
		wareDistributorService.adminApprove(wareDistId);		
		return Response.ok().build();
	}

	@GET
	@Path("/reject_by_from_place/{ware_dist_id}/{description}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByFromPlace(@PathParam("ware_dist_id") int wareDistId, @PathParam("description") String description) {
		wareDistributorService.fromPlaceReject(wareDistId, description);		
		return Response.ok().build();
	}
}