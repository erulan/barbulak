package kg.enesaitech.barbulak.ws;

import java.util.List;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Installment;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.InstallmentService;
import kg.enesaitech.barbulak.vo.InstallmentVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/installment")
public class InstallmentWS extends GenericWS<Installment, InstallmentVO> {

	public InstallmentWS() {
		super(Installment.class, InstallmentVO.class);
	}
	@Autowired
	InstallmentService installmentService;
	

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(InstallmentVO installmentVO) {
		Installment installment = mapper.map(installmentVO, Installment.class);
		
		installmentService.create(installment);
		
		return Response.ok().build();
	}
	
	@POST
	@Path("/evaluation_list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<InstallmentVO> searchEvaluation(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		List<Installment> searchResult = installmentService.getList();
		
		SearchResult<InstallmentVO> searchResultVO = new SearchResult<InstallmentVO>();
		
		mapper.map(searchResult, searchResultVO.getResultList());

		return searchResultVO;

	}
}