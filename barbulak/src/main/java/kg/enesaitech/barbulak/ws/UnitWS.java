package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Unit;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.UnitVO;

@Path("/unit")
public class UnitWS extends GenericWS<Unit, UnitVO>{
	
	public UnitWS(){
		super(Unit.class, UnitVO.class);
	}
}
