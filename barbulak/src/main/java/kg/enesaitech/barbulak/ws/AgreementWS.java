
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Agreement;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.AgreementVO;

@Path("/agreement")
public class AgreementWS extends GenericWS<Agreement, AgreementVO> {

	public AgreementWS() {
		super(Agreement.class, AgreementVO.class);
	}
}