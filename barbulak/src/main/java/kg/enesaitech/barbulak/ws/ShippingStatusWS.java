package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.ShippingStatus;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.ShippingStatusVO;

@Path("/shipping_status")
public class ShippingStatusWS extends GenericWS<ShippingStatus, ShippingStatusVO> {

	public ShippingStatusWS() {
		super(ShippingStatus.class, ShippingStatusVO.class);
	}
}