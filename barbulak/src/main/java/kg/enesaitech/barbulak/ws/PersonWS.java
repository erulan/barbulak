package kg.enesaitech.barbulak.ws;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import kg.enesaitech.barbulak.entity.Person;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.PersonService;
import kg.enesaitech.barbulak.vo.PersonVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/person")
public class PersonWS extends GenericWS<Person, PersonVO> {

	public PersonWS() {
		super(Person.class, PersonVO.class);
	}
	@Autowired
	PersonService personService;
	
	@POST
	@Path("/list_for_acc")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<PersonVO> searchList(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<Person> searchResult = personService.getListByCashBox(searchParameters);
		
		SearchResult<PersonVO> searchResultVO = new SearchResult<PersonVO>();
		
		mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}

}
