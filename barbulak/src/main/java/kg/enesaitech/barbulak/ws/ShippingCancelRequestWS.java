package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.ShippingCancelRequest;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.ShippingCancelRequestVO;

@Path("/shipping_cancel_request")
public class ShippingCancelRequestWS extends GenericWS<ShippingCancelRequest, kg.enesaitech.barbulak.vo.ShippingCancelRequestVO>{
	
	public ShippingCancelRequestWS(){
		super(ShippingCancelRequest.class, ShippingCancelRequestVO.class);
	}
}
