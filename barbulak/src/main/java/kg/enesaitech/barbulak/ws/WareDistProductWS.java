package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.WareDistProduct;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.WareDistProductVO;

@Path("/Ware_dist_product")
public class WareDistProductWS extends GenericWS<WareDistProduct, WareDistProductVO> {

	public WareDistProductWS() {
		super(WareDistProduct.class, WareDistProductVO.class);
	}
}