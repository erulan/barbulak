package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.InventoryCategory;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.InventoryCategoryVO;

@Path("/inventory_category")
public class InventoryCategoryWS extends GenericWS<InventoryCategory, InventoryCategoryVO> {

	public InventoryCategoryWS() {
		super(InventoryCategory.class, InventoryCategoryVO.class);
	}
}
