
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Place19Manager;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.Place19ManagerVO;

@Path("/place_19_manager")
public class Place19ManagerWS extends GenericWS<Place19Manager, Place19ManagerVO> {

	public Place19ManagerWS() {
		super(Place19Manager.class, Place19ManagerVO.class);
	}
}