package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Purchase19;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.Purchase19Service;
import kg.enesaitech.barbulak.vo.Purchase19VO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/purchase_19")
public class Purchase19WS extends GenericWS<Purchase19, Purchase19VO> {

	public Purchase19WS() {
		super(Purchase19.class, Purchase19VO.class);
	}
	@Autowired
	Purchase19Service purchase19Service;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(Purchase19VO purchaseVO) {
		Purchase19 purchase19 = mapper.map(purchaseVO, Purchase19.class);
		
		purchase19Service.create(purchase19);
		
		return Response.ok().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(Purchase19VO vo, @PathParam("id") int id) {
				
		Purchase19 e2 = genericService.get(Purchase19.class, id);
		
		if(e2 == null || e2.getId() == null || (vo.getId() != null && e2.getId() != vo.getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		Purchase19 e = mapper.map(vo, Purchase19.class);
		purchase19Service.update(e);
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_to_place/{purchase_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approve(@PathParam("purchase_id") int purchase19Id) {
		purchase19Service.toPlaceApprove(purchase19Id);		
		return Response.ok().build();
	}

	@GET
	@Path("/reject_by_to_place/{purchase_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByToPlace(@PathParam("purchase_id") int purchase19Id) {
		purchase19Service.toPlaceReject(purchase19Id);		
		return Response.ok().build();
	}
	

	@GET
	@Path("/reject_by_admin/{purchase_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByAdmin(@PathParam("purchase_id") int purchase19Id) {
		purchase19Service.adminReject(purchase19Id);		
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_admin/{purchase_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAdmin(@PathParam("purchase_id") int purchase19Id) {
		purchase19Service.adminApprove(purchase19Id);		
		return Response.ok().build();
	}
//
//	@POST
//	@Path("/approve_by_acc/{id}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response approveByAcc(Purchase19VO purchase19VO, @PathParam("id") int id) {
//				
//		Purchase19 e2 = genericService.get(Purchase19.class, id);
//		
//		if(e2 == null || e2.getId() == null || (purchase19VO.getId() != null && e2.getId() != purchase19VO.getId()))
//		{	
//			log.error("Id sent in object doesn't match path id!!!");
//			throw new BusinessException("Id sent in object doesn't match path id!!!");	
//		}
//		Purchase19 purchase = mapper.map(purchase19VO, Purchase19.class);
//		purchase19Service.accountantApprove(purchase);
//		return Response.ok().build();
//	}
	
}
