package kg.enesaitech.barbulak.ws;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.ProductionProcess;
import kg.enesaitech.barbulak.entity.ProductionProcessProduct;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.ProductionProcessService;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.ProductionProcessProductVO;
import kg.enesaitech.barbulak.vo.ProductionProcessVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/production_process")
public class ProductionProcessWS extends GenericWS<ProductionProcess, ProductionProcessVO> {

	public ProductionProcessWS() {
		super(ProductionProcess.class, ProductionProcessVO.class);
	}
	@Autowired
	ProductionProcessService productionProcessService;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(ProductionProcessVO productionProcessVO) {
		ProductionProcess productionProcess = mapper.map(productionProcessVO, ProductionProcess.class);
		
		productionProcessService.create(productionProcess);
		
		return Response.ok().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(ProductionProcessVO vo, @PathParam("id") int id) {
				
		ProductionProcess e2 = genericService.get(ProductionProcess.class, id);
		
		if(e2 == null || e2.getId() == null || (vo.getId() != null && e2.getId() != vo.getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		ProductionProcess e = mapper.map(vo, ProductionProcess.class);
		productionProcessService.update(e);
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_to_place/{production_process_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approve(@PathParam("production_process_id") int productionProcessId) {
		productionProcessService.toPlaceApprove(productionProcessId);		
		return Response.ok().build();
	}
	

	@GET
	@Path("/reject_by_admin/{production_process_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByAdmin(@PathParam("production_process_id") int productionProcessId) {
		productionProcessService.adminReject(productionProcessId);		
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_admin/{production_process_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAdmin(@PathParam("production_process_id") int productionProcessId) {
		productionProcessService.adminApprove(productionProcessId);		
		return Response.ok().build();
	}

	@POST
	@Path("/approve_by_production/{id}") //  approve kylotkanda production amounttu beresiz
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAcc(ProductionProcessVO productionProcessVO, @PathParam("id") int id) {
				
		ProductionProcess e2 = genericService.get(ProductionProcess.class, id);
		
		if(e2 == null || e2.getId() == null || (productionProcessVO.getId() != null && e2.getId() != productionProcessVO.getId()))
		{	
			log.error("Id sent in object doesn't match path id!!!");
			throw new BusinessException("Id sent in object doesn't match path id!!!");	
		}
		ProductionProcess productionProcess = mapper.map(productionProcessVO, ProductionProcess.class);
		productionProcessService.productionApprove(productionProcess);
		return Response.ok().build();
	}
	
	@POST
	@Path("/check_ingredients")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkIngredients(List<ProductionProcessProductVO> productionProcessProductVOs) {
		Set<ProductionProcessProduct> productionProcessProducts = new HashSet<ProductionProcessProduct>();
		
		for(ProductionProcessProductVO processProductVO : productionProcessProductVOs){
			productionProcessProducts.add(mapper.map(processProductVO, ProductionProcessProduct.class));
		}
		productionProcessService.checkIngredients(productionProcessProducts);
		
		return Response.ok().build();
	}
	
}
