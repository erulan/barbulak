
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Role;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.RoleVO;

@Path("/role")
public class RoleWS extends GenericWS<Role, RoleVO> {

	public RoleWS(){
		super(Role.class, RoleVO.class);
	}
}
