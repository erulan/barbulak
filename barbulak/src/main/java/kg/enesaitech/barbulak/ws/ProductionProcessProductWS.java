package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.ProductionProcessProduct;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.ProductionProcessProductVO;

@Path("/production_process_product")
public class ProductionProcessProductWS extends GenericWS<ProductionProcessProduct, ProductionProcessProductVO> {

	public ProductionProcessProductWS() {
		super(ProductionProcessProduct.class, ProductionProcessProductVO.class);
	}

}
