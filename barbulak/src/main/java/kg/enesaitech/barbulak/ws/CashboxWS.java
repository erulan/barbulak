package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Cashbox;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.CashboxService;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.CashboxVO;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/cashbox")
public class CashboxWS extends GenericWS<Cashbox, CashboxVO> {

	public CashboxWS() {
		super(Cashbox.class, CashboxVO.class);
	}
	@Autowired
	private CashboxService cashboxService;
	
	@POST
	@Path("/list_for_acc")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<CashboxVO> searchList(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<Cashbox> searchResult = cashboxService.getListByCashBox(searchParameters);
		
		SearchResult<CashboxVO> searchResultVO = new SearchResult<CashboxVO>();
		
		mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(CashboxVO vo) {
		throw new BusinessException("Access Denied!!!!!!!!!!!!!!!!!!");
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(CashboxVO vo, @PathParam("id") int id) {
		throw new BusinessException("Access Denied!!!!!!!!!!!!!!!!!!");
	}
	

	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id) {
				

		throw new BusinessException("Access Denied!!!!!!!!!!!!!!!!");
	}

}
