package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.CashboxAccountantVO;

@Path("/cashbox_accountant")
public class CashboxAccountantWS extends GenericWS<CashboxAccountant, CashboxAccountantVO>{
	
	public CashboxAccountantWS(){
		super(CashboxAccountant.class, CashboxAccountantVO.class);
	}
}
