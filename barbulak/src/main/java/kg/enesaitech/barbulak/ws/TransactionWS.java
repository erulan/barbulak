package kg.enesaitech.barbulak.ws;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Transaction;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.TransactionService;
import kg.enesaitech.barbulak.vo.BalanceByCategory;
import kg.enesaitech.barbulak.vo.DebitCreditBalance;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;
import kg.enesaitech.barbulak.vo.SearchTransaction;
import kg.enesaitech.barbulak.vo.TransactionVO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/transaction")
public class TransactionWS extends GenericWS<Transaction, TransactionVO> {

	public TransactionWS() {
		super(Transaction.class, TransactionVO.class);
	}

	@Autowired
	TransactionService transactionService;

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(TransactionVO transactionVO) {
		Transaction transaction = mapper.map(transactionVO, Transaction.class);

		transactionService.create(transaction);

		return Response.ok().build();
	}
	

	@POST
	@Path("/create_personal/{personal_category_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createWithPersonal(TransactionVO transactionVO, @PathParam("personal_category_id") int personalCategoryId) {
		Transaction transaction = mapper.map(transactionVO, Transaction.class);

		transactionService.createWithPersonal(transaction, personalCategoryId);

		return Response.ok().build();
	}
	
	
	@POST
	@Path("/get_balance_by_category")
	@Produces(MediaType.APPLICATION_JSON)
	public List<BalanceByCategory> getBalanceByCategoryList(SearchTransaction searchTransaction) {
		
		List<BalanceByCategory> balanceByCategoryList = transactionService.getBalanceByCategoryList(searchTransaction);
		
		return balanceByCategoryList;
	}
	@POST
	@Path("/debit_credit_balance")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DebitCreditBalance> getDebitCreditBalance(SearchTransaction searchTransaction) {
		
		List<DebitCreditBalance> debitCreditBalanceList = transactionService.getTest(searchTransaction);
		
		return debitCreditBalanceList;
	}
	

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(TransactionVO vo, @PathParam("id") int id) {
				
		
		return Response.serverError().build();
	}
	

	@GET
	@Path("/delete/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") int id) {
				
		transactionService.delete(id);
		
		return Response.ok().build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public TransactionVO get(@PathParam("id") int id) {
		
		Transaction e = transactionService.get(id);
		
		TransactionVO vo = mapper.map(e, TransactionVO.class);
		
		return vo;
	}
	
	
	@POST
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public SearchResult<TransactionVO> search(SearchParameters searchParameters) {

		if(searchParameters == null) searchParameters = new SearchParameters();
		SearchResult<Transaction> searchResult = transactionService.getList(searchParameters);
		
		SearchResult<TransactionVO> searchResultVO = new SearchResult<TransactionVO>();
		
		mapper.map(searchResult.getResultList(), searchResultVO.getResultList());
		searchResultVO.setTotalRecords(searchResult.getTotalRecords());

		return searchResultVO;

	}	
	
}