
package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.StaffPosition;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.StaffPositionVO;

@Path("/staff_position")
public class StaffPositionWS extends GenericWS<StaffPosition, StaffPositionVO> {

	public StaffPositionWS() {
		super(StaffPosition.class, StaffPositionVO.class);
	}
}