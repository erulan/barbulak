package kg.enesaitech.barbulak.ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import kg.enesaitech.barbulak.entity.Production19;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.service.Production19Service;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.Production19VO;

import org.springframework.beans.factory.annotation.Autowired;

@Path("/production_19")
public class Production19WS extends GenericWS<Production19, Production19VO> {

	public Production19WS() {
		super(Production19.class, Production19VO.class);
	}
	@Autowired
	Production19Service production19Service;
	
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(Production19VO production19vo) {
		Production19 productionProcess = mapper.map(production19vo, Production19.class);
		
		production19Service.create(productionProcess);
		
		return Response.ok().build();
	}

	@POST
	@Path("/update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(Production19VO vo, @PathParam("id") int id) {
				
		Production19 e2 = genericService.get(Production19.class, id);
		
		if(e2 == null || e2.getId() == null || (vo.getId() != null && e2.getId() != vo.getId()))
			log.error("Id sent in object doesn't match path id!!!");
		
		Production19 e = mapper.map(vo, Production19.class);
		production19Service.update(e);
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_to_place/{production_19_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approve(@PathParam("production_19_id") int production19Id) {
		production19Service.toPlaceApprove(production19Id);		
		return Response.ok().build();
	}
	

	@GET
	@Path("/reject_by_admin/{production_19_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByAdmin(@PathParam("production_19_id") int production19Id) {
		production19Service.adminReject(production19Id);		
		return Response.ok().build();
	}

	@GET
	@Path("/approve_by_admin/{production_19_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAdmin(@PathParam("production_19_id") int production19Id) {
		production19Service.adminApprove(production19Id);		
		return Response.ok().build();
	}

	@POST
	@Path("/approve_by_production/{id}") //  approve kylotkanda production amounttu beresiz
	@Produces(MediaType.APPLICATION_JSON)
	public Response approveByAcc(Production19VO production19vo, @PathParam("id") int id) {
				
		Production19 e2 = genericService.get(Production19.class, id);
		
		if(e2 == null || e2.getId() == null || (production19vo.getId() != null && e2.getId() != production19vo.getId()))
		{	
			log.error("Id sent in object doesn't match path id!!!");
			throw new BusinessException("Id sent in object doesn't match path id!!!");	
		}
		Production19 productionProcess = mapper.map(production19vo, Production19.class);
		production19Service.productionApprove(productionProcess);
		return Response.ok().build();
	}
	
	@POST
	@Path("/check_ingredients")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkIngredients(Production19VO production19vo) {
		Production19 production19 = mapper.map(production19vo, Production19.class);
		production19Service.checkIngredients(production19);
		
		return Response.ok().build();
	}
	

	@GET
	@Path("/reject_by_to_place/{production_19_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response rejectByToPlace(@PathParam("production_19_id") int production19Id) {
		production19Service.toPlaceReject(production19Id);		
		return Response.ok().build();
	}
	
}
