package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.Position;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.PositionVO;

@Path("/position")
public class PositionWS extends GenericWS<Position, PositionVO>{
	
	public PositionWS(){
		super(Position.class, PositionVO.class);
	}
}
