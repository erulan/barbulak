package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.ProductType;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.ProductTypeVO;

@Path("/product_type")
public class ProductTypeWS extends GenericWS<ProductType, ProductTypeVO> {
	
	public ProductTypeWS(){
		super(ProductType.class, ProductTypeVO.class);
	}
}
