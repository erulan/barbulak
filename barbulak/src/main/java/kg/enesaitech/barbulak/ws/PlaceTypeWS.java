package kg.enesaitech.barbulak.ws;

import javax.ws.rs.Path;

import kg.enesaitech.barbulak.entity.PlaceType;
import kg.enesaitech.barbulak.generics.GenericWS;
import kg.enesaitech.barbulak.vo.PlaceTypeVO;

@Path("/place_type")
public class PlaceTypeWS extends GenericWS<PlaceType, PlaceTypeVO> {

	public PlaceTypeWS() {
		super(PlaceType.class, PlaceTypeVO.class);
		// TODO Auto-generated constructor stub
	}
}
