package kg.enesaitech.barbulak.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.dao.RawPlaceHome;
import kg.enesaitech.barbulak.dao.RawPurchaseHome;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.Purchase;
import kg.enesaitech.barbulak.entity.RawPlace;
import kg.enesaitech.barbulak.entity.RawPurchase;
import kg.enesaitech.barbulak.entity.ShippingStatus;
import kg.enesaitech.barbulak.entity.StatusDate;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PurchaseService {
	
	@Autowired
	private GenericHome<Purchase> purchaseHome;	
	@Autowired
	private RawPurchaseHome rawPurchaseHome;
	@Autowired
	private RawPlaceHome rawPlaceHome;
	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private GenericHome<ShippingStatus> shippingStatusHome;
	@Autowired
	private UserCheckService checkService;
	@Autowired
	private GenericHome<StatusDate> statusDateHome;

	@Transactional
	public void create(Purchase purchase) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("production_manager")){
			Place toPlace = placeHome.findById(Place.class, purchase.getToPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
				Set<RawPurchase> rawPurchases = purchase.getRawPurchases();
				purchase.setRawPurchases(null);
				StatusDate statusDate = new StatusDate();
				statusDate.setCreated(new Date());
				Integer statusDateId = statusDateHome.persist(statusDate);
				statusDate.setId(statusDateId);
				purchase.setStatusDate(statusDate);
				purchase.setStatus(shippingStatusHome.findById(ShippingStatus.class, 1));
				Integer purchaseId = purchaseHome.persist(purchase);
				purchase.setId(purchaseId);
				for(RawPurchase rawPurchase : rawPurchases){
					rawPurchase.setPurchase(purchase);
					rawPurchaseHome.persist(rawPurchase);
				}
			} else
				throw new BusinessException("Вы не имеете права для покупки!! Вы не ответственны за "+toPlace.getName()+" !!");		
			
		} else
			throw new BusinessException("Вы не имеете права для покупки!!!");		

		
		
	}
	
	@Transactional
	public void toPlaceApprove(int purchaseId) {
		Purchase purchase = purchaseHome.findById(Purchase.class, purchaseId);
		Place toPlace = placeHome.findById(Place.class, purchase.getToPlaceId().getId());
		if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
			if(purchase.getStatus().getId()==4){
				if(toPlace.getPlaceType().getName().equals("производство") ){
					List<RawPurchase> rawPurchases = rawPurchaseHome.getByPurchaseId(purchase.getId());
					for(RawPurchase rawPurchase : rawPurchases){
								
						RawPlace toRawPlace = rawPlaceHome.getByRawIdByPlaceId(rawPurchase.getRaw().getId(), toPlace.getId());
						toRawPlace.setAmount(toRawPlace.getAmount()+rawPurchase.getAmount());
						rawPlaceHome.merge(toRawPlace);
					}
					purchase.getStatusDate().setApprovedToPlace(new Date());
					purchase.setStatus(shippingStatusHome.findById(ShippingStatus.class, 6));
					purchaseHome.merge(purchase);
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является производством!!");
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		

	}
	
	@Transactional
	public void toPlaceReject(int purchaseId) {
		Purchase purchase = purchaseHome.findById(Purchase.class, purchaseId);
		Place toPlace = placeHome.findById(Place.class, purchase.getToPlaceId().getId());
		if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
			if(purchase.getStatus().getId()==4){
				if(toPlace.getPlaceType().getName().equals("производство") ){
					purchase.getStatusDate().setRejectedToPlace(new Date());
					purchase.setStatus(shippingStatusHome.findById(ShippingStatus.class, 7));
					purchaseHome.merge(purchase);
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является производством!!");
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		

	}

	@Transactional
	public void accountantApprove(Purchase p) {
		Set<RawPurchase> rawPurchases = p.getRawPurchases();
		Purchase purchase = purchaseHome.findById(Purchase.class, p.getId());
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("purchaser")){
			if(purchase.getStatus().getId()==2){
				for(RawPurchase rawPurchase : rawPurchases){
					rawPurchase.setPurchase(purchase);
					rawPurchaseHome.merge(rawPurchase);
				}
				
				purchase.getStatusDate().setApprovedFromPlace(new Date());
				purchase.setTotalPrice(p.getTotalPrice());
				purchase.setStatus(shippingStatusHome.findById(ShippingStatus.class, 4));
				purchaseHome.merge(purchase);
			} else
				throw new BusinessException("Данный запрос НЕ подтвержден администратором !!!");		
		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");		

	}
	@Transactional
	public void adminApprove(int purchaseId) {
		Purchase purchase = purchaseHome.findById(Purchase.class, purchaseId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (purchase.getStatus().getId()==1){
				purchase.getStatusDate().setApprovedAdmin(new Date());
				purchase.setStatus(shippingStatusHome.findById(ShippingStatus.class, 2));
				purchaseHome.merge(purchase);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя подтверждать еще раз!!!");
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");		

	}

	@Transactional
	public void adminReject(int purchaseId) {
		Purchase purchase = purchaseHome.findById(Purchase.class, purchaseId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (purchase.getStatus().getId()==1){
				purchase.getStatusDate().setRejectedAdmin(new Date());
				purchase.setStatus(shippingStatusHome.findById(ShippingStatus.class, 3));
				purchaseHome.merge(purchase);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя отменить!!!");		
			
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}	
	@Transactional
	public void update(Purchase purchase) {

		Set<RawPurchase> rawPurchases = purchase.getRawPurchases();
		Purchase purchase2 = purchaseHome.findById(Purchase.class, purchase.getId());
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
				rawPurchaseHome.deleteByPurchaseId(purchase2.getId());
				for(RawPurchase rawPurchase : rawPurchases){
					rawPurchase.setPurchase(purchase2);
					rawPurchaseHome.persist(rawPurchase);
				}
				purchase.setStatusDate(purchase2.getStatusDate());
				purchase.getStatusDate().setCreated(new Date());
				purchaseHome.merge(purchase);
		} else if(currentUserSubj.hasRole("production_manager")){
			Place toPlace = placeHome.findById(Place.class, purchase2.getToPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
					rawPurchaseHome.deleteByPurchaseId(purchase2.getId());
					for(RawPurchase rawPurchase : rawPurchases){
						rawPurchase.setPurchase(purchase2);
						rawPurchaseHome.persist(rawPurchase);
					}
					purchase.setStatusDate(purchase2.getStatusDate());
					purchase.getStatusDate().setCreated(new Date());
					purchaseHome.merge(purchase);
			}else
				throw new BusinessException("Вы не имеете права для покупки!! Вы не ответственны за "+toPlace.getName()+" !!");		
			
		} else 
			throw new BusinessException("Вы не имеете права для редактирования!!!");		
		
		
	}
	

}