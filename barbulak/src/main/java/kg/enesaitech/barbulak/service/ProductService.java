package kg.enesaitech.barbulak.service;

import java.util.List;

import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.dao.ProductHome;
import kg.enesaitech.barbulak.dao.ProductPlaceHome;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.Product;
import kg.enesaitech.barbulak.entity.ProductPlace;
import kg.enesaitech.barbulak.userException.BusinessException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService {
	
	@Autowired
	private ProductHome productHome;
	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private ProductPlaceHome productPlaceHome;

	@Transactional
	public void create(Product product) {
		Integer productId = productHome.create(product);
		product.setId(productId);
		List<Place> places = placeHome.getSkladPlaces();
		for (Place place: places){
			ProductPlace productPlace = new ProductPlace();
			productPlace.setProduct(product);
			productPlace.setPlace(place);
			productPlace.setAmount(0.0);
			productPlaceHome.persist(productPlace);
		}
	}


	@Transactional
	public void delete(int id) {
		Product product = productHome.findById(Product.class, id);
		boolean isExists=false;
		List<ProductPlace> productPlaces = productPlaceHome.getProductPlacesByProductId(id);
		for (ProductPlace productPlace: productPlaces){
			if(productPlace.getAmount()!=0){
				isExists=true;
			}
		}
		if(!isExists){
			productPlaceHome.deleteByProductId(id);
		} else{
			throw new BusinessException("Невозможно удалить "+product.getName()+" продукт который есть в складе !!");			
		}
		productHome.remove(product);
	}
	
}