package kg.enesaitech.barbulak.service;

import java.util.Date;
import java.util.List;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.PersonalBalanceHome;
import kg.enesaitech.barbulak.dao.PersonalTransactionCategoryHome;
import kg.enesaitech.barbulak.dao.PersonalTransactionHome;
import kg.enesaitech.barbulak.dao.TransactionCategoryHome;
import kg.enesaitech.barbulak.dao.TransactionHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.Cashbox;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.Installment;
import kg.enesaitech.barbulak.entity.Person;
import kg.enesaitech.barbulak.entity.PersonalBalance;
import kg.enesaitech.barbulak.entity.PersonalTransaction;
import kg.enesaitech.barbulak.entity.PersonalTransactionCategory;
import kg.enesaitech.barbulak.entity.Transaction;
import kg.enesaitech.barbulak.entity.TransactionCategory;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.PersonalTransactionEvaluation;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonalTransactionService {

	@Autowired
	protected DozerBeanMapper mapper;
	@Autowired
	private PersonalTransactionHome personalTransactionHome;	
	@Autowired
	private PersonalBalanceHome personalBalanceHome;
	@Autowired
	private PersonalTransactionCategoryHome personalTransactionCategoryHome;
	@Autowired
	private GenericHome<Person> personHome;
	@Autowired
	private GenericHome<Cashbox> caseHome;
	@Autowired
	private TransactionHome transactionHome;
	@Autowired
	private TransactionCategoryHome transactionCategoryHome;
	@Autowired
	protected UserCheckService checkService;
	@Autowired
	protected UsersHome usersHome;
	@Autowired
	protected CashboxAccountantHome cashboxAccountantHome;
	@Autowired
	protected GenericHome<Installment> installmentHome;

	@Transactional
	public void create(PersonalTransaction personalTransaction) {
		PersonalBalance personalBalance = personalBalanceHome.getByPersonId(personalTransaction.getPersonalBalance().getPerson().getId());
		if (personalBalance==null){
			personalBalance = new PersonalBalance();
			if (personalTransaction.getDeposit()){
				personalBalance.setBalance(personalTransaction.getAmount());
			} else {
				personalBalance.setBalance(-personalTransaction.getAmount());				
			}
			personalBalance.setPerson(personalTransaction.getPersonalBalance().getPerson());
			personalBalanceHome.persist(personalBalance);
		} else{
			if (personalTransaction.getDeposit()){
				personalBalance.setBalance(personalBalance.getBalance()+personalTransaction.getAmount());
			} else {
				personalBalance.setBalance(personalBalance.getBalance()-personalTransaction.getAmount());				
			}
			personalBalanceHome.merge(personalBalance);
		}
		PersonalTransactionCategory personalTransactionCategory = personalTransactionCategoryHome.getByName("другие");
		personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
		personalTransaction.setPersonalBalance(personalBalance);
		personalTransactionHome.persist(personalTransaction);
	}
	
	@Transactional
	public void installmentEvaluation(List<PersonalTransactionEvaluation> personalTransactionEvaluations) {
		for (PersonalTransactionEvaluation evaluation : personalTransactionEvaluations){
			if(evaluation.getAmount()==null || evaluation.getAmount()==0){
				// skip the evaluation
			} else{
				PersonalTransaction personalTransaction = new PersonalTransaction();
				personalTransaction.setAmount(evaluation.getAmount());
				personalTransaction.setDescription(evaluation.getDescription());
	
				PersonalBalance personalBalance = personalBalanceHome.getByPersonId(evaluation.getPersonId());
				if (personalBalance==null){
					personalBalance = new PersonalBalance();
					personalBalance.setBalance(-evaluation.getAmount());		
					personalBalance.setPerson(personHome.findById(Person.class, evaluation.getPersonId()));
					personalBalanceHome.persist(personalBalance);
				} else{
					personalBalance.setBalance(personalBalance.getBalance()-personalTransaction.getAmount());				
					personalBalanceHome.merge(personalBalance);
				}
				PersonalTransactionCategory personalTransactionCategory = personalTransactionCategoryHome.getByName("определение рассрочки");
				personalTransaction.setPersonalBalance(personalBalance);
				personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
				personalTransaction.setDeposit(false);
				personalTransaction.setDate(new Date());
				personalTransactionHome.persist(personalTransaction);

				Installment installment = installmentHome.findById(Installment.class, evaluation.getInstallmentId());
				if(installment!=null){
					installment.setLeftAmount(installment.getLeftAmount()-evaluation.getAmount());
				} else{
					throw new BusinessException("No such Installment with id: "+evaluation.getInstallmentId());
				}
			}
		}
	}
	
	@Transactional
	public void salaryEvaluation(List<PersonalTransactionEvaluation> personalTransactionEvaluations) {
		for (PersonalTransactionEvaluation evaluation : personalTransactionEvaluations){
			if(evaluation.getAmount()==null || evaluation.getAmount()==0){
				// skip the evaluation
			} else{
				PersonalTransaction personalTransaction = new PersonalTransaction();
				personalTransaction.setAmount(evaluation.getAmount());
				personalTransaction.setDescription(evaluation.getDescription());
	
				PersonalBalance personalBalance = personalBalanceHome.getByPersonId(evaluation.getPersonId());
				if (personalBalance==null){
					personalBalance = new PersonalBalance();
					personalBalance.setBalance(personalTransaction.getAmount());		
					personalBalance.setPerson(personHome.findById(Person.class, evaluation.getPersonId()));
					personalBalanceHome.persist(personalBalance);
				} else{
					personalBalance.setBalance(personalBalance.getBalance()+personalTransaction.getAmount());				
					personalBalanceHome.merge(personalBalance);
				}
				PersonalTransactionCategory personalTransactionCategory = personalTransactionCategoryHome.getByName("начисление зарплаты");
				personalTransaction.setPersonalBalance(personalBalance);
				personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
				personalTransaction.setDeposit(true);
				personalTransaction.setDate(new Date());
				personalTransactionHome.persist(personalTransaction);
			}
		}
	}
	
	@Transactional
	public void salaryPayment(List<PersonalTransactionEvaluation> personalTransactionEvaluations) {
		for (PersonalTransactionEvaluation evaluation : personalTransactionEvaluations){
			if(evaluation.getAmount()==null || evaluation.getAmount()==0){
				// skip the evaluation
			} else{
				PersonalTransaction personalTransaction = new PersonalTransaction();
				personalTransaction.setAmount(evaluation.getAmount());
				personalTransaction.setDescription(evaluation.getDescription());
	
				PersonalBalance personalBalance = personalBalanceHome.getByPersonId(evaluation.getPersonId());
				Person person = personHome.findById(Person.class, evaluation.getPersonId());
				if (personalBalance==null){
	//				personalBalance = new PersonalBalance();
	//				personalBalance.setBalance(-personalTransaction.getAmount());		
	//				personalBalance.setPerson(personHome.findById(Person.class, evaluation.getPersonId()));
	//				personalBalanceHome.persist(personalBalance);
					throw new BusinessException(person.getName()+" "+person.getSurname()+" - не должен брать "+personalTransaction.getAmount()+" денег");
					
				} else{
					if(personalBalance.getBalance()<personalTransaction.getAmount()){
						throw new BusinessException(person.getName()+" "+person.getSurname()+" - не должен брать "+personalTransaction.getAmount()+" денег");					
					}else{
						personalBalance.setBalance(personalBalance.getBalance()-personalTransaction.getAmount());				
						personalBalanceHome.merge(personalBalance);					
					}
				}
				PersonalTransactionCategory personalTransactionCategory = personalTransactionCategoryHome.getByName("зарплата");
				personalTransaction.setPersonalBalance(personalBalance);
				personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
				personalTransaction.setDeposit(false);
				personalTransaction.setDate(new Date());
				personalTransactionHome.persist(personalTransaction);
				Cashbox kassa = caseHome.findById(Cashbox.class, person.getCashbox().getId());
				TransactionCategory transactionCategory = transactionCategoryHome.getByName("выплата заработной платы");
				if(transactionCategory == null){
					transactionCategory = new TransactionCategory();
					transactionCategory.setIncome(false);
					transactionCategory.setName("выплата заработной платы");
					int transactionCategoryId = transactionCategoryHome.persist(transactionCategory);
					transactionCategory.setId(transactionCategoryId);
				}
				if(kassa.getTotal()<personalTransaction.getAmount()){
					throw new BusinessException("Недостаточно средств в кассе");
				}else{
					kassa.setTotal(kassa.getTotal() - personalTransaction.getAmount());				
				}
				Transaction transaction = new Transaction();
				transaction.setAmount(personalTransaction.getAmount());
				transaction.setCashbox(kassa);
				transaction.setDate(new Date());
				transaction.setTransactionCategory(transactionCategory);
				transaction.setPerson(person);
				caseHome.merge(kassa);
				transactionHome.persist(transaction);
			}
		}
	}
	
	@Transactional
	public void delete(int id) {
		PersonalTransaction personalTransaction = personalTransactionHome.findById(PersonalTransaction.class, id);
		if(checkService.isCurrentUserAdmin()){
			PersonalBalance personalBalance = personalBalanceHome.findById(PersonalBalance.class, personalTransaction.getPersonalBalance().getId());
			if (!personalTransaction.getDeposit()){
				personalBalance.setBalance(personalBalance.getBalance()+personalTransaction.getAmount());
			} else {
				personalBalance.setBalance(personalBalance.getBalance()-personalTransaction.getAmount());				
			}
			personalBalanceHome.merge(personalBalance);
			personalTransactionHome.remove(personalTransaction);
		} else{
			throw new BusinessException("Доступ запрещен ");			
		}
	}

	@Transactional
	public PersonalTransaction get(Integer id) {
		return personalTransactionHome.findById(PersonalTransaction.class, id);
	}

	@Transactional
	public SearchResult<PersonalTransaction> getList(SearchParameters searchParameters) {
		
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return personalTransactionHome.getList(searchParameters, PersonalTransaction.class);			
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			List<CashboxAccountant> cashboxAccountants = cashboxAccountantHome
					.getCashboxAccountants(currUser.getStaff().getId());
			if(cashboxAccountants.size()==0){
				throw new BusinessException("Доступ запрещен для кассы ");				
			}
			String cashboxID = null;
			if(searchParameters.getSearchParameters().get("personalBalance.person.cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("personalBalance.person.cashbox.id");
			}
			SearchResult<PersonalTransaction> searchResult = new SearchResult<PersonalTransaction>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					if(searchParameters.getSearchParameters().get("personalBalance.person.cashbox.id")!=null){
						searchParameters.getSearchParameters().remove("personalBalance.person.cashbox.id");
					}
					searchParameters.getSearchParameters().put("personalBalance.person.cashbox.id", cashboxAccountant.getCashbox().getId()+"");
					SearchResult<PersonalTransaction> buffer = personalTransactionHome.getList(searchParameters, PersonalTransaction.class);
					searchResult.getResultList().addAll(buffer.getResultList());				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						return personalTransactionHome.getList(searchParameters, PersonalTransaction.class);
					}else{						
					}
					
				}
				}
			return searchResult;
		} else{
			throw new BusinessException("Доступ запрещен для кассы ");
		}
	}

}