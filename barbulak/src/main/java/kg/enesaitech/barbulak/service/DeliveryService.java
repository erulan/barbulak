package kg.enesaitech.barbulak.service;

import java.text.DecimalFormat;
import java.util.Date;

import kg.enesaitech.barbulak.dao.BottlePlaceHome;
import kg.enesaitech.barbulak.dao.DeliveryClientHome;
import kg.enesaitech.barbulak.dao.PersonalBalanceHome;
import kg.enesaitech.barbulak.dao.PersonalTransactionCategoryHome;
import kg.enesaitech.barbulak.dao.PersonalTransactionHome;
import kg.enesaitech.barbulak.dao.Place19ManagerHome;
import kg.enesaitech.barbulak.dao.TransactionCategoryHome;
import kg.enesaitech.barbulak.dao.TransactionHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.Agreement;
import kg.enesaitech.barbulak.entity.BottlePlace;
import kg.enesaitech.barbulak.entity.Cashbox;
import kg.enesaitech.barbulak.entity.Delivery;
import kg.enesaitech.barbulak.entity.DeliveryClient;
import kg.enesaitech.barbulak.entity.DeliveryStatus;
import kg.enesaitech.barbulak.entity.PersonalBalance;
import kg.enesaitech.barbulak.entity.PersonalTransaction;
import kg.enesaitech.barbulak.entity.PersonalTransactionCategory;
import kg.enesaitech.barbulak.entity.Place19Manager;
import kg.enesaitech.barbulak.entity.Transaction;
import kg.enesaitech.barbulak.entity.TransactionCategory;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DeliveryService {
	@Autowired
	protected DozerBeanMapper mapper;	
	
	@Autowired
	private GenericHome<Delivery> deliveryHome;	
	@Autowired
	private DeliveryClientHome deliveryClientHome;	
	@Autowired
	private GenericHome<DeliveryStatus> deliveryStatusHome;	
	@Autowired
	private Place19ManagerHome place19ManagerHome;	
	@Autowired
	private UsersHome usersHome;
	@Autowired
	private BottlePlaceHome bottlePlaceHome;
	@Autowired
	private PersonalTransactionHome personalTransactionHome;	
	@Autowired
	private PersonalBalanceHome personalBalanceHome;
	@Autowired
	private PersonalTransactionCategoryHome personalTransactionCategoryHome;
	@Autowired
	private GenericHome<Cashbox> caseHome;
	@Autowired
	private TransactionHome transactionHome;
	@Autowired
	private TransactionCategoryHome transactionCategoryHome;
	@Autowired
	private GenericHome<Agreement> agreementHome;
	private DecimalFormat decimalFormat2 = new DecimalFormat("#");
	 

	@Transactional
	public void create(Delivery delivery) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("manager_for_bottle")){
			if(delivery.getPlannedDate() == null)
				delivery.setPlannedDate(new Date());
			delivery.setDeliveryStatus(deliveryStatusHome.findById(DeliveryStatus.class, 1));
			Integer deliveryId = deliveryHome.persist(delivery);
			delivery.setId(deliveryId);
			for(DeliveryClient deliveryClient : delivery.getDeliveryClients()){
				deliveryClient.setDelivery(delivery);
				deliveryClientHome.persist(deliveryClient);
			}
		} else
			throw new BusinessException("Вы не имеете права для доставки!!!");	
	}


	@Transactional
	public void update(Delivery delivery) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("manager_for_bottle")){
			Delivery delivery2  = deliveryHome.findById(Delivery.class, delivery.getId());
			if(delivery2.getDeliveryStatus().getId() != 1){
				throw new BusinessException("Данный запрос был отправлен или завершен и нельзя редактировать!!!");					
			}
			deliveryClientHome.deleteByDeliveryId(delivery.getId());
			for(DeliveryClient deliveryClient : delivery.getDeliveryClients()){
				deliveryClient.setDelivery(delivery);
				deliveryClient.setId(null);
				deliveryClientHome.persist(deliveryClient);
			}
//			delivery.setPlannedDate(new Date());
			delivery.setDeliveryStatus(deliveryStatusHome.findById(DeliveryStatus.class, 1));
			deliveryHome.merge(delivery);
		} else
			throw new BusinessException("Вы не имеете права для доставки!!!");	
	}


	@Transactional
	public void send(Delivery delivery) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("manager_for_bottle")){
			Delivery delivery2  = deliveryHome.findById(Delivery.class, delivery.getId());
			if(delivery2.getDeliveryStatus().getId() != 1){
				throw new BusinessException("Данный запрос был отправлен или завершен и нельзя отправить!!!");					
			}

			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			Place19Manager place19Manager = place19ManagerHome.getByManagerId(currUser.getStaff().getId());
			if(place19Manager==null){
				throw new BusinessException("Извините, вы не ответственны  для любого склада!!!");	
			}
			int allBottleAmount =0;

			deliveryClientHome.deleteByDeliveryId(delivery.getId());
			for(DeliveryClient deliveryClient : delivery.getDeliveryClients()){
				deliveryClient.setDelivery(delivery);
				deliveryClient.setId(null);
				deliveryClientHome.persist(deliveryClient);
				allBottleAmount+= deliveryClient.getAmount();
			}
			BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(place19Manager.getPlace().getId());
			if (bottlePlace.getBottleAmount()<allBottleAmount){
				throw new BusinessException("для отправки необходимо "+(allBottleAmount-bottlePlace.getBottleAmount())+" шт 19л бутыль!!!");					
			}else{
				bottlePlace.setBottleAmount(bottlePlace.getBottleAmount()-allBottleAmount);
				bottlePlaceHome.merge(bottlePlace);
			}
			delivery2.setSentDate(new Date());
			delivery2.setDeliveryStatus(deliveryStatusHome.findById(DeliveryStatus.class, 2));
			deliveryHome.merge(delivery2);
		} else
			throw new BusinessException("Вы не имеете права для доставки!!!");	
	}
	
	@Transactional
	public void complete(Delivery delivery) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("manager_for_bottle")){
			Delivery delivery2  = deliveryHome.findById(Delivery.class, delivery.getId());
			if(delivery2.getDeliveryStatus().getId() != 2){
				throw new BusinessException("Данный запрос НЕ отправлен или завершен и нельзя завершить!!!");					
			}

			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			Place19Manager place19Manager = place19ManagerHome.getByManagerId(currUser.getStaff().getId());
			if(place19Manager==null){
				throw new BusinessException("Извините, вы не ответственны  для любого склада!!!");	
			}
			int returnedBottleAmount =0;
			int emptyBottleAmount =0;
			PersonalTransactionCategory personalTransactionCategory = personalTransactionCategoryHome.getByName("19л бутыль");

			TransactionCategory transactionCategoryZalog = transactionCategoryHome.getByName("Залог на 19л бутылей");
			if(transactionCategoryZalog == null){
				transactionCategoryZalog = new TransactionCategory();
				transactionCategoryZalog.setIncome(true);
				transactionCategoryZalog.setName("Залог на 19л бутылей");
				int transactionCategoryId = transactionCategoryHome.persist(transactionCategoryZalog);
				transactionCategoryZalog.setId(transactionCategoryId);
			}
			TransactionCategory transactionCategoryProdaja = transactionCategoryHome.getByName("Продажа 19л бутылей");
			if(transactionCategoryProdaja == null){
				transactionCategoryProdaja = new TransactionCategory();
				transactionCategoryProdaja.setIncome(true);
				transactionCategoryProdaja.setName("Продажа 19л бутылей");
				int transactionCategoryId = transactionCategoryHome.persist(transactionCategoryProdaja);
				transactionCategoryProdaja.setId(transactionCategoryId);
			}
			for(DeliveryClient deliveryClient : delivery.getDeliveryClients()){				
				returnedBottleAmount+= deliveryClient.getReturned();
				emptyBottleAmount+= deliveryClient.getReturnedEmpty();
				Agreement agreement = agreementHome.findById(Agreement.class, deliveryClient.getAgreement().getId());
				Double balance = 0.0;
				
				PersonalBalance personalBalance = personalBalanceHome.getByPersonId(agreement.getClient().getId());
				Cashbox kassa = caseHome.findById(Cashbox.class, agreement.getClient().getCashbox().getId());
				if (personalBalance==null){
					personalBalance = new PersonalBalance();
					personalBalance.setBalance(0.0);
					personalBalance.setPerson(agreement.getClient());
					Integer pbId = personalBalanceHome.persist(personalBalance);
					personalBalance.setId(pbId);
				}
				if(deliveryClient.getPledgeNumberForBottle()==null){
					deliveryClient.setPledgeNumberForBottle(0.0);
				}
				if(deliveryClient.getReturned()==null){
					deliveryClient.setReturned(0);
				}
				if(deliveryClient.getReturnedEmpty()==null){
					deliveryClient.setReturnedEmpty(0);
				}
				if(deliveryClient.getPaid()==null){
					deliveryClient.setPaid(0.0);
				}
				double remainingPaid = deliveryClient.getPaid();			
				int takenBottleAmount = deliveryClient.getAmount() - deliveryClient.getReturned();
				if(takenBottleAmount!=0){
					balance = balance - takenBottleAmount* agreement.getUnitPriceForBottle();
					PersonalTransaction personalTransaction = new PersonalTransaction();
					personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
					personalTransaction.setPersonalBalance(personalBalance);
					personalTransaction.setDeposit(false);
					personalTransaction.setDate(new Date());
					personalTransaction.setAmount(takenBottleAmount* agreement.getUnitPriceForBottle());
					personalTransaction.setDescription("сумма на "+takenBottleAmount+" бутыль");
					personalTransactionHome.create19Manager(personalTransaction);
				}
				

				if(deliveryClient.getPledgeNumberForBottle()!=null && deliveryClient.getPledgeNumberForBottle()!=0)
				{	
					double totalPledgeSom = deliveryClient.getPledgeNumberForBottle()* agreement.getPledgeFee();
					balance = balance - totalPledgeSom;
					PersonalTransaction personalTransactionZalog = new PersonalTransaction();
					personalTransactionZalog.setPersonalTransactionCategory(personalTransactionCategory);
					personalTransactionZalog.setPersonalBalance(personalBalance);
					personalTransactionZalog.setDeposit(false);
					personalTransactionZalog.setDate(new Date());
					personalTransactionZalog.setAmount(totalPledgeSom);
					personalTransactionZalog.setDescription("залог на "+deliveryClient.getPledgeNumberForBottle()+" тар");
					personalTransactionHome.create19Manager(personalTransactionZalog);
			
					if(deliveryClient.getPaid()< totalPledgeSom){
						throw new BusinessException("Для завершения "+agreement.getClient().getName()+" обязательно должен платить залог на "+deliveryClient.getPledgeNumberForBottle()+" тар");
					} else{
						balance = balance + (totalPledgeSom);
						remainingPaid = deliveryClient.getPaid()-totalPledgeSom;
						PersonalTransaction personalTransaction = new PersonalTransaction();
						personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
						personalTransaction.setPersonalBalance(personalBalance);
						personalTransaction.setDeposit(true);
						personalTransaction.setDate(new Date());
						personalTransaction.setAmount(totalPledgeSom);
						personalTransaction.setDescription("оплата, залог на "+deliveryClient.getPledgeNumberForBottle()+" тар");
						personalTransactionHome.create19Manager(personalTransaction);
						
						kassa.setTotal(kassa.getTotal() + totalPledgeSom);
						Transaction transaction = new Transaction();
						transaction.setAmount(totalPledgeSom);
						transaction.setCashbox(kassa);
						transaction.setDate(new Date());
						transaction.setTransactionCategory(transactionCategoryZalog);
						transaction.setPerson(agreement.getClient());
						transactionHome.createBy19Manager(transaction);
					}
				}
				
				if(remainingPaid!=0){
					balance = balance + remainingPaid;
					PersonalTransaction personalTransaction = new PersonalTransaction();
					personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
					personalTransaction.setPersonalBalance(personalBalance);
					personalTransaction.setDeposit(true);
					personalTransaction.setDate(new Date());
					personalTransaction.setAmount(remainingPaid);
					personalTransactionHome.create19Manager(personalTransaction);
					
					kassa.setTotal(kassa.getTotal() + remainingPaid);
					Transaction transaction = new Transaction();
					transaction.setAmount(remainingPaid);
					transaction.setCashbox(kassa);
					transaction.setDate(new Date());
					transaction.setTransactionCategory(transactionCategoryProdaja);
					transaction.setPerson(agreement.getClient());
					transactionHome.createBy19Manager(transaction);
				}
				personalBalance.setBalance(personalBalance.getBalance()+balance);
				personalBalanceHome.merge(personalBalance);
				caseHome.merge(kassa);
				agreement.setConfidenceBottleAmount(agreement.getConfidenceBottleAmount()+(deliveryClient.getAmount()-deliveryClient.getReturned()-deliveryClient.getReturnedEmpty()-Integer.parseInt(decimalFormat2.format(deliveryClient.getPledgeNumberForBottle()))));
				agreementHome.merge(agreement);
				deliveryClient.setDelivery(delivery);
				deliveryClientHome.merge(deliveryClient);
			}
			
			BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(place19Manager.getPlace().getId());
			bottlePlace.setBottleAmount(bottlePlace.getBottleAmount()+returnedBottleAmount);
			bottlePlace.setEmptyBottleAmount(bottlePlace.getEmptyBottleAmount()+emptyBottleAmount);
			bottlePlaceHome.merge(bottlePlace);
			
			delivery.setCompletedDate(new Date());
			delivery.setDeliveryStatus(deliveryStatusHome.findById(DeliveryStatus.class, 3));
			deliveryHome.merge(delivery);
		} else
			throw new BusinessException("Вы не имеете права для доставки!!!");	
	}
}