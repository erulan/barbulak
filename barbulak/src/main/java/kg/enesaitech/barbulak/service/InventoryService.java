package kg.enesaitech.barbulak.service;

import kg.enesaitech.barbulak.dao.InventoryHome;
import kg.enesaitech.barbulak.entity.Inventory;
import kg.enesaitech.barbulak.userException.BusinessException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InventoryService {
	
	@Autowired
	private InventoryHome inventoryHome;

	@Transactional
	public void create(Inventory inventory) {		
		Inventory e2 = inventoryHome.getByInventoryNO(inventory.getInventoryNo());
		if (e2!=null){
			throw new BusinessException("Такой инвентарный номер существует !! Введите другое.");	
		}
		inventoryHome.persist(inventory);
	}
	@Transactional
	public void update(Inventory inventory) {		
		Inventory e2 = inventoryHome.getByInventoryNO(inventory.getInventoryNo());
		if (e2!=null && !e2.getId().equals(inventory.getId())){
			throw new BusinessException("Такой инвентарный номер существует !! Введите другое.");	
		}
		inventoryHome.merge(inventory);
	}
	
	
	
}