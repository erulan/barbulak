package kg.enesaitech.barbulak.service;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.Cashbox;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.Person;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserCheckService {

	@Autowired
	protected UsersHome usersHome;
	@Autowired
	protected CashboxAccountantHome cashboxAccountantHome;
	@Autowired
	protected GenericHome<Person> personHome;
	@Autowired
	protected GenericHome<Cashbox> cashboxHome;

	@Transactional
	public Boolean isCurrentUserResposiblrForCashbox(int cashboxId) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
				.toString());
		CashboxAccountant cashboxAccountant = cashboxAccountantHome
				.getByCashBoxId(cashboxId);
		if (cashboxAccountant!=null && cashboxAccountant.getStaff().getId().equals(currUser.getStaff().getId())) {
			return true;
		}
		else{
			return false;
		}
	}
	@Transactional
	public Boolean isPersonInCorrectCashbox(int personId, int cashboxId) {
		Person person = personHome.findById(Person.class, personId);
		Cashbox cashbox = cashboxHome.findById(Cashbox.class, cashboxId);
		if(person.getCashbox().getId().equals(cashbox.getId())){
			return true;
		}
		else{
			return false;
		}
	}
	@Transactional
	public Boolean isStaffIsCurrentUser(Integer staffId) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
				.toString());
		if (staffId.equals(currUser.getStaff().getId())) {
			return true;
		}
		else{
			return false;
		}
	}
	@Transactional
	public Boolean isCurrentUserAdminOrAccountant(int cashboxId) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			return true;
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			CashboxAccountant cashboxAccountant = cashboxAccountantHome
					.getByCashBoxId(cashboxId);
			if (cashboxAccountant!=null && cashboxAccountant.getStaff().getId().equals(currUser.getStaff().getId())) {
				return true;
			}
			else{
				return false;
			}
		} else{
			return false;
		}
		
	}
	
	@Transactional
	public Boolean isCurrentUserAdminManagerAccountant(int cashboxId) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return true;
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			CashboxAccountant cashboxAccountant = cashboxAccountantHome
					.getByCashBoxId(cashboxId);
			if (cashboxAccountant!=null && cashboxAccountant.getStaff().getId().equals(currUser.getStaff().getId())) {
				return true;
			}
			else{
				return false;
			}
		} else{
			return false;
		}
		
	}
	
	@Transactional
	public Boolean isCurrentUserAdminOrManager() {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return true;
		} else{
			return false;
		}
		
	}

	@Transactional
	public Boolean isCurrentUserAdmin() {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			return true;
		} else{
			return false;
		}
		
	}

}
