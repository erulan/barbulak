package kg.enesaitech.barbulak.service;

import java.util.List;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.Person;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService {
	
	@Autowired
	private GenericHome<Person> personHome;
	@Autowired
	private UsersHome usersHome;
	@Autowired
	private CashboxAccountantHome cashboxAccountantHome;


	@Transactional
	public SearchResult<Person> getListByCashBox(SearchParameters searchParameters) {
		
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return personHome.getList(searchParameters, Person.class);			
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			List<CashboxAccountant> cashboxAccountants = cashboxAccountantHome
					.getCashboxAccountants(currUser.getStaff().getId());
			if(cashboxAccountants.size()==0){
				throw new BusinessException("Доступ запрещен для кассы ");				
			}
			String cashboxID = null;
			if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("cashbox.id");
			}
			SearchResult<Person> searchResult = new SearchResult<Person>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
						searchParameters.getSearchParameters().remove("cashbox.id");
					}
					searchParameters.getSearchParameters().put("cashbox.id", cashboxAccountant.getCashbox().getId()+"");
					SearchResult<Person> buffer = personHome.getList(searchParameters, Person.class);
					searchResult.getResultList().addAll(buffer.getResultList());				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						return personHome.getList(searchParameters, Person.class);
					}else{						
					}
					
				}
				}
			return searchResult;
		} else{
			throw new BusinessException("Доступ запрещен для кассы ");
		}
	}	
	
}