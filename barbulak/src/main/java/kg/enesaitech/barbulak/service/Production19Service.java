package kg.enesaitech.barbulak.service;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import kg.enesaitech.barbulak.dao.BottlePlaceHome;
import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.dao.RawPlaceHome;
import kg.enesaitech.barbulak.entity.BottlePlace;
import kg.enesaitech.barbulak.entity.BottleRaw;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.Production19;
import kg.enesaitech.barbulak.entity.RawPlace;
import kg.enesaitech.barbulak.entity.ShippingStatus;
import kg.enesaitech.barbulak.entity.StatusDate;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Production19Service {
	
	@Autowired
	private GenericHome<Production19> production19Home;	
	@Autowired
	private BottlePlaceHome bottlePlaceHome;
	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private GenericHome<ShippingStatus> shippingStatusHome;
	@Autowired
	private UserCheckService checkService;
	@Autowired
	private RawPlaceHome rawPlaceHome;
	@Autowired
	private GenericHome<BottleRaw> ingredient19Home;
	@Autowired
	private GenericHome<StatusDate> statusDateHome;
	private DecimalFormat decimalFormat = new DecimalFormat("#.######");
	private DecimalFormat decimalFormat2 = new DecimalFormat("#");

	@Transactional
	public void create(Production19 production19) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("production_manager")){
//			Place toPlace = placeHome.findById(Place.class, productionProcess.getToPlaceId().getId());
			Place proizvodstvo = placeHome.getProizvodstvo();
			if(checkService.isStaffIsCurrentUser(proizvodstvo.getStaff().getId())){
				checkIngredients(production19);
				StatusDate statusDate = new StatusDate();
				statusDate.setCreated(new Date());
				Integer statusDateId = statusDateHome.persist(statusDate);
				statusDate.setId(statusDateId);
				production19.setStatusDate(statusDate);
				production19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 1));
				production19Home.persist(production19);
			} else
				throw new BusinessException("Вы не имеете права для покупки!! Вы не ответственны за "+proizvodstvo.getName()+" !!");		
			
		} else
			throw new BusinessException("Вы не имеете права для покупки!!!");		

		
		
	}
	
	@Transactional
	public void toPlaceApprove(int production19Id) {
		Production19 production19 = production19Home.findById(Production19.class, production19Id);
		Place toPlace = placeHome.findById(Place.class, production19.getToPlaceId().getId());
		if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
			if(production19.getStatus().getId()==4){
				if(toPlace.getPlaceType().getName().equals("склад") ){
						
					BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(toPlace.getId());
					bottlePlace.setBottleAmount(bottlePlace.getBottleAmount()+Integer.parseInt(decimalFormat2.format(production19.getProductionAmount())));
					bottlePlaceHome.merge(bottlePlace);

					production19.getStatusDate().setApprovedToPlace(new Date());
					production19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 6));
					production19Home.merge(production19);
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является складом!!");
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		

	}
	

	@Transactional
	public void toPlaceReject(int productionProcessId) {
		Production19 production19 = production19Home.findById(Production19.class, productionProcessId);
		Place toPlace = placeHome.findById(Place.class, production19.getToPlaceId().getId());
		if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
			if(production19.getStatus().getId()==4){
				if(toPlace.getPlaceType().getName().equals("склад") ){					
					production19.getStatusDate().setRejectedToPlace(new Date());
					production19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 7));
					production19Home.merge(production19);
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является складом!!");
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		

	}

	@Transactional
	public void productionApprove(Production19 p) {
		Production19 production19 = production19Home.findById(Production19.class, p.getId());
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("production_manager")){
			if(production19.getStatus().getId()==2){
				Place proizvodstvo = placeHome.getProizvodstvo();
				checkIngredientsByProduction(p);
				SearchParameters searchParameters = new SearchParameters();
				SearchResult<BottleRaw> ingredients = ingredient19Home.getList(searchParameters, BottleRaw.class);
				for(BottleRaw ingredient : ingredients.getResultList()){
					RawPlace rawPlace = rawPlaceHome.getByRawIdByPlaceId(ingredient.getRaw().getId(), proizvodstvo.getId());
					if(rawPlace.getAmount()<ingredient.getAmount()*p.getProductionAmount()){
						throw new BusinessException("Невозможно производить 19 бутыль продукт, сырье "+rawPlace.getRaw().getName()+" нет в "+proizvodstvo.getName()+" !!");						
					} else{
						rawPlace.setAmount(rawPlace.getAmount()-ingredient.getAmount()*p.getProductionAmount());
						rawPlaceHome.merge(rawPlace);
					}
				}
				BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(proizvodstvo.getId());
				if(bottlePlace.getEmptyBottleAmount()<p.getProductionAmount()){
					throw new BusinessException(" тара '"+decimalFormat2.format((p.getPlannedAmount()-bottlePlace.getEmptyBottleAmount()))+"' шт");
				} else{
					bottlePlace.setEmptyBottleAmount(bottlePlace.getEmptyBottleAmount()-Integer.parseInt(decimalFormat2.format(p.getProductionAmount())));
					bottlePlaceHome.merge(bottlePlace);
				}
				
				production19.setProductionAmount(p.getProductionAmount());
				production19.getStatusDate().setApprovedFromPlace(new Date());
				production19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 4));
				production19Home.merge(production19);
			} else
				throw new BusinessException("Данный запрос НЕ подтвержден администратором !!!");		
		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");		
	}
	
	@Transactional
	public void adminApprove(int productionProcessId) {
		Production19 production19 = production19Home.findById(Production19.class, productionProcessId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (production19.getStatus().getId()==1){
				production19.getStatusDate().setApprovedAdmin(new Date());
				production19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 2));
				production19Home.merge(production19);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя подтверждать еще раз!!!");
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");		

	}

	@Transactional
	public void adminReject(int productionProcessId) {
		Production19 production19 = production19Home.findById(Production19.class, productionProcessId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (production19.getStatus().getId()==1){
				production19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 3));
				production19.getStatusDate().setRejectedAdmin(new Date());
				production19Home.merge(production19);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя отменить!!!");		
			
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}	
	@Transactional
	public void update(Production19 production19) {

		Production19 productionProcess2 = production19Home.findById(Production19.class, production19.getId());
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("production_manager")){
			checkIngredients(production19);
			Place proizvodstvo = placeHome.getProizvodstvo();
			SearchParameters searchParameters = new SearchParameters();
			SearchResult<BottleRaw> ingredients = ingredient19Home.getList(searchParameters, BottleRaw.class);
			for(BottleRaw ingredient : ingredients.getResultList()){
				RawPlace rawPlace = rawPlaceHome.getByRawIdByPlaceId(ingredient.getRaw().getId(), proizvodstvo.getId());
				if(rawPlace.getAmount()<ingredient.getAmount()*production19.getPlannedAmount()){
					throw new BusinessException("Невозможно планировать 19 бутыль продукт, сырье  "+rawPlace.getRaw().getName()+" нет в "+proizvodstvo.getName()+" !!");						
				}
			}
			production19.setStatusDate(productionProcess2.getStatusDate());
			production19.getStatusDate().setCreated(new Date());
			production19Home.merge(production19);
		} else 
			throw new BusinessException("Вы не имеете права для редактирования!!!");		
		
		
	}

	@Transactional
	public void checkIngredients(Production19 production19) {
		Place proizvodstvo = placeHome.getProizvodstvo();
		Map<Integer, Double> rawIdAmounts = new HashMap<Integer, Double>();
		SearchParameters searchParameters = new SearchParameters();
		SearchResult<BottleRaw> ingredients = ingredient19Home.getList(searchParameters, BottleRaw.class);
		for(BottleRaw ingredient : ingredients.getResultList()){
			if(rawIdAmounts.get(ingredient.getRaw().getId())==null){
				rawIdAmounts.put(ingredient.getRaw().getId(), ingredient.getAmount()*production19.getPlannedAmount());
			}else{
				Double amount = rawIdAmounts.get(ingredient.getRaw().getId());
				rawIdAmounts.remove(ingredient.getRaw().getId());
				rawIdAmounts.put(ingredient.getRaw().getId(), ingredient.getAmount()*production19.getPlannedAmount()+amount);
			}
		}
		boolean isFalse = false;
		String message= "Для планирования необходимо: ";
		for (Entry<Integer, Double> rawAmount : rawIdAmounts.entrySet()){
			RawPlace rawPlace = rawPlaceHome.getByRawIdByPlaceId(rawAmount.getKey(), proizvodstvo.getId());
			if(rawPlace.getAmount()<rawAmount.getValue()){
				isFalse = true;
				if(rawPlace.getRaw().getUnit().getId()==3){
					message = message +rawPlace.getRaw().getName()+" - '"+decimalFormat2.format((rawAmount.getValue()-rawPlace.getAmount()))+"' "+rawPlace.getRaw().getUnit().getName()+", ";
				} else {
					message = message +rawPlace.getRaw().getName()+" - '"+decimalFormat.format((rawAmount.getValue()-rawPlace.getAmount()))+"' "+rawPlace.getRaw().getUnit().getName()+", ";
				}
				
			}	
		}

		BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(proizvodstvo.getId());
		if(bottlePlace.getEmptyBottleAmount()<production19.getPlannedAmount()){
			isFalse=true;
			message = message +" тара '"+decimalFormat2.format((production19.getPlannedAmount()-bottlePlace.getEmptyBottleAmount()))+"' шт, ";
		}
		if(isFalse){
			message = message.substring(0, message.length()-2) +".";
			throw new BusinessException(message);						
			
		}
	}	

	@Transactional
	public void checkIngredientsByProduction(Production19 production19) {
		Place proizvodstvo = placeHome.getProizvodstvo();
		Map<Integer, Double> rawIdAmounts = new HashMap<Integer, Double>();
		SearchParameters searchParameters = new SearchParameters();
		SearchResult<BottleRaw> ingredients = ingredient19Home.getList(searchParameters, BottleRaw.class);
		for(BottleRaw ingredient : ingredients.getResultList()){
			if(rawIdAmounts.get(ingredient.getRaw().getId())==null){
				rawIdAmounts.put(ingredient.getRaw().getId(), ingredient.getAmount()*production19.getProductionAmount());
			}else{
				Double amount = rawIdAmounts.get(ingredient.getRaw().getId());
				rawIdAmounts.remove(ingredient.getRaw().getId());
				rawIdAmounts.put(ingredient.getRaw().getId(), ingredient.getAmount()*production19.getProductionAmount()+amount);
			}
		}
		boolean isFalse = false;
		String message= "Для производство необходимо: ";
		for (Entry<Integer, Double> rawAmount : rawIdAmounts.entrySet()){
			RawPlace rawPlace = rawPlaceHome.getByRawIdByPlaceId(rawAmount.getKey(), proizvodstvo.getId());
			if(rawPlace.getAmount()<rawAmount.getValue()){
				isFalse = true;
				if(rawPlace.getRaw().getUnit().getId()==3){
					message = message +rawPlace.getRaw().getName()+" - '"+decimalFormat2.format((rawAmount.getValue()-rawPlace.getAmount()))+"' "+rawPlace.getRaw().getUnit().getName()+", ";
				} else {
					message = message +rawPlace.getRaw().getName()+" - '"+decimalFormat.format((rawAmount.getValue()-rawPlace.getAmount()))+"' "+rawPlace.getRaw().getUnit().getName()+", ";
				}
				
			}	
		}

		BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(proizvodstvo.getId());
		if(bottlePlace.getEmptyBottleAmount()<production19.getPlannedAmount()){
			isFalse=true;
			message = message +" тара '"+decimalFormat2.format((production19.getProductionAmount()-bottlePlace.getEmptyBottleAmount()))+"' шт, ";
		}
		if(isFalse){
			message = message.substring(0, message.length()-2) +".";
			throw new BusinessException(message);						
			
		}
	}	
	

}