package kg.enesaitech.barbulak.service;

import java.text.DecimalFormat;
import java.util.Date;

import kg.enesaitech.barbulak.dao.BottlePlaceHome;
import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.entity.BottlePlace;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.Purchase19;
import kg.enesaitech.barbulak.entity.ShippingStatus;
import kg.enesaitech.barbulak.entity.StatusDate;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Purchase19Service {
	
	@Autowired
	private GenericHome<Purchase19> purchase19Home;
	@Autowired
	private BottlePlaceHome bottlePlaceHome;
	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private GenericHome<ShippingStatus> shippingStatusHome;
	@Autowired
	private UserCheckService checkService;
	@Autowired
	private GenericHome<StatusDate> statusDateHome;
	private DecimalFormat decimalFormat2 = new DecimalFormat("#");

	@Transactional
	public void create(Purchase19 purchase19) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("purchaser")){
			Place toPlace = placeHome.findById(Place.class, purchase19.getToPlaceId().getId());
			if(toPlace.getPlaceType().getName().equals("склад") ){		
				StatusDate statusDate = new StatusDate();
				statusDate.setCreated(new Date());
				Integer statusDateId = statusDateHome.persist(statusDate);
				statusDate.setId(statusDateId);
				purchase19.setStatusDate(statusDate);
				purchase19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 1));
				purchase19Home.persist(purchase19);
			} else
				throw new BusinessException("Вы не имеете права для покупки!! "+toPlace.getName()+" не является складом!!");
		} else
			throw new BusinessException("Вы не имеете права для покупки!!!");		

		
		
	}
	
	@Transactional
	public void toPlaceApprove(int purchase19Id) {
		Purchase19 purchase19 = purchase19Home.findById(Purchase19.class, purchase19Id);
		Place toPlace = placeHome.findById(Place.class, purchase19.getToPlaceId().getId());
		if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
			if(purchase19.getStatus().getId()==2){
				if(toPlace.getPlaceType().getName().equals("склад") ){
				
					BottlePlace toBottlePlace = bottlePlaceHome.getByPlaceId(toPlace.getId());
					toBottlePlace.setEmptyBottleAmount(toBottlePlace.getEmptyBottleAmount()+Integer.parseInt(decimalFormat2.format(purchase19.getAmount())));
					bottlePlaceHome.merge(toBottlePlace);
					purchase19.getStatusDate().setApprovedToPlace(new Date());
					purchase19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 6));
					purchase19Home.merge(purchase19);
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является складом!!");
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		

	}
	
	@Transactional
	public void toPlaceReject(int purchase19Id) {
		Purchase19 purchase19 = purchase19Home.findById(Purchase19.class, purchase19Id);
		Place toPlace = placeHome.findById(Place.class, purchase19.getToPlaceId().getId());
		if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
			if(purchase19.getStatus().getId()==2){
				if(toPlace.getPlaceType().getName().equals("склад") ){
					purchase19.getStatusDate().setRejectedToPlace(new Date());
					purchase19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 7));
					purchase19Home.merge(purchase19);
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является складом!!");
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		

	}
//
//	@Transactional
//	public void accountantApprove(Purchase19 p) {
//		Purchase19 purchase19 = purchase19Home.findById(Purchase19.class, p.getId());
//		Subject currentUserSubj = SecurityUtils.getSubject();
//		if(currentUserSubj.hasRole("purchaser")){
//			if(purchase19.getStatus().getId()==2){
//				purchase19.setUnitPrice(p.getUnitPrice());
//				purchase19.setDescription(p.getDescription());
//				purchase19.getStatusDate().setApprovedFromPlace(new Date());
//				purchase19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 4));
//				purchase19Home.merge(purchase19);
//			} else
//				throw new BusinessException("Данный запрос НЕ подтвержден администратором !!!");		
//		
//		} else
//			throw new BusinessException("Вы не имеете права для подтверждения!!!");		
//
//	}
	@Transactional
	public void adminApprove(int purchase19Id) {
		Purchase19 purchase19 = purchase19Home.findById(Purchase19.class, purchase19Id);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (purchase19.getStatus().getId()==1){
				purchase19.getStatusDate().setApprovedAdmin(new Date());
				purchase19.setStatus(shippingStatusHome.findById(ShippingStatus.class, 2));
				purchase19Home.merge(purchase19);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя подтверждать еще раз!!!");
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");		

	}

	@Transactional
	public void adminReject(int purchase19Id) {
		Purchase19 purchase = purchase19Home.findById(Purchase19.class, purchase19Id);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (purchase.getStatus().getId()==1){
				purchase.getStatusDate().setRejectedAdmin(new Date());
				purchase.setStatus(shippingStatusHome.findById(ShippingStatus.class, 3));
				purchase19Home.merge(purchase);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя отменить!!!");		
			
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}	
	@Transactional
	public void update(Purchase19 purchase19) {

		Purchase19 purchase192 = purchase19Home.findById(Purchase19.class, purchase19.getId());
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("purchaser")){
			Place toPlace = placeHome.findById(Place.class, purchase192.getToPlaceId().getId());
			if(toPlace.getPlaceType().getName().equals("склад") ){
				purchase19.setAmount(purchase19.getAmount());
				purchase19.setStatusDate(purchase192.getStatusDate());
				purchase19.getStatusDate().setCreated(new Date());
				purchase19Home.merge(purchase19);
			}else
				throw new BusinessException("Вы не имеете права для покупки!! "+toPlace.getName()+" не является складом!!");	
			
		} else 
			throw new BusinessException("Вы не имеете права для редактирования!!!");		
		
		
	}
	

}