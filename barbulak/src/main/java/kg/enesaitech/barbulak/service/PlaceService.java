package kg.enesaitech.barbulak.service;

import java.util.List;

import kg.enesaitech.barbulak.dao.BottlePlaceHome;
import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.dao.ProductHome;
import kg.enesaitech.barbulak.dao.ProductPlaceHome;
import kg.enesaitech.barbulak.dao.RawHome;
import kg.enesaitech.barbulak.dao.RawPlaceHome;
import kg.enesaitech.barbulak.entity.BottlePlace;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.PlaceType;
import kg.enesaitech.barbulak.entity.Product;
import kg.enesaitech.barbulak.entity.ProductPlace;
import kg.enesaitech.barbulak.entity.Raw;
import kg.enesaitech.barbulak.entity.RawPlace;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlaceService {

	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private ProductHome productHome;
	@Autowired
	private RawHome rawHome;
	@Autowired
	private ProductPlaceHome productPlaceHome;
	@Autowired
	private RawPlaceHome rawPlaceHome;
	@Autowired
	private BottlePlaceHome bottlePlaceHome;
	
	@Autowired
	private GenericHome<PlaceType> placeTypeHome;	

	@Transactional
	public void create(Place place) {
		Integer placeId = placeHome.create(place);
		if (place.getPlaceType().getId() == 2) {
			place.setId(placeId);
			List<Product> products = productHome.getProducts();
			for (Product product : products) {
				ProductPlace productPlace = new ProductPlace();
				productPlace.setProduct(product);
				productPlace.setPlace(place);
				productPlace.setAmount(0.0);
				productPlaceHome.persist(productPlace);
			}
			BottlePlace bottlePlace = new BottlePlace();
			bottlePlace.setBottleAmount(0);
			bottlePlace.setEmptyBottleAmount(0);
			bottlePlace.setPlace(place);
			bottlePlaceHome.persist(bottlePlace);
		} else if (place.getPlaceType().getId() == 3){
			List<Raw> raws = rawHome.getRaws();
			for (Raw raw : raws) {
				RawPlace rawPlace = new RawPlace();
				rawPlace.setRaw(raw);
				rawPlace.setPlace(place);
				rawPlace.setAmount(0.0);
				rawPlaceHome.persist(rawPlace);
			}

			BottlePlace bottlePlace = new BottlePlace();
			bottlePlace.setBottleAmount(0);
			bottlePlace.setEmptyBottleAmount(0);
			bottlePlace.setPlace(place);
			bottlePlaceHome.persist(bottlePlace);
		}
	}
	@Transactional
	public void update(Place place) {
		
		Place oldPlace = placeHome.findById(Place.class, place.getId());
		PlaceType placeType = placeTypeHome.findById(PlaceType.class, place.getPlaceType().getId());
		if (oldPlace.getPlaceType().getName().equals("офис") && placeType.getName().equals("склад")){
			List<Product> products = productHome.getProducts();
			for (Product product : products) {
				ProductPlace productPlace = new ProductPlace();
				productPlace.setProduct(product);
				productPlace.setPlace(place);
				productPlace.setAmount(0.0);
				productPlaceHome.persist(productPlace);
			}

			BottlePlace bottlePlace = new BottlePlace();
			bottlePlace.setBottleAmount(0);
			bottlePlace.setEmptyBottleAmount(0);
			bottlePlace.setPlace(place);
			bottlePlaceHome.persist(bottlePlace);
		} else if (oldPlace.getPlaceType().getName().equals("офис") && placeType.getName().equals("производство")){
			List<Raw> raws = rawHome.getRaws();
			for (Raw raw : raws) {
				RawPlace rawPlace = new RawPlace();
				rawPlace.setRaw(raw);
				rawPlace.setPlace(place);
				rawPlace.setAmount(0.0);
				rawPlaceHome.persist(rawPlace);
			}

			BottlePlace bottlePlace = new BottlePlace();
			bottlePlace.setBottleAmount(0);
			bottlePlace.setEmptyBottleAmount(0);
			bottlePlace.setPlace(place);
			bottlePlaceHome.persist(bottlePlace);
		} else if (oldPlace.getPlaceType().getName().equals("производство") && placeType.getName().equals("склад")){
			List<Product> products = productHome.getProducts();
			for (Product product : products) {
				ProductPlace productPlace = new ProductPlace();
				productPlace.setProduct(product);
				productPlace.setPlace(place);
				productPlace.setAmount(0.0);
				productPlaceHome.persist(productPlace);
			}

			boolean isExists=false;

			List<RawPlace> rawPlaces = rawPlaceHome.getRawPlacesByPlaceId(place.getId());
			for (RawPlace rawPlace: rawPlaces){
				if(rawPlace.getAmount()>0){
					isExists=true;
				}
			}
			if(!isExists){
				rawPlaceHome.deleteByPlaceId(place.getId());
			}
			else{
				throw new BusinessException("Невозможно изменить склад в котором есть продукты или сырье !!");
			}
		} else if (oldPlace.getPlaceType().getName().equals("производство") && placeType.getName().equals("офис")){
			boolean isExists=false;

			List<RawPlace> rawPlaces = rawPlaceHome.getRawPlacesByPlaceId(place.getId());
			for (RawPlace rawPlace: rawPlaces){
				if(rawPlace.getAmount()>0){
					isExists=true;
				}
			}
			if(!isExists){
				rawPlaceHome.deleteByPlaceId(place.getId());
			}
			else{
				throw new BusinessException("Невозможно изменить склад в котором есть продукты или сырье !!");
			}
		} else if (oldPlace.getPlaceType().getName().equals("склад") && placeType.getName().equals("офис")){
			boolean isExists=false;

			List<ProductPlace> productPlaces = productPlaceHome.getProductPlacesByPlaceId(place.getId());
			for (ProductPlace productPlace: productPlaces){
				if(productPlace.getAmount()>0){
					isExists=true;
				}
			}
			if(!isExists){
				productPlaceHome.deleteByPlaceId(place.getId());
			}
			else{
				throw new BusinessException("Невозможно изменить склад в котором есть продукты или сырье !!");
			}
		} else if (oldPlace.getPlaceType().getName().equals("склад") && placeType.getName().equals("производство")){
			boolean isExists=false;

			List<ProductPlace> productPlaces = productPlaceHome.getProductPlacesByPlaceId(place.getId());
			for (ProductPlace productPlace: productPlaces){
				if(productPlace.getAmount()>0){
					isExists=true;
				}
			}
			if(!isExists){
				productPlaceHome.deleteByPlaceId(place.getId());
			}
			else{
				throw new BusinessException("Невозможно изменить склад в котором есть продукты или сырье !!");
			}
			List<Raw> raws = rawHome.getRaws();
			for (Raw raw : raws) {
				RawPlace rawPlace = new RawPlace();
				rawPlace.setRaw(raw);
				rawPlace.setPlace(place);
				rawPlace.setAmount(0.0);
				rawPlaceHome.persist(rawPlace);
			}
		}	
		placeHome.merge(place);
	}

	@Transactional
	public Place get(final Class<Place> type, Integer id) {
		return placeHome.findById(type, id);
	}

	@Transactional
	public void delete(int id) {

		Place place = placeHome.findById(Place.class, id);
		if(place.getPlaceType().getName().equals("склад")){
			boolean isExists=false;
			List<ProductPlace> productPlaces = productPlaceHome.getProductPlacesByPlaceId(id);
			for (ProductPlace productPlace: productPlaces){
				if(productPlace.getAmount()!=0){
					isExists=true;
				}
			}
			if(!isExists){
				productPlaceHome.deleteByPlaceId(id);
			} else{
				throw new BusinessException("Невозможно удалить склад в котором есть продукты или сырье !!");
			}
		}
		else if(place.getPlaceType().getName().equals("производство")){
			boolean isExists=false;
			
			List<RawPlace> rawPlaces = rawPlaceHome.getRawPlacesByPlaceId(id);
			for (RawPlace rawPlace: rawPlaces){
				if(rawPlace.getAmount()!=0){
					isExists=true;
				}
			}
			if(!isExists){
				rawPlaceHome.deleteByPlaceId(id);
			} else{
				throw new BusinessException("Невозможно удалить склад в котором есть продукты или сырье !!");
			}
		}
		placeHome.remove(place);
		
		
	}

}