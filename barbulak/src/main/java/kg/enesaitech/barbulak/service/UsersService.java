package kg.enesaitech.barbulak.service;

import java.util.HashSet;

import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.Staff;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.entity.UsersRole;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsersService {
	
	@Autowired
	private UsersHome usersHome;
	
	@Autowired
	private StaffService staffService;
	
	@Autowired
	private GenericHome<Staff> staffGenericHome;
	
	@Transactional
	public void createByStaffId(int id) {
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("staff.id", String.valueOf(id));
		
		SearchResult<Users> usersList = usersHome.getList(searchParameters, Users.class);
		Staff staff;

		if(usersList.getTotalRecords() > 0){
//			return Response.notModified("There is user already").build();
			//TODO
			return; 
		}
		else{
			staff = staffGenericHome.findById(Staff.class, id);
		}

		if(staff.getPersonNumber().isEmpty()){
			staff.setPersonNumber(staffService.getPersonNumber());
			staffGenericHome.merge(staff);
		}
		
		Users users = new Users(staff, staff.getPersonNumber(), usersHome.getMD5(staff.getPersonNumber() + "12345"), "active", new HashSet<UsersRole>());
		
		usersHome.persist(users);
	}

	public Users getByUserName(String string) {
		return usersHome.getByUserName(string);
	}
	
	@Transactional
	public Users update(Users detachedInstance) {
		detachedInstance.setUserPass(usersHome.getMD5(detachedInstance.getUserPass()));
		return usersHome.merge(detachedInstance);
	}
	
}