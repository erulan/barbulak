package kg.enesaitech.barbulak.service;

import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.Role;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.entity.UsersRole;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.generics.GenericService;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsersRoleService extends GenericService<UsersRole, GenericHome<UsersRole>>{
	
	@Autowired
	UsersHome usersHome;
	
	@Transactional
	public boolean deleteByRoleId(int roleId, int userId){
		Subject currentUser = SecurityUtils.getSubject();
		Users users = usersHome.getByUserName(currentUser.getPrincipal().toString());
				
		SearchParameters searchParameters = new SearchParameters();
		searchParameters.addParameter("users.id", String.valueOf(userId));
		searchParameters.addParameter("role.id", String.valueOf(roleId));
		SearchResult<UsersRole> usersRoleList = this.getList(searchParameters, UsersRole.class);
		
		if(usersRoleList.getTotalRecords() > 0){
			UsersRole usersRole = usersRoleList.getResultList().get(0);
			if(usersRole.getRole().getName().equals("admin") && userId == users.getId()){throw new BusinessException("Нельзя удалить роль администратора!!!");}
			this.delete(UsersRole.class, usersRole.getId());
			return true;
		}else{
			return false;
		}
	}
}
