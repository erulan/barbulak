package kg.enesaitech.barbulak.service;

import java.util.ArrayList;
import java.util.List;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.Installment;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InstallmentService {
	@Autowired
	private GenericHome<Installment> installmentHome;
	@Autowired
	private UsersHome usersHome;
	@Autowired
	private CashboxAccountantHome cashboxAccountantHome;

	@Transactional
	public List<Installment> getList() {
		List<Installment> installments = new ArrayList<Installment>();
		SearchParameters searchParameters = new SearchParameters();
		Subject currentUserSubj = SecurityUtils.getSubject();
		SearchResult<Installment> searchResult;
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			searchResult = installmentHome.getList(searchParameters, Installment.class);	
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			List<CashboxAccountant> cashboxAccountants = cashboxAccountantHome
					.getCashboxAccountants(currUser.getStaff().getId());
			if(cashboxAccountants.size()==0){
				throw new BusinessException("Доступ запрещен для кассы ");				
			}
			String cashboxID = null;
			if(searchParameters.getSearchParameters().get("staff.cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("staff.cashbox.id");
			}
			searchResult = new SearchResult<Installment>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					if(searchParameters.getSearchParameters().get("staff.cashbox.id")!=null){
						searchParameters.getSearchParameters().remove("staff.cashbox.id");
					}
					searchParameters.getSearchParameters().put("staff.cashbox.id", cashboxAccountant.getCashbox().getId()+"");
					SearchResult<Installment> buffer = installmentHome.getList(searchParameters, Installment.class);
					searchResult.getResultList().addAll(buffer.getResultList());				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						searchResult = installmentHome.getList(searchParameters, Installment.class);
					}else{						
					}
					
				}
			}
			if(searchParameters.getSearchParameters().get("installment.staff.cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("installment.staff.cashbox.id");
			}
			searchResult = new SearchResult<Installment>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					if(searchParameters.getSearchParameters().get("installment.staff.cashbox.id")!=null){
						searchParameters.getSearchParameters().remove("installment.staff.cashbox.id");
					}
					searchParameters.getSearchParameters().put("installment.staff.cashbox.id", cashboxAccountant.getCashbox().getId()+"");
					SearchResult<Installment> buffer = installmentHome.getList(searchParameters, Installment.class);
					searchResult.getResultList().addAll(buffer.getResultList());				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						searchResult = installmentHome.getList(searchParameters, Installment.class);
					}else{						
					}
					
				}
			}
		} else{
			throw new BusinessException("Доступ запрещен для кассы ");
		}		
		for(Installment installment : searchResult.getResultList()){
			if(installment.getLeftAmount()>0){
				installments.add(installment);
			}
		}
		return installments;
	}
	
	@Transactional
	public Integer create(Installment installment) {
		System.out.println("asdf");
		installment.setLeftAmount(installment.getTotal());
		return installmentHome.persist(installment);
	}

}