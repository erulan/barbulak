package kg.enesaitech.barbulak.service;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import kg.enesaitech.barbulak.dao.BottlePlaceHome;
import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.dao.ProductPlaceHome;
import kg.enesaitech.barbulak.dao.Shipping19Home;
import kg.enesaitech.barbulak.dao.ShippingByProductHome;
import kg.enesaitech.barbulak.entity.BottlePlace;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.ProductPlace;
import kg.enesaitech.barbulak.entity.Shipping;
import kg.enesaitech.barbulak.entity.Shipping19;
import kg.enesaitech.barbulak.entity.ShippingByProduct;
import kg.enesaitech.barbulak.entity.ShippingStatus;
import kg.enesaitech.barbulak.entity.StatusDate;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.ShippingByProductBy19;
import kg.enesaitech.barbulak.vo.ShippingByProductVO;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ShippingService {

	@Autowired
	protected DozerBeanMapper mapper;	
	
	@Autowired
	private GenericHome<Shipping> shippingHome;	
	@Autowired
	private ShippingByProductHome shippingByProductHome;
	@Autowired
	private Shipping19Home shippingBy19Home;
	@Autowired
	private ProductPlaceHome productPlaceHome;
	@Autowired
	private BottlePlaceHome bottlePlaceHome;
	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private GenericHome<ShippingStatus> shippingStatusHome;
	@Autowired
	private UserCheckService checkService;
	@Autowired
	private GenericHome<StatusDate> statusDateHome;
	private DecimalFormat decimalFormat2 = new DecimalFormat("#");

	@Transactional
	public void createRequest(ShippingByProductBy19 shippingByProductByRaw) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Shipping shipping = mapper.map(shippingByProductByRaw.getShipping(), Shipping.class);
			StatusDate statusDate = new StatusDate();
			statusDate.setCreated(new Date());
			Integer statusDateId = statusDateHome.persist(statusDate);
			statusDate.setId(statusDateId);
			shipping.setStatusDate(statusDate);
			shipping.setSent(false);
			shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 1));
			Integer shippingId = shippingHome.persist(shipping);
			shipping.setId(shippingId);
			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
			Place toPlace = placeHome.findById(Place.class, shipping.getPlaceByToPlaceId().getId());
			if(! fromPlace.getPlaceType().getName().equals("склад")){
				throw new BusinessException(fromPlace.getName()+" не является складом !!");		
			} else if(! toPlace.getPlaceType().getName().equals("склад")){
				throw new BusinessException(toPlace.getName()+" не является складом !!");
			}
			
			if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
				for(ShippingByProductVO shippingByProductVO : shippingByProductByRaw.getShippingByProducts()){
					ShippingByProduct shippingByProduct = mapper.map(shippingByProductVO, ShippingByProduct.class);
					shippingByProduct.setShipping(shipping);
					shippingByProductHome.persist(shippingByProduct);	
				}
				if(shippingByProductByRaw.getShipping19()!=null &&
						!(shippingByProductByRaw.getShipping19().getEmptyBottleAmount()==null &&
						shippingByProductByRaw.getShipping19().getBottleAmount()==null)){
					Shipping19 shipping19 = mapper.map(shippingByProductByRaw.getShipping19(), Shipping19.class);
					shipping19.setShipping(shipping);
					shippingBy19Home.persist(shipping19);					
				}
			} else
				throw new BusinessException("Вы не имеете права для запроса транспортировки!! Вы не ответственны за "+toPlace.getName()+" !!");		
			
		} else
			throw new BusinessException("Вы не имеете права для запроса транспортировки!!!");		

		
		
	}
	@Transactional
	public void sendShipping(ShippingByProductBy19 shippingByProductByRaw) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Shipping shipping = mapper.map(shippingByProductByRaw.getShipping(), Shipping.class);
			StatusDate statusDate = new StatusDate();
			statusDate.setCreated(new Date());
			Integer statusDateId = statusDateHome.persist(statusDate);
			statusDate.setId(statusDateId);
			shipping.setSent(true);
			shipping.setStatusDate(statusDate);
			shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 1));
			Integer shippingId = shippingHome.persist(shipping);
			shipping.setId(shippingId);
			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
			Place toPlace = placeHome.findById(Place.class, shipping.getPlaceByToPlaceId().getId());
			if(! fromPlace.getPlaceType().getName().equals("склад")){
				throw new BusinessException(fromPlace.getName()+" не является складом !!");		
			} else if(! toPlace.getPlaceType().getName().equals("склад")){
				throw new BusinessException(toPlace.getName()+" не является складом !!");
			}
			
			if(checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId())){

				String message= "Для транспортировки необходимо: ";
				boolean isFalse = false;
				for(ShippingByProductVO shippingByProductVO : shippingByProductByRaw.getShippingByProducts()){
					ShippingByProduct shippingByProduct = mapper.map(shippingByProductVO, ShippingByProduct.class);
					shippingByProduct.setShipping(shipping);
					shippingByProductHome.persist(shippingByProduct);
					
					ProductPlace fromProductPlace = productPlaceHome.getByProductIdByPlaceId(shippingByProduct.getProduct().getId(), fromPlace.getId());
					if(fromProductPlace.getAmount()<shippingByProduct.getAmount()){
						isFalse = true;
						message = message +fromProductPlace.getProduct().getName()+" - '"+decimalFormat2.format((shippingByProduct.getAmount()-fromProductPlace.getAmount()))+"' "+"шт, ";
					}
				}
				if(shippingByProductByRaw.getShipping19()!=null &&
						!(shippingByProductByRaw.getShipping19().getEmptyBottleAmount()==null &&
						shippingByProductByRaw.getShipping19().getBottleAmount()==null)){
					BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(fromPlace.getId());
					if(shippingByProductByRaw.getShipping19().getBottleAmount()!=null){
						if(bottlePlace.getBottleAmount()<shippingByProductByRaw.getShipping19().getBottleAmount()){
							isFalse = true;
							message = message + "19-лт бутыль - '"+decimalFormat2.format((shippingByProductByRaw.getShipping19().getBottleAmount()-bottlePlace.getBottleAmount()))+"' "+"шт, ";
						}
					}
					if(shippingByProductByRaw.getShipping19().getEmptyBottleAmount()!=null){
						if(bottlePlace.getEmptyBottleAmount()<shippingByProductByRaw.getShipping19().getEmptyBottleAmount()){
							isFalse = true;
							message = message + "19-лт тара - '"+decimalFormat2.format((shippingByProductByRaw.getShipping19().getEmptyBottleAmount()-bottlePlace.getEmptyBottleAmount()))+"' "+"шт, ";
						}
					}
					Shipping19 shipping19 = mapper.map(shippingByProductByRaw.getShipping19(), Shipping19.class);
					shipping19.setShipping(shipping);
					shippingBy19Home.persist(shipping19);					
				}

				if(isFalse){
					message = message.substring(0, message.length()-2) +".";
					throw new BusinessException(message);						
					
				}
				
			} else
				throw new BusinessException("Вы не имеете права для отправки транспортировки!! Вы не ответственны за "+fromPlace.getName()+" !!");		
			
		} else
			throw new BusinessException("Вы не имеете права для отправки транспортировки!!!");		

		
		
	}
	
	@Transactional
	public void sendEmptyBottleShipping(ShippingByProductBy19 shippingByProductByRaw) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Place proizvodstvo = placeHome.getProizvodstvo();
			Shipping shipping = mapper.map(shippingByProductByRaw.getShipping(), Shipping.class);
			StatusDate statusDate = new StatusDate();
			statusDate.setCreated(new Date());
			Integer statusDateId = statusDateHome.persist(statusDate);
			statusDate.setId(statusDateId);
			shipping.setSent(true);
			shipping.setStatusDate(statusDate);
			shipping.setPlaceByToPlaceId(proizvodstvo);
			shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 1));
			Integer shippingId = shippingHome.persist(shipping);
			shipping.setId(shippingId);
			
			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
			if(! fromPlace.getPlaceType().getName().equals("склад")){
				throw new BusinessException(fromPlace.getName()+" не является складом !!");		
			}
			
			if(checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId())){

				String message= "Для транспортировки необходимо: ";
				boolean isFalse = false;
				
				if(shippingByProductByRaw.getShipping19()!=null &&
						shippingByProductByRaw.getShipping19().getEmptyBottleAmount()!=null){
					if(shippingByProductByRaw.getShipping19().getBottleAmount()!=null){
						throw new BusinessException("вы  не можете отправить 19 литр продукт в производство  !!");	
					}
					BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(fromPlace.getId());
					if(shippingByProductByRaw.getShipping19().getEmptyBottleAmount()!=null){
						if(bottlePlace.getEmptyBottleAmount()<shippingByProductByRaw.getShipping19().getEmptyBottleAmount()){
							isFalse = true;
							message = message + "19-лт тара - '"+decimalFormat2.format((shippingByProductByRaw.getShipping19().getEmptyBottleAmount()-bottlePlace.getEmptyBottleAmount()))+"' "+"шт, ";
						}
					} else{
						throw new BusinessException("Пожалуйста укажите правильно число тар !!");	
					}
					Shipping19 shipping19 = mapper.map(shippingByProductByRaw.getShipping19(), Shipping19.class);
					shipping19.setShipping(shipping);
					shippingBy19Home.persist(shipping19);					
				}

				if(isFalse){
					message = message.substring(0, message.length()-2) +".";
					throw new BusinessException(message);						
					
				}
				
			} else
				throw new BusinessException("Вы не имеете права для отправки транспортировки!! Вы не ответственны за "+fromPlace.getName()+" !!");		
			
		} else
			throw new BusinessException("Вы не имеете права для отправки транспортировки!!!");		

		
		
	}


	@Transactional
	public void requestEmptyBottleShipping(ShippingByProductBy19 shippingByProductByRaw) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("production_manager")){
			Place proizvodstvo = placeHome.getProizvodstvo();
			Shipping shipping = mapper.map(shippingByProductByRaw.getShipping(), Shipping.class);
			StatusDate statusDate = new StatusDate();
			statusDate.setCreated(new Date());
			Integer statusDateId = statusDateHome.persist(statusDate);
			statusDate.setId(statusDateId);
			shipping.setSent(false);
			shipping.setStatusDate(statusDate);
			shipping.setPlaceByToPlaceId(proizvodstvo);
			shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 1));
			Integer shippingId = shippingHome.persist(shipping);
			shipping.setId(shippingId);
			
			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
			if(! fromPlace.getPlaceType().getName().equals("склад")){
				throw new BusinessException(fromPlace.getName()+" не является складом !!");		
			}
			
			if(checkService.isStaffIsCurrentUser(proizvodstvo.getStaff().getId())){

//				String message= "Для запроса транспортировки необходимо: ";
//				boolean isFalse = false;
				
				if(shippingByProductByRaw.getShipping19()!=null &&
						shippingByProductByRaw.getShipping19().getEmptyBottleAmount()!=null){
					if(shippingByProductByRaw.getShipping19().getBottleAmount()!=null){
						throw new BusinessException("вы  не можете отправить 19 литр продукт в производство  !!");	
					}
					if(shippingByProductByRaw.getShipping19().getEmptyBottleAmount()!=null){
//						if(bottlePlace.getEmptyBottleAmount()<shippingByProductByRaw.getShipping19().getEmptyBottleAmount()){
//							isFalse = true;
//							message = message + "19-лт тара - '"+decimalFormat2.format((shippingByProductByRaw.getShipping19().getEmptyBottleAmount()-bottlePlace.getEmptyBottleAmount()))+"' "+"шт, ";
//						}
					} else{
						throw new BusinessException("Пожалуйста укажите правильно число тар !!");	
					}
					Shipping19 shipping19 = mapper.map(shippingByProductByRaw.getShipping19(), Shipping19.class);
					shipping19.setShipping(shipping);
					shippingBy19Home.persist(shipping19);					
				}
				
			} else
				throw new BusinessException("Вы не имеете права для запроса транспортировки!! Вы не ответственны за "+proizvodstvo.getName()+" !!");		
			
		} else
			throw new BusinessException("Вы не имеете права для отправки транспортировки!!!");		

		
		
	}
	@Transactional
	public void toPlaceApprove(int shippingId) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Shipping shipping = shippingHome.findById(Shipping.class, shippingId);
			Place toPlace = placeHome.findById(Place.class, shipping.getPlaceByToPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
				if(shipping.getShippingStatus().getId()==4){
					if(toPlace.getPlaceType().getName().equals("склад") ){
						List<ShippingByProduct> shippingByProducts = shippingByProductHome.getShippingByProductByShippingId(shipping.getId());
						for(ShippingByProduct shippingByProduct : shippingByProducts){
									
							ProductPlace toProductPlace = productPlaceHome.getByProductIdByPlaceId(shippingByProduct.getProduct().getId(), toPlace.getId());
							toProductPlace.setAmount(toProductPlace.getAmount()+shippingByProduct.getAmount());
							productPlaceHome.merge(toProductPlace);
						}
						Shipping19 shipping19 = shippingBy19Home.getShipping19ByShippingId(shipping.getId());
						if(shipping19!=null){
							BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(toPlace.getId());
							if(shipping19.getBottleAmount()!=null)
							bottlePlace.setBottleAmount(bottlePlace.getBottleAmount()+shipping19.getBottleAmount());
							if(shipping19.getEmptyBottleAmount()!=null)
							bottlePlace.setEmptyBottleAmount(bottlePlace.getEmptyBottleAmount()+shipping19.getEmptyBottleAmount());
							bottlePlaceHome.merge(bottlePlace);
						}
						shipping.getStatusDate().setApprovedToPlace(new Date());
						shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 6));
						shippingHome.merge(shipping);
					} else
						throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является складом!!");
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");	
	}

	@Transactional
	public void proizvodstvoApprove(int shippingId) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("production_manager")){
			Shipping shipping = shippingHome.findById(Shipping.class, shippingId);
			Place toPlace = placeHome.findById(Place.class, shipping.getPlaceByToPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
				if(shipping.getShippingStatus().getId()==4){
					if(toPlace.getPlaceType().getName().equals("производство") ){
						
						Shipping19 shipping19 = shippingBy19Home.getShipping19ByShippingId(shipping.getId());
						if(shipping19!=null){
							BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(toPlace.getId());
							if(shipping19.getEmptyBottleAmount()!=null)
							bottlePlace.setEmptyBottleAmount(bottlePlace.getEmptyBottleAmount()+shipping19.getEmptyBottleAmount());
							bottlePlaceHome.merge(bottlePlace);
						} else 
							throw new BusinessException("транспортировка не может быть пустым !!");
						shipping.getStatusDate().setApprovedToPlace(new Date());
						shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 6));
						shippingHome.merge(shipping);
					} else
						throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является складом!!");
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");	
	}

	@Transactional
	public void adminApprove(int shippingId) {
		Shipping shipping = shippingHome.findById(Shipping.class, shippingId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (shipping.getShippingStatus().getId()==1){
				if(shipping.getSent()){
					shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 4));
					shipping.getStatusDate().setApprovedFromPlace(new Date());
					Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
					if(fromPlace.getPlaceType().getName().equals("склад") ){
						List<ShippingByProduct> shippingByProducts = shippingByProductHome.getShippingByProductByShippingId(shipping.getId());

						boolean isFalse = false;
						String message= "Для транспортировки необходимо: ";
						for(ShippingByProduct shippingByProduct : shippingByProducts){
									
							ProductPlace fromProductPlace = productPlaceHome.getByProductIdByPlaceId(shippingByProduct.getProduct().getId(), fromPlace.getId());
							if(fromProductPlace.getAmount()<shippingByProduct.getAmount()){
								isFalse = true;
								message = message +fromProductPlace.getProduct().getName()+" - '"+decimalFormat2.format((shippingByProduct.getAmount()-fromProductPlace.getAmount()))+"' "+"шт, ";
							}else{
								fromProductPlace.setAmount(fromProductPlace.getAmount()-shippingByProduct.getAmount());
								productPlaceHome.merge(fromProductPlace);
							}
						}
						Shipping19 shipping19 = shippingBy19Home.getShipping19ByShippingId(shipping.getId());
						if(shipping19!=null){
							BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(fromPlace.getId());
							if(shipping19.getBottleAmount()!=null){
								if(bottlePlace.getBottleAmount()<shipping19.getBottleAmount()){
									isFalse = true;
									message = message + "19-лт бутыль - '"+decimalFormat2.format((shipping19.getBottleAmount()-bottlePlace.getBottleAmount()))+"' "+"шт, ";
								}else{
									bottlePlace.setBottleAmount(bottlePlace.getBottleAmount()-shipping19.getBottleAmount());
								}
							}
							if(shipping19.getEmptyBottleAmount()!=null){
								if(bottlePlace.getEmptyBottleAmount()<shipping19.getEmptyBottleAmount()){
									isFalse = true;
									message = message + "19-лт тара - '"+decimalFormat2.format((shipping19.getEmptyBottleAmount()-bottlePlace.getEmptyBottleAmount()))+"' "+"шт, ";
								}else{
									bottlePlace.setEmptyBottleAmount(bottlePlace.getEmptyBottleAmount()-shipping19.getEmptyBottleAmount());
								}
							}
							bottlePlaceHome.merge(bottlePlace);
						}

						if(isFalse){
							message = message.substring(0, message.length()-2) +".";
							throw new BusinessException(message);						
							
						}
					} else
						throw new BusinessException("Вы не имеете права для подтверждения!! "+fromPlace.getName()+" не является складом!!");
				} else{
					shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 2));					
				}
				shipping.getStatusDate().setApprovedAdmin(new Date());
				shippingHome.merge(shipping);
			} else
				throw new BusinessException("Вы не имеете права для подтверждения. Данный запрос был подтвержден или отменен!!!");
		
			} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");		

	}

	@Transactional
	public void adminReject(int shippingId, String description) {
		Shipping shipping = shippingHome.findById(Shipping.class, shippingId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (shipping.getShippingStatus().getId()==1){
				shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 3));
				shipping.getStatusDate().setRejectedAdmin(new Date());
				shipping.setDescription(description);
				shippingHome.merge(shipping);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя отменить!!!");		
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}	


	@Transactional
	public void fromPlaceReject(int shippingId, String description) {
		Shipping shipping = shippingHome.findById(Shipping.class, shippingId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId())){
				if (shipping.getShippingStatus().getId()==2 && !shipping.getSent()){
					shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 5));
					shipping.getStatusDate().setRejectedFromPlace(new Date());
					shipping.setDescription(description);
					shippingHome.merge(shipping);
				} else
					throw new BusinessException("Данный запрос был уже подтвержден или НЕ подтвержден администратором и нельзя отменить!!!");		
			
			} else
				throw new BusinessException("Вы не имеете права для запроса транспортировки!! Вы не ответственны за "+fromPlace.getName()+" !!");		
			
			
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}	

	@Transactional
	public void toPlaceReject(int shippingId, String description) {
		Shipping shipping = shippingHome.findById(Shipping.class, shippingId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Place toPlace = placeHome.findById(Place.class, shipping.getPlaceByToPlaceId().getId());
			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
				if (shipping.getShippingStatus().getId()==4){
					shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 7));
					shipping.getStatusDate().setRejectedToPlace(new Date());
					shipping.setDescription(description);
					shippingHome.merge(shipping);
					List<ShippingByProduct> shippingByProducts = shippingByProductHome.getShippingByProductByShippingId(shipping.getId());
					for(ShippingByProduct shippingByProduct : shippingByProducts){
								
						ProductPlace fromProductPlace = productPlaceHome.getByProductIdByPlaceId(shippingByProduct.getProduct().getId(), fromPlace.getId());
						fromProductPlace.setAmount(fromProductPlace.getAmount()+shippingByProduct.getAmount());
						productPlaceHome.merge(fromProductPlace);
					}
					Shipping19 shipping19 = shippingBy19Home.getShipping19ByShippingId(shipping.getId());
					if(shipping19!=null){
						BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(fromPlace.getId());
						if(shipping19.getBottleAmount()!=null)
						bottlePlace.setBottleAmount(bottlePlace.getBottleAmount()+shipping19.getBottleAmount());
						if(shipping19.getEmptyBottleAmount()!=null)
						bottlePlace.setEmptyBottleAmount(bottlePlace.getEmptyBottleAmount()+shipping19.getEmptyBottleAmount());
						bottlePlaceHome.merge(bottlePlace);
					}
				} else
					throw new BusinessException("Данный запрос был уже подтвержден или НЕ подтвержден администратором и нельзя отменить!!!");		
			
			} else
				throw new BusinessException("Вы не имеете права для отмены транспортировки!! Вы не ответственны за "+toPlace.getName()+" !!");		
			
			
		} else
			throw new BusinessException("Вы не имеете права для отмены транспортировки!!!");		

	}	
	@Transactional
	public void update(ShippingByProductBy19 shippingByProductByRaw) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Shipping shipping = mapper.map(shippingByProductByRaw.getShipping(), Shipping.class);
			Shipping shipping2 = shippingHome.findById(Shipping.class, shipping.getId());
			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
			Place toPlace = placeHome.findById(Place.class, shipping.getPlaceByToPlaceId().getId());
			if(! fromPlace.getPlaceType().getName().equals("склад")){
				throw new BusinessException(fromPlace.getName()+" не является складом !!");		
			} else if(! toPlace.getPlaceType().getName().equals("склад")){
				throw new BusinessException(toPlace.getName()+" не является складом !!");
			}
			
			if((!shipping2.getSent() && checkService.isStaffIsCurrentUser(toPlace.getStaff().getId()))
					||(shipping2.getSent() && checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId()))){
				if(shipping.getShippingStatus().getId()==1 || 
						shipping.getShippingStatus().getId()==3 || 
						shipping.getShippingStatus().getId()==5 || 
						shipping.getShippingStatus().getId()==7){
					shippingByProductHome.deleteByShippingId(shipping.getId());
					for(ShippingByProductVO shippingByProductVO : shippingByProductByRaw.getShippingByProducts()){
						ShippingByProduct shippingByProduct = mapper.map(shippingByProductVO, ShippingByProduct.class);
						shippingByProduct.setShipping(shipping);
						shippingByProductHome.persist(shippingByProduct);	
					}
					shippingBy19Home.deleteByShippingId(shipping.getId());
					if(shippingByProductByRaw.getShipping19()!=null){
						Shipping19 shipping19 = mapper.map(shippingByProductByRaw.getShipping19(), Shipping19.class);
						shipping19.setShipping(shipping);
						shipping19.setId(null);
						shippingBy19Home.persist(shipping19);					
					}
					shipping.setStatusDate(shipping2.getStatusDate());
					shipping.setSent(shipping2.getSent());
					shipping.getStatusDate().setCreated(new Date());
					shippingHome.merge(shipping);
				} else
					throw new BusinessException("Данный запрос был подтвержден и нельзя изменить!!!");		
			} else
				throw new BusinessException("Вы не имеете права для запроса транспортировки!! Вы не ответственны за "+toPlace.getName()+" !!");		
			
		} else
			throw new BusinessException("Вы не имеете права для запроса транспортировки!!!");		
	
	}

	@Transactional
	public void fromPlaceApprove(int shippingId) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Shipping shipping = shippingHome.findById(Shipping.class, shippingId);
			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId())){
				if(shipping.getShippingStatus().getId()==2 && !shipping.getSent()){
					if(fromPlace.getPlaceType().getName().equals("склад") ){
						List<ShippingByProduct> shippingByProducts = shippingByProductHome.getShippingByProductByShippingId(shipping.getId());

						boolean isFalse = false;
						String message= "Для транспортировки необходимо: ";
						for(ShippingByProduct shippingByProduct : shippingByProducts){
									
							ProductPlace fromProductPlace = productPlaceHome.getByProductIdByPlaceId(shippingByProduct.getProduct().getId(), fromPlace.getId());
							if(fromProductPlace.getAmount()<shippingByProduct.getAmount()){
								isFalse = true;
								message = message +fromProductPlace.getProduct().getName()+" - '"+decimalFormat2.format((shippingByProduct.getAmount()-fromProductPlace.getAmount()))+"' "+"шт, ";
							}else{
								fromProductPlace.setAmount(fromProductPlace.getAmount()-shippingByProduct.getAmount());
								productPlaceHome.merge(fromProductPlace);
							}
						}
						Shipping19 shipping19 = shippingBy19Home.getShipping19ByShippingId(shipping.getId());
						if(shipping19!=null){
							BottlePlace bottlePlace = bottlePlaceHome.getByPlaceId(fromPlace.getId());
							if(shipping19.getBottleAmount()!=null){
								if(bottlePlace.getBottleAmount()<shipping19.getBottleAmount()){
									isFalse = true;
									message = message + "19-лт бутыль - '"+decimalFormat2.format((shipping19.getBottleAmount()-bottlePlace.getBottleAmount()))+"' "+"шт, ";
								}else{
									bottlePlace.setBottleAmount(bottlePlace.getBottleAmount()-shipping19.getBottleAmount());
								}
							}
							if(shipping19.getEmptyBottleAmount()!=null){
								if(bottlePlace.getEmptyBottleAmount()<shipping19.getEmptyBottleAmount()){
									isFalse = true;
									message = message + "19-лт тара - '"+decimalFormat2.format((shipping19.getEmptyBottleAmount()-bottlePlace.getEmptyBottleAmount()))+"' "+"шт, ";
								}else{
									bottlePlace.setEmptyBottleAmount(bottlePlace.getEmptyBottleAmount()-shipping19.getEmptyBottleAmount());
								}
							}
							bottlePlaceHome.merge(bottlePlace);
						}

						if(isFalse){
							message = message.substring(0, message.length()-2) +".";
							throw new BusinessException(message);						
							
						}
						shipping.getStatusDate().setApprovedFromPlace(new Date());
						shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 4));
						shippingHome.merge(shipping);
					} else
						throw new BusinessException("Вы не имеете права для подтверждения!! "+fromPlace.getName()+" не является складом!!");
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  подтвержден !!");		
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+fromPlace.getName()+" !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");	
	}

//	@Transactional
//	public void cancelShippinig(int shippingId, boolean isShippingPendingFinished) {
//		
//		Subject currentUserSubj = SecurityUtils.getSubject();
//		if (currentUserSubj.hasRole("admin")){
//			Shipping shipping = shippingHome.findById(Shipping.class, shippingId);
//
//			Place fromPlace = placeHome.findById(Place.class, shipping.getPlaceByFromPlaceId().getId());
//			if(shipping.getShippingStatus().getId()==5){
//				if(fromPlace.getPlaceType().getName().equals("склад")){
//					List<ShippingByProduct> shippingByProducts = shippingByProductHome.getShippingByProductByShippingId(shipping.getId());
//					for(ShippingByProduct shippingByProduct : shippingByProducts){
//								
//						ProductPlace fromPlaceProduct = productPlaceHome.getByProductIdByPlaceId(shippingByProduct.getProduct().getId(), fromPlace.getId());
//						fromPlaceProduct.setAmount(fromPlaceProduct.getAmount()+shippingByProduct.getAmount());
//						productPlaceHome.merge(fromPlaceProduct);
//					}
//					Shipping19 shipping19 = shippingBy19Home.getShipping19ByShippingId(shipping.getId());	
//						
//					BottlePlace fromBottlePlace = bottlePlaceHome.getByPlaceId(fromPlace.getId());
//					fromBottlePlace.setBottleAmount(fromBottlePlace.getBottleAmount()+shipping19.getBottleAmount());
//					fromBottlePlace.setEmptyBottleAmount(fromBottlePlace.getEmptyBottleAmount()+shipping19.getEmptyBottleAmount());
//					bottlePlaceHome.merge(fromBottlePlace);
//	
//					shipping.setShippingStatus(shippingStatusHome.findById(ShippingStatus.class, 3));
//					shippingHome.merge(shipping);
//				} else
//					throw new BusinessException("You cannot cancel!! "+fromPlace.getName()+" is not a sklad !!");
//				if(isShippingPendingFinished){
//					Place toPlace = placeHome.findById(Place.class, shipping.getPlaceByToPlaceId().getId());
//					if(toPlace.getPlaceType().getName().equals("склад")){
//						List<ShippingByProduct> shippingByProducts = shippingByProductHome.getShippingByProductByShippingId(shipping.getId());
//						for(ShippingByProduct shippingByProduct : shippingByProducts){
//									
//							ProductPlace toPlaceProduct = productPlaceHome.getByProductIdByPlaceId(shippingByProduct.getProduct().getId(), toPlace.getId());
//							if(toPlaceProduct.getAmount()<shippingByProduct.getAmount()){
//								throw new BusinessException("There is no such amount of "
//										+toPlaceProduct.getProduct().getName()+" in "+toPlaceProduct.getPlace().getName()+" "
//										+ "!! toplacetegi product chygyp ketiptir da ne kylavz emi )( ");						
//							}else{
//								toPlaceProduct.setAmount(toPlaceProduct.getAmount()-shippingByProduct.getAmount());
//								productPlaceHome.merge(toPlaceProduct);
//							}
//						}
//						Shipping19 shipping19 = shippingBy19Home.getShipping19ByShippingId(shipping.getId());
//							
//						BottlePlace toBottlePlace = bottlePlaceHome.getByPlaceId(toPlace.getId());
//						if (toBottlePlace.getBottleAmount()<shipping19.getBottleAmount()){
//							throw new BusinessException("Невозможно cancel shipping который нет в "+toBottlePlace.getPlace().getName()+" !!");
//						} 
//						else if (toBottlePlace.getEmptyBottleAmount()<shipping19.getEmptyBottleAmount()){
//							throw new BusinessException("Невозможно cancel shipping (тара) который нет в "+toBottlePlace.getPlace().getName()+" !!");
//							
//						}else{
//							toBottlePlace.setBottleAmount(toBottlePlace.getBottleAmount()-shipping19.getBottleAmount());
//							toBottlePlace.setEmptyBottleAmount(toBottlePlace.getEmptyBottleAmount()-shipping19.getEmptyBottleAmount());
//							bottlePlaceHome.merge(toBottlePlace);
//						}
//		
//					} else
//						throw new BusinessException("You cannot cancel!! "+toPlace.getName()+" is not a sklad !!");
//				} // end of pending finish
//			} else
//				throw new BusinessException("You cannot cancel!! Shipping is not requested to cancel !!");
//					
//			
//			
//		} else
//			throw new BusinessException("You cannot cancel shipping!! access Denied !!");		
//
//	}
}
