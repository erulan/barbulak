package kg.enesaitech.barbulak.service;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import kg.enesaitech.barbulak.dao.PersonalBalanceHome;
import kg.enesaitech.barbulak.dao.PersonalTransactionCategoryHome;
import kg.enesaitech.barbulak.dao.PersonalTransactionHome;
import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.dao.ProductPlaceHome;
import kg.enesaitech.barbulak.dao.WareDistProductHome;
import kg.enesaitech.barbulak.entity.PersonalBalance;
import kg.enesaitech.barbulak.entity.PersonalTransaction;
import kg.enesaitech.barbulak.entity.PersonalTransactionCategory;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.ProductPlace;
import kg.enesaitech.barbulak.entity.WareDistProduct;
import kg.enesaitech.barbulak.entity.WareDistStatus;
import kg.enesaitech.barbulak.entity.WareDistributor;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.WareDistProductVO;
import kg.enesaitech.barbulak.vo.WareDistributorByProduct;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WareDistributorService {
	@Autowired
	protected DozerBeanMapper mapper;	
	
	@Autowired
	private GenericHome<WareDistributor> wareDistributorHome;	
	@Autowired
	private WareDistProductHome wareDistProductHome;
	@Autowired
	private GenericHome<WareDistStatus> wareDistStatusHome;
	@Autowired
	private ProductPlaceHome productPlaceHome;
//	@Autowired
//	private RawPlaceHome rawPlaceHome;
	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private UserCheckService checkService;
	private DecimalFormat decimalFormat2 = new DecimalFormat("#");

	@Autowired
	private PersonalTransactionHome personalTransactionHome;
	@Autowired
	private PersonalBalanceHome personalBalanceHome;
	@Autowired
	private PersonalTransactionCategoryHome personalTransactionCategoryHome;

	@Transactional
	public void create(WareDistributorByProduct wareDistributorByProduct) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			WareDistributor wareDistributor = mapper.map(wareDistributorByProduct.getWareDistributor(), WareDistributor.class);
			wareDistributor.setCreated(new Date ());
			wareDistributor.setWareDistStatus(wareDistStatusHome.findById(WareDistStatus.class, 1));
			Integer wareDistributorId = wareDistributorHome.persist(wareDistributor);
			wareDistributor.setId(wareDistributorId);
			Place fromPlace = placeHome.findById(Place.class, wareDistributor.getPlaceByFromPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId())){
				if(fromPlace.getPlaceType().getName().equals("склад")){
	
					String message= "Для дистрибуции необходимо: ";
					boolean isFalse = false;
					for(WareDistProductVO wareDistProductVOs : wareDistributorByProduct.getWareDistProducts()){
						WareDistProduct wareDistProduct = mapper.map(wareDistProductVOs, WareDistProduct.class);
						wareDistProduct.setWareDistributor(wareDistributor);
						wareDistProductHome.persist(wareDistProduct);
		
						ProductPlace fromProductPlace = productPlaceHome.getByProductIdByPlaceId(wareDistProduct.getProduct().getId(), wareDistributor.getPlaceByFromPlaceId().getId());
						if(fromProductPlace.getAmount()<wareDistProduct.getAmount()){
							isFalse = true;
							message = message +fromProductPlace.getProduct().getName()+" - '"+decimalFormat2.format((wareDistProduct.getAmount()-fromProductPlace.getAmount()))+"' "+"шт, ";
						}
					}
	
	
					if(isFalse){
						message = message.substring(0, message.length()-2) +".";
						throw new BusinessException(message);						
						
					}
				}
				else{
					throw new BusinessException("Ошибка! Место должно быть СКЛАД !!!");
				}
			} else
				throw new BusinessException("Вы не имеете права!! Вы не ответственны за "+fromPlace.getName()+" !!");	

		} else
			throw new BusinessException("Вы не имеете права для дистрибуции!!!");	
	}

	

	@Transactional
	public void adminReject(int wareDistributorId, String description) {
		WareDistributor wareDistributor = wareDistributorHome.findById(WareDistributor.class, wareDistributorId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (wareDistributor.getWareDistStatus().getId()==1){
				wareDistributor.setWareDistStatus(wareDistStatusHome.findById(WareDistStatus.class, 3));
				wareDistributor.setDescription(description);
				wareDistributorHome.merge(wareDistributor);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором или отменен и нельзя отменить!!!");		
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}	

	@Transactional
	public void fromPlaceReject(int wareDistributorId, String description) {
		WareDistributor wareDistributor = wareDistributorHome.findById(WareDistributor.class, wareDistributorId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){

			Place fromPlace = placeHome.findById(Place.class, wareDistributor.getPlaceByFromPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId())){
				if (wareDistributor.getWareDistStatus().getId()==2){
					wareDistributor.setWareDistStatus(wareDistStatusHome.findById(WareDistStatus.class, 5));
					wareDistributor.setDescription(description);
					wareDistributorHome.merge(wareDistributor);
				} else
					throw new BusinessException("Данный запрос был подтвержден администратором или отменен и нельзя отменить!!!");		
			} else
				throw new BusinessException("Вы не имеете права!! Вы не ответственны за "+fromPlace.getName()+" !!");	
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}


	@Transactional
	public void adminApprove(int shippingId) {
		WareDistributor wareDistributor = wareDistributorHome.findById(WareDistributor.class, shippingId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (wareDistributor.getWareDistStatus().getId()==1){
				wareDistributor.setWareDistStatus(wareDistStatusHome.findById(WareDistStatus.class, 2));
				wareDistributor.setApprovedAdmin(new Date());
				wareDistributorHome.merge(wareDistributor);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором или отменен и нельзя подтверждать!!!");		
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}
	
	@Transactional
	public void fromPlaceApprove(Integer wareDistributorId) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			WareDistributor wareDistributor = wareDistributorHome.findById(WareDistributor.class, wareDistributorId);

			if (wareDistributor.getWareDistStatus().getId()==2){
				double totalPrice=0.0;
				wareDistributor.setSentDate(new Date());
				wareDistributor.setWareDistStatus(wareDistStatusHome.findById(WareDistStatus.class, 4));
				wareDistributorHome.merge(wareDistributor);
				Place fromPlace = placeHome.findById(Place.class, wareDistributor.getPlaceByFromPlaceId().getId());
				if(checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId())){
					if(fromPlace.getPlaceType().getName().equals("склад")){

						String message= "Для дистрибуции необходимо: ";
						boolean isFalse = false;
						List<WareDistProduct> wareDistProducts = wareDistProductHome.getByWareDistributorId(wareDistributorId);
						for(WareDistProduct wareDistProduct : wareDistProducts){
							ProductPlace fromProductPlace = productPlaceHome.getByProductIdByPlaceId(wareDistProduct.getProduct().getId(), wareDistributor.getPlaceByFromPlaceId().getId());
							if(fromProductPlace.getAmount()<wareDistProduct.getAmount()){
								isFalse = true;
								message = message +fromProductPlace.getProduct().getName()+": '"+decimalFormat2.format((wareDistProduct.getAmount()-fromProductPlace.getAmount()))+"' "+"шт, ";
							} else {
								totalPrice += (wareDistProduct.getUnitPrice()+wareDistProduct.getChangeInPrice())*wareDistProduct.getAmount();
								fromProductPlace.setAmount(fromProductPlace.getAmount()-wareDistProduct.getAmount());
								productPlaceHome.merge(fromProductPlace);
							}
						}
		
						if(isFalse){
							message = message.substring(0, message.length()-2) +".";
							throw new BusinessException(message);						
						}
						PersonalBalance personalBalance = personalBalanceHome.getByPersonId(wareDistributor.getStaff().getId());
						if (personalBalance==null){
							personalBalance = new PersonalBalance();
							personalBalance.setBalance(-totalPrice);	
							personalBalance.setPerson(wareDistributor.getStaff());
							personalBalanceHome.persist(personalBalance);
						} else{
							personalBalance.setBalance(personalBalance.getBalance()-totalPrice);				
							
							personalBalanceHome.merge(personalBalance);
						}
						PersonalTransaction personalTransaction = new PersonalTransaction();
						personalTransaction.setAmount(totalPrice);
						personalTransaction.setDeposit(false);
						personalTransaction.setDate(new Date());
						personalTransaction.setDescription("дистрибуция");
						PersonalTransactionCategory personalTransactionCategory = personalTransactionCategoryHome.getByName("дистрибуция");
						personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
						personalTransaction.setPersonalBalance(personalBalance);
						personalTransactionHome.createStoreManager(personalTransaction);
					}
					else{
						throw new BusinessException("Ошибка! Место должно быть СКЛАД !!!");
					}
				} else
					throw new BusinessException("Вы не имеете права!! Вы не ответственны за "+fromPlace.getName()+" !!");	
			} else
				throw new BusinessException("Данный запрос НЕ подтвержден администратором или отменен!!!");	
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");
	}
	
	

	@Transactional
	public void update(WareDistributorByProduct wareDistributorByProduct) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			WareDistributor wareDistributor = mapper.map(wareDistributorByProduct.getWareDistributor(), WareDistributor.class);
			WareDistributor wareDistributor2 = wareDistributorHome.findById(WareDistributor.class, wareDistributorByProduct.getWareDistributor().getId());
			
			Place fromPlace = placeHome.findById(Place.class, wareDistributor.getPlaceByFromPlaceId().getId());
			if(checkService.isStaffIsCurrentUser(fromPlace.getStaff().getId())){
				if(fromPlace.getPlaceType().getName().equals("склад")){
					if(wareDistributor2.getWareDistStatus().getId()==1 || 
							wareDistributor2.getWareDistStatus().getId()==3 || 
							wareDistributor2.getWareDistStatus().getId()==5){ 
						String message= "Для дистрибуции необходимо: ";
						boolean isFalse = false;
						wareDistProductHome.deleteByWareDistributorId(wareDistributor.getId());
						for(WareDistProductVO wareDistProductVOs : wareDistributorByProduct.getWareDistProducts()){
							WareDistProduct wareDistProduct = mapper.map(wareDistProductVOs, WareDistProduct.class);
							wareDistProduct.setWareDistributor(wareDistributor);
							wareDistProduct.setId(null);
							wareDistProductHome.persist(wareDistProduct);
			
							ProductPlace fromProductPlace = productPlaceHome.getByProductIdByPlaceId(wareDistProduct.getProduct().getId(), wareDistributor.getPlaceByFromPlaceId().getId());
							if(fromProductPlace.getAmount()<wareDistProduct.getAmount()){
								isFalse = true;
								message = message +fromProductPlace.getProduct().getName()+" - '"+decimalFormat2.format((wareDistProduct.getAmount()-fromProductPlace.getAmount()))+"' "+"шт, ";
							}
						}
		
		
						if(isFalse){
							message = message.substring(0, message.length()-2) +".";
							throw new BusinessException(message);						
							
						}
						wareDistributor.setCreated(new Date());
						wareDistributor.setWareDistStatus(wareDistStatusHome.findById(WareDistStatus.class, 1));
						wareDistributorHome.merge(wareDistributor);

					} else
						throw new BusinessException("Данный запрос был подтвержден и нельзя изменить!!!");	
				}
				else{
					throw new BusinessException("Ошибка! Место должно быть СКЛАД !!!");
				}
			} else
				throw new BusinessException("Вы не имеете права!! Вы не ответственны за "+fromPlace.getName()+" !!");	

		} else
			throw new BusinessException("Вы не имеете права для дистрибуции!!!");	
	}
}