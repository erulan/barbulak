package kg.enesaitech.barbulak.service;

import java.util.ArrayList;
import java.util.List;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.PersonalBalanceHome;
import kg.enesaitech.barbulak.dao.PersonalTransactionCategoryHome;
import kg.enesaitech.barbulak.dao.PersonalTransactionHome;
import kg.enesaitech.barbulak.dao.TransactionHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.Cashbox;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.PersonalBalance;
import kg.enesaitech.barbulak.entity.PersonalTransaction;
import kg.enesaitech.barbulak.entity.PersonalTransactionCategory;
import kg.enesaitech.barbulak.entity.Transaction;
import kg.enesaitech.barbulak.entity.TransactionCategory;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.BalanceByCategory;
import kg.enesaitech.barbulak.vo.DebitCreditBalance;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;
import kg.enesaitech.barbulak.vo.SearchTransaction;
import kg.enesaitech.barbulak.vo.TransactionCategoryVO;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionService {

	@Autowired
	private TransactionHome transactionHome;
	@Autowired
	private GenericHome<TransactionCategory> transactionCategoryHome;
	@Autowired
	private GenericHome<Cashbox> caseHome;
	@Autowired
	protected DozerBeanMapper mapper;
	@Autowired
	protected UserCheckService checkService;
	@Autowired
	protected UsersHome usersHome;
	@Autowired
	protected CashboxAccountantHome cashboxAccountantHome;
	@Autowired
	private PersonalTransactionHome personalTransactionHome;	
	@Autowired
	private PersonalBalanceHome personalBalanceHome;
	@Autowired
	private PersonalTransactionCategoryHome personalTransactionCategoryHome;
	
	@Transactional
	public void create(Transaction transaction) {
		Cashbox kassa = caseHome.findById(Cashbox.class, transaction.getCashbox().getId());
		if(checkService.isCurrentUserResposiblrForCashbox(kassa.getId())){
			TransactionCategory transactionCategory = transactionCategoryHome.findById(TransactionCategory.class, transaction.getTransactionCategory().getId());

			if (transactionCategory.getIncome()) {
				
				kassa.setTotal(kassa.getTotal() + transaction.getAmount());
			} else {
				if(kassa.getTotal()<transaction.getAmount()){
					throw new BusinessException("Недостаточно средств на кассе");
				}else{
					kassa.setTotal(kassa.getTotal() - transaction.getAmount());				
				}
			}
			caseHome.merge(kassa);
			transactionHome.persist(transaction);
		} else{
			throw new BusinessException("Доступ запрещен для кассы "+kassa.getName());			
		}
			
	}
	
	
	@Transactional
	public void createWithPersonal(Transaction transaction, int personalTransactionCategoryId) {
		Cashbox kassa = caseHome.findById(Cashbox.class, transaction.getCashbox().getId());
		if(checkService.isCurrentUserResposiblrForCashbox(kassa.getId())){
			TransactionCategory transactionCategory = transactionCategoryHome.findById(TransactionCategory.class, transaction.getTransactionCategory().getId());

			if (transactionCategory.getIncome()) {
				
				kassa.setTotal(kassa.getTotal() + transaction.getAmount());
			} else {
				if(kassa.getTotal()<transaction.getAmount()){
					throw new BusinessException("Недостаточно средств на кассе");
				}else{
					kassa.setTotal(kassa.getTotal() - transaction.getAmount());				
				}
			}
			caseHome.merge(kassa);
			transactionHome.persist(transaction);
			PersonalBalance personalBalance = personalBalanceHome.getByPersonId(transaction.getPerson().getId());
			PersonalTransaction personalTransaction = new PersonalTransaction();
			personalTransaction.setDescription(transaction.getDescription());
			personalTransaction.setAmount(transaction.getAmount());
			personalTransaction.setDate(transaction.getDate());
			
			TransactionCategory transactionCategory2 = transactionCategoryHome.findById(TransactionCategory.class, transaction.getTransactionCategory().getId());
			
			if (personalBalance==null){
				personalBalance = new PersonalBalance();
				if (transactionCategory2.getIncome()){
					personalBalance.setBalance(transaction.getAmount());
					personalTransaction.setDeposit(true);
				} else {
					personalBalance.setBalance(-transaction.getAmount());	
					personalTransaction.setDeposit(false);			
				}
				personalBalance.setPerson(transaction.getPerson());
				Integer personalbalanceId = personalBalanceHome.persist(personalBalance);
				personalBalance.setId(personalbalanceId);
			} else{
				if (transactionCategory2.getIncome()){
					personalBalance.setBalance(personalBalance.getBalance()+transaction.getAmount());
					personalTransaction.setDeposit(true);
				} else {
					personalBalance.setBalance(personalBalance.getBalance()-transaction.getAmount());				
					personalTransaction.setDeposit(false);
				}
				personalBalanceHome.merge(personalBalance);
			}
			PersonalTransactionCategory personalTransactionCategory = personalTransactionCategoryHome.findById(PersonalTransactionCategory.class, personalTransactionCategoryId);
			personalTransaction.setPersonalTransactionCategory(personalTransactionCategory);
			personalTransaction.setPersonalBalance(personalBalance);
			personalTransactionHome.persist(personalTransaction);
		} else{
			throw new BusinessException("Доступ запрещен для кассы "+kassa.getName());			
		}
			
	}


	public List<BalanceByCategory> getBalanceByCategoryList(SearchTransaction searchTransaction) {
		SearchParameters searchParameters = new SearchParameters();
		List<BalanceByCategory> resultList = new ArrayList<BalanceByCategory>();
		SearchResult<Cashbox> cashboxes = caseHome.getList(searchParameters, Cashbox.class);
		SearchResult<TransactionCategory> categories = transactionCategoryHome.getList(searchParameters, TransactionCategory.class);
		for(TransactionCategory category : categories.getResultList()){
			searchTransaction.setTransactionCategoryId(category.getId());
			BalanceByCategory balanceByCategory = new BalanceByCategory();
			balanceByCategory.setTransactionCategoryVO(mapper.map(category, TransactionCategoryVO.class));
			List<DebitCreditBalance> debitCreditBalances = new ArrayList<DebitCreditBalance>();
			Double totalSumAmount=0.0;
			for(Cashbox cashbox : cashboxes.getResultList()){
				searchTransaction.setCashboxId(cashbox.getId());
				Double sumAmount = transactionHome.getTransactionsByCategoryFromDateToDate(searchTransaction);
				if(sumAmount==null) sumAmount=0.0;
				DebitCreditBalance debitCreditBalance = new DebitCreditBalance();
				debitCreditBalance.setCashboxName(cashbox.getName());
				debitCreditBalance.setBalance(sumAmount);
				debitCreditBalances.add(debitCreditBalance);
				totalSumAmount = totalSumAmount + sumAmount;
			}
			DebitCreditBalance debitCreditBalance = new DebitCreditBalance();
			debitCreditBalance.setCashboxName("Итого");
			debitCreditBalance.setBalance(totalSumAmount);
			debitCreditBalances.add(debitCreditBalance);
			balanceByCategory.setDebitCreditBalanceList(debitCreditBalances);
			resultList.add(balanceByCategory);
		}		
			
		return resultList;
	}
	public List<DebitCreditBalance> getTest(SearchTransaction searchTransaction) {
		SearchParameters searchParameters = new SearchParameters();
		SearchResult<Cashbox> cashboxes = caseHome.getList(searchParameters, Cashbox.class);
		List<DebitCreditBalance> resultList = new ArrayList<DebitCreditBalance>();
		for(Cashbox cashbox : cashboxes.getResultList()){
			searchTransaction.setCashboxId(cashbox.getId());
			DebitCreditBalance debitCreditBalance = new DebitCreditBalance();
			Double notIncome = transactionHome.getSumFromDateToDate(searchTransaction, false);
			Double income = transactionHome.getSumFromDateToDate(searchTransaction, true);
			if(income == null){
				income = 0.0;
			}
			if(notIncome == null){
				notIncome = 0.0;
			}
			debitCreditBalance.setCashboxName(cashbox.getName());
			debitCreditBalance.setDebit(notIncome);
			debitCreditBalance.setCredit(income);
			debitCreditBalance.setBalance(income-notIncome);
			resultList.add(debitCreditBalance);
		}
		searchTransaction.setCashboxId(null);
		DebitCreditBalance debitCreditBalance = new DebitCreditBalance();
		Double notIncome = transactionHome.getSumFromDateToDate(searchTransaction, false);
		Double income = transactionHome.getSumFromDateToDate(searchTransaction, true);
		if(income == null){
			income = 0.0;
		}
		if(notIncome == null){
			notIncome = 0.0;
		}		
		debitCreditBalance.setCashboxName("Итого");
		debitCreditBalance.setDebit(notIncome);
		debitCreditBalance.setCredit(income);
		
		debitCreditBalance.setBalance(income-notIncome);
		resultList.add(debitCreditBalance);
		return resultList;
	}

	@Transactional
	public void delete(int id) {
		Transaction transaction = transactionHome.findById(Transaction.class, id);
		Cashbox kassa = caseHome.findById(Cashbox.class, transaction.getCashbox().getId());
		if(checkService.isCurrentUserAdmin()){
			TransactionCategory transactionCategory = transactionCategoryHome.findById(TransactionCategory.class, transaction.getTransactionCategory().getId());

			if (!transactionCategory.getIncome()) {
				
				kassa.setTotal(kassa.getTotal() + transaction.getAmount());
			} else {
				if(kassa.getTotal()<transaction.getAmount()){
					throw new BusinessException("Such amount is gone chygyp ketti kassadan");
				}else{
					kassa.setTotal(kassa.getTotal() - transaction.getAmount());				
				}
			}
			caseHome.merge(kassa);
			transactionHome.remove(transaction);
		} else{
			throw new BusinessException("Доступ запрещен для кассы "+kassa.getName());			
		}
	}

	@Transactional
	public Transaction get(Integer id) {
		return transactionHome.findById(Transaction.class, id);
	}

	@Transactional
	public SearchResult<Transaction> getList(SearchParameters searchParameters) {
		
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return transactionHome.getList(searchParameters, Transaction.class);			
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			List<CashboxAccountant> cashboxAccountants = cashboxAccountantHome
					.getCashboxAccountants(currUser.getStaff().getId());
			if(cashboxAccountants.size()==0){
				throw new BusinessException("Доступ запрещен для кассы ");				
			}
			String cashboxID = null;
			if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("cashbox.id");
			}
			SearchResult<Transaction> searchResult = new SearchResult<Transaction>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
						searchParameters.getSearchParameters().remove("cashbox.id");
					}
					searchParameters.getSearchParameters().put("cashbox.id", cashboxAccountant.getCashbox().getId()+"");
					SearchResult<Transaction> buffer = transactionHome.getList(searchParameters, Transaction.class);
					searchResult.getResultList().addAll(buffer.getResultList());				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						return transactionHome.getList(searchParameters, Transaction.class);
					}else{						
					}
					
				}
				}
			return searchResult;
		} else{
			throw new BusinessException("Доступ запрещен для кассы ");
		}
	}
	
}