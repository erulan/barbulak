package kg.enesaitech.barbulak.service;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import kg.enesaitech.barbulak.dao.IngredientHome;
import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.dao.ProductPlaceHome;
import kg.enesaitech.barbulak.dao.ProductionProcessProductHome;
import kg.enesaitech.barbulak.dao.RawPlaceHome;
import kg.enesaitech.barbulak.entity.Ingredient;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.ProductPlace;
import kg.enesaitech.barbulak.entity.ProductionProcess;
import kg.enesaitech.barbulak.entity.ProductionProcessProduct;
import kg.enesaitech.barbulak.entity.RawPlace;
import kg.enesaitech.barbulak.entity.ShippingStatus;
import kg.enesaitech.barbulak.entity.StatusDate;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductionProcessService {
	
	@Autowired
	private GenericHome<ProductionProcess> productionProcessHome;	
	@Autowired
	private ProductionProcessProductHome productionProcessProductHome;
	@Autowired
	private ProductPlaceHome productPlaceHome;
	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private GenericHome<ShippingStatus> shippingStatusHome;
	@Autowired
	private UserCheckService checkService;
	@Autowired
	private RawPlaceHome rawPlaceHome;
	@Autowired
	private IngredientHome ingredientHome;
	@Autowired
	private GenericHome<StatusDate> statusDateHome;
	private DecimalFormat decimalFormat = new DecimalFormat("#.######");
	private DecimalFormat decimalFormat2 = new DecimalFormat("#");

	@Transactional
	public void create(ProductionProcess productionProcess) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("production_manager")){
//			Place toPlace = placeHome.findById(Place.class, productionProcess.getToPlaceId().getId());
			Place proizvodstvo = placeHome.getProizvodstvo();
			if(checkService.isStaffIsCurrentUser(proizvodstvo.getStaff().getId())){
				Set<ProductionProcessProduct> productionProcesses = productionProcess.getProductionProcessProducts();
				checkIngredients(productionProcesses);
				productionProcess.setProductionProcessProducts(null);
				StatusDate statusDate = new StatusDate();
				statusDate.setCreated(new Date());
				Integer statusDateId = statusDateHome.persist(statusDate);
				statusDate.setId(statusDateId);
				productionProcess.setStatusDate(statusDate);
				productionProcess.setStatus(shippingStatusHome.findById(ShippingStatus.class, 1));
				Integer productionProcessId = productionProcessHome.persist(productionProcess);
				productionProcess.setId(productionProcessId);
				for(ProductionProcessProduct processProduct : productionProcesses){
					processProduct.setProductionProcess(productionProcess);
					productionProcessProductHome.persist(processProduct);
				}
			} else
				throw new BusinessException("Вы не имеете права для покупки!! Вы не ответственны за "+proizvodstvo.getName()+" !!");		
			
		} else
			throw new BusinessException("Вы не имеете права для покупки!!!");		

		
		
	}
	
	@Transactional
	public void toPlaceApprove(int productionProcessId) {
		ProductionProcess productionProcess = productionProcessHome.findById(ProductionProcess.class, productionProcessId);
		Place toPlace = placeHome.findById(Place.class, productionProcess.getToPlaceId().getId());
		if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
			if(productionProcess.getStatus().getId()==4){
				if(toPlace.getPlaceType().getName().equals("склад") ){
					List<ProductionProcessProduct> productionProcessProducts = productionProcessProductHome.getByProductionProcessId(productionProcess.getId());
					for(ProductionProcessProduct processProduct : productionProcessProducts){
								
						ProductPlace productPlace = productPlaceHome.getByProductIdByPlaceId(processProduct.getProduct().getId(), toPlace.getId());
						productPlace.setAmount(productPlace.getAmount()+processProduct.getProductionAmount());
						productPlaceHome.merge(productPlace);
					}
					productionProcess.getStatusDate().setApprovedToPlace(new Date());
					productionProcess.setStatus(shippingStatusHome.findById(ShippingStatus.class, 6));
					productionProcessHome.merge(productionProcess);
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является складом!!");
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		

	}
	

	@Transactional
	public void toPlaceReject(int productionProcessId) {
		ProductionProcess productionProcess = productionProcessHome.findById(ProductionProcess.class, productionProcessId);
		Place toPlace = placeHome.findById(Place.class, productionProcess.getToPlaceId().getId());
		if(checkService.isStaffIsCurrentUser(toPlace.getStaff().getId())){
			if(productionProcess.getStatus().getId()==4){
				if(toPlace.getPlaceType().getName().equals("склад") ){					
					productionProcess.getStatusDate().setRejectedToPlace(new Date());
					productionProcess.setStatus(shippingStatusHome.findById(ShippingStatus.class, 7));
					productionProcessHome.merge(productionProcess);
				} else
					throw new BusinessException("Вы не имеете права для подтверждения!! "+toPlace.getName()+" не является складом!!");
			} else
				throw new BusinessException("Вы не имеете права для подтверждения!! Транспортировка не  отправлена !!");		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!! Вы не ответственны за "+toPlace.getName()+" !!");		

	}

	@Transactional
	public void productionApprove(ProductionProcess p) {
		Set<ProductionProcessProduct> productionProcessProducts = p.getProductionProcessProducts();
		ProductionProcess productionProcess = productionProcessHome.findById(ProductionProcess.class, p.getId());
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("production_manager")){
			if(productionProcess.getStatus().getId()==2){
				Place proizvodstvo = placeHome.getProizvodstvo();
				checkIngredientsByProduction(productionProcessProducts);
				for(ProductionProcessProduct processProduct : productionProcessProducts){
					SearchParameters searchParameters = new SearchParameters();
					searchParameters.getSearchParameters().put("product.id", processProduct.getProduct().getId()+"");
					SearchResult<Ingredient> ingredients = ingredientHome.getList(searchParameters, Ingredient.class);
					for(Ingredient ingredient : ingredients.getResultList()){
						RawPlace rawPlace = rawPlaceHome.getByRawIdByPlaceId(ingredient.getRaw().getId(), proizvodstvo.getId());
						if(rawPlace.getAmount()<ingredient.getAmount()*processProduct.getProductionAmount()){
							throw new BusinessException("Невозможно производить "+processProduct.getProduct().getName()+", сырье "+rawPlace.getRaw().getName()+" нет в "+proizvodstvo.getName()+" !!");						
						} else{
							rawPlace.setAmount(rawPlace.getAmount()-ingredient.getAmount()*processProduct.getProductionAmount());
							rawPlaceHome.merge(rawPlace);
						}
					}
					processProduct.setProductionProcess(productionProcess);
					productionProcessProductHome.merge(processProduct);
				}
				
				productionProcess.getStatusDate().setApprovedFromPlace(new Date());
				productionProcess.setStatus(shippingStatusHome.findById(ShippingStatus.class, 4));
				productionProcessHome.merge(productionProcess);
			} else
				throw new BusinessException("Данный запрос НЕ подтвержден администратором !!!");		
		
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");		
	}
	
	@Transactional
	public void adminApprove(int productionProcessId) {
		ProductionProcess productionProcess = productionProcessHome.findById(ProductionProcess.class, productionProcessId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (productionProcess.getStatus().getId()==1){
				productionProcess.getStatusDate().setApprovedAdmin(new Date());
				productionProcess.setStatus(shippingStatusHome.findById(ShippingStatus.class, 2));
				productionProcessHome.merge(productionProcess);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя подтверждать еще раз!!!");
		} else
			throw new BusinessException("Вы не имеете права для подтверждения!!!");		

	}

	@Transactional
	public void adminReject(int productionProcessId) {
		ProductionProcess productionProcess = productionProcessHome.findById(ProductionProcess.class, productionProcessId);
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin")){
			if (productionProcess.getStatus().getId()==1){
				productionProcess.setStatus(shippingStatusHome.findById(ShippingStatus.class, 3));
				productionProcess.getStatusDate().setRejectedAdmin(new Date());
				productionProcessHome.merge(productionProcess);
			} else
				throw new BusinessException("Данный запрос был подтвержден администратором и нельзя отменить!!!");		
			
		} else
			throw new BusinessException("Вы не имеете права для отмены!!!");		

	}	
	@Transactional
	public void update(ProductionProcess productionProcess) {

		Set<ProductionProcessProduct> processProducts = productionProcess.getProductionProcessProducts();
		ProductionProcess productionProcess2 = productionProcessHome.findById(ProductionProcess.class, productionProcess.getId());
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("production_manager")){
				Place proizvodstvo = placeHome.getProizvodstvo();
				productionProcessProductHome.deleteByProductionProcessId(productionProcess2.getId());
				for(ProductionProcessProduct processProduct : processProducts){
					SearchParameters searchParameters = new SearchParameters();
					searchParameters.getSearchParameters().put("product.id", processProduct.getProduct().getId()+"");
					SearchResult<Ingredient> ingredients = ingredientHome.getList(searchParameters, Ingredient.class);
					for(Ingredient ingredient : ingredients.getResultList()){
						RawPlace rawPlace = rawPlaceHome.getByRawIdByPlaceId(ingredient.getRaw().getId(), proizvodstvo.getId());
						if(rawPlace.getAmount()<ingredient.getAmount()*processProduct.getPlannedAmount()){
							throw new BusinessException("Невозможно планировать " +ingredient.getProduct().getName()+", сырье "+rawPlace.getRaw().getName()+" нет в "+proizvodstvo.getName()+" !!");						
						}
					}
					processProduct.setProductionProcess(productionProcess2);
					productionProcessProductHome.persist(processProduct);
				}
				productionProcess.setStatusDate(productionProcess2.getStatusDate());
				productionProcess.getStatusDate().setCreated(new Date());
				productionProcessHome.merge(productionProcess);
		} else 
			throw new BusinessException("Вы не имеете права для редактирования!!!");		
		
		
	}

	@Transactional
	public void checkIngredients(Set<ProductionProcessProduct> processProducts) {
		Place proizvodstvo = placeHome.getProizvodstvo();
		Map<Integer, Double> rawIdAmounts = new HashMap<Integer, Double>();
		for(ProductionProcessProduct processProduct : processProducts){
			SearchParameters searchParameters = new SearchParameters();
			searchParameters.getSearchParameters().put("product.id", processProduct.getProduct().getId()+"");
			SearchResult<Ingredient> ingredients = ingredientHome.getList(searchParameters, Ingredient.class);
			for(Ingredient ingredient : ingredients.getResultList()){
				if(rawIdAmounts.get(ingredient.getRaw().getId())==null){
					rawIdAmounts.put(ingredient.getRaw().getId(), ingredient.getAmount()*processProduct.getPlannedAmount());
				}else{
					Double amount = rawIdAmounts.get(ingredient.getRaw().getId());
					rawIdAmounts.remove(ingredient.getRaw().getId());
					rawIdAmounts.put(ingredient.getRaw().getId(), ingredient.getAmount()*processProduct.getPlannedAmount()+amount);
				}
							
			}
			
		}
		boolean isFalse = false;
		String message= "Для планирования необходимо: ";
		for (Entry<Integer, Double> rawAmount : rawIdAmounts.entrySet()){
			RawPlace rawPlace = rawPlaceHome.getByRawIdByPlaceId(rawAmount.getKey(), proizvodstvo.getId());
			if(rawPlace.getAmount()<rawAmount.getValue()){
				isFalse = true;
				if(rawPlace.getRaw().getUnit().getId()==3){
					message = message +rawPlace.getRaw().getName()+" - '"+decimalFormat2.format((rawAmount.getValue()-rawPlace.getAmount()))+"' "+rawPlace.getRaw().getUnit().getName()+", ";
				} else {
					message = message +rawPlace.getRaw().getName()+" - '"+decimalFormat.format((rawAmount.getValue()-rawPlace.getAmount()))+"' "+rawPlace.getRaw().getUnit().getName()+", ";
				}
				
			}	
		}
		if(isFalse){
			message = message.substring(0, message.length()-2) +".";
			throw new BusinessException(message);						
			
		}
	}	
	
	@Transactional
	public void checkIngredientsByProduction(Set<ProductionProcessProduct> processProducts) {
		Place proizvodstvo = placeHome.getProizvodstvo();
		Map<Integer, Double> rawIdAmounts = new HashMap<Integer, Double>();
		for(ProductionProcessProduct processProduct : processProducts){
			SearchParameters searchParameters = new SearchParameters();
			searchParameters.getSearchParameters().put("product.id", processProduct.getProduct().getId()+"");
			SearchResult<Ingredient> ingredients = ingredientHome.getList(searchParameters, Ingredient.class);
			for(Ingredient ingredient : ingredients.getResultList()){
				if(rawIdAmounts.get(ingredient.getRaw().getId())==null){
					rawIdAmounts.put(ingredient.getRaw().getId(), ingredient.getAmount()*processProduct.getProductionAmount());
				}else{
					Double amount = rawIdAmounts.get(ingredient.getRaw().getId());
					rawIdAmounts.remove(ingredient.getRaw().getId());
					rawIdAmounts.put(ingredient.getRaw().getId(), ingredient.getAmount()*processProduct.getProductionAmount()+amount);
				}
							
			}
			
		}
		boolean isFalse = false;
		String message= "Для производство необходимо: ";
		for (Entry<Integer, Double> rawAmount : rawIdAmounts.entrySet()){
			RawPlace rawPlace = rawPlaceHome.getByRawIdByPlaceId(rawAmount.getKey(), proizvodstvo.getId());
			if(rawPlace.getAmount()<rawAmount.getValue()){
				isFalse = true;
				if(rawPlace.getRaw().getUnit().getId()==3){
					message = message +rawPlace.getRaw().getName()+" - '"+decimalFormat2.format((rawAmount.getValue()-rawPlace.getAmount()))+"' "+rawPlace.getRaw().getUnit().getName()+", ";
				} else {
					message = message +rawPlace.getRaw().getName()+" - '"+decimalFormat.format((rawAmount.getValue()-rawPlace.getAmount()))+"' "+rawPlace.getRaw().getUnit().getName()+", ";
				}
				
			}	
		}
		if(isFalse){
			message = message.substring(0, message.length()-2) +".";
			throw new BusinessException(message);						
			
		}
	}	
	

}