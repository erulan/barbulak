package kg.enesaitech.barbulak.service;

import java.util.List;
import java.util.Set;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.StaffHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.Client;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientService {
	
	@Autowired
	private StaffHome staffHome;
	@Autowired
	private GenericHome<Client> clientHome;
	@Autowired
	private UsersHome usersHome;
	@Autowired
	private CashboxAccountantHome cashboxAccountantHome;

	@Transactional
	public void create(Client client) {
		

		client.setPersonNumber(getPersonNumber());
		clientHome.persist(client);
	}
	
	public String getPersonNumber(){
		String personNo = "99999";
		
		Set<String> personNumbers = staffHome.personNumList();
		
		//Person number must be between 10000 and 50000;
		for(int number = 50000; number < 100000; number++){
			if(!personNumbers.contains(Integer.toString(number))){
				personNo = Integer.toString(number);
				break;
			}
		}
		
		return personNo;
	}

	@Transactional
	public SearchResult<Client> getListByCashBox(SearchParameters searchParameters) {
		
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return clientHome.getList(searchParameters, Client.class);			
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			List<CashboxAccountant> cashboxAccountants = cashboxAccountantHome
					.getCashboxAccountants(currUser.getStaff().getId());
			if(cashboxAccountants.size()==0){
				throw new BusinessException("Доступ запрещен для кассы ");				
			}
			String cashboxID = null;
			if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("cashbox.id");
			}
			SearchResult<Client> searchResult = new SearchResult<Client>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
						searchParameters.getSearchParameters().remove("cashbox.id");
					}
					searchParameters.getSearchParameters().put("cashbox.id", cashboxAccountant.getCashbox().getId()+"");
					SearchResult<Client> buffer = clientHome.getList(searchParameters, Client.class);
					searchResult.getResultList().addAll(buffer.getResultList());				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						return clientHome.getList(searchParameters, Client.class);
					}else{						
					}
					
				}
				}
			return searchResult;
		} else{
			throw new BusinessException("Доступ запрещен для кассы ");
		}
	}	
	
}