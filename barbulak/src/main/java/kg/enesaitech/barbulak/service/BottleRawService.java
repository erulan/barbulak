package kg.enesaitech.barbulak.service;

import java.util.List;

import kg.enesaitech.barbulak.entity.BottleRaw;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BottleRawService {
	
	@Autowired
	private GenericHome<BottleRaw> genericBottleRawHome;

	
	@Transactional
	public boolean createList(List<BottleRaw> bollteRaws){
		
		SearchResult<BottleRaw> bottleRawList = genericBottleRawHome.getList(new SearchParameters(), BottleRaw.class);
		for(BottleRaw bottleRaw : bottleRawList.getResultList()){
			genericBottleRawHome.remove(bottleRaw);
		}
		
		for(BottleRaw bottleRaw : bollteRaws){
			genericBottleRawHome.persist(bottleRaw);
		}
		return true;
	}
	
}