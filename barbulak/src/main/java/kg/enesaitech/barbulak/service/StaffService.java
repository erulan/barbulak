package kg.enesaitech.barbulak.service;

import java.util.List;
import java.util.Set;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.StaffHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.Position;
import kg.enesaitech.barbulak.entity.Staff;
import kg.enesaitech.barbulak.entity.StaffPosition;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.generics.GenericService;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;
import kg.enesaitech.barbulak.vo.StaffPositionList;
import kg.enesaitech.barbulak.vo.StaffPositionVO;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StaffService {
	
	@Autowired
	protected DozerBeanMapper mapper;	
	
	@Autowired
	private StaffHome staffHome;
	@Autowired
	private GenericService<Staff, GenericHome<Staff>> genericStaffService;
	@Autowired
	private GenericService<Position, GenericHome<Position>> genericPositionService;
	@Autowired
	private GenericHome<StaffPosition> staffPositionGenericHome;
	@Autowired
	private UsersHome usersHome;
	@Autowired
	private CashboxAccountantHome cashboxAccountantHome;

	@Transactional
	public void create(Staff staff) {
		

		staff.setPersonNumber(getPersonNumber());
		staffHome.persist(staff);
	}
	
	public String getPersonNumber(){
		String personNo = "49999";
		
		Set<String> personNumbers = staffHome.personNumList();
		
		//Person number must be between 10000 and 50000;
		for(int number = 10000; number < 50000; number++){
			if(!personNumbers.contains(Integer.toString(number))){
				personNo = Integer.toString(number);
				break;
			}
		}
		
		return personNo;
	}

	@Transactional
	public SearchResult<Staff> getListByCashBox(SearchParameters searchParameters) {
		
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return staffHome.getList(searchParameters, Staff.class);			
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			List<CashboxAccountant> cashboxAccountants = cashboxAccountantHome
					.getCashboxAccountants(currUser.getStaff().getId());
			if(cashboxAccountants.size()==0){
				throw new BusinessException("Доступ запрещен для кассы ");				
			}
			String cashboxID = null;
			if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("cashbox.id");
			}
			SearchResult<Staff> searchResult = new SearchResult<Staff>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
						searchParameters.getSearchParameters().remove("cashbox.id");
					}
					searchParameters.getSearchParameters().put("cashbox.id", cashboxAccountant.getCashbox().getId()+"");
					SearchResult<Staff> buffer = staffHome.getList(searchParameters, Staff.class);
					searchResult.getResultList().addAll(buffer.getResultList());				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						return staffHome.getList(searchParameters, Staff.class);
					}else{						
					}
					
				}
				}
			return searchResult;
		} else{
			throw new BusinessException("Доступ запрещен для кассы ");
		}
	}

	@Transactional
	public void createWithPositions(StaffPositionList staffPositionList) {
		Staff staff = mapper.map(staffPositionList.getStaff(), Staff.class);
		staff.setPersonNumber(getPersonNumber());
		for(Integer positionId : staffPositionList.getPositionIdList()){
			staff.getPositions().add(genericPositionService.get(Position.class, positionId));
		}
		
		staffHome.persist(staff);
	}

	@Transactional
	public void updateWithPosition(StaffPositionList staffPositionList) {
		Staff staff = mapper.map(staffPositionList.getStaff(), Staff.class);
		
		for(Integer positionId : staffPositionList.getPositionIdList()){
			staff.getPositions().add(genericPositionService.get(Position.class, positionId));
		}
		
		genericStaffService.update(staff);
	}	
	
}