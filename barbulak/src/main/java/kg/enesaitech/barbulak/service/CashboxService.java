package kg.enesaitech.barbulak.service;

import java.util.List;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.Cashbox;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.Client;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CashboxService {
	
	@Autowired
	private GenericHome<Client> clientHome;
	@Autowired
	private UsersHome usersHome;
	@Autowired
	private CashboxAccountantHome cashboxAccountantHome;
	@Autowired
	private GenericHome<Cashbox> cashboxHome;

	
	@Transactional
	public SearchResult<Cashbox> getListByCashBox(SearchParameters searchParameters) {
		
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return cashboxHome.getList(searchParameters, Cashbox.class);
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			List<CashboxAccountant> cashboxAccountants = cashboxAccountantHome
					.getCashboxAccountants(currUser.getStaff().getId());
			if(cashboxAccountants.size()==0){
				throw new BusinessException("Доступ запрещен для кассы ");				
			}
			String cashboxID = null;
			if(searchParameters.getSearchParameters().get("cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("cashbox.id");
			}
			SearchResult<Cashbox> searchResult = new SearchResult<Cashbox>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					
					searchResult.getResultList().add(cashboxHome.findById(Cashbox.class, cashboxAccountant.getCashbox().getId()));				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						searchResult.getResultList().add(cashboxHome.findById(Cashbox.class, cashboxAccountant.getCashbox().getId()));				
					}else{						
					}
					
				}
			}
			return searchResult;
		} else{
			throw new BusinessException("Доступ запрещен для кассы ");
		}
	}	
	
}