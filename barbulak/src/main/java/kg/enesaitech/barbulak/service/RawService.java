package kg.enesaitech.barbulak.service;

import java.util.List;

import kg.enesaitech.barbulak.dao.PlaceHome;
import kg.enesaitech.barbulak.dao.RawHome;
import kg.enesaitech.barbulak.dao.RawPlaceHome;
import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.entity.Raw;
import kg.enesaitech.barbulak.entity.RawPlace;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RawService {
	
	@Autowired
	private RawHome rawHome;
	@Autowired
	private PlaceHome placeHome;
	@Autowired
	private RawPlaceHome rawPlaceHome;

	@Transactional
	public void create(Raw raw) {
		Integer rawId = rawHome.create(raw);
		raw.setId(rawId);
		Place place = placeHome.getProizvodstvo();
			RawPlace rawPlace = new RawPlace();
			rawPlace.setRaw(raw);
			rawPlace.setPlace(place);
			rawPlace.setAmount(0.0);
			rawPlaceHome.persist(rawPlace);
	}


	@Transactional
	public void delete(int id) {
		Raw raw = rawHome.findById(Raw.class, id);
		boolean isExists=false;
		List<RawPlace> rawPlaces = rawPlaceHome.getRawPlacesByRawId(id);
		for (RawPlace rawPlace: rawPlaces){
			if(rawPlace.getAmount()!=0){
				isExists=true;
			}
		}
		if(!isExists){
			rawPlaceHome.deleteByProductId(id);
		} else {
			throw new BusinessException("Невозможно удалить "+raw.getName()+" сырье который есть в складе !!");	
		}
		rawHome.remove(raw);
	}	
	
	@Transactional
	public SearchResult<RawPlace> getMinStockAmount(SearchParameters searchParameters) {
		if (searchParameters.getSearchParameters().get("raw.name") != null) {
			searchParameters.getSearchParameters().put("rawName", searchParameters.getSearchParameters().get("raw.name"));
		}
		if (searchParameters.getSearchParameters().get("raw.unit.name") != null) {
			searchParameters.getSearchParameters().put("unitName", searchParameters.getSearchParameters().get("raw.unit.name"));
		}
		if (searchParameters.getSearchParameters().get("raw.minStockAmount") != null) {
			searchParameters.getSearchParameters().put("minStockAmount", searchParameters.getSearchParameters().get("raw.minStockAmount"));
		}
			return rawPlaceHome.getMinStockAmount(searchParameters);
		
	}	
}