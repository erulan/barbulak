package kg.enesaitech.barbulak.service;

import java.util.Map.Entry;

import kg.enesaitech.barbulak.dao.IngredientHome;
import kg.enesaitech.barbulak.entity.Ingredient;
import kg.enesaitech.barbulak.entity.Product;
import kg.enesaitech.barbulak.entity.Raw;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.vo.IngredientRaw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IngredientService {
	
	@Autowired
	private GenericHome<Product> productHome;	
	@Autowired
	private GenericHome<Raw> rawHome;
	@Autowired
	private IngredientHome ingredientHome;

	@Transactional
	public void create(IngredientRaw ingredientRaw) {
		Product product = productHome.findById(Product.class, ingredientRaw.getProduct().getId());
		ingredientHome.deleteByProductId(product.getId());
		for(Entry<Integer, Double> rawIdAmountEntry : ingredientRaw.getRawIdAmount().entrySet()){
			
			Ingredient ingredient = new Ingredient();
			ingredient.setProduct(product);
			ingredient.setRaw(rawHome.findById(Raw.class, rawIdAmountEntry.getKey()));
			ingredient.setAmount(rawIdAmountEntry.getValue());
			ingredientHome.persist(ingredient);
		}
		
	}

}