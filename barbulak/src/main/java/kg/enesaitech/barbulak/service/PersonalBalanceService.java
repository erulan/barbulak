package kg.enesaitech.barbulak.service;

import java.util.List;

import kg.enesaitech.barbulak.dao.CashboxAccountantHome;
import kg.enesaitech.barbulak.dao.PersonalBalanceHome;
import kg.enesaitech.barbulak.dao.UsersHome;
import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.entity.PersonalBalance;
import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonalBalanceService {

	@Autowired
	private PersonalBalanceHome personalBalanceHome;
	@Autowired
	protected UsersHome usersHome;
	@Autowired
	protected CashboxAccountantHome cashboxAccountantHome;


	@Transactional
	public SearchResult<PersonalBalance> getList(SearchParameters searchParameters) {
		
		// do service select * from cahsboxaccountant by accountant if current user is accountant 
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("admin") || currentUserSubj.hasRole("manager")){
			return personalBalanceHome.getList(searchParameters, PersonalBalance.class);			
		} else if(currentUserSubj.hasRole("accountant")){
			Users currUser = usersHome.getByUserName(currentUserSubj.getPrincipal()
					.toString());
			List<CashboxAccountant> cashboxAccountants = cashboxAccountantHome
					.getCashboxAccountants(currUser.getStaff().getId());
			if(cashboxAccountants.size()==0){
				throw new BusinessException("Доступ запрещен для кассы ");				
			}
			String cashboxID = null;
			if(searchParameters.getSearchParameters().get("person.cashbox.id")!=null){
				cashboxID = searchParameters.getSearchParameters().get("person.cashbox.id");
			}
			SearchResult<PersonalBalance> searchResult = new SearchResult<PersonalBalance>();
			for (CashboxAccountant cashboxAccountant : cashboxAccountants){
				if(cashboxID==null){
					if(searchParameters.getSearchParameters().get("person.cashbox.id")!=null){
						searchParameters.getSearchParameters().remove("person.cashbox.id");
					}
					searchParameters.getSearchParameters().put("person.cashbox.id", cashboxAccountant.getCashbox().getId()+"");
					SearchResult<PersonalBalance> buffer = personalBalanceHome.getList(searchParameters, PersonalBalance.class);
					searchResult.getResultList().addAll(buffer.getResultList());				
				}else{
					if(cashboxID.equals(cashboxAccountant.getCashbox().getId()+"")){
						return personalBalanceHome.getList(searchParameters, PersonalBalance.class);
					}else{						
					}
					
				}
				}
			return searchResult;
		} else{
			throw new BusinessException("Доступ запрещен для кассы ");
		}
	}

}