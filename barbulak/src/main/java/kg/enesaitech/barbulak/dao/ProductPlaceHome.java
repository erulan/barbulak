package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.ProductPlace;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class ProductPlaceHome extends GenericHome<ProductPlace>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void deleteByPlaceId(int placeId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM ProductPlace b "
					+ "WHERE b.place.id = :placeId");
			query.setParameter("placeId", placeId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("delete by place id failed", re);
			throw re;
		}
	}
	
	public void deleteByProductId(int ProductId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM ProductPlace b "
					+ "WHERE b.product.id = :ProductId");
			query.setParameter("ProductId", ProductId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("delete by product id failed", re);
			throw re;
		}
	}
	
	public List<ProductPlace> getProductPlacesByPlaceId(int placeId) {
		try{
			Query query = entityManager.createQuery("FROM ProductPlace b "
					+ "Where b.place.id = :placeId ");
			query.setParameter("placeId", placeId);			
			@SuppressWarnings("unchecked")
			List<ProductPlace> productPlaces = query.getResultList();
			return productPlaces;
		} catch (RuntimeException re) {
			log.error("productPlaces by PlaceID list error", re);
			throw re;
		}
	}	

	public List<ProductPlace> getProductPlacesByProductId(int productId) {
		try{
			Query query = entityManager.createQuery("FROM ProductPlace b "
					+ "Where b.product.id = :productId ");
			query.setParameter("productId", productId);			
			@SuppressWarnings("unchecked")
			List<ProductPlace> productPlaces = query.getResultList();
			return productPlaces;
		} catch (RuntimeException re) {
			log.error("productPlaces by ProductID list error", re);
			throw re;
		}
	}
	
	public ProductPlace getByProductIdByPlaceId(Integer productId, Integer placeId) {
		try {
			Query query = entityManager.createQuery("from ProductPlace b where b.product.id = :productId and b.place.id = :placeId ");
			query.setParameter("productId", productId);
			query.setParameter("placeId", placeId);
			ProductPlace productPlace = (ProductPlace) query.getSingleResult();
			return productPlace;
		} catch (RuntimeException re) {
			log.error("ProductPlace by productId by placeID get error", re);
			throw re;
		}
	}

}
