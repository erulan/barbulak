package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.Raw;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class RawHome extends GenericHome<Raw>  {


	@PersistenceContext
	private EntityManager entityManager;
	
	public Integer create(Raw transientInstance) {
		Integer rawId = null;
		log.debug("persisting Raw instance");
		try {
			entityManager.persist(transientInstance);
			entityManager.flush();
			rawId = transientInstance.getId();
			log.debug("Raw persist successful");
			return rawId;
		} catch (RuntimeException re) {
			log.error("Raw persist failed", re);
			throw re;
		}
	}

	public List<Raw> getRaws() {
		try{
			Query query = entityManager.createQuery("FROM Raw");
			@SuppressWarnings("unchecked")
			List<Raw> raws = query.getResultList();
			return raws;
		} catch (RuntimeException re) {
			log.error("Raw list error", re);
			throw re;
		}
	}	

}
