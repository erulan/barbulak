package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.BottlePlace;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class BottlePlaceHome extends GenericHome<BottlePlace>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void deleteByPlaceId(int placeId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM BottlePlace b "
					+ "WHERE b.place.id = :placeId");
			query.setParameter("placeId", placeId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("BottlePlace delete by place id failed", re);
			throw re;
		}
	}
	
	
	

	
	public BottlePlace getByPlaceId(Integer placeId) {
		try {
			Query query = entityManager.createQuery("from BottlePlace b where b.place.id = :placeId ");
			query.setParameter("placeId", placeId);
			BottlePlace rawPlace = (BottlePlace) query.getSingleResult();
			return rawPlace;
		} catch (RuntimeException re) {
			log.error("BottlePlace by placeID get error", re);
			throw re;
		}
	}

}
