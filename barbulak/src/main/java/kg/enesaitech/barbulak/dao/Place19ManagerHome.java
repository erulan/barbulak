package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.Place19Manager;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class Place19ManagerHome extends GenericHome<Place19Manager> {

	@PersistenceContext
	private EntityManager entityManager;

	
	public Place19Manager getByManagerId(int managerId) {
		try{
			Query query = entityManager.createQuery("FROM Place19Manager b where b.manager.id = :managerId ");
			query.setParameter("managerId", managerId);
			Place19Manager place19Manager = (Place19Manager) query.getSingleResult();
			return place19Manager;
		} catch (RuntimeException re) {
			return null;
		}
	}
}

