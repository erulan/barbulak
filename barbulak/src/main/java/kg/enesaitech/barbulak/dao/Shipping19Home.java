package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.Shipping19;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class Shipping19Home extends GenericHome<Shipping19>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Shipping19 getShipping19ByShippingId(int shippingId) {
		try{
			Query query = entityManager.createQuery("FROM Shipping19 b "
					+ "Where b.shipping.id = :shippingId ");
			query.setParameter("shippingId", shippingId);	
			Shipping19 shipping19 = (Shipping19) query.getSingleResult();
			return shipping19;
		} catch (RuntimeException re) {
			log.error("Shipping19 by shipping id result null", re);
			return null;
		}
	}	

	public void deleteByShippingId(int shippingId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM Shipping19 b "
					+ "WHERE b.shipping.id = :shippingId");
			query.setParameter("shippingId", shippingId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("Shipping19 delete by shippingId failed", re);
			throw re;
		}
	}
	
}
