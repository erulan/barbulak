package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.PersonalBalance;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class PersonalBalanceHome extends GenericHome<PersonalBalance>  {

	@PersistenceContext
	private EntityManager entityManager;

	public PersonalBalance getByPersonId(Integer personId) {
		try {
			Query query = entityManager.createQuery("from PersonalBalance b where b.person.id = :personId");
			query.setParameter("personId", personId);
			PersonalBalance personalBalance = (PersonalBalance) query.getSingleResult();
			return personalBalance;
		} catch (RuntimeException re) {
			return null;
		}
	}

}
