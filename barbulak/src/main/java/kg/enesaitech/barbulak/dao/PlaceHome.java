package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.Place;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class PlaceHome extends GenericHome<Place>  {

	@PersistenceContext
	private EntityManager entityManager;

	
	public Integer create(Place transientInstance) {
		Integer placeId = null;
		log.debug("persisting Place instance");
		try {
			entityManager.persist(transientInstance);
			entityManager.flush();
			placeId = transientInstance.getId();
			log.debug("persist successful");
			return placeId;
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}
	
	public List<Place> getSkladPlaces() {
		try{
			Query query = entityManager.createQuery("FROM Place b where b.placeType.name='склад' ");
			@SuppressWarnings("unchecked")
			List<Place> places = query.getResultList();
			return places;
		} catch (RuntimeException re) {
			log.error("place list error", re);
			throw re;
		}
	}
	public Place getProizvodstvo() {
		try{
			Query query = entityManager.createQuery("FROM Place b where b.placeType.name='производство' ");
			@SuppressWarnings("unchecked")
			List<Place> places = query.getResultList();
			return places.get(0);
		} catch (RuntimeException re) {
			log.error("get proizvodstvo error", re);
			throw re;
		}
	}
	
	public List<Place> getSkladAndProductionPlaces() {
		try{
			Query query = entityManager.createQuery("FROM Place b where b.placeType.name='склад' or b.placeType.name='производство' ");
			@SuppressWarnings("unchecked")
			List<Place> places = query.getResultList();
			return places;
		} catch (RuntimeException re) {
			log.error("place list error", re);
			throw re;
		}
	}

}
