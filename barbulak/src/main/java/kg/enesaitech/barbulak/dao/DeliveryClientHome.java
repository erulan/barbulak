package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.DeliveryClient;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class DeliveryClientHome extends GenericHome<DeliveryClient>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void deleteByDeliveryId(Integer deliveryId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM DeliveryClient b "
					+ "WHERE b.delivery.id = :deliveryId");
			query.setParameter("deliveryId", deliveryId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("DeliveryClient delete by deliveryId id failed", re);
			throw re;
		}
	}
	
	
	

	
	public List<DeliveryClient> getByDeliveryId(Integer deliveryId) {
		try {
			Query query = entityManager.createQuery("from DeliveryClient b where b.delivery.id = :deliveryId ");
			query.setParameter("deliveryId", deliveryId);
			@SuppressWarnings("unchecked")
			List<DeliveryClient> wareDistProducts = query.getResultList();
			return wareDistProducts;
		} catch (RuntimeException re) {
			log.error("DeliveryClient by deliveryId get error", re);
			throw re;
		}
	}

}
