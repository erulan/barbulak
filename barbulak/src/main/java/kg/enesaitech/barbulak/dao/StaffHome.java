package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import kg.enesaitech.barbulak.entity.Staff;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class StaffHome extends GenericHome<Staff>  {

	@PersistenceContext
	private EntityManager entityManager;
	

	public Set<String> personNumList() {
		Set<String> staffNumbers = new HashSet<String>();
		try {
			staffNumbers = new HashSet<String>(entityManager.createQuery(" select personNumber from Person b ").getResultList());
		} catch (RuntimeException re) {
			log.error("staff num error", re);
		}
		return staffNumbers;
	}
}
