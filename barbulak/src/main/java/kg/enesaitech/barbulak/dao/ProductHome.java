package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.Product;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class ProductHome extends GenericHome<Product>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Integer create(Product transientInstance) {
		Integer productId = null;
		log.debug("persisting Product instance");
		try {
			entityManager.persist(transientInstance);
			entityManager.flush();
			productId = transientInstance.getId();
			log.debug("persist successful");
			return productId;
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public List<Product> getProducts() {
		try{
			Query query = entityManager.createQuery("FROM Product");
			@SuppressWarnings("unchecked")
			List<Product> products = query.getResultList();
			return products;
		} catch (RuntimeException re) {
			log.error("product list error", re);
			throw re;
		}
	}	

}
