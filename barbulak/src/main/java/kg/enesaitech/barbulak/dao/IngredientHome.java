package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.Ingredient;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class IngredientHome extends GenericHome<Ingredient>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void deleteByProductId(int productId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM Ingredient b "
					+ "WHERE b.product.id = :productId");
			query.setParameter("productId", productId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("Ingredient delete by productId id failed", re);
			throw re;
		}
	}

}