package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.RawPurchase;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class RawPurchaseHome extends GenericHome<RawPurchase>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public List<RawPurchase> getByPurchaseId(int purchaseId) {
		try{
			Query query = entityManager.createQuery("FROM RawPurchase b "
					+ "Where b.purchase.id = :purchaseId ");
			query.setParameter("purchaseId", purchaseId);			
			@SuppressWarnings("unchecked")
			List<RawPurchase> rawPurchases = query.getResultList();
			return rawPurchases;
		} catch (RuntimeException re) {
			log.error("rawPurchases by purchase id list error", re);
			throw re;
		}
	}	
	public void deleteByPurchaseId(int purchaseId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM RawPurchase b "
					+ "WHERE b.purchase.id = :purchaseId");
			query.setParameter("purchaseId", purchaseId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("RawPurchase delete by purchase id failed", re);
			throw re;
		}
	}
	
}
