package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.ShippingByProduct;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class ShippingByProductHome extends GenericHome<ShippingByProduct>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public List<ShippingByProduct> getShippingByProductByShippingId(int shippingId) {
		try{
			Query query = entityManager.createQuery("FROM ShippingByProduct b "
					+ "Where b.shipping.id = :shippingId ");
			query.setParameter("shippingId", shippingId);			
			@SuppressWarnings("unchecked")
			List<ShippingByProduct> shippingByProducts = query.getResultList();
			return shippingByProducts;
		} catch (RuntimeException re) {
			log.error("shippingByProducts by shipping id list error", re);
			throw re;
		}
	}	
	public void deleteByShippingId(int shippingId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM ShippingByProduct b "
					+ "WHERE b.shipping.id = :shippingId");
			query.setParameter("shippingId", shippingId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("ShippingByProduct delete by shippingId failed", re);
			throw re;
		}
	}
	
}
