package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.TransactionCategory;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class TransactionCategoryHome extends GenericHome<TransactionCategory>  {

	@PersistenceContext
	private EntityManager entityManager;

	public TransactionCategory getByName(String name) {
		try {
			Query query = entityManager.createQuery("from TransactionCategory b where b.name = :name");
			query.setParameter("name", name);
			TransactionCategory personalTransactionCategory = (TransactionCategory) query.getSingleResult();
			return personalTransactionCategory;
		} catch (RuntimeException re) {
			return null;
		}
	}

}
