package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.RawPlace;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class RawPlaceHome extends GenericHome<RawPlace>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void deleteByPlaceId(int placeId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM RawPlace b "
					+ "WHERE b.place.id = :placeId");
			query.setParameter("placeId", placeId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("RawPlace delete by place id failed", re);
			throw re;
		}
	}
	
	public void deleteByProductId(int rawId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM RawPlace b "
					+ "WHERE b.raw.id = :rawId");
			query.setParameter("rawId", rawId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("RawPlace delete by rawId id failed", re);
			throw re;
		}
	}
	
	public List<RawPlace> getRawPlacesByPlaceId(int placeId) {
		try{
			Query query = entityManager.createQuery("FROM RawPlace b "
					+ "Where b.place.id = :placeId ");
			query.setParameter("placeId", placeId);			
			@SuppressWarnings("unchecked")
			List<RawPlace> productPlaces = query.getResultList();
			return productPlaces;
		} catch (RuntimeException re) {
			log.error("RawPlaces by PlaceID list error", re);
			throw re;
		}
	}	

	public List<RawPlace> getRawPlacesByRawId(int rawId) {
		try{
			Query query = entityManager.createQuery("FROM RawPlace b "
					+ "Where b.raw.id = :rawId ");
			query.setParameter("rawId", rawId);			
			@SuppressWarnings("unchecked")
			List<RawPlace> productPlaces = query.getResultList();
			return productPlaces;
		} catch (RuntimeException re) {
			log.error("RawPlaces by rawID list error", re);
			throw re;
		}
	}

	
	public RawPlace getByRawIdByPlaceId(Integer rawId, Integer placeId) {
		try {
			Query query = entityManager.createQuery("from RawPlace b where b.raw.id = :rawId and b.place.id = :placeId ");
			query.setParameter("rawId", rawId);
			query.setParameter("placeId", placeId);
			RawPlace rawPlace = (RawPlace) query.getSingleResult();
			return rawPlace;
		} catch (RuntimeException re) {
			log.error("RawPlace by rawID by placeID get error", re);
			throw re;
		}
	}
//	public List<RawPlace> getAlertMin() {
//		try{
//			Query query = entityManager.createQuery("FROM RawPlace b "
//					+ "Where b.amount <= b.raw.minStockAmount and b.place.placeType.name == 'производство' ");		
//			@SuppressWarnings("unchecked")
//			List<RawPlace> productPlaces = query.getResultList();
//			return productPlaces;
//		} catch (RuntimeException re) {
//			log.error("RawPlaces Alert by MIN STOCK AMOUNT list error", re);
//			throw re;
//		}
//	}
	public SearchResult<RawPlace> getMinStockAmount(SearchParameters searchParameters) {
		SearchResult<RawPlace> searchResult = new SearchResult<RawPlace>();
		Map<String, Boolean> queryParamIsString = new HashMap<String, Boolean>();
		
		String searchString = prepareQueryString(searchParameters, queryParamIsString);
		
			try {

				Query query = entityManager.createQuery(searchString);
				Query queryCount = entityManager.createQuery("Select count(*) " + searchString);

				//Set Parameters after creating Query. Parameters(queryParamIsString key->paramName, value->isString) prepared in Prepare Parameters part of prepareQueryString function
				for (Entry<String, Boolean> varName : queryParamIsString.entrySet()) {
				    query.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				    queryCount.setParameter(varName.getKey(), varName.getValue() ? "%"+searchParameters.getSearchParameters().get(varName.getKey()).toUpperCase()+"%" :searchParameters.getSearchParameters().get(varName.getKey()));
				}
				
				Integer totalData =  ((Number)queryCount.getSingleResult()).intValue();
				searchResult.setTotalRecords(totalData);
				if(searchParameters.getStartIndex() != null){
					query.setFirstResult(searchParameters.getStartIndex());
				}
				if(searchParameters.getResultQuantity() != null){
					query.setMaxResults(searchParameters.getResultQuantity());
				}
				@SuppressWarnings("unchecked")
				List<RawPlace> rawPlaces = query.getResultList();
				searchResult.setResultList(rawPlaces);
			} catch (RuntimeException re) {
				log.error("list error", re);
				throw re;
			}
		

		return searchResult;
	}
	
	private String prepareQueryString(SearchParameters searchParameters, Map<String, Boolean> queryParamIsString){
		String searchString = " FROM RawPlace b "
				+ "Where b.amount <= b.raw.minStockAmount and b.place.placeType.name = 'производство' ";
		List<String> searchStringList = new ArrayList<String>();
		List<String> orderList = new ArrayList<String>();
		
		//Prepare Parameters Part
		if (!searchParameters.getSearchParameters().isEmpty()) {
			if (searchParameters.getSearchParameters().get("rawName") != null) {
				searchStringList.add(" and upper(b.raw.name) like :rawName ");
				queryParamIsString.put("rawName", true);
			}
			if (searchParameters.getSearchParameters().get("unitName") != null) {
				searchStringList.add(" and upper(b.raw.unit.name) like :unitName ");
				queryParamIsString.put("unitName", true);
			}
			
			if (searchParameters.getSearchParameters().get("amount") != null) {
				searchStringList.add(" and cast(b.amount as string) = :amount ");
				queryParamIsString.put("amount", false);
			}
			
			if (searchParameters.getSearchParameters().get("minStockAmount") != null) {
				searchStringList.add(" and cast(b.raw.minStockAmount as string) = :minStockAmount ");
				queryParamIsString.put("minStockAmount", false);
			}
		}

		searchString += StringUtils.join(searchStringList, " ");
		//Set order by part
		if (!searchParameters.getOrderParamDesc().isEmpty()) {
			if (searchParameters.getOrderParamDesc().get("raw.name") != null) {
				orderList.add(" b.raw.name " + (searchParameters.getOrderParamDesc().get("raw.name") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("raw.unit.name") != null) {
				orderList.add(" b.raw.unit.name " + (searchParameters.getOrderParamDesc().get("raw.unit.name") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("amount") != null) {
				orderList.add(" b.amount " + (searchParameters.getOrderParamDesc().get("amount") ? " DESC " : " ASC "));
			}
			if (searchParameters.getOrderParamDesc().get("raw.minStockAmount") != null) {
				orderList.add(" b.raw.minStockAmount " + (searchParameters.getOrderParamDesc().get("raw.minStockAmount") ? " DESC " : " ASC "));
			}
			
			if (orderList.size() > 0) {
				searchString += " order by ";
			}
			
			searchString += StringUtils.join(orderList, " , ");
		}
		
		return searchString;
	}; 

}
