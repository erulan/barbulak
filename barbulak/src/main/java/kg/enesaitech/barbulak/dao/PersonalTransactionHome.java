package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import kg.enesaitech.barbulak.entity.Person;
import kg.enesaitech.barbulak.entity.PersonalTransaction;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.others.RequestComparisonType;
import kg.enesaitech.barbulak.service.UserCheckService;
import kg.enesaitech.barbulak.userException.BusinessException;
import kg.enesaitech.barbulak.vo.SearchParamWithType;
import kg.enesaitech.barbulak.vo.SearchParameters;
import kg.enesaitech.barbulak.vo.SearchResult;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class PersonalTransactionHome extends GenericHome<PersonalTransaction>  {

	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private UserCheckService checkService;
	@Autowired
	private GenericHome<Person> genericPersonHome;
	
	
	public Integer persist(PersonalTransaction transientInstance) {
		Person person = genericPersonHome.findById(Person.class, transientInstance.getPersonalBalance().getPerson().getId());
		if(checkService.isCurrentUserAdminOrAccountant(person.getCashbox().getId())){
			Integer entityId = null;
			log.debug("persisting Transaction instance");
			try {
				entityManager.persist(transientInstance);
				entityManager.flush();
				entityId = transientInstance.getId();
				log.debug("persist successful");
				return entityId;
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				throw re;
			}
		} else {
			throw new BusinessException("You cannot make a transaction!! You are not responsible for cashbox !!");		
		}
	}
	public Integer createStoreManager(PersonalTransaction transientInstance) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("store_manager")){
			Integer entityId = null;
			log.debug("persisting Transaction instance");
			try {
				entityManager.persist(transientInstance);
				entityManager.flush();
				entityId = transientInstance.getId();
				log.debug("persist successful");
				return entityId;
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				throw re;
			}
		}else{
			throw new BusinessException("Access Denied!!!");			
		}
	}
	public Integer create19Manager(PersonalTransaction transientInstance) {
		Subject currentUserSubj = SecurityUtils.getSubject();
		if(currentUserSubj.hasRole("manager_for_bottle")){
			Integer entityId = null;
			log.debug("persisting Transaction instance");
			try {
				entityManager.persist(transientInstance);
				entityManager.flush();
				entityId = transientInstance.getId();
				log.debug("persist successful");
				return entityId;
			} catch (RuntimeException re) {
				log.error("persist failed", re);
				throw re;
			}
		}else{
			throw new BusinessException("Access Denied!!!");			
		}
	}

	public void remove(PersonalTransaction persistentInstance) {
		if(checkService.isCurrentUserAdminOrAccountant(persistentInstance.getPersonalBalance().getPerson().getCashbox().getId())){
			log.debug("removing Transaction instance");
			try {
				entityManager.remove(persistentInstance);
				log.debug("remove successful");
			} catch (RuntimeException re) {
				log.error("remove failed", re);
				throw re;
			}
		} else {
			throw new BusinessException("You cannot make a personal transaction!! You are not responsible for cashbox !!");		
		}
	}

	public PersonalTransaction merge(PersonalTransaction detachedInstance) {
		if(checkService.isCurrentUserAdminOrAccountant(detachedInstance.getPersonalBalance().getPerson().getCashbox().getId())){
			log.debug("merging Transaction instance");
			try {
				PersonalTransaction result = entityManager.merge(detachedInstance);
				log.debug("merge successful");
				return result;
			} catch (RuntimeException re) {
				log.error("merge failed", re);
				throw re;
			}
		} else {
			throw new BusinessException("You cannot make a transaction!! You are not responsible for cashbox !!");		
		}
	}

	public PersonalTransaction findById(final Class<PersonalTransaction> type, Integer id) {
		log.debug("getting Transaction instance with id: " + id);
		try {
			PersonalTransaction instance = entityManager.find(type, id);
			log.debug("get successful");
			if(checkService.isCurrentUserAdminManagerAccountant(instance.getPersonalBalance().getPerson().getCashbox().getId())){
				return instance;
			} else {
				throw new BusinessException("You cannot make a transaction!! You are not responsible for cashbox !!");		
			}
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	
	public SearchResult<PersonalTransaction> getList(SearchParameters searchParameters, Class<PersonalTransaction> class_type) {
		log.debug("getting list Transaction  ");
		try {
			SearchResult<PersonalTransaction> searchResult = new SearchResult<PersonalTransaction>();
			
			// Query setups
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<PersonalTransaction> cQuery = builder.createQuery(class_type);
			CriteriaQuery<Long> cQueryCount = builder.createQuery(Long.class);
			Root<PersonalTransaction> entity = cQuery.from(class_type);
			
			// Query Select Part
			cQuery.select(entity);
			cQueryCount.select(builder.count(cQueryCount.from(class_type)));
			
			// Predicate preperation
			Predicate[] predicatesArray = searchParameters.getSearchParameters() != null ? preparePredicate(builder, entity, searchParameters.getSearchParameters()) : new Predicate[0];
			//this predicates are prepared with concern of comparison operator
			Predicate[] predicatesArrayWithOperation = searchParameters.getSearchParamWithTypes() != null ? preparePredicateWithType(builder, entity, searchParameters.getSearchParamWithTypes()) : new Predicate[0];
			predicatesArray = ArrayUtils.addAll(predicatesArray,predicatesArrayWithOperation);

			// Setting where clause
			// entitiyManager.createQuery is called there because we used other Root element. 
			// And this call will set generated alias as it and new query will add its alias in a new manner
			entityManager.createQuery(cQueryCount);
			cQueryCount.where(predicatesArray);
			cQuery.where(predicatesArray);

			// Count. Getting total record 
			Long count = entityManager.createQuery(cQueryCount).getSingleResult();

			// Order Parameter Set
			if(searchParameters.getOrderParamDesc() != null)
				setOrderParams(cQuery, builder, searchParameters.getOrderParamDesc(), entity);
			
			// query Request List
			TypedQuery<PersonalTransaction> typedQuery = entityManager.createQuery(cQuery);
			typedQuery.setFirstResult(searchParameters.getStartIndex() != null ? searchParameters.getStartIndex() : 0);
			typedQuery.setMaxResults(searchParameters.getResultQuantity() != null ? searchParameters.getResultQuantity() : 100000);
			
			// Building up searchResult
			List<PersonalTransaction> resultList = typedQuery.getResultList();
			searchResult.setResultList(resultList);
			searchResult.setTotalRecords(count.intValue());
			
			log.debug("list successful");
			return searchResult;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	

	
	
	protected Predicate[] preparePredicate(CriteriaBuilder builder, Root<PersonalTransaction> entity, Map<String, String> searchParams){
		List<Predicate> predicates = new ArrayList<Predicate>();
		for (Map.Entry<String, String> entry : searchParams.entrySet()) {
		    String key = entry.getKey();
		    String value = entry.getValue();
		    
		    Path<PersonalTransaction> path = getEntityPath(entity, key);
		    
		    if(key != null && value != null && path != null){
		    	if(value.equals("")){}
		    	else if(isNumeric(value))
		    		predicates.add(builder.equal(path, Integer.valueOf(value)));
		    	else if(isBoolean(value))
		    		predicates.add(builder.equal(path, Boolean.valueOf(value)));
		    	else 
		    		predicates.add(builder.like(getEntityPathAsString(entity, key), "%"+ value + "%"));
		    }
		}
		return predicates.toArray(new Predicate[]{});
	}
	

	protected Predicate[] preparePredicateWithType(CriteriaBuilder builder, Root<PersonalTransaction> entity, List<SearchParamWithType> searchParamWithTypes){
		List<Predicate> predicates = new ArrayList<Predicate>();
		for (SearchParamWithType searchParamWithType: searchParamWithTypes) {
		    String field = searchParamWithType.getSearchField();
		    String value = searchParamWithType.getSearchValue();
		    RequestComparisonType fieldType = searchParamWithType.getSearchType();
		    
		    Path<PersonalTransaction> path = getEntityPath(entity, field);
		    
		    if(searchParamWithType.getSearchField() != null && searchParamWithType.getSearchValue() != null && path != null){
		    	
		    		switch (fieldType) {
					case EQUAL:
						if(isNumeric(value))
							predicates.add(builder.equal(path, Double.valueOf(value)));
						else if(isBoolean(value))
				    		predicates.add(builder.equal(path, Boolean.valueOf(value)));
						break;
					case NOT_EQUAL:
						if(isNumeric(value))
							predicates.add(builder.notEqual(path, Double.valueOf(value)));
						else if(isBoolean(value))
							predicates.add(builder.notEqual(path, Double.valueOf(value)));
						break;
//					case GREATER_THAN:
//						predicates.add(builder.gt(Double.valueOf(path), Double.valueOf(value)));
//						break;
//					case GRAETER_OR_EQUAL:
//						predicates.add(builder.ge(path, Integer.valueOf(value)));
//						break;
//					case LESS_THAN:
//						predicates.add(builder.lt(path, Integer.valueOf(value)));
//						break;
//					case LESS_THAN_OR_EQUAL:
//						predicates.add(builder.le(path, Integer.valueOf(value)));
//						break;
//					case BETWEEN:
//						predicates.add(builder.between(path, Date.valueOf(value)));
//						break;
					case LIKE:
						predicates.add(builder.like(getEntityPathAsString(entity, value), "%"+ value + "%"));
						break;
					default:
						predicates.add(builder.equal(path, Integer.valueOf(value)));
						break;
					}		    		
		    }
		}
		return predicates.toArray(new Predicate[]{});
	}
	
	protected void setOrderParams(CriteriaQuery<PersonalTransaction> select, CriteriaBuilder builder, Map<String, Boolean> orderParamDesc, Root<PersonalTransaction> entity){
		for (Map.Entry<String, Boolean> entry : orderParamDesc.entrySet()) {
			String key = entry.getKey();
		    Boolean value = entry.getValue();
		    
		    Path<PersonalTransaction> path = getEntityPath(entity, key);
		    
		    if(key != null && value != null && path != null){
				if(value) select.orderBy(builder.desc(path));
				else select.orderBy(builder.asc(path));
		    }
		}
	}
	
	protected Path<PersonalTransaction> getEntityPath(Root<PersonalTransaction> entity, String key) {
		
		List<String> pathStringList = Arrays.asList(key.split("\\."));
		
		try{
			Path<PersonalTransaction> path = entity.get(pathStringList.get(0));
			for(int i = 1; i <= pathStringList.size()-1; i++){
				try{
					path = path.get(pathStringList.get(i));
			    }catch(Exception e){
			        return null;
			    }
			}
			return path;
	    }catch(Exception e){
	        return null;
	    }
	}
	
	protected Path<String> getEntityPathAsString(Root<PersonalTransaction> entity, String key) {
		
		List<String> pathStringList = Arrays.asList(key.split("\\."));
		Path<String> path = null;
		
		try{
			for(int i = 0; i <= pathStringList.size()-1; i++){
				if(i == 0 && pathStringList.size() <= 1){
					path = entity.<String>get(pathStringList.get(i));
				}else if(i==0){
					path = entity.get(pathStringList.get(i));
				}else if(pathStringList.size()-1 == i){
					path = path.<String>get(pathStringList.get(i));
				}else{
					path = path.get(pathStringList.get(i));
				}
			}
	    }catch(Exception e){
	        path = null;
	    }
		return path;
		
	}

	public static boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}
	
	public static boolean isBoolean(String str)
	{
	  return str == "true" || str == "false";
	}
	
//	public List<SumTransactionCategory> getTransactionsByCategoryFromDateToDate(SearchTransaction searchTransaction) {
//		try{
//			String hql = " SELECT sum(b.amount) as sumAmount, b.transactionCategory FROM Transaction b "
//					+" where date >= :fromDate and date <= :toDate ";
//			if(searchTransaction.getCashboxId()!=null && searchTransaction.getCashboxId() != 0){
//				hql = hql+ " and b.cashbox.id= :cashboxId ";
//			}
//			hql = hql+ " group by b.transactionCategory.id ";
//			Query query = entityManager.createQuery(hql);
//			query.setParameter("fromDate", searchTransaction.getFromDate());
//			query.setParameter("toDate", searchTransaction.getToDate());
//			if(searchTransaction.getCashboxId()!=null && searchTransaction.getCashboxId() != 0){
//				query.setParameter("cashboxId", searchTransaction.getCashboxId());
//			}		
//			@SuppressWarnings("unchecked")
//			List<SumTransactionCategory> sumTransactionCategories = query.getResultList();
//			return sumTransactionCategories;
//		} catch (RuntimeException re) {
//			log.error("Transaction by category from date to date list error", re);
//			throw re;
//		}
//	}
//	public Double getTransactionsByCategoryFromDateToDate(SearchTransaction searchTransaction) {
//		try{
//			String hql = " SELECT sum(b.amount) as sumAmount FROM PersonalTransaction b "
//					+" where date >= :fromDate and date <= :toDate ";
//			if(searchTransaction.getCashboxId()!=null && searchTransaction.getCashboxId() != 0){
//				hql = hql+ " and b.cashbox.id= :cashboxId ";
//			}
//			
//			Query query = entityManager.createQuery(hql);
//			query.setParameter("fromDate", searchTransaction.getFromDate());
//			query.setParameter("toDate", searchTransaction.getToDate());
//			if(searchTransaction.getCashboxId()!=null && searchTransaction.getCashboxId() != 0){
//				query.setParameter("cashboxId", searchTransaction.getCashboxId());
//			}
//			if(searchTransaction.getTransactionCategoryId()!=null && searchTransaction.getTransactionCategoryId() != 0){
//				query.setParameter("transactionCategoryId", searchTransaction.getTransactionCategoryId());
//			}		
//			Double sumAmount = (Double) query.getSingleResult();
//			return sumAmount;
//		} catch (RuntimeException re) {
//			log.error("PersonalTransaction by category from date to date list error", re);
//			throw re;
//		}
//	}
//	public Double getSumFromDateToDate(SearchTransaction searchTransaction, Boolean income) {
//		try{
////			
//			String hql = " select sum(b.amount) as rashody FROM PersonalTransaction b "
//					+" where date >= :fromDate and date <= :toDate ";
//			if(searchTransaction.getCashboxId()!=null && searchTransaction.getCashboxId() != 0){
//				hql = hql+ " and b.cashbox.id= :cashboxId ";
//			}
//			if(income != null){
//				hql = hql+ " and b.transactionCategory.income= :income ";
//			}
//			Query query = entityManager.createQuery(hql);
//			query.setParameter("fromDate", searchTransaction.getFromDate());
//			query.setParameter("toDate", searchTransaction.getToDate());
//			if(searchTransaction.getCashboxId()!=null && searchTransaction.getCashboxId() != 0){
//				query.setParameter("cashboxId", searchTransaction.getCashboxId());
//			}
//			if(income != null){
//				query.setParameter("income", income);
//			}		
//			Double rashody = (Double) query.getSingleResult();
//			return rashody;
//		} catch (RuntimeException re) {
//			log.error("Transaction by from date to date list error", re);
//			throw re;
//		}
//	}
}
