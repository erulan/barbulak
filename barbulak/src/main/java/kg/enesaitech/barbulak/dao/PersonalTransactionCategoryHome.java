package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.PersonalTransactionCategory;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class PersonalTransactionCategoryHome extends GenericHome<PersonalTransactionCategory>  {

	@PersistenceContext
	private EntityManager entityManager;

	public PersonalTransactionCategory getByName(String name) {
		try {
			Query query = entityManager.createQuery("from PersonalTransactionCategory b where b.name = :name");
			query.setParameter("name", name);
			PersonalTransactionCategory personalTransactionCategory = (PersonalTransactionCategory) query.getSingleResult();
			return personalTransactionCategory;
		} catch (RuntimeException re) {
			log.error("PersonalTransactionCategory by name error", re);
			throw re;
		}
	}

}
