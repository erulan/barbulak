package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.Users;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Users.
 * @see kg.NSI_Tech.home.UsersVO
 * @author Hibernate Tools
 */

@Repository
public class UsersHome extends GenericHome<Users>{

	private static final Log log = LogFactory.getLog(UsersHome.class);

	@PersistenceContext
	private EntityManager entityManager;
	
	
	//This method is created to be used by shiro realm
	public Users getByUserName(String string) {
		Users user = null;
		
		try {				
			Query query = entityManager.createQuery("from Users where userName = :name");
			query.setParameter("name", string);
			user = (Users) query.getSingleResult();
		} catch (RuntimeException re) {
			log.error("list error", re);
		}
		
		return user;
	}
	
	
	//This method is created to be used by shiro realm
	public Users getByUsersRole(String string) {
		Users user = null;
		try {
			Query query = entityManager.createQuery("select u From Users u left join u.usersRoles ur left join ur.role r where r.name = :name");
			query.setParameter("name", string);
			user = (Users) query.getResultList().get(0);
		} catch (RuntimeException re) {
			log.error("get BY UsersRole Error", re);
		}
		
		return user;
	}
	
	public String getMD5(String password){
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte[] digest = md.digest();
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		
	}

}
