package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.WareDistProduct;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class WareDistProductHome extends GenericHome<WareDistProduct>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public void deleteByWareDistributorId(Integer wareDistributorId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM WareDistProduct b "
					+ "WHERE b.wareDistributor.id = :wareDistributorId");
			query.setParameter("wareDistributorId", wareDistributorId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("WareDistProduct delete by wareDistributor id failed", re);
			throw re;
		}
	}
	
	
	

	
	public List<WareDistProduct> getByWareDistributorId(Integer wareDistributorId) {
		try {
			Query query = entityManager.createQuery("from WareDistProduct b where b.wareDistributor.id = :wareDistributorId ");
			query.setParameter("wareDistributorId", wareDistributorId);
			@SuppressWarnings("unchecked")
			List<WareDistProduct> wareDistProducts = query.getResultList();
			return wareDistProducts;
		} catch (RuntimeException re) {
			log.error("WareDistProduct by wareDistributorId get error", re);
			throw re;
		}
	}

}
