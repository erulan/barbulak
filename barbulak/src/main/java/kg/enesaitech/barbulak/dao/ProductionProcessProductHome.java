package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.ProductionProcessProduct;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class ProductionProcessProductHome extends GenericHome<ProductionProcessProduct>  {

	@PersistenceContext
	private EntityManager entityManager;
	
	public List<ProductionProcessProduct> getByProductionProcessId(int productionProcessId) {
		try{
			Query query = entityManager.createQuery("FROM ProductionProcessProduct b "
					+ "Where b.productionProcess.id = :productionProcessId ");
			query.setParameter("productionProcessId", productionProcessId);			
			@SuppressWarnings("unchecked")
			List<ProductionProcessProduct> rawPurchases = query.getResultList();
			return rawPurchases;
		} catch (RuntimeException re) {
			log.error("ProductionProcessProduct by ProductionProcess id list error", re);
			throw re;
		}
	}	
	public void deleteByProductionProcessId(int productionProcessId) {
		try{
			Query query = entityManager.createQuery("DELETE FROM ProductionProcessProduct b "
					+ "WHERE b.productionProcess.id = :productionProcessId");
			query.setParameter("productionProcessId", productionProcessId);			
			query.executeUpdate();
		} catch (RuntimeException re) {
			log.error("ProductionProcessProduct delete by ProductionProcess id failed", re);
			throw re;
		}
	}
	
}
