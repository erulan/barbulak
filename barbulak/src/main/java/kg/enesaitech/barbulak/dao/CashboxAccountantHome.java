package kg.enesaitech.barbulak.dao;

// Generated Apr 8, 2015 5:05:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import kg.enesaitech.barbulak.entity.CashboxAccountant;
import kg.enesaitech.barbulak.generics.GenericHome;

import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Role.
 * @see kg.NSI_Tech.home.RoleVO
 * @author Hibernate Tools
 */


@Repository
public class CashboxAccountantHome extends GenericHome<CashboxAccountant> {

	@PersistenceContext
	private EntityManager entityManager;

	
	public CashboxAccountant getByCashBoxId(int cashboxId) {
		try{
			Query query = entityManager.createQuery("FROM CashboxAccountant b where b.cashbox.id = :cashboxId ");
			query.setParameter("cashboxId", cashboxId);
			CashboxAccountant cashboxAccountant = (CashboxAccountant) query.getSingleResult();
			return cashboxAccountant;
		} catch (RuntimeException re) {
			return null;
		}
	}


	public List<CashboxAccountant> getCashboxAccountants(int staffId) {
		try{
			Query query = entityManager.createQuery("FROM CashboxAccountant b where b.staff.id = :staffId ");
			query.setParameter("staffId", staffId);
			@SuppressWarnings("unchecked")
			List<CashboxAccountant> cashboxAccountants = query.getResultList();
			return cashboxAccountants;
		} catch (RuntimeException re) {
			log.error("CashboxAccountant list error", re);
			throw re;
		}
	}
}

