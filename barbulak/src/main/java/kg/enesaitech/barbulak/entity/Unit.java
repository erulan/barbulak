package kg.enesaitech.barbulak.entity;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import kg.enesaitech.barbulak.generics.CommonEntity;

/**
 * Unit generated by hbm2java
 */
@Entity
@Table(name = "unit", catalog = "barbulak")
public class Unit extends CommonEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "name", nullable = false, length = 60)
	private String name;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "unit")
	private Set<Capacity> capacities = new HashSet<Capacity>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "unit")
	private Set<Raw> raws = new HashSet<Raw>(0);

	public Unit() {
	}

	public Unit(String name) {
		this.name = name;
	}

	public Unit(String name, Set<Capacity> capacities, Set<Raw> raws) {
		this.name = name;
		this.capacities = capacities;
		this.raws = raws;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Capacity> getCapacities() {
		return this.capacities;
	}

	public void setCapacities(Set<Capacity> capacities) {
		this.capacities = capacities;
	}

	public Set<Raw> getRaws() {
		return this.raws;
	}

	public void setRaws(Set<Raw> raws) {
		this.raws = raws;	
	}

}
