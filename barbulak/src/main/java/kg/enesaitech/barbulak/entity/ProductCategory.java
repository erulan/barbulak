package kg.enesaitech.barbulak.entity;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import kg.enesaitech.barbulak.generics.CommonEntity;

/**
 * ProductCategory generated by hbm2java
 */
@Entity
@Table(name = "product_category", catalog = "barbulak")
public class ProductCategory extends CommonEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Column(name = "name", nullable = false, length = 60)
	private String name;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "productCategory")
	private Set<ProductType> productTypes = new HashSet<ProductType>(0);

	public ProductCategory() {
	}

	public ProductCategory(String name) {
		this.name = name;
	}

	public ProductCategory(String name, Set<ProductType> productTypes) {
		this.name = name;
		this.productTypes = productTypes;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ProductType> getProductTypes() {
		return this.productTypes;
	}

	public void setProductTypes(Set<ProductType> productTypes) {
		this.productTypes = productTypes;
	}

}
