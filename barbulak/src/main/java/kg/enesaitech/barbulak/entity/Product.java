package kg.enesaitech.barbulak.entity;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import kg.enesaitech.barbulak.generics.CommonEntity;

/**
 * Product generated by hbm2java
 */
@Entity
@Table(name = "product", catalog = "barbulak")
public class Product extends CommonEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "capacity_id", nullable = false)
	private Capacity capacity;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "product_type_id", nullable = false)
	private ProductType productType;

	@Column(name = "name", nullable = false, length = 60)
	private String name;

	@Column(name = "price", precision = 22, scale = 0)
	private Double price;

	@Column(name = "cost", precision = 22, scale = 0)
	private Double cost;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	private Set<ProductPlace> productPlaces = new HashSet<ProductPlace>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	private Set<ShippingByProduct> shippingByProducts = new HashSet<ShippingByProduct>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
	private Set<WareDistProduct> wareDistProducts = new HashSet<WareDistProduct>(0);

	public Product() {
	}

	public Product(Capacity capacity, ProductType productType, String name) {
		this.capacity = capacity;
		this.productType = productType;
		this.name = name;
	}

	public Product(Capacity capacity, ProductType productType, String name,
			Double price, Double cost, Set<ProductPlace> productPlaces, Set<ShippingByProduct> shippingByProducts,
			Set<WareDistProduct> wareDistProducts) {
		this.capacity = capacity;
		this.productType = productType;
		this.name = name;
		this.price = price;
		this.cost = cost;
		this.productPlaces = productPlaces;
		this.shippingByProducts = shippingByProducts;
		this.wareDistProducts = wareDistProducts;
	}

	public Capacity getCapacity() {
		return this.capacity;
	}

	public void setCapacity(Capacity capacity) {
		this.capacity = capacity;
	}

	public ProductType getProductType() {
		return this.productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ProductPlace> getProductPlaces() {
		return this.productPlaces;
	}

	public void setProductPlaces(Set<ProductPlace> productPlaces) {
		this.productPlaces = productPlaces;
	}

	public Set<ShippingByProduct> getShippingByProducts() {
		return this.shippingByProducts;
	}

	public void setShippingByProducts(Set<ShippingByProduct> shippingByProducts) {
		this.shippingByProducts = shippingByProducts;
	}

	public Set<WareDistProduct> getWareDistProducts() {
		return this.wareDistProducts;
	}

	public void setWareDistProducts(Set<WareDistProduct> wareDistProducts) {
		this.wareDistProducts = wareDistProducts;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

}
