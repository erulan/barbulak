package kg.enesaitech.barbulak.entity;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import kg.enesaitech.barbulak.generics.CommonEntity;

@Entity
@Table(name = "place_19_manager", catalog = "barbulak")
public class Place19Manager extends CommonEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "place_id", nullable = false)
	private Place place;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "manager_id", nullable = false)
	private Staff manager;

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public Staff getManager() {
		return manager;
	}

	public void setManager(Staff manager) {
		this.manager = manager;
	}


}
