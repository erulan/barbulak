package kg.enesaitech.barbulak.entity;

// Generated Apr 8, 2015 5:03:52 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import kg.enesaitech.barbulak.generics.CommonEntity;

/**
 * ProductCategory generated by hbm2java
 */
@Entity
@Table(name = "transaction_category", catalog = "barbulak")
public class TransactionCategory extends CommonEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Column(name = "name", nullable = false, length = 60)
	private String name;

	@Column(name = "income", columnDefinition = "BIT")
	private Boolean income;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "transactionCategory")
	private Set<Transaction> transactions = new HashSet<Transaction>(0);

	public TransactionCategory() {
	}

	public TransactionCategory(String name, Boolean income) {
		this.name = name;
		this.income = income;
	}

	public TransactionCategory(String name, Set<Transaction> transactions, Boolean income) {
		this.name = name;
		this.transactions = transactions;
		this.income = income;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transaction> transactions) {
		this.transactions = transactions;
	}

	public Boolean getIncome() {
		return income;
	}

	public void setIncome(Boolean income) {
		this.income = income;
	}

}
