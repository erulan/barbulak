package kg.enesaitech.barbulak.userException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class BusinessException extends WebApplicationException{

	private static final long serialVersionUID = 1L;
	
	public BusinessException()
    {
        this("У вас есть логические ощибки, пожалуйста проверьте и изправьте эти ощибки.");
    }

	public BusinessException(String message){
		super(Response.status(Status.METHOD_NOT_ALLOWED).type(MediaType.APPLICATION_JSON).entity("{\"message\": \"" + message + "\"}").build());
	}
}
