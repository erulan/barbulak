package kg.enesaitech.barbulak.others;

import java.util.HashMap;
import java.util.List;

import kg.enesaitech.barbulak.dao.InventoryHome;
import kg.enesaitech.barbulak.entity.Inventory;
import kg.enesaitech.barbulak.generics.GenericHome;
import kg.enesaitech.barbulak.vo.SearchParameters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("anotherBean")
public class WriteOffInventory {
	
	@Autowired
	protected InventoryHome inventoryHome;
	
	@Autowired 
	protected GenericHome<kg.enesaitech.barbulak.entity.InventoryStatus> genericInventoryStatusHome;
	
	protected static final Log log = LogFactory.getLog(WriteOffInventory.class);
    
	SearchParameters searchParameters = new SearchParameters(0, 10000, new HashMap<String, String>(), new HashMap<String, Boolean>());
	
	
	@Transactional
    public void writeOff(){
    	
    	log.error("logging in writofff ***************************************");
        List<Inventory> inventories = inventoryHome.getListForWriteOff();
        
        searchParameters.getSearchParameters().put("name", InventoryStatus.WROTE_OFF.getName());
        kg.enesaitech.barbulak.entity.InventoryStatus inventoryStatus = genericInventoryStatusHome.getList(searchParameters, kg.enesaitech.barbulak.entity.InventoryStatus.class).getResultList().get(0);
        for(Inventory inventory : inventories ){
        	
        	inventory.setInventoryStatus(inventoryStatus);
        	inventoryHome.merge(inventory);
        }
    }
     
}