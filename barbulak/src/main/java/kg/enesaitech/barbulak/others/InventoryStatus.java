package kg.enesaitech.barbulak.others;

public enum InventoryStatus {

	SOLD("продано"),
    TO_SELL("продается"),
    OWNED_BY_COMPANY("собственность фирмы"),
    WROTE_OFF("списано");    

    private String name;

    InventoryStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }    

}
