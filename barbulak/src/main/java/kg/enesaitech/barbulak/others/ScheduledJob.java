package kg.enesaitech.barbulak.others;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
 
public class ScheduledJob extends QuartzJobBean{
 
     
    private WriteOffInventory writeOffInventory; 
     
     
    @Override
    protected void executeInternal(JobExecutionContext arg0)
            throws JobExecutionException {
        writeOffInventory.writeOff();
    }
 
    public void setAnotherBean(WriteOffInventory anotherBean) {
        this.writeOffInventory = anotherBean;
    }
}
