

angular.module('app')

.factory("restService", function(Restangular, $rootScope){	
	return Restangular;
})

.factory("Utils", function(restService, $modal, $state){
	return {
		callDeleteModal: function(deletepath, id, $location){
			var modalInstance = $modal.open({
			      templateUrl: '../../assets/tpl/blocks/deleteModal.html',
			      controller: 'DeleteModalCtrl',
			      size: 'sm'
			    });

			    modalInstance.result.then(function (selectedItem) {
			    	restService.one(deletepath).customGET("delete/" + id).then(function(data){
			    		alert("Успешно удален!");
			    		//$location.path("/" + deletepath);
			    		$state.reload();
			    	});
			    }, function () {
			      return;
			    });
		},
		callUserModal: function(deletepath, id, $location){
			var modalInstance = $modal.open({
			      templateUrl: '../../assets/tpl/blocks/userModal.html',
			      controller: 'UserModalCtrl',
			      size: 'sm'
			    });

			    modalInstance.result.then(function (selectedItem) {
			    	restService.one("users").customGET("create_user_by_staffid/" + id).then(function(data){
			    		alert("Учетная запись создана!");
			    		//$location.path("/" + deletepath);
			    		$route.reload();
			    	});
			    }, function () {
			      return;
			    });
		},
		ngTableGetData: function($defer, params, path) {
        	
			var searchParams = {};
        	var prm = params.parameters();

        	searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
        	searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
        	
        	searchParams["searchParameters"] = {};
        	angular.forEach(prm.filter, function(value, key){
        		searchParams.searchParameters[key] = value;
        	});
        	
        	searchParams["orderParamDesc"] = {};
        	angular.forEach(prm.sorting, function(value, key){
        		searchParams.orderParamDesc[key] = (value == "desc");
        	});
        	
        	restService.all(path).customPOST(searchParams, ['list']).then(function(data){
        		params.total(data.originalElement.totalRecords);
                // set new data
                $defer.resolve(data.originalElement.resultList);
    		});
        }
	}
});