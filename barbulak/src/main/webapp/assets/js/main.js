'use strict';

/* Controllers */

angular.module('app')
  .controller('AppCtrl', ['$scope', 'localStorageService', '$window', 
    function(              $scope,  localStorageService,   $window ) {
      // add 'ie' classes to html
      var isIE = !!navigator.userAgent.match(/MSIE/i);
      isIE && angular.element($window.document.body).addClass('ie');
      isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');

      // config
      $scope.app = {
        name: 'Barbulak',
        version: '1.0',
        // for chart colors
        color: {
          primary: '#7266ba',
          info:    '#23b7e5',
          success: '#27c24c',
          warning: '#fad733',
          danger:  '#f05050',
          light:   '#e8eff0',
          dark:    '#3a3f51',
          black:   '#1c2b36'
        },
        settings: {
          themeID: 1,
          navbarHeaderColor: 'bg-success',
          navbarCollapseColor: 'bg-success',
          asideColor: 'bg-black',
          headerFixed: true,
          asideFixed: true,
          asideFolded: false,
          asideDock: false,
          container: false
        }
      }

      // save settings to local storage
      if ( angular.isDefined(localStorageService.get("settings")) && localStorageService.get("settings") != null) {
        $scope.app.settings = localStorageService.get("settings");
      } else {
    	  localStorageService.set('settings', $scope.app.settings);
      }
      $scope.$watch('app.settings', function(){
        if( $scope.app.settings.asideDock  &&  $scope.app.settings.asideFixed ){
          // aside dock and fixed must set the header fixed.
          $scope.app.settings.headerFixed = true;
        }
        // save to local storage
        localStorageService.set('settings', $scope.app.settings);
      }, true);


      function isSmartDevice( $window )
      {
          // Adapted from http://www.detectmobilebrowsers.com
          var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
          // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
          return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
      }

  }])
  
  .controller('DeleteModalCtrl', ['$scope', '$modalInstance', '$rootScope', '$state', function ($scope, $modalInstance, $rootScope, $state) {

	  $scope.ok = function () {
	    $modalInstance.close('ok');
	    $state.reload();
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	    $state.reload();
	  };
	  
	  
}])
  .controller('UserModalCtrl', ['$scope', '$modalInstance', '$rootScope', function ($scope, $modalInstance, $rootScope) {

	  $scope.ok = function () {
	    $modalInstance.close('ok');
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	  };
	  
	  
}])

 .controller('ProfileCtrl', ['$scope', '$rootScope', 'restService', function ($scope, $rootScope, restService) {
	 $scope.username = $scope.userInfo.personNumber;
	 $scope.update = function(){
	    	restService.all("security").customPOST({"username": $scope.username, "current_p" : $scope.current_p, "new_p" : $scope.new_p,"new_p_repeat" : $scope.new_p_repeat},  "change_password").then(function(data){
	    		alert("Ваш пароль успешно изменен!");
	    	});
	    	
	    }
}])

;
