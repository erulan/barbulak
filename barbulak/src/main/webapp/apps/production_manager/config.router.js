'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/production_manager/shipping/list');
          $stateProvider
              .state('production_manager', {
                  abstract: true,
                  url: '/production_manager',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //shipping
              .state('production_manager.shipping', {
                  abstract: true,
                  url: '/shipping',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/production_manager/shipping/controller.js']);
                      }]
                    }
              })
              .state('production_manager.shipping.list', {
                  url: '/list',
                  templateUrl: '../../apps/production_manager/shipping/list.html',
              })
              .state('production_manager.shipping.add', {
            	  url: '/add',
            	  templateUrl:'../../apps/production_manager/shipping/add.html',
              })
              .state('production_manager.shipping.edit', {
            	  url: '/edit/:id',
            	  templateUrl:'../../apps/production_manager/shipping/edit.html',
              })
              
              //shipping_19
              .state('production_manager.shipping_19', {
                  abstract: true,
                  url: '/shipping_19',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/production_manager/shipping_19/controller.js']);
                      }]
                    }
              })
              .state('production_manager.shipping_19.list', {
                  url: '/list',
                  templateUrl: '../../apps/production_manager/shipping_19/list.html',
              })
              .state('production_manager.shipping_19.add', {
            	  url: '/add',
            	  templateUrl:'../../apps/production_manager/shipping_19/add.html',
              })
              .state('production_manager.shipping_19.edit', {
            	  url: '/edit/:id',
            	  templateUrl:'../../apps/production_manager/shipping_19/edit.html',
              })
              .state('production_manager.shipping_19.product', {
            	  url: '/product',
            	  templateUrl:'../../apps/production_manager/shipping_19/product.html',
              })
              .state('production_manager.shipping_19.tar_request', {
            	  url: '/tar_request',
            	  templateUrl:'../../apps/production_manager/shipping_19/tar_request.html',
              })
              .state('production_manager.shipping_19.tar_request_list', {
            	  url: '/tar_request_list',
            	  templateUrl:'../../apps/production_manager/shipping_19/tar_request_list.html',
              })

              
              //production/raw_material_request
              .state('production_manager.raw_material_request',{
            	  abstract: true,
            	  url: '/raw_material_request',
            	  template: '<ui-view>',
            	  resolve:{
            		  deps:['$ocLazyLoad',
            		        function($ocLazyLoad){
            			  return $ocLazyLoad.load(['../../apps/production_manager/raw_material_request/controller.js']);
            		  }]
            	  }
              })
              .state('production_manager.raw_material_request.list',{
            	  url: '/list',
            	  templateUrl: '../../apps/production_manager/raw_material_request/list.html',
              })
              .state('production_manager.raw_material_request.add',{
            	  url:'/add',
            	  templateUrl:'../../apps/production_manager/raw_material_request/add.html',
              })
              .state('production_manager.raw_material_request.productRawRequest',{
            	  url:'/add',
            	  templateUrl:'../../apps/production_manager/raw_material_request/productRawRequest.html',
              })
              .state('production_manager.raw_material_request.raw_materila_list',{
            	  url: '/list',
            	  templateUrl: '../../apps/production_manager/raw_material_request/raw_materila_list.html',
              })
              .state('production_manager.raw_material_request.min_raw_materila_list',{
            	  url: '/list',
            	  templateUrl: '../../apps/production_manager/raw_material_request/min_raw_materila_list.html',
              })
               .state('production_manager.raw_material_request.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/production_manager/raw_material_request/edit.html',

              })
              
              //production/defect
              .state('production_manager.defect',{
            	  abstract: true,
            	  url: '/defect',
            	  template: '<ui-view>',
            	  resolve:{
            		  deps:['$ocLazyLoad',
            		        function($ocLazyLoad){
            			  return $ocLazyLoad.load(['../../apps/production_manager/defect/controller.js']);
            		  }]
            	  }
              })
              .state('production_manager.defect.list',{
            	  url: '/list',
            	  templateUrl: '../../apps/production_manager/defect/list.html',
              })
              .state('production_manager.defect.add',{
            	  url:'/add',
            	  templateUrl:'../../apps/production_manager/defect/add.html',
              })
              .state('production_manager.defect.edit',{
            	  url:'/edit',
            	  templateUrl:'../../apps/production_manager/defect/edit.html',
              })
              .state('production_manager.defect.info',{
            	  url: '/info',
            	  templateUrl: '../../apps/production_manager/defect/info.html',
              })
              
      }
    ]
  );
