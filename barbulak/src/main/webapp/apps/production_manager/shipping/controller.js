'use strict';

/* Controllers */
  // signin controller


app.controller('ListShippingCtrl', ['$scope', 'restService','ngTableParams','Utils','$modal','$window', 
                                  function($scope, restService, ngTableParams,Utils, $modal, $window) {

	$scope.getShipping ={};
	$scope.shippingProduct ={};
	$scope.shippingDetail = {};
	$scope.shippingInfo = {};
	$scope.shippingInfo.shippingStatus = {};
	$scope.shippingCancelInfo = {};	
	$scope.shippingCancelInfo.shipping = {};
	$scope.shippingCancelInfo.staff = {};
	$scope.placeByFromPlace = {};
	$scope.placeByFromPlaceId = {};
	$scope.productionChange ={};
	
	$scope.getShippingList = function($defer, params, path) {				
					var searchParams = {};
					var prm = params.parameters();
				
					searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
					searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
					searchParams["searchParameters"] = {};
//					searchParams["searchParamWithTypes"] = [{searchField:"shippingStatus.id", searchValue: 4, searchType: "NOT_EQUAL"}];
					angular.forEach(prm.filter, function(value, key){
						searchParams.searchParameters[key] = value;
					});
					searchParams["orderParamDesc"] = {"statusDate.created" : true};
					angular.forEach(prm.sorting, function(value, key){
						searchParams.orderParamDesc[key] = (value == "desc");
					});
					restService.all(path).customPOST(searchParams, ['list']).then(function(data){
						params.total(data.originalElement.totalRecords);
				        // set new data
				        $defer.resolve(data.originalElement.resultList);
					});
	}

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
        	"statusDate.created" : true     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){

        	restService.one("security").customGET('current_user').then(function(data){
	        	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id, "placeType.name": "производство"}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.placeByFromPlaceId = place.id;
	    			});
	
	            	$scope.getShippingList($defer, params, "production_process");
	    		});
        	});
        },
    });

	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.showShippingDetails = function(shippingId){
		$('#button_approve').hide();
		$('#button_cancel').hide();
		$scope.shippingProducts = {};
		$scope.shippingInfo = {};
		restService.all("production_process_product").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"productionProcess.id": shippingId}}, "list").then(function(data){
			$scope.shippingProducts = data.originalElement.resultList;
		});
		
		restService.one("production_process").customGET(shippingId).then(function(data){
			$scope.shippingInfo = data.originalElement;
			if($scope.shippingInfo.status.name == 'Запрос'){
				$('#button_cancel').show();
			}else if($scope.shippingInfo.status.name == "Подтвержден администратором"){
				$('#button_approve').show();
			}
		});
		
		$('.toggle_2').show(1000);
	}
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.sendProducts = function(id){
			console.log($scope.shippingProducts);
			console.log($scope.productionAmount);
			angular.forEach($scope.shippingProducts, function(product,key){
				product.productionAmount = $scope.productionChange[product.id].productionAmount;
				product.description = $scope.productionChange[product.id].description;
			console.log($scope.productionChange[product.id].productionAmount);
			});
			$scope.shippingInfo.productionProcessProducts = $scope.shippingProducts; 
			console.log($scope.shippingProducts);
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
	
		modalInstance.result.then(function (selectedItem) {
			restService.one("production_process").customPOST($scope.shippingInfo, "approve_by_production/"+id).then(function(data){
					$scope.tableParams.reload();
					$scope.shippingInfo.status.name="Отправлено";
					alert("Транспортировка успешно отправлена!");
					$('#button_approve').hide();
			});
			}, function () {
		      return;
	    });
	};
	
	$scope.shippingCancel = function(id, description){

		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		
		modalInstance.result.then(function (selectedItem) {
        	restService.one("security").customGET('current_user').then(function(data){
        		$scope.userId = data.originalElement.id;
        	});
			var shippingCancelInfo = {"date" : new Date(), "shipping" : {"id" : id}, "staff" : {"id" : $scope.userId}}
			$scope.shippingInfo.shippingStatus.id = 5;
			restService.one("shipping").customPOST($scope.shippingInfo, "update/" + id).then(function(data){
				$scope.shippingInfo.shippingStatus.name="Запрос на отмену";
				alert("Запрос на отмену транспортировки отправлен администратору!");
				$scope.tableParams.reload();
				restService.one("shipping_cancel_request").customPOST(shippingCancelInfo, "create").then(function(data){
					$('cancel_btn').hide();
				});
			});
  		}, function () {
		      return;
	    });
	};
	$scope.toggleTable(2);
	
	
	
  }]);
app.controller('AddShippingCtrl', ['$scope', 'restService','ngTableParams', 'Utils', function($scope, restService, ngTableParams, Utils){
	$scope.placeByFromPlace = {};
	$scope.placeByFromPlace[0] ={};
	$scope.placeByFromPlaceId = {};
	$scope.placeByToPlaceId = {};
	$scope.shippingByProducts = [];
	$scope.shippingByRaws = [];
	$scope.sendProducts = {};
	$scope.getShipping = {};
	$scope.currentUser = {};
	$scope.currentUserId = {};
	$scope.places = [];
	
	$('.tableSubmit').hide();
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"name": "Балыкчы склад"}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			$scope.placeByToPlaceId2 = place.id;
			$scope.placeByToPlaceId = place.id;
		});
	});
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "производство"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
			});
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			if(place.placeType.name == 'склад'){
				$scope.places.push(place);
			}
		});
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "product");
        },
    });
	
	restService.all("product").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.products = data.originalElement.resultList;
	});
	
	restService.all("shipping").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.getShipping = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.date = new Date();
	
	$scope.addToSendList = function(productId,amount){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			if($scope.sendProducts[productId]==null){
				$('.button_'+productId).hide(1000);
				$('.button_'+productId).show(1000);				
				
				angular.forEach($scope.products, function(product, key){
    				if(productId == product.id){
    					$scope.sendProducts[productId] = product;
    				}
    			});
				$scope.sendProducts[productId].plannedAmount = amount; 
				
				$('.tableSubmit').show(1000);
			}else{
				$scope.sendProducts[productId].plannedAmount = amount;
				$('.button_'+productId).hide(1000);
				$('.button_'+productId).show(1000);	
			}
			
			angular.forEach($scope.sendProducts, function(product, key){
				$scope.shippingByProducts.push({"plannedAmount": product.plannedAmount, "product": {"id" : product.id}, "description":"description"});
			});
			restService.one("production_process").customPOST($scope.shippingByProducts, "check_ingredients").then(function(data){
				$scope.shippingByProducts = [];
			});
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
		
	}
	
	$scope.cancelProduct = function(productId){
		
		delete $scope.sendProducts[productId];
		if(Object.keys($scope.sendProducts).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.saveSendList = function(){
		if($scope.placeByToPlaceId!=-1){
			if(Object.keys($scope.sendProducts).length!=0){
				angular.forEach($scope.sendProducts, function(product, key){
					$scope.shippingByProducts.push({"plannedAmount": product.plannedAmount, "product": {"id" : product.id}});
				});
				var shipping = {"toPlaceId": {"id" : parseInt($scope.placeByToPlaceId) }, "status" : {"id" : 1}, "description": $scope.description,"productionProcessProducts": $scope.shippingByProducts};
				restService.one("production_process").customPOST(shipping, "create").then(function(data){
					$('.tableSubmit').hide(1000);
					alert("Список запланированных продуктов успешно отправлен!");
					$scope.tableParams.reload();
					$scope.sendProducts = {};
					$scope.shippingByProducts = [];
					$scope.description = null;
				});
			}else{
				alert("Пожалуйста сначала выберите продукции для отправки!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Назначения!");
		}
	}
}]);
app.controller('EditShippingCtrl', ['$scope', 'restService','ngTableParams', 'Utils','$stateParams', function($scope, restService, ngTableParams, Utils,$stateParams){
	$scope.placeByFromPlace = {};
	$scope.placeByFromPlace[0] ={};
	$scope.placeByFromPlaceId = {};
	$scope.placeByToPlaceId = {};
	$scope.shippingByProducts = [];
	$scope.shippingByRaws = [];
	$scope.sendProducts = {};
	$scope.getShipping = {};
	$scope.currentUser = {};
	$scope.currentUserId = {};
	$scope.places = [];
	
//	$('.tableSubmit').hide();

	$scope.shippingId = $stateParams.id;
	
	restService.all("production_process_product").customPOST({startIndex: 0, resultQuantity: 1000, "searchParameters":{"productionProcess.id": $scope.shippingId}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(product, key){
			restService.all("product").customGET(product.product.id).then(function(data){
				console.log();
				$scope.sendProducts[product.product.id] = data.originalElement;
				$scope.sendProducts[product.product.id].plannedAmount = product.plannedAmount;

			});
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"name": "Балыкчы склад"}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			$scope.placeByToPlaceId2 = place.id;
			$scope.placeByToPlaceId = place.id;
		});
	});
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "производство"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
			});
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			if(place.placeType.name == 'склад'){
				$scope.places.push(place);
			}
		});
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "product");
        },
    });
	
	restService.all("product").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.products = data.originalElement.resultList;
	});
	
	restService.all("shipping").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.getShipping = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	$scope.date1= $scope.date.getDate()+'-'+$scope.date.getMonth()+'-'+$scope.date.getFullYear();
	
	$scope.addToSendList = function(productId,amount){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			if($scope.sendProducts[productId]==null){
				$('.button_'+productId).hide(1000);
				$('.button_'+productId).show(1000);				
				
				angular.forEach($scope.products, function(product, key){
    				if(productId == product.id){
    					$scope.sendProducts[productId] = product;
    				}
    			});
				$scope.sendProducts[productId].plannedAmount = amount; 
				$('.tableSubmit').show(1000);
			}else{
				$scope.sendProducts[productId].plannedAmount = amount;
				$('.button_'+productId).hide(1000);
				$('.button_'+productId).show(1000);	
			}
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
		
	}
	
	$scope.cancelProduct = function(productId){
		
		delete $scope.sendProducts[productId];
		if(Object.keys($scope.sendProducts).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.saveSendList = function(){
		if($scope.placeByToPlaceId!=-1){
			if(Object.keys($scope.sendProducts).length!=0){
				angular.forEach($scope.sendProducts, function(product, key){
					$scope.shippingByProducts.push({"plannedAmount": product.plannedAmount, "product": {"id" : product.id}});
				});
				var shipping = {"id": $scope.shippingId,"toPlaceId": {"id" : parseInt($scope.placeByToPlaceId) }, "status" : {"id" : 1}, "description": $scope.description,"productionProcessProducts": $scope.shippingByProducts};
				restService.one("production_process").customPOST(shipping, "update/"+$scope.shippingId).then(function(data){
					$('.tableSubmit').hide(1000);
					alert("Список запланированных продуктов успешно отправлен!");
					$scope.tableParams.reload();
					$scope.sendProducts = {};
					$scope.shippingByProducts = [];
					$scope.description = null;
				});
			}else{
				alert("Пожалуйста сначала выберите продукции для отправки!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Назначения!");
		}
	}
}]);
