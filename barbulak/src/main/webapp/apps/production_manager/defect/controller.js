'use strict';

/* Controllers */
  // signin controller


app.controller('ListDefectCtrl', ['$scope', 'restService','ngTableParams','Utils', 
                                  function($scope, restService, ngTableParams,Utils) {
    $scope.name = "defect";
    
    $scope.defect = {};
    
    $scope.defectId = $routeParams.id;
    
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "defect");
        },
    });
    
    	restService.one("defect").customPOST($scope.defect, "list" ).then(function(data){
    		alert("Success in defect update!");
    	//	console.log(list);
    	//	$location.path("");
    	})
 /*   
    $scope.del = function(){
    	restService.one("defect").customPOST({"defectId": $scope.defect.id}, "delete").then(function(data){
    		alert("Success in defect update!");
    	//	$location.path("");
    	})
    }*/
    
    
  }]);
app.controller('AddDefectCtrl', ['$scope', 'restService', function($scope, restService){
	
	$scope.defect = {};
	
	$scope.addDefect = function(){
		restService.one("defect").customPOST($scope.defect, "create").then(function(data){
			alert("Defect additing was successfuly!");
		});
	}
}]);

app.controller('EditDefectCtrl' ['$scope', 'restService', '$routeParams', function($scope, restService, $routeParams){
	
}]);


app.controller('InfoDefectCtrl', ['$scope','restService', '$routeParams', function($scope, restService, $routeParams) {
    
    $scope.defect = {};
    $scope.defectId = $routeParams.id;
    
    $scope.button = function(){
    	restService.one("defect").customPOST($scope.defect, "update/" + $routeParams.id).then(function(data){
    		alert("Information in defect was updated!");
    	})
    }
}]);