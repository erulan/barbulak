'use strict';

/* Controllers */
  // signin controller

app.controller('RawMaterialListCtrl', ['$scope','restService','ngTableParams','Utils',  function($scope, restService, ngTableParams, Utils) {
    
	$scope.placeByFromPlaceId = {};
	
	
    $scope.getProductsList = function($defer, params, path) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		
		searchParams["searchParameters"] = {"place.id" : $scope.placeByFromPlaceId};

		searchParams["searchParamWithTypes"] = [{searchField:"amount", searchValue: 0, searchType: "NOT_EQUAL"}];
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		
		
		searchParams["orderParamDesc"] = {};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		 	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}

    
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	restService.one("security").customGET('current_user').then(function(data){
            		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id,"placeType.name": "производство"}}, "list").then(function(data){
    	    			$scope.placeByFromPlace = data.originalElement.resultList;
    	    			angular.forEach(data.originalElement.resultList, function(place, key){
    	    				$scope.placeByFromPlaceId = place.id;
    	    			});

    	            	$scope.getProductsList($defer, params, "raw_place");
            		});
        		});
        },
    });

	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
  }]);
app.controller('MinRawMaterialListCtrl', ['$scope','restService','ngTableParams','Utils',  function($scope, restService, ngTableParams, Utils) {
    
	$scope.placeByFromPlaceId = {};
	
	
    $scope.getProductsList = function($defer, params, path) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		
		searchParams["searchParameters"] = {"place.id" : $scope.placeByFromPlaceId};

		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		
		
		searchParams["orderParamDesc"] = {};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		 	
		restService.all(path).customPOST(searchParams, ['alert_raw']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}

    
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	restService.one("security").customGET('current_user').then(function(data){
            		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id,"placeType.name": "производство"}}, "list").then(function(data){
    	    			$scope.placeByFromPlace = data.originalElement.resultList;
    	    			angular.forEach(data.originalElement.resultList, function(place, key){
    	    				$scope.placeByFromPlaceId = place.id;
    	    			});

    	            	$scope.getProductsList($defer, params, "raw_place");
            		});
        		});
        },
    });

	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
  }]);

app.controller('ListRawRequestCtrl', ['$scope', 'restService','ngTableParams','Utils','$modal','$window', function($scope, restService, ngTableParams,Utils,$modal,$window ) {

	$scope.getShipping ={};
	$scope.shippingRaw ={};
	$scope.shippingDetail = {};
	$scope.shippingInfo = {};
	$scope.shippingCancelInfo = {};	
	$scope.shippingCancelInfo.shipping = {};
	$scope.shippingCancelInfo.staff = {};
	$scope.placeByFromPlace = {};
	
	$scope.getShippingList = function($defer, params, path) {
					var searchParams = {};
					var prm = params.parameters();
				
					searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
					searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
					angular.forEach(prm.filter, function(value, key){
						searchParams.searchParameters[key] = value;
					});
					searchParams["orderParamDesc"] = {"statusDate.created" : true};
					angular.forEach(prm.sorting, function(value, key){
						searchParams.orderParamDesc[key] = (value == "desc");
					});					  	
					restService.all(path).customPOST(searchParams, ['list']).then(function(data){
						params.total(data.originalElement.totalRecords);
				        // set new data
				        $defer.resolve(data.originalElement.resultList);
					});
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){

        	restService.one("security").customGET('current_user').then(function(data){
	        	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id, "placeType.name": "производство"}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.placeByFromPlaceId = place.id;
	    			});
	
	            	$scope.getShippingList($defer, params, "purchase");
	    		});
	        	
        	});
        },
    });

	$scope.showShippingDetails = function(purchaseId){
		$('#button_approve').hide();
		$('#button_cancel').hide();
		$scope.shippingRaws = {};
		$scope.shippingInfo = {};
		restService.all("raw_purchase").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"purchase.id": purchaseId}}, "list").then(function(data){
			$scope.shippingRaws = data.originalElement.resultList;
		});
		restService.one("purchase").customGET(purchaseId).then(function(data){
			$scope.shippingInfo = data.originalElement;
			if($scope.shippingInfo.status.name == "Запрос"){
				$('#button_cancel').show();
			}else if($scope.shippingInfo.status.name == "Подтвержден отправителем"){
				$('#button_approve').show();
				$('#button_cancel').show();
			}
			
		});
		$('.toggle_2').show(1000);
	}
	
	$scope.shippingApprove = function(id){
		
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
	
		modalInstance.result.then(function (selectedItem) {
			restService.one("purchase").customGET("approve_by_to_place/"+id).then(function(data){
					$scope.shippingInfo.status.name="Получено";
					$scope.tableParams.reload();
					alert("Транспортировка успешно завершена!");
					$('#button_approve').hide();
			});
			}, function () {
		      return;
	    });
	};
	
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};

	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	

	
	$scope.shippingCancel = function(id){

		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		
		modalInstance.result.then(function (data) {
//			console.log($scope.shippingInfo);
//			if($scope.shippingInfo.status.name == "Запрос"){
//				$scope.shippingInfo.status = {"id": 3}; 
//				$scope.shippingInfo.toPlaceId = {"id": $scope.shippingInfo.toPlaceId.id};// Canceled
//				angular.forEach($scope.shippingRaws, function(raw, key){
//					$scope.shippingInfo.rawPurchases = [{"amount":raw.amount, "raw":{"id":raw.raw.id}}];
//				})
//				restService.one("purchase").customPOST($scope.shippingInfo, "update/" + id).then(function(data){
//					$scope.shippingInfo.status.name="отменен";
//					$('#button_cancel').hide(1000);
//					alert("Запрос отменен!");
//	
//					 $scope.tableParams.reload();
//				});
//			}else if($scope.shippingInfo.status.name == "Подтвержден отправителем"){
				restService.one("purchase").customGET("reject_by_to_place/"+id).then(function(data){
					alert("Запрос Сырья отменен!");
					$scope.tableParams.reload();
				});
//			}
		}, function () {
		      return;
	    });
	};

	$scope.toggleTable(2);
	
  }]);
app.controller('AddRawRequestCtrl', ['$scope', 'restService','ngTableParams', 'Utils', function($scope, restService, ngTableParams, Utils){
	
	$scope.sendRaws = {};
	$scope.sendRawsArray = [];
	$scope.purchase = {};
	$scope.place = {};
	$scope.status = {};
	
	$('.tableSubmit').hide();
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "производство"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.place.id = place.id;
			});
		});
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "raw");
        },
    });
	
	restService.all("raw").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.raws = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	
	$scope.addToSendList = function(rawId,amount){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			if($scope.sendRaws[rawId]==null){
				$('.button_'+rawId).hide(1000);
				$('.button_'+rawId).show(1000);				
				
				angular.forEach($scope.raws, function(raw, key){
					if(raw.id == rawId){
						$scope.sendRaws[rawId] = raw;
					}
				});
				$scope.sendRaws[rawId].amount = amount; 
				$('.tableSubmit').show(1000);
			}else{
				$scope.sendRaws[rawId].amount = amount;
				$('.button_'+rawId).hide(1000);
				$('.button_'+rawId).show(1000);	
			}
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
	}
	
	$scope.cancelRaw = function(rawId){
		
		delete $scope.sendRaws[rawId];
		if(Object.keys($scope.sendRaws).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.saveSendList = function(description){
		if($scope.placeByToPlaceId!=-1){
			if(Object.keys($scope.sendRaws).length!=0){
				angular.forEach($scope.sendRaws, function(raw, key){
						$scope.sendRawsArray.push({"amount": raw.amount, "raw": {"id" : raw.id}});
					});
				
				restService.all("shipping_status").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"id": 4}}, "list").then(function(data){
					$scope.placeByFromPlace = data.originalElement.resultList;
					angular.forEach(data.originalElement.resultList, function(status, key){
						$scope.status.id = status.id;
						$scope.purchase = {"toPlaceId": $scope.place, "rawPurchases": $scope.sendRawsArray, "status" : $scope.status, "description": description}
						console.log($scope.purchase);
						restService.one("purchase").customPOST($scope.purchase, "create").then(function(data){
							$('.tableSubmit').hide(1000);
							alert("Запрос Сырья успешно отправлен!");
							$scope.tableParams.reload();
							$scope.sendRaws = {};
							$scope.shippingByRaws = [];
							$scope.description = null;
						});
					});
				});
				
			}else{
				alert("Пожалуйста сначала выберите cырье для запроса!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Назначения!");
		}
	}
}]);
app.controller('EditRawRequestCtrl', ['$scope', 'restService','ngTableParams', 'Utils','$state','$stateParams','$location', function($scope, restService, ngTableParams, Utils,$state,$stateParams,$location){
	
	$scope.sendRaws = {};
	$scope.sendRawsArray = [];
	$scope.purchase = {};
	$scope.place = {};
	$scope.status = {};
	
	$('.tableSubmit').show(1000);
	$scope.purchaseId = $stateParams.id;
	restService.all("raw_purchase").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"purchase.id": $scope.purchaseId}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(raw, key){
			restService.all("raw").customGET(raw.raw.id).then(function(data){
				console.log();
				$scope.sendRaws[raw.raw.id] = data.originalElement;
				$scope.sendRaws[raw.raw.id].amount = raw.amount;

			});
		});
	});
	restService.all("purchase").customGET($scope.purchaseId).then(function(data){
		$scope.description= data.originalElement.description;
	});
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "производство"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.place.id = place.id;
			});
		});
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "raw");
        },
    });
	
	restService.all("raw").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.raws = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	
	$scope.addToSendList = function(rawId,amount){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			console.log($scope.sendRaws);
			console.log($scope.sendRaws[rawId]);
			if($scope.sendRaws[rawId]==null){
				$('.button_'+rawId).hide(1000);
				$('.button_'+rawId).show(1000);				
				
				angular.forEach($scope.raws, function(raw, key){
					if(raw.id == rawId){
						$scope.sendRaws[rawId] = raw;
					}
				});
				$scope.sendRaws[rawId].amount = amount; 
				$('.tableSubmit').show(1000);
			}else{
				$scope.sendRaws[rawId].amount = amount;
				$('.button_'+rawId).hide(1000);
				$('.button_'+rawId).show(1000);	
			}
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
	}
	
	$scope.cancelRaw = function(rawId){
		console.log($scope.sendRaws);
		console.log($scope.sendRaws[rawId]);
		console.log(rawId);
		delete $scope.sendRaws[rawId];
		if(Object.keys($scope.sendRaws).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.saveSendList = function(description){
			if(Object.keys($scope.sendRaws).length!=0){
				angular.forEach($scope.sendRaws, function(raw, key){
					$scope.sendRawsArray.push({"amount": raw.amount, "raw": {"id" : raw.id}});
				});
				
				restService.all("shipping_status").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"id": 1}}, "list").then(function(data){
					$scope.placeByFromPlace = data.originalElement.resultList;
					angular.forEach(data.originalElement.resultList, function(status, key){
						$scope.status.id = status.id;
						console.log(description);
						$scope.purchase = {"id":$scope.purchaseId,"toPlaceId": $scope.place, "rawPurchases": $scope.sendRawsArray, "status" : $scope.status,"description":description}
						console.log($scope.purchase);
						console.log($scope.purchaseId);
						restService.one("purchase").customPOST($scope.purchase, "update/"+$scope.purchaseId).then(function(data){
							$('.tableSubmit').hide(1000);
							alert("Запрос Сырья успешно изменен!");
				    		$location.path("production_manager/raw_material_request/list");
							$scope.tableParams.reload();
							$scope.sendRaws = {};
							$scope.shippingByRaws = [];
							$scope.description = null;
						});
					});
				});
				
			}else{
				alert("Пожалуйста сначала выберите cырье для запроса!!");
			}
		}
}]);

//app.controller('productRawRequestCtrl', ['$scope', 'restService','ngTableParams', 'Utils', function($scope, restService, ngTableParams, Utils){
//		
//	$scope.placeByFromPlace = {};
//	$scope.placeByFromPlace[0] ={};
//	$scope.placeByFromPlaceId = {};
//	$scope.placeByToPlaceId = {};
//	$scope.shippingByProducts = [];
//	$scope.shippingByRaws = [];
//	$scope.sendProducts = {};
//	$scope.sendRaws = {};
//	$scope.getShipping = {};
//	$scope.currentUser = {};
//	$scope.currentUserId = {};
//	$scope.places = [];
//	
//	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"name": "Балыкчы склад"}}, "list").then(function(data){
//		angular.forEach(data.originalElement.resultList, function(place, key){
//			$scope.placeByToPlaceId2 = place.id;
//			$scope.placeByToPlaceId = place.id;
//		});
//	});
//	
//	$('.tableSubmit').hide();
//	$('.toggle_2').hide();
//	
//	restService.one("security").customGET('current_user').then(function(data){
//		$scope.currentUser  = data.originalElement;
//		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "производство"}}, "list").then(function(data){
//			$scope.placeByFromPlace = data.originalElement.resultList;
//			angular.forEach(data.originalElement.resultList, function(place, key){
//				$scope.placeByFromPlaceId = place.id;
//			});
//		});
//	});
//	
//	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
//		angular.forEach(data.originalElement.resultList, function(place, key){
//			if(place.placeType.name == 'склад'){
//				$scope.places.push(place);
//			}
//		});
//	});
//	
//	$scope.tableParams = new ngTableParams({
//        page: 1,            // show first page
//        count: 10, 			// count per page
//        sorting: {
//            name: 'asc'     // initial sorting
//        },				
//    }, {
//        total: 0, // length of data
//        getData: function($defer, params){
//        	Utils.ngTableGetData($defer, params, "product");
//        },
//    });
//	
//	restService.all("product").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
//		$scope.products = data.originalElement.resultList;
//	});
//	
//	restService.all("shipping").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
//		$scope.getShipping = data.originalElement.resultList;
//	});
//	
//	$scope.toggleTable = function(n){
//		$('.toggle_'+n).toggle(1000);
//	}
//	
//	$scope.date = new Date();
//	$scope.date1= $scope.date.getDate()+'-'+$scope.date.getMonth()+'-'+$scope.date.getFullYear();
//	
//	$scope.addToSendList = function(productId,amount){ // Add selected product to permanent list.
//		if(amount!=null && amount>0){
//			if($scope.sendRaws!=null){
//				$scope.sendRaws = {};
//				$('.toggle_2').hide();
//			}
//			if($scope.sendProducts[productId]==null){
//				$('.button_'+productId).hide(1000);
//				$('.button_'+productId).show(1000);				
//				
//				angular.forEach($scope.products, function(product, key){
//					if(product.id == productId){
//						$scope.sendProducts[productId] = product;
//					}
//				});
//				$scope.sendProducts[productId].amount = amount; 
//				$('.tableSubmit').show(1000);
//			}else{
//
//				$('.button_'+productId).hide(1000);
//				$('.button_'+productId).show(1000);
//				$scope.sendProducts[productId].amount = amount;
//			}
//		}else{
//			alert("Пожалуйста укажите верное количество!");
//		}
//		
//	}
//	
//	$scope.cancelProduct = function(productId){
//		delete $scope.sendProducts[productId];
//		$scope.sendRaws = {};
//		if(Object.keys($scope.sendProducts).length==0){
//			$('.tableSubmit').hide(1000);
//		}
//	}
//	
//	$scope.countRaw = function(){
//		$scope.sendRaws = {};
//		angular.forEach($scope.sendProducts, function(product, key){
//			restService.all("ingredient").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"product.id": product.id}}, "list").then(function(data){
//				angular.forEach(data.originalElement.resultList, function(ingredient, key){
//					if($scope.sendRaws[ingredient.raw.id]==null){
//						$scope.sendRaws[ingredient.raw.id] = ingredient.raw;
//						$scope.sendRaws[ingredient.raw.id].amount = ingredient.amount*product.amount;
//					}else{
//						$scope.sendRaws[ingredient.raw.id].amount += ingredient.amount*product.amount;
//					}
//				});
//			});
//		});
//		$('.toggle_1').hide();
//		$('.toggle_2').show();
//	}
//	
//	$scope.saveSendList = function(){
//		if($scope.placeByToPlaceId!=-1){
//			if(Object.keys($scope.sendRaws).length!=0){
//				angular.forEach($scope.sendRaws, function(raw, key){
//					$scope.shippingByRaws.push({"amount": raw.amount, "raw": {"id" : raw.id}});
//				});
//
//				var shipping = {"date": new Date, "placeByFromPlaceId": {"id" : parseInt($scope.placeByToPlaceId)}, "placeByToPlaceId": {"id" : $scope.placeByFromPlaceId }, "shippingStatus" : {"id" : 4}, "description": $scope.description};
//				var listShippingByProduct = {"shipping": shipping, "shippingByProducts": [], "shippingByRaws" : $scope.shippingByRaws}
//				restService.one("shipping").customPOST(listShippingByProduct, "create_list").then(function(data){
//					$('.tableSubmit').hide(1000);
//					alert("Запрос Сырья успешно отправлен!");
//					$scope.tableParams.reload();
//					$scope.sendRaws = {};
//					$scope.sendProducts = {};
//					$scope.shippingByRaws = [];
//					$scope.description = null;
//				});
//			}else{
//				alert("Пожалуйста сначала выберите cырье для запроса!!");
//			}
//		}else{
//			alert("Пожалуйста выберите Пункт Назначения!");
//		}
//	}
//}]);




//	}
//}]);