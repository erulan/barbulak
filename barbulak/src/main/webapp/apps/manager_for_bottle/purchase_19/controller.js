'use strict';

/* Controllers */
  // signin controller
app.controller('Purchaser19ListCtrl', ['$scope', 'restService','ngTableParams','Utils','$modal','$window', function($scope, restService, ngTableParams,Utils,$modal,$window) {

	/*scope.$watch('var',function(scope) {*/
	
	$scope.getpurchase19 ={};
	$scope.purchase19Raw ={};
	$scope.purchase19Detail = {};
	$scope.purchase19Info = {};
	$scope.purchase19CancelInfo = {};	
	$scope.purchase19CancelInfo.purchase = {};
	$scope.purchase19CancelInfo.staff = {};
	$scope.placeByFromPlace = {};
	$scope.name = "";
	
	$scope.getShippingList = function($defer, params, path) {
					var searchParams = {};
					var prm = params.parameters();
				
					searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
					searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
					searchParams["searchParameters"] = {"approvedByAdmin": true };
					/*searchParams["orderParamDesc"] = {"reqDate": true };*/
					searchParams["orderParamDesc"] = {"approveDate" : false};
					angular.forEach(prm.filter, function(value, key){
						searchParams.searchParameters[key] = value;
					});
					searchParams["orderParamDesc"] = {"reqDate": true };
					angular.forEach(prm.sorting, function(value, key){
						searchParams.orderParamDesc[key] = (value == "desc");
					});
					/*searchParams["orderParamDesc"] = {"approveDate" : false};*/
					
				/*	angular.forEach(prm.sorting, function(value, key){
						searchParams.orderParamDesc[key] = (value == "desc");
					});*/
					  	
					restService.all(path).customPOST(searchParams, ['list']).then(function(data){
						params.total(data.originalElement.totalRecords);
				        // set new data
				        $defer.resolve(data.originalElement.resultList);
					});
	}
	
			$scope.tableParams = new ngTableParams({
		        page: 1,            // show first page
		        count: 10, 			// count per page
		        sorting: {
		            name: 'asc'     // initial sorting
		        },		
		    }, {
		        total: 0, // length of data
		        getData: function($defer, params){
		        	
		        	restService.one("security").customGET('current_user').then(function(data){
			        	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id, "placeType.name": "производство"}}, "list").then(function(data){
			    			$scope.placeByFromPlace = data.originalElement.resultList;
			    			angular.forEach(data.originalElement.resultList, function(place, key){
			    				$scope.placeByFromPlaceId = place.id;
			    			});
			    			
			            	$scope.getShippingList($defer, params, "purchase_19");
			    		});
		        		
		        	});
		        },
		    });
/*	$scope.$watch($scope.purchaseRaws,function(newValue, oldValue){
		alert("VAoWATCH");
		angular.forEach($scope.purchaseRaws, function(raw,key){
			$scope.totalPrice += price[raw.id]*raw.amount;
		})
	})*/
	$scope.updateTotalPrice = function(){
		$scope.totalPrice = 0;
		angular.forEach($scope.purchaseRaws, function(raw,key){
			(raw.unitPrice!=null)? $scope.totalPrice +=raw.unitPrice*raw.amount : $scope.totalPrice += 0;
					
	})
	};

			
	$scope.showPurchaseDetails = function(purchase_19Id){
		$('#button_approve').hide();
		$('#button_cancel').hide();
		restService.all("purchase_19").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"purchase_19.id": purchase_19Id}}, "list").then(function(data){
			$scope.purchaseRaws = data.originalElement.resultList;
			$scope.updateTotalPrice();
		});
		/*restService.one("purchase_19").customGET(purchaseId).then(function(data){
			$scope.purchaseInfo = data.originalElement;
			if($scope.purchaseInfo.status.name == "Подтвержден администратором"){
				$('#button_approve').show();
			}else if($scope.purchaseInfo.status.name == "Подтвержден отправителем"){
				$('#button_cancel').show();
			}
		});*/
		$('.toggle_2').show(1000);
	}
	
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=1000, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.purchaseApprove = function(id){
		
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		

		
		modalInstance.result.then(function (selectedItem) {
			
				console.log($scope.totalPrice);
			if($scope.totalPrice !=null && $scope.totalPrice>0){
				$scope.purchase = {"id": $scope.purchaseInfo.id, "totalPrice": $scope.totalPrice};
				$scope.purchase.rawPurchases = $scope.purchaseRaws;
				console.log($scope.purchase)
				restService.one("purchase_19").customPOST($scope.purchase, "approve_by_acc/"+$scope.purchaseInfo.id).then(function(data){
						$scope.purchaseInfo.status.name="Отправлено";
						$scope.purchaseInfo.status.id = 1;
						$('#button_approve').hide(1000);
						alert("Закуп сырья успешно завершен, общая сумма составила!"+$scope.totalPrice);
				});
			}else{
				$scope.totalPrice = 0;
				alert("пожалуйста укажите правильную сумму сырья!");
			}
		}, function () {
		      return;
	    });
	};
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	$scope.shippingCancel = function(id){
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		modalInstance.result.then(function (selectedItem) {
			//var purchaseCancelInfo = {"date" : new Date(), "purchase" : {"id" : id}, "staff" : {"id" : 1}}
			$scope.purchaseInfo.status = {"id" :  3 }; // Canceled
			$scope.shippingInfo.toPlaceId = {"id": $scope.purchaseInfo.toPlaceId.id};// Canceled
			angular.forEach($scope.purchaseRaw, function(raw, key){
				$scope.purchaseInfo.rawPurchases = [{"amount":raw.amount, "raw":{"id":raw.raw.id}}];
			})
			restService.one("shipping").customPOST($scope.purchaseInfo, "update/" + id).then(function(data){
				$scope.purchaseInfo.status.name="отменен";
				 $scope.tableParams.reload();
			});
		}, function () {
		      return;
	    });
	};
	$scope.toggleTable(2);
	
	/*});*/
  }]);

