'use strict';

/* Controllers */
  // signin controller
app.controller('DeliveryListCtrl', ['$scope', '$state', 'restService', 'ngTableParams', 'Utils','$modal',  function($scope, $state, restService, ngTableParams, Utils, $modal) {
	
	$scope.delivery = {};
	$scope.displayTableRow = {};
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            "plannedDate": true     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "delivery");
        },
    });
	
	
	restService.all("delivery_status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(entry, key){
			if(entry.name == "отправлено") $scope.deliveryStatusSentId = entry.id;
			if(entry.name == "завершено") $scope.deliveryStatusCompletedId = entry.id;
		});
	});
	
	$scope.deleteDelivery = function(id){
		Utils.callDeleteModal("delivery", id);
		$state.reload();
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  
    
    $scope.init = function() {
        $scope.displayTableRow.agent = {show: true, name: 'Агент'};
        $scope.displayTableRow.plannedDate = {show: true, name: 'Дата Планировки'};
        $scope.displayTableRow.sentDate = {show: true, name: 'Дата Отправки'};
        $scope.displayTableRow.completedDate = {show: true, name: 'Дата Завершении'};
        $scope.displayTableRow.deliveryStatus = {show: true, name: 'Статус'};
        
    }
  }]);
app.controller('DeliveryAddCtrl', ['$scope', '$timeout', '$state', 'restService', 'ngTableParams', 'Utils','$modal',  function($scope, $timeout, $state, restService, ngTableParams, Utils, $modal) {

    $scope.delivery = {};
    $scope.delivery.agent = {};
    $scope.delivery.deliveryClients = [];
    $scope.deliveryClient = {};
    $scope.deliveryClient.agreement = {};
    
    $scope.datepicker = {};
    
    restService.all("staff_position").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"position.name": "Торговый агент"}}, "list").then(function(data){
		$scope.staffPositions =  data.originalElement.resultList;
	});
    restService.all("agreement").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"client.clientFor19Bottle": true}}, "list").then(function(data){
		$scope.agreements =  data.originalElement.resultList;
	});
    
    $scope.addDeliveryClient = function(){
    	var duplicated = false;
    	if($scope.deliveryClient == null || $scope.deliveryClient.agreement == null || $scope.deliveryClient.agreement.id == null || $scope.deliveryClient.amount == null) return;
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		if(entry.agreement.id == $scope.deliveryClient.agreement.id){
    			alert("Вы уже добавили этого клиента.");
    			duplicated = true;;
    		}
    	});
    	if(!duplicated) $scope.delivery.deliveryClients.push(angular.copy($scope.deliveryClient));
    	$scope.deliveryClient = {agreement: null};
    }
    
    $scope.removeDeliveryClient = function(agreementId){
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		if(entry.agreement.id == agreementId){
    			$scope.delivery.deliveryClients.splice(key, 1);
    			$timeout(function() {
    				$scope.$apply();
				})
    		}	
    	});
    } 
    
    $scope.create = function(){
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		$scope.delivery.deliveryClients[key].agreement = {"id": entry.agreement.id};
    	});
    	restService.one("delivery").customPOST($scope.delivery, "create").then(function(data){
    		alert("Успешно создана!");
    		$state.go('manager_for_bottle.delivery.list');
    	});
    }
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
	  $scope.toggleTable = function(n){
			$('.toggle_'+n).toggle(1000);
		}
	

}]);


app.controller('DeliveryEditCtrl', ['$scope', '$timeout', '$state', '$stateParams', 'restService', 'ngTableParams', 'Utils','$modal',  function($scope, $timeout, $state, $stateParams, restService, ngTableParams, Utils, $modal) {

    $scope.deliveryClient = {};
    $scope.deliveryClient.agreement = {};
    $scope.deliveryId = $stateParams.id;
    
    $scope.datepicker = {};
    
    
    restService.all("delivery").customGET( $scope.deliveryId).then(function(data){
		$scope.delivery= data.originalElement;
	});
    restService.all("staff_position").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"position.name": "Торговый агент"}}, "list").then(function(data){
		$scope.staffPositions =  data.originalElement.resultList;
	});
    restService.all("agreement").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"client.clientFor19Bottle": true}}, "list").then(function(data){
		$scope.agreements =  data.originalElement.resultList;
	});
    
    $scope.addDeliveryClient = function(){
    	var duplicated = false;
    	if($scope.deliveryClient == null || $scope.deliveryClient.agreement == null || $scope.deliveryClient.agreement.id == null || $scope.deliveryClient.amount == null) return;
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		if(entry.agreement.id == $scope.deliveryClient.agreement.id){
    			alert("Вы уже добавили этого клиента.");
    			duplicated = true;;
    		}
    	});
    	if(!duplicated) $scope.delivery.deliveryClients.push(angular.copy($scope.deliveryClient));
    	$scope.deliveryClient = {agreement: null};
    }
    
    $scope.removeDeliveryClient = function(agreementId){
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		if(entry.agreement.id == agreementId){
    			$scope.delivery.deliveryClients.splice(key, 1);
    			$timeout(function() {
    				$scope.$apply();
				})
    		}	
    	});
    } 
    
    $scope.update = function(){
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		$scope.delivery.deliveryClients[key].agreement = {"id": entry.agreement.id};
    	});
    	restService.one("delivery").customPOST($scope.delivery, "update/" + $scope.deliveryId).then(function(data){
    		alert("Успешно Обновлено!");
    		$state.go('manager_for_bottle.delivery.list');
    	});
    }
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
	  $scope.toggleTable = function(n){
			$('.toggle_'+n).toggle(1000);
		}
	

}]);

app.controller('DeliverySendCtrl', ['$scope', '$timeout', '$state', '$stateParams', 'restService', 'ngTableParams', 'Utils','$modal', '$window',  
                                    function($scope, $timeout, $state, $stateParams, restService, ngTableParams, Utils, $modal,$window) {

    $scope.deliveryClient = {};
    $scope.deliveryClient.agreement = {};
    $scope.deliveryId = $stateParams.id;
    
    $scope.datepicker = {};
    
    
    restService.all("delivery").customGET( $scope.deliveryId).then(function(data){
		$scope.delivery = data.originalElement;
	});
    restService.all("staff_position").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"position.name": "Торговый агент"}}, "list").then(function(data){
		$scope.staffPositions =  data.originalElement.resultList;
	});
    restService.all("agreement").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"client.clientFor19Bottle": true}}, "list").then(function(data){
		$scope.agreements =  data.originalElement.resultList;
	});
    
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=1000, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	/*$scope.init = function() {
    	$scope.showNS = false;
    	$scope.showNum = true;
    	$scope.showDate1 = false;
    	$scope.showDate2 = false;
    	$scope.showDate3 = false;
    	$scope.showZalog = true;
    	$scope.showMin = true;
    	$scope.showCol = true;
    	$scope.showSum = false;
    	$scope.showPod = false;
    	$scope.showOpi = true;
    }*/
	
    $scope.addDeliveryClient = function(){
    	var duplicated = false;
    	if($scope.deliveryClient == null || $scope.deliveryClient.agreement == null || $scope.deliveryClient.agreement.id == null || $scope.deliveryClient.amount == null) return;
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		if(entry.agreement.id == $scope.deliveryClient.agreement.id){
    			alert("Вы уже добавили этого клиента.");
    			duplicated = true;;
    		}
    	});
    	if(!duplicated) $scope.delivery.deliveryClients.push(angular.copy($scope.deliveryClient));
    	$scope.deliveryClient = {agreement: null};
    }
    
    $scope.removeDeliveryClient = function(agreementId){
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		if(entry.agreement.id == agreementId){
    			$scope.delivery.deliveryClients.splice(key, 1);
    			$timeout(function() {
    				$scope.$apply();
				})
    		}	
    	});
    } 
    
    $scope.send = function(){
    	angular.forEach($scope.delivery.deliveryClients, function(entry, key){
    		$scope.delivery.deliveryClients[key].agreement = {"id": entry.agreement.id};
    	});
    	restService.one("delivery").customPOST($scope.delivery, "send").then(function(data){
    		alert("Успешно Отправлен!");
    		$state.go('manager_for_bottle.delivery.list');
    	});
    }
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
	  $scope.toggleTable = function(n){
			$('.toggle_'+n).toggle(1000);
		}
	

}]);

app.controller('DeliveryCompleteCtrl', ['$scope', '$timeout', '$state', '$stateParams', 'restService', 'ngTableParams', 'Utils','$modal',  function($scope, $timeout, $state, $stateParams, restService, ngTableParams, Utils, $modal) {

    $scope.deliveryClient = {};
    $scope.deliveryClient.agreement = {};
    $scope.deliveryId = $stateParams.id;    
    
    restService.all("delivery").customGET( $scope.deliveryId).then(function(data){
		$scope.delivery= data.originalElement;
	});
    
    $scope.complete = function(){
    	restService.one("delivery").customPOST($scope.delivery, "complete").then(function(data){
    		alert("Успешно Завершено!");
    		$state.go('manager_for_bottle.delivery.list');
    	});
    }
    
	  $scope.toggleTable = function(n){
			$('.toggle_'+n).toggle(1000);
		}
	

}]);
app.controller('DeliveryViewCtrl', ['$scope', '$timeout', '$state', '$stateParams', 'restService', 'ngTableParams', 'Utils','$modal',  function($scope, $timeout, $state, $stateParams, restService, ngTableParams, Utils, $modal) {

    $scope.deliveryClient = {};
    $scope.deliveryClient.agreement = {};
    $scope.deliveryId = $stateParams.id;
    
    $scope.datepicker = {};
    
    
    restService.all("delivery").customGET( $scope.deliveryId).then(function(data){
		$scope.delivery = data.originalElement;
	});
    restService.all("staff_position").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"position.name": "Торговый агент"}}, "list").then(function(data){
		$scope.staffPositions =  data.originalElement.resultList;
	});
    restService.all("agreement").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"client.clientFor19Bottle": true}}, "list").then(function(data){
		$scope.agreements =  data.originalElement.resultList;
	});
    
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=1000, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	 $scope.datepicker.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.datepicker.opened = true;
		  };
		  
		  $scope.datepicker.format = 'dd/MM/yyyy';

		  $scope.datepicker.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };
		
    

}]);