'use strict';

/* Controllers */
  // signin controller
app.controller('Reminder19ListCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils','$modal','$stateParams', 
                                      function($scope, $location, restService, ngTableParams, Utils, $modal, $stateParams) {
	
	$scope.reminder_19 = {};
	
    $scope.reminder_19Id = $stateParams.id;
    
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "reminder_19");
        },
    });
	
	$scope.deleteRemider19 = function(id){
		Utils.callDeleteModal("reminder_19", id);
		$state.reload();
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  
    
    $scope.reminder19Approve = function(id){
		console.log("reminder 19 approve is work");
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/reminder19Modal.html',
		      controller: 'ReminderModal',
		      size: 'md'
		    });
		console.log("reminder 19 approve is work2");
		modalInstance.result.then(function (selectedItem) {
			console.log("reminder 19 approve is work 2");
			$state.reload();
			}, function () {
				
		      return;
	    });
	};
	
	
   
    $scope.init = function() {
        $scope.ShowName = true;
        $scope.ShowSurname = true;
        $scope.ShowNum = true;
        $scope.ShowDate = true;
        $scope.ShowOpis = true;
        $scope.ShowOpc = true;
    }
  }]);
app.controller('ReminderModal', ['$scope', 'restService','$modalInstance','$stateParams', '$rootScope', function ($scope, restService, $stateParams,$modalInstance, $rootScope ) {

	$scope.datepicker = {};
	$scope.reminder_19 = {};
	$scope.reminder_19Id = $stateParams.id;
	
	

	
	  $scope.save = function () {
		restService.one("reminder_19").customPOST($scope.reminder_19, "update/" + $scope.reminder_19Id).then(function(data){
				console.log("update is work");
					alert("Успешно отклонен!");
					$scope.tableParams.reload();
//					$('#button_approve').hide();
			$modalInstance.close('save');
			
		});
	    
	  };

	  $scope.cancel = function () {
	    $modalInstance.dismiss('cancel');
	    $state.reload();
	  };
	  
	  $scope.datepicker.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.datepicker.opened = true;
		  };
		  
		  $scope.datepicker.format = 'dd/MM/yyyy';

		  $scope.datepicker.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };
	  
}]);
app.controller('Reminder19AddCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils','$modal',  function($scope, $location, restService, ngTableParams, Utils, $modal) {

    $scope.reminder_19 = {};
    $scope.datepicker = {};
    
    restService.all("reminder_19").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.reminder_19s = data.originalElement.resultList;
	});
    
    restService.all("agreement").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.agreements = data.originalElement.resultList;
	});
    
    $scope.create = function(){
    	restService.one("reminder_19").customPOST($scope.reminder_19, "create").then(function(data){
    		alert("Успешно создана!");
    		$location.path("/manager_for_bottle/reminder_19/list");
    	});
    }
    
    
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
	
	

}]);
app.controller('Reminder19ViewCtrl', ['$scope', '$window', '$location', 'restService', '$stateParams', function($scope, $window, $location, restService, $stateParams) {
    $scope.name = "ADDCONTROLLER";
    $scope.agreement = {};
    $scope.datepicker = {};
	$scope.agreementId = $stateParams.id;
	
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
		};
	
	restService.all("agreement").customGET( $scope.agreementId).then(function(data){
		$scope.agreement= data.originalElement;
	});
	
    restService.all("client").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.clients = data.originalElement.resultList;
	
		$scope.update = function(){
	    	restService.one("agreement").customPOST($scope.agreement, "update/" + $scope.agreementId).then(function(data){
	    		alert("Успешно обновлена!");
	    		$location.path("manager_for_bottle/agreement/list");
	    	});
	    }
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	 
	  });
    }
  
  ]);
