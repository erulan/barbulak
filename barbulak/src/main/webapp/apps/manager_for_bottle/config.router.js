'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/manager_for_bottle/reminder_19/list');
          $stateProvider
              .state('manager_for_bottle', {
                  abstract: true,
                  url: '/manager_for_bottle',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //client
              .state('manager_for_bottle.client', {
                  abstract: true,
                  url: '/client',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager_for_bottle/client/controller.js']);
                      }]
                    }
              })
              .state('manager_for_bottle.client.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager_for_bottle/client/list.html',
              })
              .state('manager_for_bottle.client.add', {
                  url: '/add',
                  templateUrl: '../../apps/manager_for_bottle/client/add.html',
              })
              .state('manager_for_bottle.client.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/manager_for_bottle/client/edit.html',
              })
              
              //product_type
              .state('manager_for_bottle.agreement', {
                  abstract: true,
                  url: '/agreement',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager_for_bottle/agreement/controller.js']);
                      }]
                    }
              })
              .state('manager_for_bottle.agreement.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager_for_bottle/agreement/list.html',
              })
              .state('manager_for_bottle.agreement.add', {
                  url: '/add',
                  templateUrl: '../../apps/manager_for_bottle/agreement/add.html',
              })
               .state('manager_for_bottle.agreement.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/manager_for_bottle/agreement/edit.html',

              })
              .state('manager_for_bottle.agreement.view', {
                  url: '/view/:id',
                  templateUrl: '../../apps/manager_for_bottle/agreement/view.html',

              })
              
               //lost_bottle
              .state('manager_for_bottle.lost_bottle', {
                  abstract: true,
                  url: '/lost_bottle',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager_for_bottle/lost_bottle/controller.js']);
                      }]
                    }
              })
              .state('manager_for_bottle.lost_bottle.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager_for_bottle/lost_bottle/list.html',
              })
              .state('manager_for_bottle.lost_bottle.add', {
                  url: '/add',
                  templateUrl: '../../apps/manager_for_bottle/lost_bottle/add.html',
              })
            
              
               //reminder_19
              .state('manager_for_bottle.reminder_19', {
                  abstract: true,
                  url: '/reminder_19',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager_for_bottle/reminder_19/controller.js']);
                      }]
                    }
              })
              .state('manager_for_bottle.reminder_19.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager_for_bottle/reminder_19/list.html',
              })
              .state('manager_for_bottle.reminder_19.add', {
                  url: '/add',
                  templateUrl: '../../apps/manager_for_bottle/reminder_19/add.html',
              })
              .state('manager_for_bottle.reminder_19.view', {
                  url: '/view/:id',
                  templateUrl: '../../apps/manager_for_bottle/reminder_19/view.html',

              })
              
              
               //delivery
              .state('manager_for_bottle.delivery', {
                  abstract: true,
                  url: '/delivery',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager_for_bottle/delivery/controller.js']);
                      }]
                    }
              })
              .state('manager_for_bottle.delivery.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager_for_bottle/delivery/list.html',
              })
              .state('manager_for_bottle.delivery.add', {
                  url: '/add',
                  templateUrl: '../../apps/manager_for_bottle/delivery/add.html',

              })
              .state('manager_for_bottle.delivery.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/manager_for_bottle/delivery/edit.html',

              })
              .state('manager_for_bottle.delivery.send', {
                  url: '/send/:id',
                  templateUrl: '../../apps/manager_for_bottle/delivery/send.html',

              })
              .state('manager_for_bottle.delivery.complete', {
                  url: '/complete/:id',
                  templateUrl: '../../apps/manager_for_bottle/delivery/complete.html',

              })
              .state('manager_for_bottle.delivery.view', {
                  url: '/view',
                  templateUrl: '../../apps/manager_for_bottle/delivery/view.html',

              })
              
              
             
      }
    ]
  );
