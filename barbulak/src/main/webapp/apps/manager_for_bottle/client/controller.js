'use strict';

/* Controllers */
  // signin controller
app.controller('ClientListCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils','$modal', 
                                  function($scope, $location, restService, ngTableParams, Utils, $modal) {
	
	$scope.cashboxs = {};
	$scope.datepicker = {};
	$scope.client = {};
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "client");
        },
    });
	$scope.deleteClient = function(id){
		Utils.callDeleteModal("client", id);
		$state.reload();
	};
    
    restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000, "searchParameters":{"name": "Бишкек"}}, "list").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.exportData = function () {
			var table= document.getElementById("exportable");
			var html = table.outerHTML;
			window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
	    };

	  $scope.init = function() {
	    	$scope.showBl = true;
	    	$scope.showName = true;
	    	$scope.showPassport = false;
	    	$scope.showBirthDate = false;
	    	$scope.showLive = true;
	    	$scope.showDest = true;
	    	$scope.showNum = true;
	    	$scope.showMail = false;
	    	$scope.showCash = false;
	    	$scope.showOper = true;
	    	
	    }
	  
  }]);

app.controller('ClientAddCtrl', ['$scope', 'restService','$state', function($scope, restService, $state) {

	$scope.client = {};
	$scope.client.cashbox = {};
	$scope.datepicker = {};
	
	restService.all("client").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.clients = data.originalElement.resultList;
	});
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000, "searchParameters":{"name": "Бишкек"}}, "list").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});

	$scope.create = function(){
		restService.one("client").customPOST($scope.client, "create").then(function(){
			alert("Успешно добавлен!");
			$state.go("manager_for_bottle.client.list");
		});
	};
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };

	 

}]);

app.controller('ClientEditCtrl', ['$scope', 'restService','$state','$stateParams','$modal', function($scope, restService, $state, $stateParams,$modal) {

	$scope.client = {};
	
	$scope.clientId = $stateParams.id;
	$scope.datepicker = {};
	
	restService.all("client").customGET( $scope.clientId ).then(function(data){
		$scope.client = data.originalElement;
	});
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000, "searchParameters":{"name": "Бишкек"}}, "list").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});

	$scope.update = function(){
		restService.one("client").customPOST($scope.client, "update/" + $scope.clientId).then(function(){
			alert("Успешно был обнавлен!");
			$state.go("manager_for_bottle.client.list");
		});
	};
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };

	 

}]);
