'use strict';

/* Controllers */
  // signin controller
app.controller('LostBottleListCtrl', ['$scope', '$state', 'restService', 'ngTableParams', 'Utils','$modal',  function($scope, $state, restService, ngTableParams, Utils, $modal) {
	
	$scope.lostBottle = {};
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "lost_bottle");
        },
    });
	
	
	$scope.deleteLostBottle = function(id){
		Utils.callDeleteModal("lost_bottle", id);
		$state.reload();
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  
    
    $scope.init = function() {
        $scope.ShowName = true;
        $scope.ShowSurname = true;
        $scope.ShowDate = true;
        $scope.ShowCol = true;
        $scope.ShowOpis = true;
        $scope.ShowOpc = true;
    }
  }]);
app.controller('LostBottleAddCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils','$modal',  function($scope, $location, restService, ngTableParams, Utils, $modal) {

    $scope.lostBottle = {};
    $scope.lostBottle.agreement = {};
    $scope.lostBottle.agreement.id;
    
    $scope.datepicker = {};
    
    restService.all("lost_bottle").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.lost_bottles = data.originalElement.resultList;
	});
    
    restService.all("agreement").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.agreements = data.originalElement.resultList;
	});
    
    $scope.create = function(){
    	restService.one("lost_bottle").customPOST($scope.lostBottle, "create").then(function(data){
    		alert("Успешно создана!");
    		$location.path("/manager_for_bottle/lost_bottle/list");
    	});
    }
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
	
	

}]);