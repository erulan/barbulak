'use strict';

/* Controllers */
  // signin controller
app.controller('AgreementListCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils','$modal', '$state', function($scope, $location, restService, ngTableParams, Utils, $modal, $state) {
	
	restService.all("agreement").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.agreements = data.originalElement.resultList;
	});
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "agreement");
        },
    });
	
	
	$scope.deleteAgreement = function(id){
		Utils.callDeleteModal("agreement", id);
		$state.reload();
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
    $scope.init = function() {
    	$scope.showBl = true;
    	$scope.showNum = false;
    	$scope.showClient = true;
    	$scope.showCreate = true;
    	$scope.showEnd = true;
    	$scope.showAmount = true;
    	$scope.showConf = false;
    	$scope.showVirtual = false;
    	$scope.showpledgeFee = true;
    	$scope.showunitPriceForBottle = true;
    	$scope.showdispanserAmount = false;
    	$scope.showdispanserDescription = false;
    	$scope.showlastDispanserWashDate = false;
    	$scope.showlastPurchaseDate = false;
    	$scope.showalertDate = false;
    	$scope.showlostBottleNumber = false;
    	$scope.showPrim = false;
    	$scope.showOper = true;
    }
  }])
  app.controller('AgreementAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
    $scope.name = "ADDCONTROLLER";
    $scope.agreement = {};
    $scope.datepicker = {};
	$scope.datepicker2 = {};
	$scope.datepicker3 = {};
	$scope.datepicker4 = {};
	$scope.datepicker5 = {};
	$scope.datepicker6 = {};
/*
    $scope.agreement.lastDispanserWashDate = new Date();*/
	//$scope.datepicker3= $scope.date.getDate()+'-'+$scope.date.getMonth()+'-'+$scope.date.getFullYear();
    
	//$scope.agreement.lastDispanserWashDate = $scope.date.getDate()+'-'+$scope.date.getMonth()+'-'+$scope.date.getFullYear();
	
    restService.all("client").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.clients = data.originalElement.resultList;
	
    $scope.create = function(){
    	restService.one("agreement").customPOST($scope.agreement, "create").then(function(data){
    		alert("Успешно создана!");
    		$location.path("manager_for_bottle/agreement/list");
    	});
    }
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
	  
	  $scope.datepicker2.format = 'dd/MM/yyyy';

	  $scope.datepicker2.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  }

	  $scope.datepicker3.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.datepicker3.opened = true;
		  };
		  
		  $scope.datepicker3.format = 'dd/MM/yyyy';

		  $scope.datepicker3.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };
		  
		  $scope.datepicker4.open = function($event) {
			    $event.preventDefault();
			    $event.stopPropagation();

			    $scope.datepicker4.opened = true;
			  };
			  
			  $scope.datepicker4.format = 'dd/MM/yyyy';

			  $scope.datepicker4.dateOptions = {
			    formatYear: 'yy',
			    startingDay: 1,
			    language: 'ru'
			  };
			  
			  $scope.datepicker5.open = function($event) {
				    $event.preventDefault();
				    $event.stopPropagation();

				    $scope.datepicker5.opened = true;
				  };
				  
				  $scope.datepicker5.format = 'dd/MM/yyyy';

				  $scope.datepicker5.dateOptions = {
				    formatYear: 'yy',
				    startingDay: 1,
				    language: 'ru'
				  };
				  
	  $scope.datepicker6.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.datepicker6.opened = true;
		  };
		  
		  $scope.datepicker6.format = 'dd/MM/yyyy';

		  $scope.datepicker6.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };
	  });
    }
  ])
  
  app.controller('AgreementEditCtrl', ['$scope', '$location', 'restService', '$stateParams', function($scope, $location, restService, $stateParams) {
    $scope.name = "ADDCONTROLLER";
    $scope.agreement = {};
    $scope.datepicker = {};
	$scope.datepicker2 = {};
	$scope.datepicker3 = {};
	$scope.datepicker4 = {};
	$scope.datepicker5 = {};
	$scope.agreementId = $stateParams.id;
	
	restService.all("agreement").customGET( $scope.agreementId).then(function(data){
		$scope.agreement= data.originalElement;
		
//		$scope.agreement.startDate = new Date($scope.agreement.startDate).toISOString();
	});
	
    restService.all("client").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.clients = data.originalElement.resultList;
	
		$scope.update = function(){
	    	restService.one("agreement").customPOST($scope.agreement, "update/" + $scope.agreementId).then(function(data){
	    		alert("Успешно обновлена!");
	    		$location.path("manager_for_bottle/agreement/list");
	    	});
	    }
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
	  
	  $scope.datepicker2.format = 'dd/MM/yyyy';

	  $scope.datepicker2.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  }

	  $scope.datepicker3.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.datepicker3.opened = true;
		  };
		  
		  $scope.datepicker3.format = 'dd/MM/yyyy';

		  $scope.datepicker3.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };
		  
		  $scope.datepicker4.open = function($event) {
			    $event.preventDefault();
			    $event.stopPropagation();

			    $scope.datepicker4.opened = true;
			  };
			  
			  $scope.datepicker4.format = 'dd/MM/yyyy';

			  $scope.datepicker4.dateOptions = {
			    formatYear: 'yy',
			    startingDay: 1,
			    language: 'ru'
			  };
			  
			  $scope.datepicker5.open = function($event) {
				    $event.preventDefault();
				    $event.stopPropagation();

				    $scope.datepicker5.opened = true;
				  };
				  
				  $scope.datepicker5.format = 'dd/MM/yyyy';

				  $scope.datepicker5.dateOptions = {
				    formatYear: 'yy',
				    startingDay: 1,
				    language: 'ru'
				  };
	  });
    }
  ])
  app.controller('AgreementViewCtrl', ['$scope', '$window', '$location', 'restService', '$stateParams', function($scope, $window, $location, restService, $stateParams) {
    $scope.name = "ADDCONTROLLER";
    $scope.agreement = {};
    $scope.datepicker = {};
	$scope.datepicker2 = {};
	$scope.datepicker3 = {};
	$scope.datepicker4 = {};
	$scope.datepicker5 = {};
	$scope.agreementId = $stateParams.id;
	
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
		};
	
	restService.all("agreement").customGET( $scope.agreementId).then(function(data){
		$scope.agreement= data.originalElement;
	});
	
    restService.all("client").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.clients = data.originalElement.resultList;
	
		$scope.update = function(){
	    	restService.one("agreement").customPOST($scope.agreement, "update/" + $scope.agreementId).then(function(data){
	    		alert("Успешно обновлена!");
	    		$location.path("manager_for_bottle/agreement/list");
	    	});
	    }
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
	  
	  $scope.datepicker2.format = 'dd/MM/yyyy';

	  $scope.datepicker2.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  }

	  $scope.datepicker3.open = function($event) {
		    $event.preventDefault();
		    $event.stopPropagation();

		    $scope.datepicker3.opened = true;
		  };
		  
		  $scope.datepicker3.format = 'dd/MM/yyyy';

		  $scope.datepicker3.dateOptions = {
		    formatYear: 'yy',
		    startingDay: 1,
		    language: 'ru'
		  };
		  
		  $scope.datepicker4.open = function($event) {
			    $event.preventDefault();
			    $event.stopPropagation();

			    $scope.datepicker4.opened = true;
			  };
			  
			  $scope.datepicker4.format = 'dd/MM/yyyy';

			  $scope.datepicker4.dateOptions = {
			    formatYear: 'yy',
			    startingDay: 1,
			    language: 'ru'
			  };
			  
			  $scope.datepicker5.open = function($event) {
				    $event.preventDefault();
				    $event.stopPropagation();

				    $scope.datepicker5.opened = true;
				  };
				  
				  $scope.datepicker5.format = 'dd/MM/yyyy';

				  $scope.datepicker5.dateOptions = {
				    formatYear: 'yy',
				    startingDay: 1,
				    language: 'ru'
				  };
	  });
    }
  
  ]);
