'use strict';

/* Controllers */
  // signin controller


app.controller('ListDistributionCtrl', ['$scope', 'restService','ngTableParams','Utils','$modal','$window', 
                                  function($scope, restService, ngTableParams,Utils, $modal, $window) {

	$scope.getShipping ={};
	$scope.shippingProduct ={};
	$scope.shippingDetail = {};
	$scope.shippingInfo = {};
	$scope.shippingInfo.shippingStatus = {};
	$scope.shippingCancelInfo = {};	
	$scope.shippingCancelInfo.shipping = {};
	$scope.shippingCancelInfo.staff = {};
	$scope.placeByFromPlaceId = {};
	$scope.userId = {};
	$scope.totalPrice =0;
	
	$scope.getShippingList = function($defer, params, path) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		
		searchParams["searchParameters"] = {"placeByFromPlaceId" : $scope.placeByFromPlaceId};
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {"created" : true};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		
//		searchParams.searchParameters["shippingStatus.id"] = [1,2,3,5];    	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	restService.one("security").customGET('current_user').then(function(data){
        		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.placeByFromPlaceId = place.id;
	    			});
	            	$scope.getShippingList($defer, params, "ware_distributor");
        		});
    		});
        },
    });

	$scope.showShippingDetails = function(shippingId){
		$('#button_approve').hide();
		$('#button_cancel').hide();
		restService.all("Ware_dist_product").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"wareDistributor.id": shippingId}}, "list").then(function(data){
			$scope.shippingProducts = data.originalElement.resultList;
			$scope.totalPrice = 0;
			angular.forEach($scope.shippingProducts, function(sendProduct, key){
				$scope.totalPrice += sendProduct.amount *(sendProduct.unitPrice+sendProduct.changeInPrice);
			})
		});
		
		restService.one("ware_distributor").customGET(shippingId).then(function(data){
			$scope.shippingInfo = data.originalElement;
			if($scope.shippingInfo.wareDistStatus.name == "Запрос"){
				$('#button_cancel').show();
			}else if ($scope.shippingInfo.wareDistStatus.name == "Подтвержден администратором"){
				$('#button_approve').show();
				$('#button_cancel').show();
			}
		});
		
		$('.toggle_2').show(1000);
	}
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.shippingApprove = function(id){
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		
			modalInstance.result.then(function (selectedItem) {
			restService.one("ware_distributor").customGET("approve_by_from_place/"+id).then(function(data){
				alert("Продукция успешно отправлена торговому агенту !");
				$('#button_approve').hide(1000);
				 $scope.tableParams.reload(); 
			});
			}, function () {
			      return;
		    });
	}
	
	
	$scope.shippingCancel = function(id, description){
		
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });

		modalInstance.result.then(function (selectedItem) {
			restService.one("ware_distributor").customGET("reject_by_from_place/"+id+'/'+description).then(function(data){
				alert("Запрос продукции отменен!");
				$scope.tableParams.reload();
			});
		}, function () {
		      return;
	    });
	}	
	$scope.toggleTable(2);
	
  }]);
app.controller('AddDistributionCtrl', ['$scope', 'restService','ngTableParams', 'Utils','$window', function($scope, restService, ngTableParams, Utils,$window){
	
	$scope.staffId = {};
	$scope.staff = {};
	$scope.wareDistProducts = [];
	$scope.sendProducts = {};
	$scope.placeByFromPlaceId = {};
	$scope.totalPrice =0;
	$scope.date = new Date();
	$scope.staffId=-1;	
	$('.tableSubmit').hide();
	
	restService.all("staff_position").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"position.name": "Торговый агент"}}, "list").then(function(data){
		$scope.staff =  data.originalElement.resultList;
	});
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
				restService.all("product_place").customPOST({startIndex: 0, resultQuantity: 1000, "searchParameters":{"place.id" : $scope.placeByFromPlaceId}}, "list").then(function(data){
					$scope.products = data.originalElement.resultList;
				});
			});
		});
	});
	
	$scope.getProductsList = function($defer, params, path) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;

		searchParams["searchParameters"] = {"place.id" : $scope.placeByFromPlaceId};
		searchParams["searchParamWithTypes"] = [{searchField:"amount", searchValue: 0, searchType: "NOT_EQUAL"}];
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {"date" : true};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		
//		searchParams.searchParameters["shippingStatus.id"] = [1,2,3,5];    	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	restService.one("security").customGET('current_user').then(function(data){
        		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.placeByFromPlaceId = place.id;
	    			});

	    			$scope.getProductsList($defer, params, "product_place");
        		});
    		});
        },
    });
	
	
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.init = function() {
		$scope.showPrice  = true;
        $scope.showFizzy = false;
        $scope.showCapacity = true;
        $scope.showCategory = true;
        $scope.showType = true;
        $scope.showName = true;
    }
	
	$scope.addToSendList = function(productId,amount, changeInPrice){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			if(changeInPrice!=null){
					$('.button_'+productId).hide(1000);
					$('.button_'+productId).show(1000);				
					
					angular.forEach($scope.products, function(product, key){
						
        				if(productId == product.product.id){
        					if(product.amount >= amount){
        						delete $scope.sendProducts[productId];
        			        	
        						$scope.sendProducts[productId] = product.product;
	        					$scope.sendProducts[productId].amount = amount;
	        					$scope.sendProducts[productId].changeInPrice = changeInPrice; 
	        					$scope.sendProducts[productId].unitPrice = product.product.price;

	        					$scope.totalPrice =0;
	        					angular.forEach($scope.sendProducts, function(sendProduct, key){
	        						$scope.totalPrice += sendProduct.amount *(sendProduct.unitPrice+sendProduct.changeInPrice);
	        					})
        					}else{
        						alert("Данного продукта: '"+product.product.name+"' на складе только"+product.amount+" !");
        					}
    					}
        			});
					$('.tableSubmit').show(1000);
			}else{
				alert("Пожалуйста укажите верную разницу в цене!");
			}
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
		
	}
	
	$scope.cancelProduct = function(productId){
		delete $scope.sendProducts[productId];
		$scope.totalPrice =0;
		angular.forEach($scope.sendProducts, function(sendProduct, key){
			$scope.totalPrice += sendProduct.amount *(sendProduct.unitPrice+sendProduct.changeInPrice);
		})

		if(Object.keys($scope.sendProducts).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.saveSendList = function(){
		if($scope.staffId!=-1){
			if(Object.keys($scope.sendProducts).length!=0){
				angular.forEach($scope.sendProducts, function(product, key){
					$scope.wareDistProducts.push({"amount": product.amount, "product": {"id" : product.id}, "unitPrice": product.unitPrice, "changeInPrice": product.changeInPrice});
				});
				
				var wareDistributor = {"date": new Date, "placeByFromPlaceId": {"id" : $scope.placeByFromPlaceId}, "staff": {"id" : parseInt($scope.staffId) },"description": $scope.description};
				
				var listShippingByProduct = {"wareDistributor": wareDistributor, "wareDistProducts": $scope.wareDistProducts}
				
				restService.one("ware_distributor").customPOST(listShippingByProduct, "create_list").then(function(data){
				
					$('.tableSubmit').hide(1000);
					alert("Передача продукции торговому агенту успешно сохраненa");
					$scope.tableParams.reload();
					$scope.sendProducts = {};
					$scope.wareDistProducts = [];
					$scope.description = null;
					$scope.staffId=-1;	
				});
			}else{
				alert("Пожалуйста сначала выберите продукции для выдачи!!");
			}
		}else{
			alert("Пожалуйста выберите Торгового агента!");
		}
	}
}]);

//app.controller('EditDefectCtrl' ['$scope', 'restService', '$routeParams', function($scope, restService, $routeParams){
//	
//}]);
//
//
//app.controller('InfoDefectCtrl', ['$scope','restService', '$routeParams', function($scope, restService, $routeParams) {
//    
//    $scope.defect = {};
//    $scope.defectId = $routeParams.id;
//    
//    $scope.button = function(){
//    	restService.one("defect").customPOST($scope.defect, "update/" + $routeParams.id).then(function(data){
//    		alert("Information in defect was updated!");
//    	})
//    }
//}]);