'use strict';

/* Controllers */
  // signin controller


app.controller('ListShipping19Ctrl', ['$scope', 'restService','ngTableParams','Utils','$modal','$window', 
                                  function($scope, restService, ngTableParams,Utils, $modal, $window) {

	$scope.getShipping ={};
	$scope.shippingProduct ={};
	$scope.shippingDetail = {};
	$scope.shippingInfo = {};
	$scope.shippingInfo.shippingStatus = {};
	$scope.shippingCancelInfo = {};	
	$scope.shippingCancelInfo.shipping = {};
	$scope.shippingCancelInfo.staff = {};
	$scope.placeByFromPlace = {};
	$scope.placeByFromPlaceId = {};
	$scope.productionChange ={};
	$scope.currentPlaceId ={};
	
	$scope.getShippingList = function($defer, params, path) {				
					var searchParams = {};
					var prm = params.parameters();
					
					searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
					searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
					searchParams["searchParameters"] = {"toPlaceId": $scope.currentPlaceId};
					angular.forEach(prm.filter, function(value, key){
						searchParams.searchParameters[key] = value;
					});

					searchParams["searchParamWithTypes"] = [{searchField:"status.id", searchValue: 1, searchType: "NOT_EQUAL"}];
					searchParams["orderParamDesc"] = {"statusDate.created" : true};
					angular.forEach(prm.sorting, function(value, key){
						searchParams.orderParamDesc[key] = (value == "desc");
					});
					restService.all(path).customPOST(searchParams, ['list']).then(function(data){
						params.total(data.originalElement.totalRecords);
				        // set new data
				        $defer.resolve(data.originalElement.resultList);
					});
	}

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){

			restService.one("security").customGET('current_user').then(function(data){
	    		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id, "placeType.name": "склад"}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.currentPlaceId = place.id;
	    	        	$scope.getShippingList($defer, params, "production_19");
	    	        
	    			});
	    		});
			});
		},
    });

	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.showShippingDetails = function(shippingId){
		$('#button_approve').hide();
		$('#button_cancel').hide();
		$scope.shippingInfo = {};
		
		restService.one("production_19").customGET(shippingId).then(function(data){
			$scope.shippingInfo = data.originalElement;
			if($scope.shippingInfo.status.name == "Подтвержден отправителем"){
				$('#button_approve').show();
				$('#button_cancel').show();
			}
		});
		
		$('.toggle_2').show(1000);
	}
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		console.log(data.originalElement);
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "склад"}}, "list").then(function(data){
			console.log(data.originalElement.resultList);
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
			});
		});
	});
	
	$scope.sendProducts = function(id){
			console.log($scope.shippingInfo);
			
			var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
	
		modalInstance.result.then(function (selectedItem) {
			restService.one("production_19").customGET("approve_by_to_place/"+id).then(function(data){
					$scope.tableParams.reload();
					$scope.shippingInfo.status.name="Отправлено";
					alert("Транспортировка успешно получено!");
					$('#button_approve').hide();
			});
			}, function () {
		      return;
	    });
	};
	
//	$scope.shippingCancel = function(id, description){
//
//		var modalInstance = $modal.open({
//		      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
//		      controller: 'DeleteModalCtrl',
//		      size: 'md'
//		    });
//		
//		modalInstance.result.then(function (selectedItem) {
//        	restService.one("security").customGET('current_user').then(function(data){
//        		$scope.userId = data.originalElement.id;
//        	});
//			var shippingCancelInfo = {"date" : new Date(), "shipping" : {"id" : id}, "staff" : {"id" : $scope.userId}}
//			$scope.shippingInfo.shippingStatus.id = 5;
//			restService.one("shipping").customPOST($scope.shippingInfo, "update/" + id).then(function(data){
//				$scope.shippingInfo.shippingStatus.name="Запрос на отмену";
//				alert("Запрос на отмену транспортировки отправлен администратору!");
//				$scope.tableParams.reload();
//				restService.one("shipping_cancel_request").customPOST(shippingCancelInfo, "create").then(function(data){
//					$('cancel_btn').hide();
//				});
//			});
//  		}, function () {
//		      return;
//	    });
//	};
	$scope.toggleTable(2);
	
	
	
  }]);
app.controller('ListShipping19EmptyCtrl', ['$scope', 'restService','ngTableParams','Utils','$modal','$window', 
                                      function($scope, restService, ngTableParams,Utils, $modal, $window) {

    	$scope.getShipping ={};
    	$scope.shippingProduct ={};
    	$scope.shippingDetail = {};
    	$scope.shippingInfo = {};
    	$scope.shippingInfo.shippingStatus = {};
    	$scope.shippingCancelInfo = {};	
    	$scope.shippingCancelInfo.shipping = {};
    	$scope.shippingCancelInfo.staff = {};
    	$scope.placeByFromPlace = {};
    	$scope.placeByFromPlaceId = {};
    	$scope.productionChange ={};
    	$scope.currentPlaceId ={};
    	
    	$scope.getShippingList = function($defer, params, path) {				
    					var searchParams = {};
    					var prm = params.parameters();
    					
    					searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
    					searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
    					searchParams["searchParameters"] = {"toPlaceId": $scope.currentPlaceId};
    					angular.forEach(prm.filter, function(value, key){
    						searchParams.searchParameters[key] = value;
    					});

    					searchParams["searchParamWithTypes"] = [{searchField:"status.id", searchValue: 1, searchType: "NOT_EQUAL"}];
    					searchParams["orderParamDesc"] = {"statusDate.created" : true};
    					angular.forEach(prm.sorting, function(value, key){
    						searchParams.orderParamDesc[key] = (value == "desc");
    					});
    					restService.all(path).customPOST(searchParams, ['list']).then(function(data){
    						params.total(data.originalElement.totalRecords);
    				        // set new data
    				        $defer.resolve(data.originalElement.resultList);
    					});
    	}

    	
    	$scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10, 			// count per page
            sorting: {
                name: 'asc'     // initial sorting
            },				
        }, {
            total: 0, // length of data
            getData: function($defer, params){

    			restService.one("security").customGET('current_user').then(function(data){
    	    		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id, "placeType.name": "склад"}}, "list").then(function(data){
    	    			$scope.placeByFromPlace = data.originalElement.resultList;
    	    			angular.forEach(data.originalElement.resultList, function(place, key){
    	    				$scope.currentPlaceId = place.id;
    	    	        	$scope.getShippingList($defer, params, "purchase_19");
    	    	        
    	    			});
    	    		});
    			});
    		},
        });

    	$scope.exportData = function(){
    		   var table = document.getElementById('print').innerHTML;
    		   var myWindow = $window.open('', '', 'width=800, height=600');
    		   myWindow.document.write(table);
    		   myWindow.print();
    	};
    	
    	$scope.showShippingDetails = function(shippingId){
    		$('#button_approve').hide();
    		$('#button_cancel').hide();
    		$scope.shippingInfo = {};
    		
    		restService.one("purchase_19").customGET(shippingId).then(function(data){
    			$scope.shippingInfo = data.originalElement;
    			if($scope.shippingInfo.status.name == "Подтвержден администратором"){
    				$('#button_approve').show();
    				$('#button_cancel').show();
    			}
    		});
    		
    		$('.toggle_2').show(1000);
    	}
    	
    	$scope.toggleTable = function(n){
    		$('.toggle_'+n).toggle(1000);
    	}
    	
    	restService.one("security").customGET('current_user').then(function(data){
    		$scope.currentUser  = data.originalElement;
    		console.log(data.originalElement);
    		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "склад"}}, "list").then(function(data){
    			console.log(data.originalElement.resultList);
    			$scope.placeByFromPlace = data.originalElement.resultList;
    			angular.forEach(data.originalElement.resultList, function(place, key){
    				$scope.placeByFromPlaceId = place.id;
    			});
    		});
    	});
    	
    	$scope.sendProducts = function(id){
    			console.log($scope.shippingInfo);
    			
    			var modalInstance = $modal.open({
    		      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
    		      controller: 'DeleteModalCtrl',
    		      size: 'md'
    		    });
    	
    		modalInstance.result.then(function (selectedItem) {
    			restService.one("purchase_19").customGET("approve_by_to_place/"+id).then(function(data){
    					$scope.tableParams.reload();
    					$scope.shippingInfo.status.name="Отправлено";
    					alert("Транспортировка успешно получено!");
    					$('#button_approve').hide();
    			});
    			}, function () {
    		      return;
    	    });
    	};
    	
//    	$scope.shippingCancel = function(id, description){
    //
//    		var modalInstance = $modal.open({
//    		      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
//    		      controller: 'DeleteModalCtrl',
//    		      size: 'md'
//    		    });
//    		
//    		modalInstance.result.then(function (selectedItem) {
//            	restService.one("security").customGET('current_user').then(function(data){
//            		$scope.userId = data.originalElement.id;
//            	});
//    			var shippingCancelInfo = {"date" : new Date(), "shipping" : {"id" : id}, "staff" : {"id" : $scope.userId}}
//    			$scope.shippingInfo.shippingStatus.id = 5;
//    			restService.one("shipping").customPOST($scope.shippingInfo, "update/" + id).then(function(data){
//    				$scope.shippingInfo.shippingStatus.name="Запрос на отмену";
//    				alert("Запрос на отмену транспортировки отправлен администратору!");
//    				$scope.tableParams.reload();
//    				restService.one("shipping_cancel_request").customPOST(shippingCancelInfo, "create").then(function(data){
//    					$('cancel_btn').hide();
//    				});
//    			});
//      		}, function () {
//    		      return;
//    	    });
//    	};
    	$scope.toggleTable(2);
    	
    	
    	
      }]);
app.controller('AddShipping19Ctrl', ['$scope', 'restService','ngTableParams', 'Utils', function($scope, restService, ngTableParams, Utils){
	$scope.placeByFromPlaceId = {};
	$scope.placeByToPlaceId = {};
	$scope.shipping19 = {};
	$scope.currentUser = {};
	$scope.currentUserId = {};
	$scope.places = [];
	
//	$('.tableSubmit').hide();
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"name": "Балыкчы склад"}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			$scope.placeByToPlaceId2 = place.id;
			$scope.placeByToPlaceId = place.id;
		});
	});
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "производство"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
			});
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"placeType.name": "склад"}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			if(place.placeType.name == 'склад'){
				$scope.places.push(place);
			}
		});
	});
	
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.date = new Date();
	
	$scope.saveSendList = function(){
		if($scope.placeByToPlaceId!=-1){
			if($scope.shipping19.bottleAmount!=null){

				var shipping = {"toPlaceId": {"id" : parseInt($scope.placeByToPlaceId) },"description": $scope.description,"plannedAmount": $scope.shipping19.bottleAmount};
				restService.one("production_19").customPOST(shipping, "create").then(function(data){
					$('.tableSubmit').hide(1000);
					alert("Список запланированных продуктов успешно отправлен!");
					$scope.description = null;
					$scope.shipping19 = null;
				});
			}else{
				alert("Пожалуйста укажите количетво для отправки!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Назначения!");
		}
	}
}]);
app.controller('EditShipping19Ctrl', ['$scope', 'restService','ngTableParams', 'Utils','$stateParams','$location','$windows', function($scope, restService, ngTableParams, Utils,$stateParams,$location,$window){
	$scope.placeByFromPlace = {};
	$scope.placeByToPlaceId = {};
	$scope.currentUser = {};
	$scope.currentUserId = {};
	$scope.places = [];
	
//	$('.tableSubmit').hide();

	$scope.shippingId = $stateParams.id;
	
	restService.one("production_19").customGET($scope.shippingId).then(function(data){
		$scope.shippingInfo  = data.originalElement;
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"name": "Балыкчы склад"}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			$scope.placeByToPlaceId2 = place.id;
			$scope.placeByToPlaceId = place.id;
		});
	});
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "производство"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
			});
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			if(place.placeType.name == 'склад'){
				$scope.places.push(place);
			}
		});
	});
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	
	$scope.saveSendList = function(){
		if($scope.placeByToPlaceId!=-1){
			if($scope.shippingInfo != null){
				
//				var shipping = {"toPlaceId": {"id" : parseInt($scope.placeByToPlaceId) },"description": $scope.description,"plannedAmount": $scope.shipping19.bottleAmount};
				restService.one("production_19").customPOST($scope.shippingInfo, "update/"+$scope.shippingId).then(function(data){
					$('.tableSubmit').hide(1000);
					alert("Список запланированных продуктов успешно отправлен!");
		    		$location.path("/shipping_19/list");
					$scope.description = null;
				});
			}else{
				alert("Пожалуйста сначала выберите продукции для отправки!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Назначения!");
		}
	}
}]);
app.controller('TarSendCtrl', ['$scope', 'restService','ngTableParams', 'Utils','$window', function($scope, restService, ngTableParams, Utils,$window){
	$scope.placeByFromPlaceId = {};
	$scope.placeByToPlaceId = {};
	$scope.shipping19 = {};
	$scope.currentUser = {};
	$scope.currentUserId = {};
	$scope.places = [];
	$scope.bottle_place = {};
	
//	$('.tableSubmit').hide();
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"name": "Балыкчы производства"}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			$scope.placeByToPlaceId2 = place.id;
			$scope.placeByToPlaceId = place.id;
		});
	});
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "склад"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
			});
			restService.all("bottle_place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"place.id": $scope.placeByFromPlaceId}}, "list").then(function(data){
				angular.forEach(data.originalElement.resultList, function(bottle, key){
					$scope.bottle_place = bottle;
				});
			});
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"placeType.name": "производство"}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			if(place.placeType.name == 'производство'){
				$scope.places.push(place);
			}
		});
	});
	
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.date = new Date();
	
	$scope.saveSendList = function(){
		if($scope.placeByToPlaceId!=-1){
			if($scope.shipping19.emptyBottleAmount!=null){
				if($scope.bottle_place.emptyBottleAmount >= $scope.shipping19.emptyBottleAmount){
					var shipping = {"placeByFromPlaceId": {"id" : $scope.placeByFromPlaceId }, "placeByToPlaceId": {"id" : parseInt($scope.placeByToPlaceId)}, "description": $scope.description};
//					, "shippingStatus" : {"id" : 1}
					var listShippingBy19 = {"shipping": shipping, "shippingByProducts": null, "shipping19":$scope.shipping19}
					
//					var shipping = {"toPlaceId": {"id" : parseInt($scope.placeByToPlaceId) },"description": $scope.description,"plannedAmount": $scope.shipping19.bottleAmount};
					restService.one("shipping").customPOST(listShippingBy19, "create_empty_bottle_send").then(function(data){
						$('.tableSubmit').hide(1000);
						alert("Запрос тар успешно отправлен!");
						$scope.description = null;
						$scope.shipping19 = null;
					});
				}else{
					alert("Недостаточно тар в складе!!");
				}
			}else{
				alert("Пожалуйста укажите количетво тар!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Запроса!");
		}
	}
}]);
