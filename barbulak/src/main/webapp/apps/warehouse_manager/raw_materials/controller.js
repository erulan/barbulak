'use strict';

/* Controllers */
  // signin controller


app.controller('RawMaterialListCtrl', ['$scope','restService','ngTableParams','Utils',  function($scope, restService, ngTableParams, Utils) {
    
	$scope.placeByFromPlaceId = {};
	
    $scope.getProductsList = function($defer, params, path) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		
		searchParams["searchParameters"] = {"place.id" : $scope.placeByFromPlaceId};

		searchParams["searchParamWithTypes"] = [{searchField:"amount", searchValue: 0, searchType: "NOT_EQUAL"}];
		
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		 	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	restService.one("security").customGET('current_user').then(function(data){
            		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id,"placeType.name": "склад"}}, "list").then(function(data){
    	    			$scope.placeByFromPlace = data.originalElement.resultList;
    	    			angular.forEach(data.originalElement.resultList, function(place, key){
    	    				$scope.placeByFromPlaceId = place.id;
    	    			});

    	            	$scope.getProductsList($defer, params, "raw_place");
            		});
        		});
        },
    });

	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
  }]);
app.controller('ListRawRequestCtrl', ['$scope', 'restService','ngTableParams','Utils','$modal', function($scope, restService, ngTableParams,Utils,$modal ) {

	$scope.getShipping ={};
	$scope.shippingRaw ={};
	$scope.shippingDetail = {};
	$scope.shippingInfo = {};
	$scope.shippingCancelInfo = {};	
	$scope.shippingCancelInfo.shipping = {};
	$scope.shippingCancelInfo.staff = {};
	$scope.placeByFromPlace = {};
	$scope.productionId = {};
	
	$scope.getShippingList = function($defer, params, path) {
					var searchParams = {};
					var prm = params.parameters();
				
					searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
					searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;

					console.log($scope.productionId);
					searchParams["searchParameters"] = {"placeByFromPlaceId": $scope.placeByFromPlaceId, "placeByToPlaceId": $scope.productionId, "shippingStatus.id": 4 };
					angular.forEach(prm.filter, function(value, key){
						searchParams.searchParameters[key] = value;
					});
					searchParams["orderParamDesc"] = {"statusDate.created" : true};
					angular.forEach(prm.sorting, function(value, key){
						searchParams.orderParamDesc[key] = (value == "desc");
					});
					
					searchParams.searchParameters["shippingStatus.id"] = 4;    	
					restService.all(path).customPOST(searchParams, ['list']).then(function(data){
						params.total(data.originalElement.totalRecords);
				        // set new data
				        $defer.resolve(data.originalElement.resultList);
					});
	}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){

        	restService.one("security").customGET('current_user').then(function(data){
	        	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id,"placeType.name": "склад"}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.placeByFromPlaceId = place.id;
	    			});
	    			restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"placeType.name": "производство"}}, "list").then(function(data){
	    				angular.forEach(data.originalElement.resultList, function(place, key){
	    					$scope.productionId = place.id;
	    					console.log(place.id+place.name);
	    					console.log($scope.productionId);
	    				});

    					console.log($scope.productionId);
		            	$scope.getShippingList($defer, params, "shipping");
	    			});
	
	    		});
	        	
        	});
        },
    });

	$scope.showShippingDetails = function(shippingId){
		$('#button_approve').show();
		$('#button_cancel').show();
		restService.all("shipping_by_raw").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"shipping.id": shippingId}}, "list").then(function(data){
			$scope.shippingRaws = data.originalElement.resultList;
		});
		restService.one("shipping").customGET(shippingId).then(function(data){
			$scope.shippingInfo = data.originalElement;
			console.log($scope.shippingInfo);
		});
		$('.toggle_2').show(1000);
	}
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.shippingApprove = function(id){
			
			var modalInstance = $modal.open({
			      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
			      controller: 'DeleteModalCtrl',
			      size: 'md'
			    });
		
		modalInstance.result.then(function (selectedItem) {
			restService.one("shipping").customGET("approve_requested/"+id).then(function(data){
					$scope.shippingInfo.shippingStatus.name="отправлено";
					alert("Транспортировка успешно отправлена!");
					$('#button_approve').hide();
					$scope.tableParams.reload();
			});
			}, function () {
		      return;
	    });
		
	};
	
	$scope.shippingCancel = function(id){

		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		
		modalInstance.result.then(function (selectedItem) {
//			var shippingInfo = {"date" : new Date(), "id" : id, "shippingStatus" : {"id" : 5}}
//			var shippingCancelInfo = {"date" : new Date(), "shipping" : {"id" : id}, "staff" : {"id" : 1}}
			
			$scope.shippingInfo.shippingStatus.id = 3; // Canceled
			restService.one("shipping").customPOST($scope.shippingInfo, "update/" + id).then(function(data){
				$scope.shippingInfo.shippingStatus.name="отменен";
//				alert("Запрос на отмену транспортировки отправлен администратору!");

				 $scope.tableParams.reload();
//				 restService.all("shipping_by_product").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"shipping.id": id}}, "list").then(function(data){
//						if(data.originalElement.resultList == null){
//							restService.one("shipping_cancel_request").customPOST(shippingCancelInfo, "create").then(function(data){
//								if($scope.shippingInfo.shippingStatus.id == 5){
//									$('.button_'+$scope.shippingInfo.id).addClass('hide');
//								}
//							});
//						}
//					});
				 
			});
		}, function () {
		      return;
	    });
	};
	$scope.toggleTable(2);
	
  }]);

