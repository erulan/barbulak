'use strict';

/* Controllers */
  // signin controller


app.controller('ProductListCtrl', ['$scope','restService','ngTableParams','Utils',  function($scope, restService, ngTableParams, Utils) {

	$scope.placeByFromPlaceId = {};
	$scope.bottle = {};
	
    $scope.getProductsList = function($defer, params, path) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;

		searchParams["searchParameters"] = {"place.id" : $scope.placeByFromPlaceId};
		searchParams["searchParamWithTypes"] = [{searchField:"amount", searchValue: 0, searchType: "NOT_EQUAL"}];
		
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		 	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	restService.one("security").customGET('current_user').then(function(data){
        		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.placeByFromPlaceId = place.id;
	    			});

	    			$scope.getProductsList($defer, params, "product_place");

	    			restService.all("bottle_place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"place.id": $scope.placeByFromPlaceId}}, "list").then(function(data){
	    				angular.forEach(data.originalElement.resultList, function(bottle, key){
	    					$scope.bottle = bottle;
	    				});
	    			});
        		});
    		});
        },
    });
	
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
  }]);
