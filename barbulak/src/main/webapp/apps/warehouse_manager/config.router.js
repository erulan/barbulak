'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/warehouse_manager/products/list');
          $stateProvider
              .state('warehouse_manager', {	
                  abstract: true,
                  url: '/warehouse_manager',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //product
              .state('warehouse_manager.products', {
                  abstract: true,
                  url: '/products',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/warehouse_manager/products/controller.js']);
                      }]
                    }
              })
              .state('warehouse_manager.products.list', {
                  url: '/list',
                  templateUrl: '../../apps/warehouse_manager/products/list.html',
              })
              
              //raw material
              .state('warehouse_manager.raw_materials', {
                  abstract: true,
                  url: '/raw_materials',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/warehouse_manager/raw_materials/controller.js']);
                      }]
                    }
              })
              .state('warehouse_manager.raw_materials.list', {
                  url: '/list',
                  templateUrl: '../../apps/warehouse_manager/raw_materials/list.html',
              })
              .state('warehouse_manager.raw_materials.rawRequestList', {
                  url: '/list',
                  templateUrl: '../../apps/warehouse_manager/raw_materials/rawRequestList.html',
              })
              
              
              //shipping
              .state('warehouse_manager.shipping', {
                  abstract: true,
                  url: '/shipping',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/warehouse_manager/shipping/controller.js']);
                      }]
                    }
              })
              .state('warehouse_manager.shipping.list', {
                  url: '/list',
                  templateUrl: '../../apps/warehouse_manager/shipping/list.html',
              })
              .state('warehouse_manager.shipping.add', {
                  url: '/add',
                  templateUrl: '../../apps/warehouse_manager/shipping/add.html',
              })
              .state('warehouse_manager.shipping.request', {
                  url: '/request',
                  templateUrl: '../../apps/warehouse_manager/shipping/request.html',
              })
              .state('warehouse_manager.shipping.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/warehouse_manager/shipping/edit.html',
              })
              
              //shipping_19
              .state('warehouse_manager.shipping_19', {
                  abstract: true,
                  url: '/shipping_19',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/warehouse_manager/shipping_19/controller.js']);
                      }]
                    }
              })
              .state('warehouse_manager.shipping_19.list', {
                  url: '/list',
                  templateUrl: '../../apps/warehouse_manager/shipping_19/list.html',
              })
              .state('warehouse_manager.shipping_19.list_empty', {
                  url: '/list_empty',
                  templateUrl: '../../apps/warehouse_manager/shipping_19/list_empty.html',
              })
              .state('warehouse_manager.shipping_19.add', {
            	  url: '/add',
            	  templateUrl:'../../apps/warehouse_manager/shipping_19/add.html',
              })
              .state('warehouse_manager.shipping_19.edit', {
            	  url: '/edit/:id',
            	  templateUrl:'../../apps/warehouse_manager/shipping_19/edit.html',
              })
              .state('warehouse_manager.shipping_19.tar_send', {
            	  url: '/tar_send',
            	  templateUrl:'../../apps/warehouse_manager/shipping_19/tar_send.html',
              })

              
            //distribution
              .state('warehouse_manager.distribution', {
                  abstract: true,
                  url: '/distribution',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/warehouse_manager/distribution/controller.js']);
                      }]
                    }
              })
              .state('warehouse_manager.distribution.list', {
                  url: '/list',
                  templateUrl: '../../apps/warehouse_manager/distribution/list.html',
              })
              .state('warehouse_manager.distribution.add', {
                  url: '/add',
                  templateUrl: '../../apps/warehouse_manager/distribution/add.html',
              })
              

      }
    ]
  );
