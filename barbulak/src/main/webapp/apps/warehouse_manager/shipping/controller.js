'use strict';

/* Controllers */
  // signin controller


app.controller('ShippingListCtrl', ['$scope', 'restService','ngTableParams','Utils','$modal', '$window',
                                  function($scope, restService, ngTableParams,Utils, $modal,$window) {

	$scope.getShipping ={};
	$scope.shippingProduct ={};
	$scope.shippingDetail = {};
	$scope.shippingInfo = {};
	$scope.shippingInfo.shippingStatus = {};
	$scope.shippingInfo.placeByFromPlaceId = {};
	$scope.shippingCancelInfo = {};	
	$scope.shippingCancelInfo.shipping = {};
	$scope.shippingCancelInfo.staff = {};
	$scope.placeByFromPlaceId = {};
	$scope.currentPlaceId ={};
	$scope.shipping19 = {};
	
	$('.toggle').toggle(1000);
	
	$scope.getShippingList = function($defer, params, path, forIn) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		
			if(forIn == "forIn"){
				searchParams["searchParameters"] = {"toPlaceId": $scope.currentPlaceId};
				angular.forEach(prm.filter, function(value, key){
					searchParams.searchParameters[key] = value;
				});
				searchParams["orderParamDesc"] = {"statusDate.created" : true};
	
				searchParams["searchParamWithTypes"] = [{searchField:"status.id", searchValue: 1, searchType: "NOT_EQUAL"}];
			}else if(forIn == "forOut"){
				searchParams["searchParameters"] = {"placeByFromPlaceId" : $scope.currentPlaceId};
				searchParams["orderParamDesc"] = {"statusDate.created" : true};
				angular.forEach(prm.filter, function(value, key){
					searchParams.searchParameters[key] = value;
				});
//				searchParams["searchParamWithTypes"] = [{searchField:"shippingStatus.id", searchValue: 1, searchType: "NOT_EQUAL"}];
			}else if(forIn == "forInFromStock"){
				searchParams["searchParameters"] = {"placeByToPlaceId" : $scope.currentPlaceId};
				searchParams["orderParamDesc"] = {"statusDate.created" : true};
				angular.forEach(prm.filter, function(value, key){
					searchParams.searchParameters[key] = value;
				});
//				searchParams["searchParamWithTypes"] = [{searchField:"shippingStatus.id", searchValue: 1, searchType: "NOT_EQUAL"}];
			
			}
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}

	
	$scope.tableParamsForIn = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	var forIn = "forIn";
        	$scope.getCurrentUserPlaceId($defer,params,forIn);
        	
        },
    });
	
	$scope.tableParamsForInFromStock = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	var forIn = "forInFromStock";
        	$scope.getCurrentUserPlaceId($defer,params,forIn);
        	},
    });
	
	$scope.tableParamsForOut = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	var forIn = "forOut";
        	$scope.getCurrentUserPlaceId($defer,params,forIn);
        },
    });
	
	$scope.getCurrentUserPlaceId = function($defer,params, forIn){
		restService.one("security").customGET('current_user').then(function(data){
    		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id, "placeType.name": "склад"}}, "list").then(function(data){
    			$scope.placeByFromPlace = data.originalElement.resultList;
    			angular.forEach(data.originalElement.resultList, function(place, key){
    				$scope.placeByFromPlaceId = place.id;
    				$scope.currentPlaceId = place.id;
    			});
    			if(forIn == "forIn"){
	    			$scope.getShippingList($defer, params, "production_process", forIn);
    			}else{
        			$scope.getShippingList($defer, params, "shipping", forIn);
    			}
    		});
		});
		
	}
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
	};
	
	$scope.shippingFromProduction = false;
	
	$scope.showShippingDetails = function(shippingId, InOrOut){
		$('#button_cancel').hide();
		$('#button_approve_send').hide();
		$('#button_approve_recieve').hide();
		if(InOrOut == "ForIn"){
			$scope.shippingFromProduction = true;
			restService.all("production_process_product").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"productionProcess.id": shippingId}}, "list").then(function(data){
				$scope.shippingProducts = data.originalElement.resultList;
			});
			restService.one("production_process").customGET(shippingId).then(function(data){
				$scope.shippingInfo = data.originalElement;
					if($scope.shippingInfo.status.name == "Подтвержден отправителем"){//if shipping status is 1(otpravleno) than show approve button
						$('#button_approve_recieve').show();	
//						$('#button_cancel').show();
					}
				});
		}else if(InOrOut == "ForOut"){
			restService.all("shipping_by_product").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"shipping.id": shippingId}}, "list").then(function(data){
				$scope.shippingProducts = data.originalElement.resultList;
			});
			restService.all("shipping_19").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"shipping.id": shippingId}}, "list").then(function(data){
				angular.forEach(data.originalElement.resultList, function(shipping, key){
					$scope.shipping19 = shipping;
				})
			});
			
			restService.one("shipping").customGET(shippingId).then(function(data){
				$scope.shippingInfo = data.originalElement;
				console.log($scope.shippingInfo.shippingStatus.name);
				console.log($scope.shippingInfo.placeByFromPlaceId);
				console.log($scope.placeByFromPlaceId);
				if($scope.shippingInfo.sent == true){
					if($scope.shippingInfo.shippingStatus.name == "Запрос"){//if shipping status is 1(otpravleno) than show cacel button
							$('#button_cancel').show();
					}
				}
				if($scope.shippingInfo.shippingStatus.name == "Подтвержден администратором"){//if shipping status is 1(otpravleno) than show cacel button
					if($scope.shippingInfo.placeByFromPlaceId.id == $scope.placeByFromPlaceId){
						$('#button_approve_send').show();
						$('#button_cancel').show();
					}
				}
			});
		}else if(InOrOut == "ForInFromStock"){
			restService.all("shipping_by_product").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"shipping.id": shippingId}}, "list").then(function(data){
				$scope.shippingProducts = data.originalElement.resultList;
			});
			restService.all("shipping_19").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"shipping.id": shippingId}}, "list").then(function(data){
				angular.forEach(data.originalElement.resultList, function(shipping, key){
					$scope.shipping19 = shipping;
				})
			});
			
			restService.one("shipping").customGET(shippingId).then(function(data){
				$scope.shippingInfo = data.originalElement;
				if($scope.shippingInfo.shippingStatus.name == "Подтвержден отправителем"){
					$('#button_approve_recieve').show();
					$('#button_cancel').show();
				}else if($scope.shippingInfo.shippingStatus.name == "Подтвержден администратором" && $scope.shippingInfo.sent == true){
					$('#button_approve_recieve').show();
				}
			});
		}
	
		
				
		$('.toggle_2').show(1000);
	}
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.shippingRecieveApprove = function(id){
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		
		if($scope.shippingFromProduction == true){

			modalInstance.result.then(function (selectedItem) {
			restService.one("production_process").customGET("approve_by_to_place/"+id).then(function(data){
				$scope.shippingInfo.status.name="Получено";
				alert("Транспортировка успешно завершена!");
				$('.button_approve').addClass('hide');
				 $scope.tableParamsForIn.reload(); 
			});
			}, function () {
			      return;
		    });
		}else{
			modalInstance.result.then(function (selectedItem) {
				restService.one("shipping").customGET("approve_by_to_place/"+id).then(function(data){
//					$scope.shippingInfo.status.name="Получено";
					alert("Транспортировка успешно завершена!");
					$('.button_approve').hide(1000);
					 $scope.tableParamsForIn.reload(); 
				});
				}, function () {
				      return;
			    });
		}
	}
	
	$scope.shippingSendApprove = function(id){
		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/shippingApproveModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		
			modalInstance.result.then(function (selectedItem) {
			restService.one("shipping").customGET("approve_by_from_place/"+id).then(function(data){
//				$scope.shippingInfo.status.name="Отправлено";
				alert("Транспортировка успешно отправлена!");
				$('#button_approve_send').hide(1000);
				 $scope.tableParamsForOut.reload(); 
			});
			}, function () {
			      return;
		    });
	}
	
	$scope.shippingCancel = function(id, description){
		
			var modalInstance = $modal.open({
			      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
			      controller: 'DeleteModalCtrl',
			      size: 'md'
			    });

			modalInstance.result.then(function (selectedItem) {
				restService.one("shipping").customGET(id).then(function(data){
					console.log(data.originalElement.placeByFromPlaceId);
					console.log(data.originalElement.placeByToPlaceId);
					console.log($scope.currentPlaceId);
					if($scope.currentPlaceId == data.originalElement.placeByFromPlaceId.id){
						restService.one("shipping").customGET("reject_by_from_place/"+id+'/'+description).then(function(data){
							alert("Запрос продукции отменен!");
							$scope.tableParamsForOut.reload();
						});
					}else if($scope.currentPlaceId == data.originalElement.placeByToPlaceId.id){
						restService.one("shipping").customGET("reject_by_to_place/"+id+'/'+description).then(function(data){
							alert("Запрос продукции отменен!");
							$scope.tableParamsForOut.reload();
						});
					}
				});
					
			}, function () {
			      return;
		    });
		}
	
  }]);

app.controller('RequestShippingCtrl', ['$scope', 'restService','ngTableParams', 'Utils', function($scope, restService, ngTableParams, Utils){
	
	$scope.placeByFromPlaceId = {};
	$scope.shippingByProducts = [];
	$scope.sendProducts = {};
	$scope.places = [];
	$scope.placeByToPlaceId = {}
	$scope.getShipping = {};
	$scope.shipping19 = {};
	
	$scope.placeByFromPlaceId=-1;	
	$('.tableSubmit').hide();
	$('.toggle').toggle();
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id,"placeType.name": "склад"}}, "list").then(function(data){
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByToPlaceId = place.id;
			});
			restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"placeType.name": "склад"}, "searchParamWithTypes":[{searchField:"id", searchValue: $scope.placeByToPlaceId, searchType: "NOT_EQUAL"}]}, "list").then(function(data){
				$scope.places = data.originalElement.resultList;
			});
		});
	});
	
	
	$scope.getShippingList = function($defer, params, path, placeId) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		searchParams["searchParameters"] = {"place.id": placeId};
		searchParams["searchParamWithTypes"] = [{searchField:"amount", searchValue: 0, searchType: "NOT_EQUAL"}];
		
			angular.forEach(prm.filter, function(value, key){
				searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {"date" : true};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}
	
	$scope.init = function() {
		$scope.showPrice  = true;
        $scope.showFizzy = false;
        $scope.showCapacity = true;
        $scope.showCategory = true;
        $scope.showType = true;
        $scope.showName = true;
    }
	
	$scope.tableParamsProducts = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
			Utils.ngTableGetData($defer, params, "product");
        },
    });
	
	restService.all("shipping").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.getShipping = data.originalElement.resultList;
	});
	
	restService.all("product").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.products = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		console.log('test');
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	$scope.date1= $scope.date.getDate()+'-'+$scope.date.getMonth()+'-'+$scope.date.getFullYear();
	
	
	$scope.addToSendList = function(selectedId,amount){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
				if($scope.sendProducts[selectedId]==null){
					$('.button_'+selectedId+'_product').hide(1000);
					$('.button_'+selectedId+'_product').show(1000);	
					$('.toggle_2').show(1000);
					
					angular.forEach($scope.products, function(product, key){
        				if(selectedId == product.id){
        					$scope.sendProducts[selectedId] = product;
        				}
        			});
					$scope.sendProducts[selectedId].amount = amount; 
					$('.tableSubmit').show(1000);
				}else{
					$('.button_'+selectedId+'_product').hide(1000);
					$('.button_'+selectedId+'_product').show(1000);	

					$scope.sendProducts[selectedId].amount = amount;
				}
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
		
	}
	$scope.add19ToSendList = function(bottleAmount,emptyBottleAmount){
		$scope.shipping19.bottleAmount = bottleAmount;
		$scope.shipping19.emptyBottleAmount = emptyBottleAmount;

		$('.button_19').hide(1000);
		$('.button_19').show(1000);	
	}
	
	$scope.cancelProduct = function(selectedId, productOrRaw){
		delete $scope.sendProducts[selectedId];
		
		if(Object.keys($scope.sendProducts).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.saveSendList = function(){
		if($scope.placeByFromPlaceId!=-1){
			if(Object.keys($scope.sendProducts).length!=0){
				if(Object.keys($scope.sendProducts).length!=0){
					console.log($scope.sendProducts);
					angular.forEach($scope.sendProducts, function(product, key){
						console.log(product);
						$scope.shippingByProducts.push({"amount": product.amount, "product": {"id" : product.id}});
						console.log($scope.shippingByProducts);
					});
					
					var shipping = {"placeByFromPlaceId": {"id" : $scope.placeByFromPlaceId}, "placeByToPlaceId": {"id" : parseInt($scope.placeByToPlaceId) }, "shippingStatus" : {"id" : 1}, "description": $scope.description};
					
					var listShippingByProduct = {"shipping": shipping, "shippingByProducts": $scope.shippingByProducts, "shipping19":$scope.shipping19}
					console.log(listShippingByProduct);
					
				}
				restService.one("shipping").customPOST(listShippingByProduct, "create_request").then(function(data){
				
					$('.tableSubmit').hide(1000);
					alert("Транспортировка продукции успешно сохранена");
					$scope.tableParamsProducts.reload();
					$scope.sendProducts = {};
					$scope.shipping19 = {};
					$scope.shippingByProducts = [];
					$scope.description = null;
				});
			}else{
				alert("Пожалуйста сначала выберите продукции для отправки!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Запросa!");
		}
	}
}]);

app.controller('EditShippingCtrl', ['$scope', 'restService','ngTableParams', 'Utils','$stateParams', function($scope, restService, ngTableParams, Utils,$stateParams){
	$scope.placeByFromPlace = {};
	$scope.placeByFromPlace[0] ={};
	$scope.placeByFromPlaceId = {};
	$scope.placeByToPlaceId = {};
	$scope.shippingByProducts = [];
	$scope.shippingByRaws = [];
	$scope.sendProducts = {};
	$scope.getShipping = {};
	$scope.currentUser = {};
	$scope.currentUserId = {};
	$scope.shipping19 = {};
	$scope.shipping = {};
	$scope.places = [];
	$scope.shipping19Warehouse ={}
	
//	$('.tableSubmit').hide();

	$scope.shippingId = $stateParams.id;

	restService.one("shipping").customGET($scope.shippingId).then(function(data){
		$scope.shipping = data.originalElement;
	});
	restService.all("shipping_by_product").customPOST({startIndex: 0, resultQuantity: 1000, "searchParameters":{"shipping.id": $scope.shippingId}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(product, key){
			restService.all("product").customGET(product.product.id).then(function(data){
				
				$scope.sendProducts[product.product.id] =  data.originalElement;
				$scope.sendProducts[product.product.id].amount = product.amount;

				console.log($scope.sendProducts);

			});
		});
		console.log($scope.sendProducts);
	});
	restService.all("shipping_19").customPOST({startIndex: 0, resultQuantity: 1000, "searchParameters":{"shipping.id": $scope.shippingId}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(product, key){
			$scope.shipping19 = product;
			delete $scope.shipping19.shipping;
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"name": "Балыкчы склад"}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			$scope.placeByToPlaceId2 = place.id;
			$scope.placeByToPlaceId = place.id;
		});
	});
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "склад"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
				restService.all("bottle_place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"place.id": $scope.placeByFromPlaceId}}, "list").then(function(data){
					angular.forEach(data.originalElement.resultList, function(bottle, key){
						$scope.shipping19Warehouse = bottle;
					});
					console.log($scope.shipping19Warehouse );
				});
			});
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			if(place.placeType.name == 'склад'){
				$scope.places.push(place);
			}
		});
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "product");
        },
    });
	
	restService.all("product").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.products = data.originalElement.resultList;
	});
	
	restService.all("shipping").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.getShipping = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	$scope.date1= $scope.date.getDate()+'-'+$scope.date.getMonth()+'-'+$scope.date.getFullYear();
	
	$scope.addToSendList = function(productId,amount){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			if($scope.sendProducts[productId]==null){
				$('.button_'+productId).hide(1000);
				$('.button_'+productId).show(1000);				
				console.log($scope.sendProducts[productId]);
				angular.forEach($scope.products, function(product, key){
    				if(productId == product.id){
    					$scope.sendProducts[productId] = product;
    				}
    			});
				$scope.sendProducts[productId].amount = amount; 

				console.log($scope.sendProducts[productId]);
				console.log($scope.sendProducts);
				$('.tableSubmit').show(1000);
			}else{
				$scope.sendProducts[productId].amount = amount;
				$('.button_'+productId).hide(1000);
				$('.button_'+productId).show(1000);	

				console.log($scope.sendProducts[productId]);
				console.log($scope.sendProducts);
			}
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
		
	}
	
	$scope.add19ToSendList = function(bottleAmount,emptyBottleAmount){
		if($scope.shipping19Warehouse.bottleAmount >= bottleAmount){
			if($scope.shipping19Warehouse.emptyBottleAmount >= emptyBottleAmount){
				$scope.shipping19.bottleAmount = bottleAmount;
				$scope.shipping19.emptyBottleAmount = emptyBottleAmount;
			
				$('.button_19').hide(1000);
				$('.button_19').show(1000);	

				$('.tableSubmit').show(1000);
			}else{
				alert("Недостаточно тар в складе!!");
			}
		}else{
			alert("Недостаточно Хан Тенгри 19л в складе!!");
		}
		
	}
	
	$scope.cancelProduct = function(productId){
		
		delete $scope.sendProducts[productId];
		if(Object.keys($scope.sendProducts).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.saveSendList = function(){
			if(Object.keys($scope.sendProducts).length!=0){
				if(Object.keys($scope.sendProducts).length!=0){
					console.log($scope.sendProducts);
					angular.forEach($scope.sendProducts, function(product, key){
						console.log(product);
						$scope.shippingByProducts.push({"amount": product.amount, "product": {"id" : product.id}});
						console.log($scope.shippingByProducts);
					});
					
//					var shipping = {"id": $scope.shippingId,"placeByFromPlaceId": {"id" : $scope.placeByFromPlaceId}, "placeByToPlaceId": {"id" : parseInt($scope.placeByToPlaceId) }, "shippingStatus" : {"id" : 1}, "description": $scope.description};
					
					var listShippingByProduct = {"shipping": $scope.shipping, "shippingByProducts": $scope.shippingByProducts, "shipping19":$scope.shipping19}
					console.log(listShippingByProduct);
					
				}
				restService.one("shipping").customPOST(listShippingByProduct, "update_list/"+$scope.shippingId).then(function(data){
				
					$('.tableSubmit').hide(1000);
					alert("Транспортировка продукции успешно изменена!");
					$scope.sendProducts = {};
					$scope.shipping19 = {};
					$scope.shippingByProducts = [];
					$scope.description = null;
				});
			}else{
				alert("Пожалуйста сначала выберите продукции для отправки!!");
			}
	}
	
}]);

app.controller('AddShippingCtrl', ['$scope', 'restService','ngTableParams', 'Utils', function($scope, restService, ngTableParams, Utils){

	$scope.placeByToPlaceId = {};
	$scope.shippingByProducts = [];
	$scope.sendProducts = {};
	$scope.places = [];
	$scope.placeByFromPlaceId = {}
	$scope.getShipping = {};
	$scope.shipping19 = {};
	$scope.shipping19Warehouse = {};
	
	$scope.placeByToPlaceId=-1;	
	$('.tableSubmit').hide();
	$('.toggle').toggle();
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id,"placeType.name": "склад"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
			});
			restService.all("bottle_place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"place.id": $scope.placeByFromPlaceId}}, "list").then(function(data){
				angular.forEach(data.originalElement.resultList, function(bottle, key){
					$scope.shipping19Warehouse = bottle;
				});
			});
			restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"placeType.name": "склад"}, "searchParamWithTypes":[{searchField:"id", searchValue: $scope.placeByFromPlaceId, searchType: "NOT_EQUAL"}]}, "list").then(function(data){
				$scope.places = data.originalElement.resultList;
			});
		});
	});
	
	$scope.getShippingList = function($defer, params, path, placeId) {
	
		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		searchParams["searchParameters"] = {"place.id": placeId};
		searchParams["searchParamWithTypes"] = [{searchField:"amount", searchValue: 0, searchType: "NOT_EQUAL"}];
		
			angular.forEach(prm.filter, function(value, key){
				searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {"date" : true};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		
	//	searchParams.searchParameters["shippingStatus.id"] = [1,2,3,5];    	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}
	
	$scope.tableParamsProducts = new ngTableParams({
	    page: 1,            // show first page
	    count: 10, 			// count per page
	    sorting: {
	        name: 'asc'     // initial sorting
	    },				
	}, {
	    total: 0, // length of data
	    getData: function($defer, params){
	    	restService.one("security").customGET('current_user').then(function(data){
	    		$scope.currentUser  = data.originalElement;
	    		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "склад"}}, "list").then(function(data){
	    			$scope.placeByFromPlace = data.originalElement.resultList;
	    			angular.forEach(data.originalElement.resultList, function(place, key){
	    				$scope.placeByFromPlaceId = place.id;
	    			});
	
	    			$scope.getShippingList($defer, params, "product_place",$scope.placeByFromPlaceId);
	    		});
	    	});
	    	
	    },
	});
	
	
	restService.all("shipping").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.getShipping = data.originalElement.resultList;
	});
	
	restService.all("product").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.products = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		console.log('test');
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	$scope.date1= $scope.date.getDate()+'-'+$scope.date.getMonth()+'-'+$scope.date.getFullYear();
	
	
	$scope.addToSendList = function(selectedId,amount){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			$('.button_'+selectedId+'_product').hide(1000);
			$('.button_'+selectedId+'_product').show(1000);	
			$('.toggle_2').show(1000);
			angular.forEach($scope.tableParamsProducts.data, function(product, key){
				
				if(selectedId == product.product.id){
					if(amount <= product.amount){
						$scope.sendProducts[selectedId] = product.product;
						$scope.sendProducts[selectedId].amount = amount; 
						console.log($scope.sendProducts);
					}else{
						alert("В складе не достаточно "+product.product.name);
					}
				}
			});
			$('.tableSubmit').show(1000);
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
		
	}
	
	$scope.add19ToSendList = function(bottleAmount,emptyBottleAmount){
		if($scope.shipping19Warehouse.bottleAmount >= bottleAmount){
			if($scope.shipping19Warehouse.emptyBottleAmount >= emptyBottleAmount){
				$scope.shipping19.bottleAmount = bottleAmount;
				$scope.shipping19.emptyBottleAmount = emptyBottleAmount;
			
				$('.button_19').hide(1000);
				$('.button_19').show(1000);	

				$('.toggle_2').show(1000);
				$('.tableSubmit').show(1000);
			}else{
				alert("Недостаточно тар в складе!!");
			}
		}else{
			alert("Недостаточно Хан Тенгри 19л в складе!!");
		}
		
	}
	
	$scope.cancelProduct = function(selectedId, productOrRaw){
		delete $scope.sendProducts[selectedId];
		
		if(Object.keys($scope.sendProducts).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.saveSendList = function(){
		if($scope.placeByToPlaceId!=-1){
			if(Object.keys($scope.sendProducts).length!=0 || $scope.shipping19.bottleAmount !=null || $scope.shipping19.emptyBottleAmount!=null){
				if(Object.keys($scope.sendProducts).length!=0){
					angular.forEach($scope.sendProducts, function(product, key){
						$scope.shippingByProducts.push({"amount": product.amount, "product": {"id" : product.id}});
					});
				}
				var shipping = {"placeByFromPlaceId": {"id" : $scope.placeByFromPlaceId}, "placeByToPlaceId": {"id" : parseInt($scope.placeByToPlaceId) }, "shippingStatus" : {"id" : 1}, "description": $scope.description};
				
				var listShippingByProduct = {"shipping": shipping, "shippingByProducts": $scope.shippingByProducts, "shipping19": $scope.shipping19}
				
				restService.one("shipping").customPOST(listShippingByProduct, "create_send").then(function(data){
				
					$('.tableSubmit').hide(1000);
					alert("Транспортировка продукции успешно сохранена");
					$scope.tableParamsProducts.reload();
					$scope.sendProducts = {};
					$scope.shipping19 = {};
					$scope.shippingByProducts = [];
					$scope.description = null;
				});
			}else{
				alert("Пожалуйста сначала выберите продукции для отправки!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Назначения!");
		}
	}
}]);
	
//app.controller('EditDefectCtrl' ['$scope', 'restService', '$routeParams', function($scope, restService, $routeParams){
//	
//}]);
//
//
//app.controller('InfoDefectCtrl', ['$scope','restService', '$routeParams', function($scope, restService, $routeParams) {
//    
//    $scope.defect = {};
//    $scope.defectId = $routeParams.id;
//    
//    $scope.button = function(){
//    	restService.one("defect").customPOST($scope.defect, "update/" + $routeParams.id).then(function(data){
//    		alert("Information in defect was updated!");
//    	})
//    }
//}]);