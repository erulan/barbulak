'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/purchaser/buy_raw_material/list');
          $stateProvider
              .state('purchaser', {
                  abstract: true,
                  url: '/purchaser',
                  templateUrl: '../../assets/tpl/app.html'
              })
                            
            //buy raw material 
              .state('purchaser.buy_raw_material', {
                  abstract: true,
                  url: '/buy_raw_material',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/purchaser/buy_raw_material/controller.js']);
                      }]
                    }
              })
              .state('purchaser.buy_raw_material.list', {
                  url: '/list',
                  templateUrl: '../../apps/purchaser/buy_raw_material/list.html',
              })
              
            //purchaser_19
              .state('purchaser.purchase_19', {
                  abstract: true,
                  url: '/purchase_19',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/purchaser/purchase_19/controller.js']);
                      }]
                    }
              })
              .state('purchaser.purchase_19.list', {
                  url: '/list',
                  templateUrl: '../../apps/purchaser/purchase_19/list.html',
              })
              .state('purchaser.purchase_19.add', {
                  url: '/add',
                  templateUrl: '../../apps/purchaser/purchase_19/add.html',
              })
      }
    ]
  );
