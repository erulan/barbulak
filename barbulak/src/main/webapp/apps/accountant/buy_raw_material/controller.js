'use strict';

/* Controllers */
  // signin controller

app.controller('BuyRawMaterailListCtrl',['$scope', 'restService', 'ngTableParams', 'Utils',  function($scope, restService, ngTableParams, Utils) {

	$scope.getShipping ={};
	$scope.shippingRaw ={};
	$scope.shippingDetail = {};
	$scope.shippingInfo = {};
	$scope.shippingCancelInfo = {};	
	$scope.shippingCancelInfo.shipping = {};
	$scope.shippingCancelInfo.staff = {};
	$scope.placeByFromPlace = {};
	$scope.purchase = {};
	
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "purchase");
    		
          },
    });

	$scope.showShippingDetails = function(shippingId){

		console.log(shippingId);
		restService.all("raw_purchase").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"shipping.id": shippingId}}, "list").then(function(data){
			$scope.shippingRaws = data.originalElement.resultList;

			console.log('by product');
			console.log($scope.shippingRaws);

		});
		restService.one("shipping").customGET(shippingId).then(function(data){
			$scope.shippingInfo = data.originalElement;
			console.log($scope.shippingInfo);
		});
		$('.toggle_2').show(1000);
		
	}
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.shippingCancel = function(id){

		var modalInstance = $modal.open({
		      templateUrl: '../../assets/tpl/blocks/deleteRequestModal.html',
		      controller: 'DeleteModalCtrl',
		      size: 'md'
		    });
		
		modalInstance.result.then(function (selectedItem) {
////			var shippingInfo = {"date" : new Date(), "id" : id, "shippingStatus" : {"id" : 5}}
//        	restService.one("security").customGET('current_user').then(function(data){
//        		$scope.userId = data.originalElement.id;
//        	});
//			var shippingCancelInfo = {"date" : new Date(), "shipping" : {"id" : id}, "staff" : {"id" : $scope.userId}}

			$scope.shippingInfo.shippingStatus.id = 3; // Canceled
			restService.one("shipping").customPOST($scope.shippingInfo, "update/" + id).then(function(data){
				$scope.shippingInfo.shippingStatus.name="отменен";
//				alert("Запрос на отмену транспортировки отправлен администратору!");

				 $scope.tableParams.reload();
//				 restService.all("shipping_by_product").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"shipping.id": id}}, "list").then(function(data){
//						if(data.originalElement.resultList == null){
//							restService.one("shipping_cancel_request").customPOST(shippingCancelInfo, "create").then(function(data){
//								if($scope.shippingInfo.shippingStatus.id == 5){
//									$('.button_'+$scope.shippingInfo.id).addClass('hide');
//								}
//							});
//						}
//					});
				 
			});
		}, function () {
		      return;
	    });
		

	};
	$scope.toggleTable(2);
	
	
}]);

/*app.controller('BuyRawMaterailAddCtrl', ['$scope', 'restService','ngTableParams','Utils','$location',function($scope, restService,  ngTableParams, Utils,$location){
	
	$scope.placeByToPlaceId = {};
	$scope.shippingByProducts = [];
	$scope.shippingByRaws = [];
	$scope.sendRaws = {};
	$scope.getShipping = {};
	$scope.places = [];
	$scope.totalPrice = 0;
	
	$scope.placeByToPlaceId=-1;	
	$('.tableSubmit').hide();
	
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.placeByFromPlaceId = place.id;
			});
		});
	});
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(place, key){
			if(place.placeType.name == 'склад'){
				$scope.places.push(place);
			}
		});
	});
	
	$scope.getRawList = function($defer, params, path) {
		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		searchParams["searchParameters"] = {"purchase.status" : 4};
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		  	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getRawList($defer, params, "purchase");
        },
    });
	
	restService.all("raw").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.raws = data.originalElement.resultList;
	});
	
	restService.all("shipping").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.getShipping = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		console.log('test');
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	$scope.date1= $scope.date.getDate()+'-'+$scope.date.getMonth()+'-'+$scope.date.getFullYear();
	
	
	$scope.addToSendList = function(rawId,amount,unitPrice){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			if($scope.sendRaws[rawId]==null){
				$('.button_'+rawId).hide(1000);
				$('.button_'+rawId).show(1000);				
				
				angular.forEach($scope.raws, function(raw, key){
					if(raw.id == rawId){
						$scope.sendRaws[rawId] = raw;
					}
				});
				
				$scope.totalPrice +=amount*unitPrice;
				$scope.sendRaws[rawId].amount = amount; 
				$scope.sendRaws[rawId].unitPrice = unitPrice; 
				
					$scope.unitPrice +=totalRawPrice/amount;
				$scope.sendRaws[rawId].totalRawPrice = totalRawPrice;
				
				$('.tableSubmit').show(1000);
			}else{
				$scope.sendRaws[productId].amount = amount;
			}
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
		
	}
	
	$scope.cancelRaw = function(rawId){

		$scope.totalPrice -=$scope.sendRaws[rawId].amount*$scope.sendRaws[rawId].unitPrice;
		delete $scope.sendRaws[rawId];
		if(Object.keys($scope.sendRaws).length==0){
			$('.tableSubmit').hide(1000);
		}
		console.log($scope.sendRaws);
	}
	
	$scope.saveSendList = function(){
		if($scope.placeByToPlaceId!=-1){
			if(Object.keys($scope.sendRaws).length!=0){
				angular.forEach($scope.sendRaws, function(raw, key){
					$scope.shippingByRaws.push({"amount": raw.amount, "raw": {"id" : raw.id}});
				});
				
				var shipping = {"date": new Date, "placeByFromPlaceId": {"id" : $scope.placeByFromPlaceId}, "placeByToPlaceId": {"id" : parseInt($scope.placeByToPlaceId) }, "shippingStatus" : {"id" : 4}, "description": $scope.description};
				
				var listShippingByProduct = {"shipping": shipping, "shippingByProducts": [], "shippingByRaws" : $scope.shippingByRaws}
				
				console.log(listShippingByProduct);
				
				restService.one("purchase").customPOST(listShippingByProduct, "create").then(function(data){
				
					$('.tableSubmit').hide(1000);
					alert("Запрос Сырья успешно отправлен!");
					
					$scope.sendRaws = {};
					$scope.shippingByRaws = [];
					$scope.description = null;
					$scope.placeByToPlaceId=-1;	
					$scope.tableParams.reload();
				});
			}else{
				alert("Пожалуйста сначала выберите cырье для запроса!!");
			}
		}else{
			alert("Пожалуйста выберите Пункт Назначения!");
		}
	}
}]);
*/
