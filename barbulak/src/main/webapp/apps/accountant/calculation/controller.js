'use strict';

/* Controllers */
  // signin controller

app.controller('EvoluationInstallmentAddCtrl', ['$scope', 'restService','$state',
                                                function($scope, restService, $state) {
	$scope.staff = {};
	$scope.staff.id;
	$scope.staff.person = {};
	$scope.staff.person.id;
	
	$scope.personalCalculationEvaluation = [];
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
		/*$scope.myChashboxId = cashboxs[0].id;*/
	});
	
	restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.staffs = data.originalElement.resultList;
		
		$scope.personalCalculationEvaluation = new Array($scope.staffs.length);
		
		//console.log("insert into array");
		angular.forEach($scope.staffs, function(staff, key){
			//console.log("insert into function");
			$scope.personalCalculationEvaluation[key] = ({
				
				"amount": staff.salary, 
				"description": "", 
				"personId": staff.id, 
				"name": staff.name, 
				"surname": staff.surname
			});//console.log("insert into key");
		});
	});
	
	
	$scope.sendToBackEnd = function(){
		console.log("insert into sen function");
		angular.forEach($scope.personalCalculationEvaluation, function(pce, key){
			delete pce.name;
			delete pce.surname;
			console.log("insert into delete");
		});
		console.log("insert into peson_trasaction");
		restService.all("personal_transaction").customPOST($scope.personalCalculationEvaluation, "salary_payment").then(function(data){
			alert("Успешно начислено");
			$state.go("accountant.personal_balance.list");
			console.log("insert into finish");
			/*document.location.reload(true);*/
			
		});
	};
	
}]);