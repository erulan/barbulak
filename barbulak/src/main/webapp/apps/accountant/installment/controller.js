'use strict';

/* Controllers */
  // signin controller

app.controller('InstallmentListCtrl',['$scope', 'restService', 'ngTableParams', 'Utils',  function($scope, restService, ngTableParams, Utils) {
	
/*	restService.all("installment").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.products = data.originalElement.resultList;
	});*/
	
	$scope.datepicker = {};
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            date: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "installment");
        },
    });
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
	$scope.init = function() {
	 	$scope.showBl = true;
        $scope.showName = true;
        $scope.showSurname = true;
        $scope.showOst = true;
        $scope.showDate = true;
        $scope.showSum = true;
        $scope.showSroc = true;   
	}
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
	  restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
			$scope.cashboxs = data.originalElement.resultList;
		});
	
}]);
app.controller('InstallmentAddCtrl',['$scope', 'restService', '$location', function($scope, restService, $location){
	$scope.installment = {};
	$scope.installment.staff = {};
	$scope.installment.staff.id;
	
	$scope.datepicker = {};
	
	restService.all("installment").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.installments = data.originalElement.resultList;
	});
	restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});
	
	$scope.create = function(){
		restService.one("installment").customPOST($scope.installment, "create").then(function(data){
			alert("Успешно добавлена рассрочка!");
			$location.path("accountant/installment/list");
		});
	};
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {

	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	

      $scope.datepickers = {
        data: {},
        options: {
            'year-format': "'yy'",
            'starting-day': 1
        },
        isopen: {}
      }
   

}]);


	
