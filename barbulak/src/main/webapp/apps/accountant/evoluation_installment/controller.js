'use strict';

/* Controllers */
  // signin controller


app.controller('EvoluationInstallmentAddCtrl', ['$scope', 'restService','$state', function($scope, restService, $state) {
	$scope.installment = {};
	$scope.installment.staff = {};
	$scope.installment.staff.id;

	$scope.personalInstallmentEvaluation = [];
	
	$scope.datepicker = {};
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});

	restService.all("installment").customPOST({startIndex: 0, resultQuantity: 1000}, "evaluation_list").then(function(data){
		$scope.installments = data.originalElement.resultList;
		
		console.log('start');
		$scope.personalInstallmentEvaluation = new Array($scope.installments.length);
		//console.log('personInsEval');
		angular.forEach($scope.installments, function(installment, key){
			//console.log('function work');
			$scope.personalInstallmentEvaluation[key] = ({
				"amount": (installment.total/installment.installmentPeriod).toFixed(2), 
				"description": "", 
				"personId": installment.staff.id, 
				"installmentId": installment.id, 
					"name": installment.staff.name, 
					"surname": installment.staff.surname
			});
		//console.log('personalEval end');
		});
	});
	
	$scope.sendToBackEnd = function(){
		angular.forEach($scope.personalInstallmentEvaluation, function(pie, key){
			delete pie.name;
			delete pie.surname;
		});
		console.log('del and go alert');
		restService.all("personal_transaction").customPOST($scope.personalInstallmentEvaluation, "installment_evaluation")
			.then(function(data){
				alert("Успешно определены рассрочки");
				$state.go("accountant.personal_balance.list");
				
		});
	};
	
}]);
