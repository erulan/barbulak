'use strict';

/* Controllers */
  // signin controller

app.controller('TransacrionListCtrl',['$scope', 'restService', 'ngTableParams', 'Utils',  function($scope, restService, ngTableParams, Utils) {
	$scope.transaction = {};
	$scope.transaction.person = {};
	
	$scope.datepicker = {};
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            date: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "transaction");
        },
    });
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
	$scope.init = function() {
	 	$scope.showBl = false;
        $scope.showName = true;
        $scope.showSurname = true;
        $scope.showData = true;
        $scope.showCol = true;
        $scope.showCash = true;
        $scope.showCat = true;
        $scope.showPer = true;
        $scope.showOpis = true;
	}
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
	  
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
}]);

app.controller('TransactionAddCtrl', ['$scope', 'restService','$location',function($scope, restService, $location,ScoreDataService){
	
	$scope.transaction = {};
	$scope.transaction.person = {};
//	$scope.transaction.person.id;
	$scope.transaction.cashbox = {};
//	$scope.transaction.case1.id;
	$scope.transaction.transactionCategory= {};
	$scope.transactionTypeID=0;
//	$scope.transaction.transactionCategory.id;
	$scope.personTypeID = 0;
	$scope.order = {"name":false};
	
	$scope.datepicker = {};
	
	restService.all("transaction").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.transactions = data.originalElement.resultList;
	});
	restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.transaction_categorys = data.originalElement.resultList;
	});
	restService.all("person").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.persons = data.originalElement.resultList;
	});
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});

	$scope.create = function(){
    	restService.one("transaction").customPOST($scope.transaction, "create").then(function(data){
			alert("Успешно была добавлена транзакция!");
			$location.path("accountant/transaction/list");
    	});
    };
    
   
    $scope.getScoreDate = function(){
    	if($scope.transactionTypeID==0){
    		restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000},
    		"list").then(function(data){
    			$scope.transaction_categorys = data.originalElement.resultList;
    		});
    	}
    	else if($scope.transactionTypeID==1){
    		restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000, 
    			"searchParameters":{"income": false}}, "list").then(function(data){
    			$scope.transaction_categorys = data.originalElement.resultList;
    		});
    	}
    	else if($scope.transactionTypeID==2){
    		restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000, 
    			"searchParameters":{"income": true}}, "list").then(function(data){
    			$scope.transaction_categorys = data.originalElement.resultList;
    		});
    	}
    };
    
    $scope.getPersons = function(){
    	if($scope.personTypeID==0){
    		restService.all("person").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list_for_acc").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    	else if($scope.personTypeID==1){

    		restService.all("staff").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    	else if($scope.personTypeID==2){
    		restService.all("client").customPOST({startIndex: 0, searchParameters:{"clientFor19Bottle":false}, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    	else if($scope.personTypeID==3){
    		restService.all("client").customPOST({startIndex: 0,  searchParameters:{"clientFor19Bottle":true}, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    	else if($scope.personTypeID==4){
    		restService.all("partner").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    };
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}]);


app.controller('PersonalTransacrionListCtrl',['$scope', 'restService', 'ngTableParams', 'Utils', 
                                              function($scope, restService, ngTableParams, Utils) {
	$scope.personal_transaction = {};
	$scope.personal_transaction.personalBalance= {};
	$scope.personal_transaction.personalBalance.person= {};
	
	$scope.datepicker = {};
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            date: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "personal_transaction");
        },
    });
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
	$scope.init = function() {
	 	$scope.showBl = false;
        $scope.showName = true;
        $scope.showSurname = true;
        $scope.showAll = true;
        $scope.showDate = true;
        $scope.showCol = true;
        $scope.showCat = true;
        $scope.showPer = true;
        $scope.showOpis = true;
	}
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	
	  
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
}]);

app.controller('PersonalTransactionAddCtrl', ['$scope', 'restService','$location',function($scope, restService,$location){
	
	$scope.personal_transaction = {};
	$scope.personaBalance = {};
	/*$scope.personal_transaction.personalBalance.id;*/
/*	$scope.personal_transaction.personal_balance.id;*/
	$scope.person = {};
	/*$scope.personal_transaction.person.id;*/
	
	$scope.datepicker = {};
	$scope.personTypeID = 0;
	$scope.order = {"name":false};
	
	
	restService.all("personal_transaction_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.personal_transaction_categorys = data.originalElement.resultList;
	});
	
	$scope.create = function(){
    	restService.one("personal_transaction").customPOST($scope.personal_transaction, "create").then(function(data){
			alert("Успешно была добавлена персональная транзакция!");
			$location.path("accountant/transaction/listP");
    	});
    };
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.getPersons = function(){
	    	if($scope.personTypeID==0){
	    		restService.all("person").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list_for_acc").then(function(data){
	    			$scope.persons = data.originalElement.resultList;
	    		});
	    	}
	    	else if($scope.personTypeID==1){

	    		restService.all("staff").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
	    			$scope.persons = data.originalElement.resultList;
	    		});
	    	}
	    	else if($scope.personTypeID==2){
	    		restService.all("client").customPOST({startIndex: 0, searchParameters:{"clientFor19Bottle":false}, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
	    			$scope.persons = data.originalElement.resultList;
	    		});
	    	}
	    	else if($scope.personTypeID==3){
	    		restService.all("client").customPOST({startIndex: 0,  searchParameters:{"clientFor19Bottle":true}, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
	    			$scope.persons = data.originalElement.resultList;
	    		});
	    	}
	    	else if($scope.personTypeID==4){
	    		restService.all("partner").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
	    			$scope.persons = data.originalElement.resultList;
	    		});
	    	}
	    };
}]);

app.controller('TransactionAddAllCtrl', ['$scope', 'restService','$location','$stateParams',
                                         function($scope, restService, $location, $stateParams){
	
	
	$scope.transaction = {};
	$scope.transaction.person = {};
	$scope.transaction.cashbox = {};
	$scope.transaction.transactionCategory= {};
	$scope.transactionTypeID=0;

	$scope.personal_transaction_categoryId;
	$scope.datepicker = {};
	$scope.personTypeID = 0;
	$scope.order = {"name":false};
	
	restService.all("transaction").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.transactions = data.originalElement.resultList;
	});
	restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.transaction_categorys = data.originalElement.resultList;
	});
	restService.all("person").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.persons = data.originalElement.resultList;
	});
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});


	restService.all("personal_transaction_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.personal_transaction_categorys = data.originalElement.resultList;
	});
	
	$scope.create = function(){
    	restService.one("transaction").customPOST($scope.transaction, "create_personal/" +$scope.personal_transaction_categoryId).then(function(data){
			alert("Успешно была добавлена транзакция!");
			$location.path("accountant/transaction/list");
    	});
    };
    
    $scope.getPersons = function(){
    	if($scope.personTypeID==0){
    		restService.all("person").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list_for_acc").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    	else if($scope.personTypeID==1){

    		restService.all("staff").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    	else if($scope.personTypeID==2){
    		restService.all("client").customPOST({startIndex: 0, searchParameters:{"clientFor19Bottle":false}, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    	else if($scope.personTypeID==3){
    		restService.all("client").customPOST({startIndex: 0,  searchParameters:{"clientFor19Bottle":true}, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    	else if($scope.personTypeID==4){
    		restService.all("partner").customPOST({startIndex: 0, "orderParamDesc" : $scope.order, resultQuantity: 1000}, "list").then(function(data){
    			$scope.persons = data.originalElement.resultList;
    		});
    	}
    };

    $scope.getScoreDate = function(){
    	if($scope.transactionTypeID==0){
    		restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000},
    		"list").then(function(data){
    			$scope.transaction_categorys = data.originalElement.resultList;
    		});
    	}
    	else if($scope.transactionTypeID==1){
    		restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000, 
    			"searchParameters":{"income": false}}, "list").then(function(data){
    			$scope.transaction_categorys = data.originalElement.resultList;
    		});
    	}
    	else if($scope.transactionTypeID==2){
    		restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000, 
    			"searchParameters":{"income": true}}, "list").then(function(data){
    			$scope.transaction_categorys = data.originalElement.resultList;
    		});
    	}
    };
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}]);


	
