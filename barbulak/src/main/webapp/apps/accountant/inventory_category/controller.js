'use strict';

/* Controllers */
  // signin controller

app.controller('InventoryCategoryListCtrl',['$scope', 'restService', 'ngTableParams', 'Utils','$state',  function($scope, restService, ngTableParams, Utils, $state) {
	$scope.inventoryCategory = {};
	
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "inventory_category");
        },
    });
	$scope.deleteCategory = function(id){
		Utils.callDeleteModal("inventory_category", id);
		$state.reload();
	};
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
}]);

app.controller('InventoryCategoryAddCtrl',['$scope', 'restService','$location', function($scope, restService,$location) {
	$scope.inventory_category = {};
	
	restService.all("inventory_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.inventory_categorys = data.originalElement.resultList;
	});
	
	$scope.create = function(){
		restService.one("inventory_category").customPOST($scope.inventory_category, "create").then(function(){
			alert("Успешно был добавлен категория инвентаря!");
			$location.path('accountant/inventory_category/list');
		});
	};
	
}]);

app.controller('InventoryCategoryEditCtrl',['$scope', 'restService','$location', '$stateParams',
                                            function($scope, restService, $location,$stateParams) {
	$scope.inventory_category = {};
	$scope.denemeId = $stateParams.id;
	
	restService.all("inventory_category").customGET( $scope.denemeId ).then(function(data){
		$scope.inventory_category = data.originalElement;
	});
	
	$scope.update = function(){
		restService.one("inventory_category").customPOST($scope.inventory_category, "update/" + $scope.denemeId ).then(function(){
			alert("Успешно был изменен категория инвентаря!");
			$location.path('accountant/inventory_category/list');
		});
	};
	
}]);
	
