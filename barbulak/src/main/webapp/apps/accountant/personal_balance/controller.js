'use strict';

/* Controllers */
  // signin controller

app.controller('PersonalBalanceListCtrl', ['$scope','restService', 'ngTableParams','Utils',
                                           function($scope, restService, ngTableParams, Utils){

	$scope.tableParams = new ngTableParams({
	        page: 1,            // show first page
	        count: 10, 			// count per page
	        sorting: {
	            person: 'desc'     // initial sorting
	        },				
	    }, {
	        total: 0, // length of data
	        getData: function($defer, params){
	        	console.log('personal_balance');
	        	Utils.ngTableGetData($defer, params, "personal_balance");
	        },
	    });
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
	
	
}]);


	
