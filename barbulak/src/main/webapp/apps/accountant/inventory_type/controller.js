'use strict';

/* Controllers */
  // signin controller

app.controller('InventoryTypeListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',
                                         function($scope, restService, ngTableParams, Utils) {
	
/*	
	restService.all("inventory_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.inventoryTypes = data.originalElement.resultList;
	});*/
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "inventory_type");
        },
    });
	$scope.deleteType = function(id){
		Utils.callDeleteModal("inventory_type", id);
		$state.reload();
//		$window.$location.reload(true);
	};
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
}]);

app.controller('InventoryTypeAddCtrl', ['$scope', 'restService','$location', function($scope, restService, $location) {
	$scope.inventory_type = {};
	$scope.inventoryCategory = {};
	$scope.inventoryCategory.id;
	
	restService.all("inventory_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.inventory_categorys = data.originalElement.resultList;
	});
	restService.all("inventory_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.inventory_types = data.originalElement.resultList;
	});
	
	$scope.create = function(){
		restService.one("inventory_type").customPOST($scope.inventory_type, "create").then(function(){
			alert("Успешно был добавлен экземпляр!");
			$location.path("accountant/inventory_type/list");
		});
	};

}]);
app.controller('InventoryTypeEditCtrl', ['$scope', 'restService','$location', '$stateParams',
                                         function($scope, restService, $location, $stateParams) {
	$scope.inventory_type = {};
	$scope.inventory_type.inventoryCategory = {};
	//$scope.inventory_type.inventoryCategory.id;
	$scope.denemeId = $stateParams.id;
	
	restService.all("inventory_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.inventory_categories = data.originalElement.resultList;
	});
	
	restService.all("inventory_type").customGET( $scope.denemeId ).then(function(data){
		$scope.inventory_type = data.originalElement;
	});
	
	$scope.update = function(){
		restService.one("inventory_type").customPOST($scope.inventory_type, "update/" + $scope.denemeId).then(function(){
			alert("Успешно был изменен тип!");
			$location.path("accountant/inventory_type/list");
		});
	};

}]);	


