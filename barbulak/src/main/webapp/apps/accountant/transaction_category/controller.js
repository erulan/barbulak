'use strict';

/* Controllers */
  // signin controller

app.controller('TransactionCategoryListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils','$state','$location',  function($scope, restService, ngTableParams, Utils, $state, $location) {
	$scope.inventoryType = {};
/*	
	restService.all("inventory_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.inventoryTypes = data.originalElement.resultList;
	});*/
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'desc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "transaction_category");
        },
    });
	$scope.deleteTransactionCategory = function(id){
		Utils.callDeleteModal("transaction_category", id);
		$state.reload();
	};
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
}]);

app.controller('TransactionCategoryAddCtrl', ['$scope', 'restService','$location', function($scope, restService, $location) {
	$scope.transaction_category = {};

	restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.transaction_categorys = data.originalElement;
	});
	$scope.create = function(){
		restService.one("transaction_category").customPOST($scope.transaction_category, "create").then(function(){
			alert("Успешна была добавлена категория транзакций!");
			$location.path("accountant/transaction_category/list");
		});
	};

}]);
app.controller('TransactionCategoryEditCtrl', ['$scope', 'restService','$location', '$stateParams',
                                         function($scope, restService, $location, $stateParams) {
	$scope.transaction_category = {};
	$scope.transaction_categoryId = $stateParams.id;
	$scope.updated = false;
	restService.all("transaction_category").customGET( $scope.transaction_categoryId ).then(function(data){
		$scope.transaction_category = data.originalElement;

		if($scope.transaction_category.income = true){
			$scope.transaction_category.income = 'true';
		}
		else{
			$scope.transaction_category.income = 'false';
		};

	});
	
	$scope.update = function(){
		$scope.updated = true;
		
		restService.one("transaction_category").customPOST($scope.transaction_category, "update/" + $scope.transaction_categoryId).then(function(){
			alert("Успешно была изменена категория транзакций!");
			$location.path("accountant/transaction_category/list");
		});
	};

}]);	


