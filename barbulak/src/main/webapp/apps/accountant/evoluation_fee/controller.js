'use strict';

/* Controllers */
  // signin controller

app.controller('EvoluationFeeAddCtrl', ['$scope', 'restService','$state', function($scope, restService, $state) {
	
	$scope.staff = {};
	$scope.staff.id;
	$scope.staff.person = {};
	$scope.staff.person.id;
	
	$scope.personalTransactionEvaluation = [];
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
	
	restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.staffs = data.originalElement.resultList;
		
		$scope.personalTransactionEvaluation = new Array($scope.staffs.length);
		
		angular.forEach($scope.staffs, function(staff, key){
			$scope.personalTransactionEvaluation[key] = ({
				"amount": staff.salary, 
				"description": "", 
				"personId": staff.id, 
				"name": staff.name, 
				"surname": staff.surname
			});
		});
	});
	
	
	$scope.sendToBackEnd = function(){
		angular.forEach($scope.personalTransactionEvaluation, function(pte, key){
			delete pte.name;
			delete pte.surname;
		});
		restService.all("personal_transaction").customPOST($scope.personalTransactionEvaluation, "salary_evaluation").then(function(data){
			alert("Успешно сохранен");
			$state.go("accountant.personal_balance.list");
			
		});
	};
	

}]);