'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/accountant/personal_balance/list');
          $stateProvider
              .state('accountant', {
                  abstract: true,
                  url: '/accountant',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //personal_balance
              .state('accountant.personal_balance', {
                  abstract: true,
                  url: '/personal_balance',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/personal_balance/controller.js']);
                      }]
                    }
              })
              .state('accountant.personal_balance.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/personal_balance/list.html',
              })
              
              
              
              //transactions
              .state('accountant.transaction', {
                  abstract: true,
                  url: '/transaction',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/transaction/controller.js']);
                      }]
                    }
              })
              .state('accountant.transaction.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/transaction/list.html',
              })
              .state('accountant.transaction.listP', {
                  url: '/listP',
                  templateUrl: '../../apps/accountant/transaction/listP.html',
              })
              .state('accountant.transaction.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/transaction/add.html',
              })
              .state('accountant.transaction.addP', {
            	  url: '/addP',
            	  templateUrl: '../../apps/accountant/transaction/addP.html',
              })
              .state('accountant.transaction.addAll', {
            	  url: '/addAll',
            	  templateUrl: '../../apps/accountant/transaction/addAll.html',
              })
              
              
              //transaction_category
              .state('accountant.transaction_category', {
                  abstract: true,
                  url: '/transaction_category',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/transaction_category/controller.js']);
                      }]
                    }
              })
              .state('accountant.transaction_category.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/transaction_category/list.html',
              })
              .state('accountant.transaction_category.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/transaction_category/add.html',
              })
              .state('accountant.transaction_category.edit', {
            	  url: '/edit/:id',
            	  templateUrl: '../../apps/accountant/transaction_category/edit.html',
              })
              
              //installment
              .state('accountant.installment', {
                  abstract: true,
                  url: '/installment',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/installment/controller.js']);
                      }]
                    }
              })
              .state('accountant.installment.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/installment/list.html',
              })
              .state('accountant.installment.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/installment/add.html',
              })
              
              //inventory
              .state('accountant.inventory', {
                  abstract: true,
                  url: '/inventory',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/inventory/controller.js']);
                      }]
                    }
              })
              .state('accountant.inventory.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/inventory/list.html',  
              })
               .state('accountant.inventory.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/inventory/add.html',
              })
              .state('accountant.inventory.edit', {
            	  url: '/edit/:id',
            	  templateUrl: '../../apps/accountant/inventory/edit.html',
              })
              .state('accountant.inventory.listS', {
                  url: '/listS',
                  templateUrl: '../../apps/accountant/inventory/listS.html',  
              })
              
              //inventory_category
              .state('accountant.inventory_category', {
                  abstract: true,
                  url: '/inventory_category',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/inventory_category/controller.js']);
                      }]
                    }
              })
              .state('accountant.inventory_category.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/inventory_category/list.html',
              })
              .state('accountant.inventory_category.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/inventory_category/add.html',
              })
              .state('accountant.inventory_category.edit', {
            	  url: '/edit/:id',
            	  templateUrl: '../../apps/accountant/inventory_category/edit.html',
              })
              
              //inventory_type
              .state('accountant.inventory_type', {
                  abstract: true,
                  url: '/inventory_type',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/inventory_type/controller.js']);
                      }]
                    }
              })
              .state('accountant.inventory_type.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/inventory_type/list.html',
              })
              .state('accountant.inventory_type.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/inventory_type/add.html',
              })
              .state('accountant.inventory_type.edit', {
            	  url: '/edit/:id',
            	  templateUrl: '../../apps/accountant/inventory_type/edit.html',
              })
              
              //evoluationInstallment
              .state('accountant.evoluation_installment', {
                  abstract: true,
                  url: '/evoluation_installment',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/evoluation_installment/controller.js']);
                      }]
                    }
              })
             /* .state('accountant.evoluation_installment.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/evoluation_installment/list.html',
              })*/
              .state('accountant.evoluation_installment.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/evoluation_installment/add.html',
              })
              
            //evoluationFee
              .state('accountant.evoluation_fee', {
                  abstract: true,
                  url: '/evoluation_fee',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/evoluation_fee/controller.js']);
                      }]
                    }
              })
       /*       .state('accountant.evoluation_fee.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/evoluation_fee/list.html',
              })*/
              .state('accountant.evoluation_fee.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/evoluation_fee/add.html',
              })
            
            //calculation
              .state('accountant.calculation', {
                  abstract: true,
                  url: '/calculation',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/calculation/controller.js']);
                      }]
                    }
              })
              .state('accountant.calculation.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/calculation/list.html',
              })
              .state('accountant.calculation.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/calculation/add.html',
              })
              
              
            //buy raw material 
              .state('accountant.buy_raw_material', {
                  abstract: true,
                  url: '/buy_raw_material',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/accountant/buy_raw_material/controller.js']);
                      }]
                    }
              })
              .state('accountant.buy_raw_material.list', {
                  url: '/list',
                  templateUrl: '../../apps/accountant/buy_raw_material/list.html',
              })
              .state('accountant.buy_raw_material.add', {
            	  url: '/add',
            	  templateUrl: '../../apps/accountant/buy_raw_material/add.html',
              })
              
      }
    ]
  );
