'use strict';

/* Controllers */
  // signin controller


app.controller('ProductPlaceCtrl', ['$scope',  function($scope) {
    $scope.deneme = "test kylyp jatabyz";
  }]);

app.controller('ProductPlaceListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, restService, ngTableParams, Utils, $state) {
	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.places = data.originalElement.resultList;
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "place");
        },
    });
	$scope.deletePlace = function(id){
		Utils.callDeleteModal("place", id);
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
    
    $scope.init = function() {
        $scope.showName = true;
        $scope.showPrice = true;
        $scope.showStaff = true;
        $scope.showAddress = true;
        $scope.showType = true;
        $scope.showOption = true;
    }
  }]);
  
app.controller('ProductPlaceAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
    //$scope.name = "ADDCONTROLLER";
    $scope.productPlace = {};
	$scope.productPlace.placeType = {};
	$scope.productPlace.staff = {};

	restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});

	restService.all("place_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.place_types = data.originalElement.resultList;
	});
	
    $scope.create = function(){
    	restService.one("place").customPOST($scope.productPlace, "create").then(function(data){
    		alert("Успешно создано!");
    		$location.path("admin/product_place/list");
    	});
    	}
  }])
  
  app.controller('ProductPlaceEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
    
	$scope.productPlace = {};
	$scope.productPlace.placeType = {};
	$scope.productPlace.staff = {};
    $scope.placeId = $stateParams.id;
    
    restService.all("place").customGET( $scope.placeId).then(function(data){
		$scope.productPlace= data.originalElement;
	});
    
    restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});
    
    restService.all("place_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.place_types = data.originalElement.resultList;
	});
    
	
    $scope.update = function(){
    	restService.one("place").customPOST($scope.productPlace, "update/" + $scope.placeId).then(function(data){
    		alert("Успешно изменено!");
    		$location.path("admin/product_place/list");
    	});
    }
  }]);;






