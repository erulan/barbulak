'use strict';

/* Controllers */
  // signin controller


app.controller('ProductCategoryListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state',  function($scope, restService, ngTableParams, Utils, $state) {
	
	/*restService.all("product_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.categories = data.originalElement.resultList;
	});*/
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "product_category");
        },
    });
	$scope.deleteCategory = function(id){
		Utils.callDeleteModal("product_category", id);
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  }]);
  
app.controller('ProductCategoryAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
	
	$scope.productCategory = {};
	
    $scope.create = function(){
    	restService.one("product_category").customPOST($scope.productCategory, "create").then(function(data){
    		alert("Успешно создана!");
    		$location.path("admin/product_category/list");
    	});
    }

  }])
  
  app.controller('ProductCategoryEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
	
	$scope.product_category = {};
//	$scope.categoryId = $routeParams.id;
	
	$scope.denemeId = $stateParams.id;
	
	restService.all("product_category").customGET( $scope.denemeId).then(function(data){
		$scope.product_category = data.originalElement;
	});
	
    $scope.update = function(){
    	restService.one("product_category").customPOST($scope.product_category, "update/" + $scope.denemeId).then(function(data){
    		alert("Успешно изменена!");
    		$location.path("admin/product_category/list");
    	});
    }
  }]);




