'use strict';

/* Controllers */
  // signin controller


app.controller('DelTransListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$location', '$state', '$timeout', function($scope, restService, ngTableParams, Utils, $location, $state, $timeout) {
		
	$scope.transaction = {};
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "transaction");
        },
    });
	$scope.deleteTrans = function(id){
    	restService.one("transaction").customGET("delete/" + id).then(function(data){
    		$timeout(function () {
    			alert("Успешно удалена!");
    		 }); 
    		$state.reload();
    	});
    }
  }])
  
  app.controller('DelPTransListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', '$timeout', function($scope, restService, ngTableParams, Utils, $state, $timeout) {
		
	$scope.ptransaction = {};
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "personal_transaction");
        },
    });
	$scope.deleteTrans = function(id){
    	restService.one("personal_transaction").customGET("delete/" + id).then(function(data){
    		$timeout(function () {
    			alert("Успешно удалена!");
    		 }); 
    		$state.reload();
    	});
    }
  }]);