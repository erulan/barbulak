'use strict';

/* Controllers */
  // signin controller


app.controller('CapacityListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, restService, ngTableParams, Utils, $state) {
	
	restService.all("capacity").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.capacities = data.originalElement.resultList;
	});
	restService.all("unit").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.units = data.originalElement.resultList;
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "capacity");
        },
    });
	$scope.deleteCapacity = function(id){
		Utils.callDeleteModal("capacity", id);
		
	};
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));

    };
    $scope.init = function() {
        $scope.showName = true;
        $scope.showCapacity = true;
    }
  }]);
  
app.controller('CapacityAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
	
	$scope.capacity = {};
	/*$scope.capacity.name = "фыва";
	$scope.capacity.capacity = {};*/
	$scope.capacity.unit = {};
	
	restService.all("unit").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.units = data.originalElement.resultList;
	});
    $scope.createt = function(){
    	restService.one("capacity").customPOST($scope.capacity, "create").then(function(data){
    		alert("Успешно создан обьем");
    		$location.path("admin/capacity/list");
    	});
    }
  }])
  
  app.controller('CapacityEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
    $scope.capacity = {};
    $scope.capacity.unit = {};
    $scope.capacityId = $stateParams.id;
    
    restService.all("capacity").customGET( $scope.capacityId).then(function(data){
		$scope.capacity= data.originalElement;
	});
    
    restService.all("unit").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.units = data.originalElement.resultList;
	});
    
    restService.all("capacity").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.capacities = data.originalElement.resultList;
	});
    
    $scope.update = function(){
    	restService.one("capacity").customPOST($scope.capacity, "update/" + $scope.capacityId).then(function(data){
    		alert("Успешно изменен обьем");
    		$location.path("admin/capacity/list");
    	});
    }
  }]);

