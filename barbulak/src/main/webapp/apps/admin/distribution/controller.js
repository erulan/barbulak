 app.controller('DistributionListCtrl', ['$scope',  '$window', '$location', 'restService', 'ngTableParams', 'Utils', '$timeout', '$state', function($scope, $window, $location, restService, ngTableParams, Utils, $timeout, $state) {
	
	$scope.purchase = {};
	$scope.production = {};
	$scope.approved = false;
	$scope.statusId = 4;
	$scope.placeByFromPlaceId = {};
	
	$scope.getShippingList = function($defer, params, path) {				
		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		searchParams["searchParameters"] = {};
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {"created" : true};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}


	$scope.tableParams = new ngTableParams({
	page: 1,            // show first page
	count: 10, 			// count per page
	sorting: {
	name: 'asc'     // initial sorting
			},				
	}, {
	total: 0, // length of data
	getData: function($defer, params){
	    	$scope.getShippingList($defer, params, "ware_distributor");
		},
	});

	$scope.approveproduction = function(productionId){
    	restService.one("ware_distributor").customGET("approve_by_admin/" + productionId).then(function(data){
    		$timeout(function () {
    			alert("Успешно потвержден!");
    		 }); 
    		$state.reload();
    	});
    }
	$scope.rejectproduction = function(productionId){
    	restService.one("ware_distributor").customGET("reject_by_admin/" + productionId).then(function(data){
    		$timeout(function () {
    			alert("Успешно отменен!");
    		 }); 
    		$state.reload();
    	});
    }
	$scope.showproductionDetails = function(purchaseId){
		$scope.purchaseRaws = {};
		restService.all("Ware_dist_product").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"wareDistributor.id": purchaseId}}, "list").then(function(data){
			$scope.purchaseRaws = data.originalElement.resultList;
		});
		restService.one("ware_distributor").customGET(purchaseId).then(function(data){
			$scope.purchaseInfo = data.originalElement;
			if($scope.purchaseInfo.wareDistStatus.name == "отправлено"){
				$('#button_approve').show();
			}else if($scope.purchaseInfo.wareDistStatus.name == "запрос сырья"){
				if($scope.purchaseInfo.approvedByAdmin == true){
					$('#button_approve').hide();
					$('#button_cancel').hide();
				}else{
					$('#button_cancel').show();
				}
			}
			
		});
		$('.toggle_2') 	.show(1000);
	}
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
		};
  }]);