'use strict';

/* Controllers */
  // signin controller


app.controller('StaffCtrl', ['$scope',  function($scope) {
    $scope.deneme = "test kylyp jatabyz";
  }]);

app.controller('StaffListCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, $location, restService, ngTableParams, Utils, $state) {
	

	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "staff");
        },
    });
	$scope.deleteStaff = function(id){
		Utils.callDeleteModal("staff", id);
		$location.path("admin/staff/list");
	};
	
	$scope.createUser = function(id){
		Utils.callUserModal("staff", id);
		$location.path("admin/staff/list");
		/*restService.one("users").customGET("create_user_by_staffid/" + id).then(function(data){
			alert("success!");
    		$location.path("admin/staff/list");
		});*/
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
    $scope.init = function() {
        $scope.showName = true;
        $scope.showSurname = true;
        $scope.showPosition = true;
        $scope.showPhone = true;
        $scope.showPassport = true;
        $scope.showSalary = true;
        $scope.showCashbox = true;
        $scope.showOffice = true;
        $scope.showOption = true;
    }
  }]);
app.controller('UsersListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, restService, ngTableParams, Utils, $state) {
	
//	restService.all("users").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
//		$scope.users = data.originalElement.resultList;
//	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "users");
        },
    });
	$scope.deleteUsers = function(id){
		Utils.callDeleteModal("users", id);
		$state.reload();
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
	
  }]);
  
app.controller('StaffAddCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils',  function($scope, $location, restService, ngTableParams, Utils) {
    //$scope.name = "ADDCONTROLLER";
    $scope.staff = {};
    $scope.staff.cashbox = {};
    $scope.selectedPositions = [];
    $scope.selectedPositionIdList = [];
    
    restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});
    
    restService.all("position").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.positions = data.originalElement.resultList;
	});
    
    $scope.create = function(){
    	angular.forEach($scope.selectedPositions, function(value, key) {
    		$scope.selectedPositionIdList.push(value.id);
    	});
    	restService.one("staff").customPOST({"staff": $scope.staff, "positionIdList": $scope.selectedPositionIdList}, "create_with_position").then(function(data){
    		alert("success");
    		$location.path("admin/staff/list");
    	});
    };
    
    $scope.removePosition = function(positionId){
    	for(var i = 0; i < $scope.selectedPositions.length; i++){
    		if(positionId == $scope.selectedPositions[i].id){
    			$scope.selectedPositions.splice(i, 1);
    		}
    	}
    };
    
    $scope.addPosition = function(position){
    	if(position == null){
    		alert("Выберите должность сначала пожалуйста");
    		return;
    	} 
    	for(var i = 0; i < $scope.selectedPositions.length; i++){
    		if($scope.selectedPositions[i].id == position.id) {
    			alert("Вы уже добавили эту должность");
    			return;
    		}
    	}
    	$scope.selectedPositions.push(angular.copy(position));
    };
    
  }])
  app.controller('StaffEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
    //$scope.name = "ADDCONTROLLER";
    $scope.staff = {};
    $scope.staff.cashbox = {};
    $scope.staffId = $stateParams.id;
    $scope.selectedPositions = [];
    $scope.selectedPositionIdList = [];
    
    restService.all("staff").customGET( $scope.staffId).then(function(data){
		$scope.staff= data.originalElement;
	});
    
    restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});
    
    restService.all("position").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.positions = data.originalElement.resultList;
	});

    restService.all("staff_position").customPOST({startIndex: 0, resultQuantity: 1000, "searchParameters": {"staff.id": $scope.staffId}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(value, key){
			$scope.selectedPositions.push(value.position);
		});
	});
    
    
    $scope.update = function(){
    	angular.forEach($scope.selectedPositions, function(value, key) {
    		$scope.selectedPositionIdList.push(value.id);
    	});
    	restService.one("staff").customPOST({"staff": $scope.staff, "positionIdList": $scope.selectedPositionIdList}, "update_with_position/" + $scope.staffId).then(function(data){
    		alert("Успешно изменен!");
    		$location.path("admin/staff/list");
    	});
    }
    
    $scope.removePosition = function(positionId){
    	for(var i = 0; i < $scope.selectedPositions.length; i++){
    		if(positionId == $scope.selectedPositions[i].id){
    			$scope.selectedPositions.splice(i, 1);
    		}
    	}
    };
    
    $scope.addPosition = function(position){
    	if(position == null){
    		alert("Выберите должность сначала пожалуйста");
    		return;
    	} 
    	for(var i = 0; i < $scope.selectedPositions.length; i++){
    		if($scope.selectedPositions[i].id == position.id) {
    			alert("Вы уже добавили эту должность");
    			return;
    		}
    	}
    	$scope.selectedPositions.push(angular.copy(position));
    };
    
  }])
  
app.controller('StaffUsersEditCtrl', ['$scope', '$location', '$stateParams', 'restService', '$rootScope', '$state',  
                                      function($scope, $location, $stateParams, restService, $rootScope, $state) {

	$scope.userId = $stateParams.id;
	
    restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});
    
    restService.all("users").customGET( $scope.userId).then(function(data){
		$scope.user= data.originalElement;
	});
    
    restService.all("users_role").customPOST({searchParameters: {"users.id": $scope.userId}, startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.userRoles = data.originalElement.resultList;
	});
    
    restService.all("role").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.roles = data.originalElement.resultList;
	});
    
    
    $scope.removeRoleOfUser = function(id){
    	restService.all("users_role").customGET( "delete_with_role_user_id/" + id + "/" + $scope.userId).then(function(data){
    		alert("Removed");
    		if($rootScope.getPathAsRole() == "admin"){$rootScope.updateRoles()}
    		$state.reload();
    	});
    }
    
    $scope.addRoleOfUser = function(id){
    	restService.all("users_role").customPOST({role: {id: $scope.roleId}, users: {id: $scope.userId} },  "create").then(function(data){
    		alert("Added");
    		if($rootScope.getPathAsRole() == "admin"){$rootScope.updateRoles()}
    		$state.reload();
    	});
    	
    }
    
  }])
  app.controller('ManagerListCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, $location, restService, ngTableParams, Utils, $state) {
	

	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "place_19_manager");
        },
    });
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
    
    $scope.deleteManager = function(id){
		Utils.callDeleteModal("place_19_manager", id);
	
    };
  }])
  
  app.controller('StaffManagerAddCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils',  function($scope, $location, restService, ngTableParams, Utils) {
    //$scope.name = "ADDCONTROLLER";

    $scope.place_id = {};
    $scope.manager_id = {};
    
    restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"placeType.id": 2}}, "list").then(function(data){
		$scope.places = data.originalElement.resultList;
	});
    
    restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});
    
    $scope.create = function(){/*
    	angular.forEach($scope.selectedPositions, function(value, key) {
    		$scope.selectedPositionIdList.push(value.id);
    	});*/
    	
    	restService.one("place_19_manager").customPOST({"place": {"id": $scope.place_id}, "manager": {"id": $scope.manager_id}}, "create").then(function(data){
    		alert("Успешно добавлен!");
    		$location.path("admin/staff/managerlist");
    	});
    };
    
  }])
  
    app.controller('StaffManagerEditCtrl', ['$scope', '$stateParams', '$location', 'restService', 'ngTableParams', 'Utils',  function($scope, $stateParams, $location, restService, ngTableParams, Utils) {
    //$scope.name = "ADDCONTROLLER";

   
    $scope.staffId = $stateParams.id;
    
    restService.all("place_19_manager").customGET( $scope.staffId).then(function(data){
		$scope.place_19_manager= data.originalElement;
	});
    
    restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"placeType.id": 2}}, "list").then(function(data){
		$scope.places = data.originalElement.resultList;
	});
    
    restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});
    
    $scope.update = function(){/*
    	angular.forEach($scope.selectedPositions, function(value, key) {
    		$scope.selectedPositionIdList.push(value.id);
    	});*/
    	
    	restService.one("place_19_manager").customPOST($scope.place_19_manager, "update/"+ $stateParams.id).then(function(data){
    		alert("Успешно обновлен!");
    		$location.path("admin/staff/managerlist");
    	});
    };
    
    
  }]);



