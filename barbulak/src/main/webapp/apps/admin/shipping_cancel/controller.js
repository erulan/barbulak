'use strict';

/* Controllers */
  // signin controller


app.controller('ShippingListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, restService, ngTableParams, Utils, $state) {
		
	$scope.shipping = {};
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "shipping_cancel_request");
        },
    });
	$scope.deleteShipping = function(shippingId){
    	restService.one("shipping").customPOST($scope.shipping, "cancel/" + shippingId).then(function(data){
    		alert("success");
    		$location.path("admin/shipping_cancel/list");
    	});
    }
	$scope.rejectShipping = function(shippingId){
    	restService.one("shipping").customPOST($scope.shipping, "update/" + shippingId).then(function(data){
    		alert("success");
    		$location.path("admin/shipping_cancel/list");
    	});
    }
  }]);
  
app.controller('RawAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
	
	$scope.raw = {};
	$scope.raw.unit = {};
	
	restService.all("unit").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.units = data.originalElement.resultList;
	});
    $scope.create = function(){
    	restService.one("raw").customPOST($scope.raw, "create").then(function(data){
    		alert("success");
    		$location.path("admin/raw_material/list");
    	});
    }
  }])
  
  app.controller('RawEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
   
	$scope.raw = {};
    $scope.rawId = $stateParams.id;
    
    restService.all("raw").customGET( $scope.rawId).then(function(data){
		$scope.raw = data.originalElement;
	});
    
    restService.all("unit").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.units = data.originalElement.resultList;
	});
    
	
    $scope.update = function(){
    	restService.one("raw").customPOST($scope.raw, "update/" + $scope.rawId).then(function(data){
    		alert("success");
    		$location.path("admin/raw_material/list");
    	});
    }
  }]);;