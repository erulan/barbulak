'use strict';

/* Controllers */
  // signin controller


app.controller('UserPositionListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state',function($scope, restService, ngTableParams, Utils, $state) {
	
	restService.all("position").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.positions = data.originalElement.resultList;
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "position");
        },
    });
	$scope.deletePosition = function(id){
		Utils.callDeleteModal("position", id);
	};
  }]);
  
app.controller('UserPositionAddCtrl', ['$scope', 'restService','$location', function($scope, restService,$location) {
	
	$scope.position = {};
	
    $scope.create = function(){
    	restService.one("position").customPOST($scope.position, "create").then(function(data){
    		alert("Успешно создан");
    		$location.path("admin/settings/list");
    	});
    }

  }])
  
  app.controller('UserPositionEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
	
	$scope.position = {};
//	$scope.categoryId = $routeParams.id;
	
	$scope.denemeId = $stateParams.id;
	
	restService.all("position").customGET( $scope.denemeId).then(function(data){
		$scope.position = data.originalElement;
	});
	
    $scope.update = function(){
    	restService.one("position").customPOST($scope.position, "update/" + $scope.denemeId).then(function(data){
    		alert("Успешно изменен");
    		$location.path("admin/settings/list");
    	});
    }
  }])
  
  app.controller('CashboxAccListCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, $location, restService, ngTableParams, Utils, $state) {
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "cashbox_accountant");
        },
    });
	$scope.deleteCashboxAccountant = function(id){
		Utils.callDeleteModal("cashbox_accountant", id);
	};
	
	    $scope.exportData = function () {
	        var blob = new Blob([document.getElementById('exportable').innerHTML], {
	            type: "application/vnd.ms-excel;charset=utf-8"
	        });
	        saveAs(blob, "Report.xls");
	    };
	
  }])
  
  app.controller('CashboxAccountantAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
	
	$scope.cashbox_accountant = {};
	
	restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});
	
    $scope.create = function(){
    	restService.one("cashbox_accountant").customPOST($scope.cashbox_accountant, "create").then(function(data){
    		alert("Успешно создан");
    		$location.path("admin/settings/acclist");
    	});
    }

  }])
  
   	app.controller('CashboxAccountantEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
	
	$scope.cashbox_accountant = {};
	$scope.denemeId = $stateParams.id;
	
	restService.all("cashbox_accountant").customGET( $scope.denemeId).then(function(data){
		$scope.cashbox_accountant = data.originalElement;
	});
	
	restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});
	
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});
	
    $scope.update = function(){
    	restService.one("cashbox_accountant").customPOST($scope.cashbox_accountant, "update/" + $scope.denemeId).then(function(data){
    		alert("Успешно изменен");
    		$location.path("admin/settings/acclist");
    	});
    }
  }]);




