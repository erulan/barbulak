'use strict';

/* Controllers */
  // signin controller


app.controller('InventoryListCtrl', ['$scope', '$state', 'restService', 'ngTableParams', 'Utils',  '$modal', function($scope, $state, restService, ngTableParams, Utils, $modal) {
	
	restService.all("inventory").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.inventories = data.originalElement.resultList;
	});
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "inventory");
        },
    });
	
	
	$scope.deleteInventory = function(id){
		Utils.callDeleteModal("inventory", id);
		$state.reload();
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
    $scope.init = function() {
    	$scope.showBl = true;
        $scope.showName = true;
        $scope.showSurname = true;
        $scope.showNum = true;
        $scope.showPlace = true;
        $scope.showEx = true;
        $scope.showPrice = true;
        $scope.showStatus = true;
        $scope.showOper = true;
        $scope.showCreate = true;
        $scope.showEnd = true;
    }
  }]);
  
app.controller('InventoryAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
    $scope.name = "ADDCONTROLLER";
    $scope.inventory = {};
    $scope.inventory.inventoryType = {};
    $scope.inventory.inventoryStatus = {};
    $scope.inventory.price = {};
    $scope.datepicker = {};
	$scope.datepicker2 = {};
    
    restService.all("inventory_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.types = data.originalElement.resultList;
	});
    
    restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.places = data.originalElement.resultList;
	});
    
    restService.all("person").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.persons = data.originalElement.resultList;
	});
    
    restService.all("inventory_status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuses = data.originalElement.resultList;
	});
    
    restService.all("price").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.prices = data.originalElement.resultList;
	});
    
	
    $scope.create = function(){
    	restService.one("inventory").customPOST($scope.inventory, "create").then(function(data){
    		alert("Успешно создана!");
    		$location.path("admin/inventory/list");
    	});
    }
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
	  
	  $scope.datepicker2.format = 'dd/MM/yyyy';

	  $scope.datepicker2.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
  }])
    
app.controller('InventoryEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
    $scope.name = "ADDCONTROLLER";
    $scope.inventory = {};
    $scope.inventory.inventory_type = {};
    $scope.inventory.inventory_status = {};
    $scope.inventory.price = {};
    $scope.datepicker = {};
	$scope.datepicker2 = {};
    
    $scope.inventoryId = $stateParams.id;
    
    restService.all("inventory_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.types = data.originalElement.resultList;
	});
    
    restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.places = data.originalElement.resultList;
	});
    
    restService.all("person").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.persons = data.originalElement.resultList;
	});
    
    restService.all("inventory").customGET( $scope.inventoryId).then(function(data){
		$scope.inventory= data.originalElement;
	});

    restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.places = data.originalElement.resultList;
	});
    
    restService.all("inventory_status").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.statuses = data.originalElement.resultList;
	});
    
    restService.all("price").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.prices = data.originalElement.resultList;
	});
    
    $scope.update = function(){
    	restService.one("inventory").customPOST($scope.inventory, "update/" + $scope.inventoryId).then(function(data){
    		alert("Успешно обновлена!");
    		$location.path("admin/inventory/list");
    	});
    }
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  $scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
	  
	  $scope.datepicker2.format = 'dd/MM/yyyy';

	  $scope.datepicker2.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
  }]);
app.controller('InventoryTypeListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', function($scope, restService, ngTableParams, Utils, $modal) {

	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "inventory_type");
        },
    });
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  }]);
app.controller('AdminInventoryListSCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', '$modal', function($scope, restService, ngTableParams, Utils, $state, $modal) {
	$scope.inventory = {};
	$scope.inventory.inventoryType = {};
	$scope.inventory.personId = {};
	$scope.inventory.placeId = {};
	
	$scope.getInventoryList = function($defer, params, path) {
		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		searchParams["searchParameters"] = {"inventoryStatus.name" : "списано"};
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		  	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
}
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	$scope.getInventoryList($defer, params, "inventory");
        },
    });
	$scope.deleteInventory = function(id){
		Utils.callDeleteModal("inventory", id);
		$state.reload();
	};
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
	
	 $scope.init = function() {
		 	$scope.showBl = true;
	        $scope.showName = true;
	        $scope.showSurname = true;
	        $scope.showNum = true;
	        $scope.showPlace = true;
	        $scope.showEx = true;
	        $scope.showPrice = true;
	        $scope.showStatus = true;
	        $scope.showOper = true;
	        $scope.showCreate = true;
	        $scope.showEnd = true;
	    }
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list_for_acc").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
	
}]);

