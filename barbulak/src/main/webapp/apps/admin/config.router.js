'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/admin/product/list');
          $stateProvider
              .state('admin', {
                  abstract: true,
                  url: '/admin',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //product
              .state('admin.product', {
                  abstract: true,
                  url: '/product',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/product/controller.js']);
                      }]
                    }
              })
              .state('admin.product.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/product/list.html',
              })
              .state('admin.product.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/product/add.html',
              })
              .state('admin.product.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/product/edit.html',

              })
              .state('admin.product.ingredient', {
                  url: '/ingredient/:id',
                  templateUrl: '../../apps/admin/product/ingredient.html',

              })
              .state('admin.product.bottleingredient', {
                  url: '/bottleingredient/:id',
                  templateUrl: '../../apps/admin/product/bottleingredient.html',

              })
              .state('admin.product.cat_list', {
                  url: '/cat_list',
                  templateUrl: '../../apps/admin/product/cat_list.html',
              })
              .state('admin.product.cat_add', {
                  url: '/cat_add',
                  templateUrl: '../../apps/admin/product/cat_add.html',
              })
              .state('admin.product.cat_edit', {
                  url: '/cat_edit/:id',
                  templateUrl: '../../apps/admin/product/cat_edit.html',

              })
              
              .state('admin.product.type_list', {
                  url: '/type_list',
                  templateUrl: '../../apps/admin/product/type_list.html',
              })
              .state('admin.product.type_add', {
                  url: '/type_add',
                  templateUrl: '../../apps/admin/product/type_add.html',
              })
              .state('admin.product.type_edit', {
                  url: '/type_edit/:id',
                  templateUrl: '../../apps/admin/product/type_edit.html',

              })
              
              
              
              //product_type
              .state('admin.product_type', {
                  abstract: true,
                  url: '/product_type',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/product_type/controller.js']);
                      }]
                    }
              })
              .state('admin.product_type.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/product_type/list.html',
              })
              .state('admin.product_type.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/product_type/add.html',
              })
               .state('admin.product_type.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/product_type/edit.html',

              })
              
               //raw
              .state('admin.raw_material', {
                  abstract: true,
                  url: '/raw_material',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/raw_material/controller.js']);
                      }]
                    }
              })
              .state('admin.raw_material.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/raw_material/list.html',
              })
              .state('admin.raw_material.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/raw_material/add.html',
              })
              .state('admin.raw_material.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/raw_material/edit.html',

              })
              .state('admin.raw_material.plist', {
                  url: '/plist',
                  templateUrl: '../../apps/admin/raw_material/plist.html',
              })
              .state('admin.raw_material.rlist', {
                  url: '/rlist',
                  templateUrl: '../../apps/admin/raw_material/raw_material_list.html',
              })
              
              
              //bottle_purchase
              .state('admin.bottle_purchase', {
                  abstract: true,
                  url: '/bottle_purchase',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/bottle_purchase/controller.js']);
                      }]
                    }
              })
              .state('admin.bottle_purchase.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/bottle_purchase/list.html',
              })
              .state('admin.bottle_purchase.prod_request', {
                  url: '/prod_request',
                  templateUrl: '../../apps/admin/bottle_purchase/production_req.html',
              })
              
              //product_category
              .state('admin.product_category', {
                  abstract: true,
                  url: '/product_category',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/product_category/controller.js']);
                      }]
                    }
              })
              .state('admin.product_category.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/product_category/list.html',
              })
              .state('admin.product_category.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/product_category/add.html',

              })
              .state('admin.product_category.edit', {
                  url: '/edit/:id',
//                  params: { id: {} },
                  templateUrl: '../../apps/admin/product_category/edit.html',

              })
              
              //product_place
              .state('admin.product_place', {
                  abstract: true,
                  url: '/product_place',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/product_place/controller.js']);
                      }]
                    }
              })
              .state('admin.product_place.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/product_place/list.html',
              })
              .state('admin.product_place.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/product_place/add.html',

              })
              .state('admin.product_place.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/product_place/edit.html',

              })
               //staff
              .state('admin.staff', {
                  abstract: true,
                  url: '/staff',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/staff/controller.js']);
                      }]
                    }
              })
              .state('admin.staff.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/staff/list.html',
              })
              .state('admin.staff.managerlist', {
                  url: '/managerlist',
                  templateUrl: '../../apps/admin/staff/managerlist.html',
              })
              .state('admin.staff.users_list', {
                  url: '/users_list',
                  templateUrl: '../../apps/admin/staff/users_list.html',
              })
              .state('admin.staff.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/staff/add.html',

              })
              .state('admin.staff.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/staff/edit.html',

              })
              .state('admin.staff.users_edit', {
                  url: '/users_edit/:id',
                  templateUrl: '../../apps/admin/staff/users_edit.html',

              })
              .state('admin.staff.manageradd', {
                  url: '/manageradd',
                  templateUrl: '../../apps/admin/staff/manageradd.html',

              })
              .state('admin.staff.manageredit', {
                  url: '/manageredit/:id',
                  templateUrl: '../../apps/admin/staff/manageredit.html',

              })
              
               //partner
              .state('admin.partner', {
                  abstract: true,
                  url: '/partner',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/partner/controller.js']);
                      }]
                    }
              })
              .state('admin.partner.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/partner/list.html',
              })
              
              .state('admin.partner.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/partner/add.html',

              })
              .state('admin.partner.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/partner/edit.html',

              })
              
              
              //capacity
              .state('admin.capacity', {
                  abstract: true,
                  url: '/capacity',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/capacity/controller.js']);
                      }]
                    }
              })
              .state('admin.capacity.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/capacity/list.html',
              })
              .state('admin.capacity.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/capacity/add.html',
              })
              .state('admin.capacity.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/capacity/edit.html',

              })
              
              //settings
              .state('admin.settings', {
                  abstract: true,
                  url: '/settings',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/settings/controller.js']);
                      }]
                    }
              })
              .state('admin.settings.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/settings/list.html',
              })
              .state('admin.settings.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/settings/add.html',

              })
              .state('admin.settings.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/settings/edit.html',
              })
              .state('admin.settings.acclist', {
                  url: '/acclist',
                  templateUrl: '../../apps/admin/settings/cashboxacc_list.html',
              })
              .state('admin.settings.accadd', {
                  url: '/accadd',
                  templateUrl: '../../apps/admin/settings/cashboxacc_add.html',
              })
              .state('admin.settings.accedit', {
                  url: '/accedit/:id',
                  templateUrl: '../../apps/admin/settings/cashboxacc_edit.html',
              })
              
              //shipping_cancel_request
              .state('admin.shipping_cancel', {
                  abstract: true,
                  url: '/shipping_cancel',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/shipping_cancel/controller.js']);
                      }]
                    }
              })
              .state('admin.shipping_cancel.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/shipping_cancel/list.html',
              })
            //inventory
              .state('admin.inventory', {
                  abstract: true,
                  url: '/inventory',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/inventory/controller.js']);
                      }]
                    }
              })
              .state('admin.inventory.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/inventory/list.html',
              })
              .state('admin.inventory.type_list', {
                  url: '/type_list',
                  templateUrl: '../../apps/admin/inventory/type_list.html',
              })
              .state('admin.inventory.listS', {
                  url: '/listS',
                  templateUrl: '../../apps/admin/inventory/listS.html',
              })
              .state('admin.inventory.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/inventory/add.html',
              })
              .state('admin.inventory.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/inventory/edit.html',
              })
              
              
            //transaction
              .state('admin.transaction', {
                  abstract: true,
                  url: '/transaction',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/transaction/controller.js']);
                      }]
                    }
              })
              .state('admin.transaction.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/transaction/list.html',
              })
              .state('admin.transaction.add', {
                  url: '/add',
                  templateUrl: '../../apps/admin/transaction/add.html',
              })
              .state('admin.transaction.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/admin/transaction/edit.html',
              })
              .state('admin.transaction.listp', {
                  url: '/listp',
                  templateUrl: '../../apps/admin/transaction/listP.html',
              })
              .state('admin.transaction.addp', {
                  url: '/addp',
                  templateUrl: '../../apps/admin/transaction/addP.html',
              })
              .state('admin.transaction.editp', {
                  url: '/editp/:id',
                  templateUrl: '../../apps/admin/transaction/editP.html',
              })
              .state('admin.transaction.del_list', {
                  url: '/del_list',
                  templateUrl: '../../apps/admin/transaction/del_list.html',
              })
              .state('admin.transaction.del_listp', {
                  url: '/del_listp',
                  templateUrl: '../../apps/admin/transaction/del_listp.html',
              })
              
              //production request
              .state('admin.production_request', {
                  abstract: true,
                  url: '/production_request',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/production_request/controller.js']);
                      }]
                    }
              })
              .state('admin.production_request.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/production_request/list.html',
              })
              .state('admin.production_request.distribution', {
                  url: '/distribution',
                  templateUrl: '../../apps/admin/production_request/distribution.html',
              })
              
              //production request
              .state('admin.distribution', {
                  abstract: true,
                  url: '/distribution',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/distribution/controller.js']);
                      }]
                    }
              })
              .state('admin.distribution.distribution', {
                  url: '/distribution',
                  templateUrl: '../../apps/admin/distribution/distribution.html',
              })
              
              //transportation
              .state('admin.transportation', {
                  abstract: true,
                  url: '/transportation',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/transportation/controller.js']);
                      }]
                    }
              })
              .state('admin.transportation.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/transportation/list.html',
              })
             
              //transaction_delete
              .state('admin.transaction_delete', {
                  abstract: true,
                  url: '/transaction_delete',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/admin/transaction_delete/controller.js']);
                      }]
                    }
              })
              .state('admin.transaction_delete.list', {
                  url: '/list',
                  templateUrl: '../../apps/admin/transaction_delete/list.html',
              })
              .state('admin.transaction_delete.listp', {
                  url: '/listp',
                  templateUrl: '../../apps/admin/transaction_delete/listp.html',
              })
              
              .state('profile', {
                  url: '/profile',
                  templateUrl: '../../assets/tpl/blocks/page_profile.html',
              })
              
      }
    ]
  );
