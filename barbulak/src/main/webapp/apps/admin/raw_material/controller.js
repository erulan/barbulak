'use strict';

/* Controllers */
  // signin controller


app.controller('RawListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, restService, ngTableParams, Utils, $state) {

	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "raw");
        },
    });
	$scope.deleteRaw = function(id){
		Utils.callDeleteModal("raw", id);
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  }]);
  
app.controller('RawAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
	
	$scope.raw = {};
	$scope.raw.unit = {};
	
	restService.all("unit").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.units = data.originalElement.resultList;
	});
    $scope.create = function(){
    	restService.one("raw").customPOST($scope.raw, "create").then(function(data){
    		alert("Успешно создано!");
    		$location.path("admin/raw_material/list");
    	});
    }
  }])
  
  app.controller('RawEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
   
	$scope.raw = {};
    $scope.rawId = $stateParams.id;
    
    restService.all("raw").customGET( $scope.rawId).then(function(data){
		$scope.raw = data.originalElement;
	});
    
    restService.all("unit").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.units = data.originalElement.resultList;
	});
    
	
    $scope.update = function(){
    	restService.one("raw").customPOST($scope.raw, "update/" + $scope.rawId).then(function(data){
    		alert("Успешно изменено!");
    		$location.path("admin/raw_material/list");
    	});
    }
  }])
 app.controller('RawMaterialListCtrl', ['$scope','restService','ngTableParams','Utils',  function($scope, restService, ngTableParams, Utils) {
    
	$scope.placeByFromPlaceId = {};
	
    $scope.getProductsList = function($defer, params, path) {

		var searchParams = {};
		var prm = params.parameters();
	
		searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
		searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
		
		//searchParams["searchParameters"] = {"place.id" : $scope.placeByFromPlaceId};

		searchParams["searchParamWithTypes"] = [{searchField:"amount", searchValue: 0, searchType: "NOT_EQUAL"}];
		
		angular.forEach(prm.filter, function(value, key){
			searchParams.searchParameters[key] = value;
		});
		searchParams["orderParamDesc"] = {};
		angular.forEach(prm.sorting, function(value, key){
			searchParams.orderParamDesc[key] = (value == "desc");
		});
		 	
		restService.all(path).customPOST(searchParams, ['list']).then(function(data){
			params.total(data.originalElement.totalRecords);
	        // set new data
	        $defer.resolve(data.originalElement.resultList);
		});
	}

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
            	restService.one("security").customGET('current_user').then(function(data){
            		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id,"placeType.name": "производство"}}, "list").then(function(data){
    	    			$scope.placeByFromPlace = data.originalElement.resultList;
    	    			angular.forEach(data.originalElement.resultList, function(place, key){
    	    				$scope.placeByFromPlaceId = place.id;
    	    			});

    	            	$scope.getProductsList($defer, params, "raw_place");
            		});
        		});
        },
    });

	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
  }]);
  app.controller('PurchaseListCtrl', ['$scope',  '$window', '$location', 'restService', 'ngTableParams', 'Utils', '$timeout', '$state', function($scope, $window, $location, restService, ngTableParams, Utils, $timeout, $state) {
	
	$scope.purchase = {};
	$scope.statusId = 4;
	$scope.approved = false;
	$scope.placeByFromPlaceId = {};
	
	/*restService.all("purchase").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"status.id": $scope.statusId, "approvedByAdmin":$scope.approved}}, "list").then(function(data){
		$scope.purchases = data.originalElement.resultList;
	});
*/
	/*restService.all("purchase").customPOST({startIndex: 0, orderParamDesc:{"reqDate":true}, resultQuantity: 1000}, "list").then(function(data){
		$scope.purchases = data.originalElement.resultList;
	});*/
	
/*scope.$watch('var',function(scope) {*/
	
	$scope.getpurchase ={};
	$scope.purchaseRaw ={};
	$scope.purchaseDetail = {};
	$scope.purchaseInfo = {};
	$scope.purchaseCancelInfo = {};	
	$scope.purchaseCancelInfo.purchase = {};
	$scope.purchaseCancelInfo.staff = {};
	$scope.placeByFromPlace = {};
	$scope.name = "";
	
	$scope.getShippingList = function($defer, params, path) {
					var searchParams = {};
					var prm = params.parameters();
				
					searchParams["startIndex"] = (angular.isNumber(prm.page) && angular.isNumber(prm.count)) ? (prm.page-1)*prm.count : 1 ;
					searchParams["resultQuantity"] = angular.isNumber(prm.count) ? prm.count : 10 ;
					
					//searchParams["orderParamDesc"] = {"approveDate" : false};
					angular.forEach(prm.filter, function(value, key){
						searchParams.searchParameters[key] = value;
					});
					searchParams["orderParamDesc"] = {"statusDate": true };
					angular.forEach(prm.sorting, function(value, key){
						searchParams.orderParamDesc[key] = (value == "desc");
					});
					restService.all(path).customPOST(searchParams, ['list']).then(function(data){
						params.total(data.originalElement.totalRecords);
				        // set new data
				        $defer.resolve(data.originalElement.resultList);
					});
	}
	
			$scope.tableParams = new ngTableParams({
		        page: 1,            // show first page
		        count: 10, 			// count per page
		        sorting: {
		            name: 'asc'     // initial sorting
		        },		
		    }, {
		        total: 0, // length of data
		        getData: function($defer, params){
		        	
		        	restService.one("security").customGET('current_user').then(function(data){
			        	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": data.originalElement.id, "placeType.name": "производство"}}, "list").then(function(data){
			    			$scope.placeByFromPlace = data.originalElement.resultList;
			    			angular.forEach(data.originalElement.resultList, function(place, key){
			    				$scope.placeByFromPlaceId = place.id;
			    			});
			    			
			            	$scope.getShippingList($defer, params, "purchase");
			    		});
		        		
		        	});
		        },
		    });
	
	$scope.approvePurchase = function(purchaseId){
    	restService.one("purchase").customGET("approve_by_admin/" + purchaseId).then(function(data){
    		$timeout(function () {
    			alert("Успешно потвержден!");
    		 }); 
    		$state.reload();
    	});
    }
	$scope.rejectPurchase = function(purchaseId){
    	restService.one("purchase").customGET("reject_by_admin/" + purchaseId).then(function(data){
    		$timeout(function () {
    			alert("Успешно отменен!");
    		 }); 
    		$state.reload();
    	});
    }
	
	$scope.showPurchaseDetails = function(purchaseId){
		restService.all("raw_purchase").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"purchase.id": purchaseId}}, "list").then(function(data){
			$scope.purchaseRaws = data.originalElement.resultList;
		});
		restService.one("purchase").customGET(purchaseId).then(function(data){
			$scope.purchaseInfo = data.originalElement;
			if($scope.purchaseInfo.status.name == "отправлено"){
				$('#button_approve').show();
			}else if($scope.purchaseInfo.status.name == "запрос сырья"){
				if($scope.purchaseInfo.approvedByAdmin == true){
					$('#button_approve').hide();
					$('#button_cancel').hide();
				}else{
					$('#button_cancel').show();
				}
			}
			
			
		});
		$('.toggle_2') 	.show(1000);
	}
/*	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };*/
	$scope.exportData = function(){
		   var table = document.getElementById('print').innerHTML;
		   var myWindow = $window.open('', '', 'width=800, height=600');
		   myWindow.document.write(table);
		   myWindow.print();
		};
  }]);
app.controller('EditRawRequestCtrl', ['$scope', 'restService','ngTableParams', 'Utils','$state','$stateParams','$location', function($scope, restService, ngTableParams, Utils,$state,$stateParams,$location){
	
	$scope.sendRaws = {};
	$scope.sendRawsArray = [];
	$scope.purchase = {};
	$scope.place = {};
	$scope.status = {};
	
	$('.tableSubmit').show(1000);
	$scope.purchaseId = $stateParams.id;
	restService.all("raw_purchase").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"purchase.id": $scope.purchaseId}}, "list").then(function(data){
		angular.forEach(data.originalElement.resultList, function(raw, key){
			restService.all("raw").customGET(raw.raw.id).then(function(data){
				console.log();
				$scope.sendRaws[raw.raw.id] = data.originalElement;
				$scope.sendRaws[raw.raw.id].amount = raw.amount;

			});
		});
	});
	restService.all("purchase").customGET($scope.purchaseId).then(function(data){
		$scope.description= data.originalElement.description;
	});
	restService.one("security").customGET('current_user').then(function(data){
		$scope.currentUser  = data.originalElement;
		restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"staff.id": $scope.currentUser.id, "placeType.name": "производство"}}, "list").then(function(data){
			$scope.placeByFromPlace = data.originalElement.resultList;
			angular.forEach(data.originalElement.resultList, function(place, key){
				$scope.place.id = place.id;
			});
		});
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "raw");
        },
    });
	
	restService.all("raw").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.raws = data.originalElement.resultList;
	});
	
	$scope.toggleTable = function(n){
		$('.toggle_'+n).toggle(1000);
	}
	
	$scope.date = new Date();
	
	$scope.addToSendList = function(rawId,amount){ // Add selected product to permanent list.
		if(amount!=null && amount>0){
			console.log($scope.sendRaws);
			console.log($scope.sendRaws[rawId]);
			if($scope.sendRaws[rawId]==null){
				$('.button_'+rawId).hide(1000);
				$('.button_'+rawId).show(1000);				
				
				angular.forEach($scope.raws, function(raw, key){
					if(raw.id == rawId){
						$scope.sendRaws[rawId] = raw;
					}
				});
				$scope.sendRaws[rawId].amount = amount; 
				$('.tableSubmit').show(1000);
			}else{
				$scope.sendRaws[rawId].amount = amount;
				$('.button_'+rawId).hide(1000);
				$('.button_'+rawId).show(1000);	
			}
		}else{
			alert("Пожалуйста укажите верное количество!");
		}
	}
	
	$scope.cancelRaw = function(rawId){
		console.log($scope.sendRaws);
		console.log($scope.sendRaws[rawId]);
		console.log(rawId);
		delete $scope.sendRaws[rawId];
		if(Object.keys($scope.sendRaws).length==0){
			$('.tableSubmit').hide(1000);
		}
	}
	
	$scope.saveSendList = function(description){
			if(Object.keys($scope.sendRaws).length!=0){
				angular.forEach($scope.sendRaws, function(raw, key){
					$scope.sendRawsArray.push({"amount": raw.amount, "raw": {"id" : raw.id}});
				});
				
				restService.all("shipping_status").customPOST({startIndex: 0, resultQuantity: 1000,"searchParameters":{"id": 4}}, "list").then(function(data){
					$scope.placeByFromPlace = data.originalElement.resultList;
					angular.forEach(data.originalElement.resultList, function(status, key){
						$scope.status.id = status.id;
						console.log(description);
						$scope.purchase = {"id":$scope.purchaseId, "reqDate": new Date(), "toPlaceId": $scope.place, "rawPurchases": $scope.sendRawsArray, "status" : $scope.status,"description":description, "approvedByAdmin":false}
						console.log($scope.purchase);
						console.log($scope.purchaseId);
						restService.one("purchase").customPOST($scope.purchase, "update/"+$scope.purchaseId).then(function(data){
							$('.tableSubmit').hide(1000);
							alert("Запрос Сырья успешно изменен!");
				    		$location.path("admin/purchase_request/list");
							$scope.tableParams.reload();
							$scope.sendRaws = {};
							$scope.shippingByRaws = [];
							$scope.description = null;
						});
					});
				});
				
			}else{
				alert("Пожалуйста сначала выберите cырье для запроса!!");
			}
		}
}]);