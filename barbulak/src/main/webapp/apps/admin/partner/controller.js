'use strict';

/* Controllers */
  // signin controller


app.controller('PartnerCtrl', ['$scope',  function($scope) {
    $scope.deneme = "test kylyp jatabyz";
  }]);

app.controller('PartnerListCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, $location, restService, ngTableParams, Utils, $state) {
	

	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "partner");
        },
    });
	$scope.deletePartner = function(id){
		Utils.callDeleteModal("partner", id);
		$location.path("admin/partner/list");
	};
	
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
   
  }]);
   
app.controller('PartnerAddCtrl', ['$scope', '$location', 'restService', 'ngTableParams', 'Utils',  function($scope, $location, restService, ngTableParams, Utils) {
    //$scope.name = "ADDCONTROLLER";
    $scope.partner = {};
    $scope.partner.cashbox = {};
    $scope.partner.personNumber = "0000";
    
    restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});
    
    $scope.create = function(){
    	restService.one("partner").customPOST($scope.partner, "create").then(function(data){
    		alert("success");
    		$location.path("admin/partner/list"); 
    	});
    };
    
  }])
 
  app.controller('PartnerEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
    //$scope.name = "ADDCONTROLLER";
    $scope.partner = {};
    $scope.partner.cashbox = {};
    $scope.partnerId = $stateParams.id;
    
    restService.all("partner").customGET( $scope.partnerId).then(function(data){
		$scope.partner= data.originalElement;
	});
    
    restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});
    
    
    

    $scope.update = function(){
    	restService.one("partner").customPOST($scope.partner, "update/" + $scope.partnerId).then(function(data){
    		alert("Успешно изменена!");
    		$location.path("admin/partner/list");
    	});
    }
    
  }])
  


