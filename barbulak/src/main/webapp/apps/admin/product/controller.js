'use strict';

/* Controllers */
  // signin controller


app.controller('ProductListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', '$state', function($scope, restService, ngTableParams, Utils, $modal, $state) {
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "product");
        },
    });
	$scope.deleteProduct = function(id){
		Utils.callDeleteModal("product", id);
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
    $scope.init = function() {
        $scope.showName = true;
        $scope.showPrice = true;
        $scope.showType = true;
        $scope.showEd = true;
        $scope.showOption = true;
    }
    //CASHBOX
    restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxs = data.originalElement.resultList;
	});
  }]);
  
app.controller('ProductAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
    $scope.name = "ADDCONTROLLER";
    $scope.product = {};
    $scope.product.productType = {};
    $scope.product.capacity = {};
    
    restService.all("product_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.types = data.originalElement.resultList;
	});
    
    restService.all("capacity").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.capacities = data.originalElement.resultList;
	});
    
	
    $scope.create = function(){
    	restService.one("product").customPOST($scope.product, "create").then(function(data){
    		alert("Успешно создан!");
    		$location.path("/product/list");
    	});
    }
  }])
    
app.controller('ProductEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
    $scope.name = "ADDCONTROLLER";
    $scope.product = {};
    $scope.product.productType = {};
    $scope.product.capacity = {};
    $scope.productId = $stateParams.id;
    
    restService.all("product").customGET( $scope.productId).then(function(data){
		$scope.product= data.originalElement;
	});
    
    restService.all("product_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.types = data.originalElement.resultList;
	});
    
    restService.all("capacity").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.capacities = data.originalElement.resultList;
	});
    
    $scope.update = function(){
    	restService.one("product").customPOST($scope.product, "update/" + $scope.productId).then(function(data){
    		alert("Успешно обновлена!");
    		$location.path("/product/list");
    	});
    }
  }])
  app.controller('IngredientCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', '$stateParams', '$state', function($scope, restService, ngTableParams, Utils, $modal, $stateParams, $state) {
	
	$scope.ingredient = {};
	$scope.productId = $stateParams.id;
	$scope.ingredients = [];
    
    restService.all("product").customGET( $scope.productId).then(function(data){
		$scope.product= data.originalElement;
	});
	
	restService.all("ingredient").customPOST({startIndex: 0, resultQuantity: 1000, searchParameters: {"product.id": $scope.productId}}, "list").then(function(data){
		$scope.ingredients = data.originalElement.resultList;
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "raw");
        },
    });
	$scope.add = function(raw, amount){
		if(amount == null){
			alert("Введите объем");
			return;
		}
		for (var i = 0; i <= $scope.ingredients.length-1; i++) {
		    if($scope.ingredients[i].raw.id == raw.id){
		    	alert("Этот ингредиент уже имеется в составе!");
		    	return;
		    }
		}
		var ingredient = {'raw': raw, 'amount': amount};
    	$scope.ingredients.push(ingredient);
    };
    
    $scope.remove = function(id){
		for (var i = 0; i <= $scope.ingredients.length-1; i++) {
		    if($scope.ingredients[i].id == id){
		    	$scope.ingredients.splice(i, 1);
		    	alert("Удален из состава!");
		    	return ;
		    }
		}
    };
	
	$scope.save = function(){
		var ingredientRaw = {"product": {}, "rawIdAmount": {}};
		ingredientRaw.product.id = $scope.productId;
		
		for (var i = 0; i <= $scope.ingredients.length-1; i++) {
			ingredientRaw.rawIdAmount[$scope.ingredients[i].raw.id] = $scope.ingredients[i].amount;
		}
		
    	restService.one("ingredient").customPOST(ingredientRaw, "create_list").then(function(data){
    		alert("Успешно сохранен!");
    	});
    }
	
	$scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };
	
  }])
  app.controller('BottleIngredientCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', '$stateParams', '$state', function($scope, restService, ngTableParams, Utils, $modal, $stateParams, $state) {
	
	  	$scope.ingredient = {};
		//$scope.productId = $stateParams.id;
		$scope.ingredients = [];
		 
	    
	    /*restService.all("product").customGET( $scope.productId).then(function(data){
			$scope.product= data.originalElement;
		});*/
		
		restService.all("bottle_raw").customPOST({startIndex: 0, resultQuantity: 1000,/* searchParameters: {"product.id": $scope.productId}*/}, "list").then(function(data){
			$scope.ingredients = data.originalElement.resultList;
		});
		
		$scope.tableParams = new ngTableParams({
	        page: 1,            // show first page
	        count: 10, 			// count per page
	        sorting: {
	            name: 'asc'     // initial sorting
	        },				
	    }, {
	        total: 0, // length of data
	        getData: function($defer, params){
	        	Utils.ngTableGetData($defer, params, "raw");
	        },
	    });
		$scope.add = function(raw, amount){
			if(amount == null){
				alert("Введите объем");
				return;
			}
			for (var i = 0; i <= $scope.ingredients.length-1; i++) {
			    if($scope.ingredients[i].raw.id == raw.id){
			    	alert("Этот ингредиент уже имеется в составе!");
			    	return;
			    }
			}
			var ingredient = {'raw': raw, 'amount': amount};
	    	$scope.ingredients.push(ingredient);
	    };
	    
	    $scope.remove = function(id){
			for (var i = 0; i <= $scope.ingredients.length-1; i++) {
			    if($scope.ingredients[i].id == id){
			    	$scope.ingredients.splice(i, 1);
			    	alert("Удален из состава!");
			    	return ;
			    }
			}
	    };
		
		$scope.save = function(){
			
			var ingredientRawList = [];
			for (var i = 0; i <= $scope.ingredients.length-1; i++) {
				ingredientRawList.push({raw : {id : $scope.ingredients[i].raw.id}, amount : $scope.ingredients[i].amount});
			}
			
	    	restService.one("bottle_raw").customPOST(ingredientRawList, "create_list").then(function(data){
	    		alert("Успешно сохранен!");
	    		$state.reload();
	    	});
	    }
		
		$scope.exportData = function () {
	        var blob = new Blob([document.getElementById('exportable').innerHTML], {
	            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	        });
	        saveAs(blob, "Report.xls");
	    };
	
  }])
app.controller('ProductCategoryListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state',  function($scope, restService, ngTableParams, Utils, $state) {
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "product_category");
        },
    });
	$scope.deleteCategory = function(id){
		Utils.callDeleteModal("product_category", id);
	};
	
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  }]);
  
app.controller('ProductCategoryAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
	
	$scope.productCategory = {};
	
    $scope.create = function(){
    	restService.one("product_category").customPOST($scope.productCategory, "create").then(function(data){
    		alert("Успешно создана!");
    		$location.path("admin/product/cat_list");
    	});
    }

  }])
  
  app.controller('ProductCategoryEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
	
	$scope.product_category = {};
//	$scope.categoryId = $routeParams.id;
	
	$scope.denemeId = $stateParams.id;
	
	restService.all("product_category").customGET( $scope.denemeId).then(function(data){
		$scope.product_category = data.originalElement;
	});
	
    $scope.update = function(){
    	restService.one("product_category").customPOST($scope.product_category, "update/" + $scope.denemeId).then(function(data){
    		alert("Успешно изменена!");
    		$location.path("admin/product/cat_list");
    	});
    }
  }])
  
  app.controller('ProductTypeListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, restService, ngTableParams, Utils, $state) {
	
	restService.all("product_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.types = data.originalElement.resultList;
	});

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "product_type");
        },
    });
	$scope.deleteType = function(id){
		Utils.callDeleteModal("product_type", id);
	};
	
	$scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };
  }]);
  
app.controller('ProductTypeAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
	
	$scope.productType = {};
	$scope.productType.productCategory = {};
	
	restService.all("product_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.categories = data.originalElement.resultList;
	});
    $scope.create = function(){
    	restService.one("product_type").customPOST($scope.productType, "create").then(function(data){
    		alert("Успешно создан!");
    		$location.path("admin/product/type_list");
    	});
    }
  }])
    app.controller('ProductTypeEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
	
	$scope.product_type = {};
	
	$scope.typeId = $stateParams.id;
	
	restService.all("product_type").customGET( $scope.typeId).then(function(data){
		$scope.product_type = data.originalElement;
	});

	restService.all("product_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.categories = data.originalElement.resultList;
	});
    $scope.update = function(){
    	restService.one("product_type").customPOST($scope.product_type, "update/" + $scope.typeId).then(function(data){
    		alert("Успешно изменен!");
    		$location.path("admin/product/type_list");
    	});
    }
  }]);
