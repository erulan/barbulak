'use strict';

/* Controllers */
  // signin controller

app.controller('TransacrionListCtrl',['$scope', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, restService, ngTableParams, Utils, $state) {
	$scope.transaction = {};
	$scope.transaction.person = {};
	
	$scope.datepicker = {};
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "transaction");
        },
    });
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
		$scope.exportData = function () {
			var table= document.getElementById("exportable");
			var html = table.outerHTML;
			window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
	    };
	    $scope.init = function() {
	        $scope.showName = true;
	        $scope.showTotal = true;
	        $scope.showDate = true;
	        $scope.showAmount = true;
	        $scope.showCategory = true;
	        $scope.showTransfer = true;
	        $scope.showDescription = true;
	        $scope.showOption = true;
	    }
}]);

app.controller('TransactionAddCtrl', ['$scope', 'restService','$location',function($scope, restService, $location){
	
	$scope.transaction = {};
	$scope.transaction.person = {};
//	$scope.transaction.person.id;
	$scope.transaction.cashbox = {};
//	$scope.transaction.case1.id;
	$scope.transaction.transactionCategory= {};
//	$scope.transaction.transactionCategory.id;
	
	$scope.datepicker = {};
	
	restService.all("transaction").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.transactions = data.originalElement.resultList;
	});
	restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.transaction_categorys = data.originalElement.resultList;
	});
	restService.all("person").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.persons = data.originalElement.resultList;
	});
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});

	
	$scope.create = function(){
    	restService.one("transaction").customPOST($scope.transaction, "create").then(function(data){
			alert("Успешно была добавлена транзакция!");
			$location.path("admin/transaction/list");
    	});
    };
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}]);
app.controller('TransactionEditCtrl', ['$scope', 'restService','$location', '$stateParams', function($scope, restService, $location, $stateParams){
	
	$scope.transaction = {};
	$scope.transaction.person = {};
//	$scope.transaction.person.id;
	$scope.transaction.cashbox = {};
//	$scope.transaction.case1.id;
	$scope.transaction.transactionCategory= {};
//	$scope.transaction.transactionCategory.id;
	$scope.transId = $stateParams.id;
	$scope.datepicker = {};
	
	restService.all("transaction").customGET( $scope.transId).then(function(data){
		$scope.transaction = data;
	});
	restService.all("transaction_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.transaction_categorys = data.originalElement.resultList;
	});
	restService.all("person").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.persons = data.originalElement.resultList;
	});
	restService.all("cashbox").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.cashboxes = data.originalElement.resultList;
	});

	
	$scope.update = function(){
    	restService.one("transaction").customPOST($scope.transaction, "update/" + $scope.transId).then(function(data){
			alert("Успешно была добавлена транзакция!");
			$location.path("admin/transaction/list");
    	});
    };
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}]);


app.controller('PersonalTransacrionListCtrl',['$scope', 'restService', 'ngTableParams', 'Utils', '$state', 
                                              function($scope, restService, ngTableParams, Utils, $state) {
	$scope.personal_transaction = {};
	$scope.personal_transaction.personalBalance= {};
	$scope.personal_transaction.personalBalance.person= {};
	
	$scope.datepicker = {};
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "personal_transaction");
        },
    });
	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
	  
		$scope.exportData = function () {
			var table= document.getElementById("exportable");
			var html = table.outerHTML;
			window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
	    };
	
}]);

app.controller('PersonalTransactionAddCtrl', ['$scope', 'restService','$location',function($scope, restService,$location){
	
	$scope.personal_transaction = {};
	$scope.personal_transaction.personalBalance = {};
/*	$scope.personal_transaction.personal_balance.id;*/
	$scope.personal_transaction.personalBalance.person = {};
	$scope.personal_transaction.personalBalance.person.id;
	
	$scope.datepicker = {};
	
	restService.all("personal_transaction").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.personal_transactions = data.originalElement.resultList;
	});
	restService.all("person").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.persons = data.originalElement.resultList;
	});
	restService.all("personal_balance").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.personal_balances = data.originalElement.resultList;
	});

	
	$scope.create = function(){
    	restService.one("personal_transaction").customPOST($scope.personal_transaction, "create").then(function(data){
			alert("Успешно была добавлена персональная транзакция!");
			$location.path("admin/transaction/listP");
    	});
    };
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])
app.controller('PersonalTransactionEditCtrl', ['$scope', 'restService','$location',function($scope, restService,$location){
	
	$scope.personal_transaction = {};
	$scope.personal_transaction.personalBalance = {};
	$scope.personal_transaction.personalBalance.person = {};
	$scope.personal_transaction.personalBalance.person.id;
	$scope.transId = $stateParams.id;
	
	$scope.datepicker = {};
	
	restService.all("personal_transaction").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.personal_transactions = data.originalElement.resultList;
	});
	restService.all("person").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.persons = data.originalElement.resultList;
	});
	restService.all("personal_balance").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.personal_balances = data.originalElement.resultList;
	});

	
	$scope.update = function(){
    	restService.one("personal_transaction").customPOST($scope.personal_transaction, "update/" + $scope.transId).then(function(data){
			alert("Успешно была изменена персональная транзакция!");
			$location.path("admin/transaction/listP");
    	});
    };
    
    $scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	  
	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };
}])

app.controller('DelTransListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$location', '$state', '$timeout', function($scope, restService, ngTableParams, Utils, $location, $state, $timeout) {
		
	$scope.transaction = {};
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "transaction");
        },
    });
	$scope.deleteTrans = function(id){
    	restService.one("transaction").customGET("delete/" + id).then(function(data){
    		$timeout(function () {
    			alert("Успешно удалена!");
    		 }); 
    		$state.reload();
    	});
    }
  }])
  
  app.controller('DelPTransListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', '$timeout', function($scope, restService, ngTableParams, Utils, $state, $timeout) {
		
	$scope.ptransaction = {};
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "personal_transaction");
        },
    });
	$scope.deleteTrans = function(id){
    	restService.one("personal_transaction").customGET("delete/" + id).then(function(data){
    		$timeout(function () {
    			alert("Успешно удалена!");
    		 }); 
    		$state.reload();
    	});
    }
  }]);


	
