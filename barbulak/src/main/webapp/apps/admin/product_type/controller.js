'use strict';

/* Controllers */
  // signin controller


app.controller('ProductTypeListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils', '$state', function($scope, restService, ngTableParams, Utils, $state) {
	
	restService.all("product_type").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.types = data.originalElement.resultList;
	});

	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "product_type");
        },
    });
	$scope.deleteType = function(id){
		Utils.callDeleteModal("product_type", id);
	};
	
	$scope.exportData = function () {
        var blob = new Blob([document.getElementById('exportable').innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, "Report.xls");
    };
  }]);
  
app.controller('ProductTypeAddCtrl', ['$scope', '$location', 'restService', function($scope, $location, restService) {
	
	$scope.productType = {};
	$scope.productType.productCategory = {};
	
	restService.all("product_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.categories = data.originalElement.resultList;
	});
    $scope.create = function(){
    	restService.one("product_type").customPOST($scope.productType, "create").then(function(data){
    		alert("Успешно создан!");
    		$location.path("admin/product_type/list");
    	});
    }
  }])
    app.controller('ProductTypeEditCtrl', ['$scope', '$location', '$stateParams', 'restService', function($scope, $location, $stateParams, restService) {
	
	$scope.product_type = {};
	
	$scope.typeId = $stateParams.id;
	
	restService.all("product_type").customGET( $scope.typeId).then(function(data){
		$scope.product_type = data.originalElement;
	});

	restService.all("product_category").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.categories = data.originalElement.resultList;
	});
    $scope.update = function(){
    	restService.one("product_type").customPOST($scope.product_type, "update/" + $scope.typeId).then(function(data){
    		alert("Успешно изменен!");
    		$location.path("admin/product_type/list");
    	});
    }
  }]);



