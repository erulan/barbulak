'use strict';

/* Controllers */
  // signin controller


app.controller('CreditListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', function($scope, restService, ngTableParams, Utils, $modal) {
	
	$scope.datepicker = {};
	$scope.datepicker2 = {};

    $scope.getTable = function(){
    	restService.all("transaction").customPOST({"fromDate": $scope.fromDate, "toDate": $scope.toDate}, "debit_credit_balance").then(function(data){
    		$scope.transactions = data;
    		$scope.last_transaction = $scope.transactions[$scope.transactions.length-1];
    	});
    }
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
		$scope.exportData = function () {
			var table= document.getElementById("exportable");
			var html = table.outerHTML;
			window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
	    };
	  
/*	  $scope.datepicker.format = 'dd/MM/yyyy';

	  $scope.datepicker.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };*/

	$scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
	  
	  /*$scope.datepicker2.format = 'dd/MM/yyyy';

	  $scope.datepicker2.dateOptions = {
	    formatYear: 'yy',
	    startingDay: 1,
	    language: 'ru'
	  };*/
  }])
  app.controller('BalanceListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', function($scope, restService, ngTableParams, Utils, $modal) {
	
	$scope.datepicker = {};
	$scope.datepicker2 = {};
	$scope.categories = {};
	$scope.bycategories = [];
	
    $scope.getTable = function(){
    	restService.all("transaction").customPOST({"fromDate": $scope.fromDate, "toDate": $scope.toDate}, "get_balance_by_category").then(function(data){
    		$scope.bycategories = data;
    		$scope.categories = $scope.bycategories[0].debitCreditBalanceList;
    	});
    }
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };

	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	 
	$scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
  }]);

