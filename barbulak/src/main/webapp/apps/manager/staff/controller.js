'use strict';

/* Controllers */
  // signin controller


app.controller('StaffCtrl', ['$scope',  function($scope) {
    $scope.deneme = "test kylyp jatabyz";
  }]);

app.controller('StaffListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  function($scope, restService, ngTableParams, Utils) {
	
	restService.all("staff").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.staffs = data.originalElement.resultList;
	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "staff");
        },
    });
	$scope.deleteStaff = function(id){
		Utils.callDeleteModal("staff", id);
	};
	
	$scope.createUser = function(id){
		restService.one("users").customGET("create_user_by_staffid/" + id).then(function(data){
			alert("Successfully Created User!");
//    		$route.reload();
		});
	};
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
    $scope.init = function() {
        $scope.showName = true;
        $scope.showSurname = true;
        $scope.showPosition = true;
        $scope.showPhone = true;
        $scope.showPassport = true;
        $scope.showSalary = true;
        $scope.showCashbox = true;
        $scope.showOffice = true;
    }
  }]);
app.controller('UsersListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  function($scope, restService, ngTableParams, Utils) {
	
//	restService.all("users").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
//		$scope.users = data.originalElement.resultList;
//	});
	
	$scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10, 			// count per page
        sorting: {
            name: 'asc'     // initial sorting
        },				
    }, {
        total: 0, // length of data
        getData: function($defer, params){
        	Utils.ngTableGetData($defer, params, "users");
        },
    });
	$scope.deleteUsers = function(id){
		Utils.callDeleteModal("users", id);
	};
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  }]);
  




