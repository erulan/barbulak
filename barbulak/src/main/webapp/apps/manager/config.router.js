'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .config(
    [          '$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 
      function ($stateProvider,   $urlRouterProvider, JQ_CONFIG) {
          
          $urlRouterProvider
              .otherwise('/manager/warehouse/list');
          $stateProvider
              .state('manager', {
                  abstract: true,
                  url: '/manager',
                  templateUrl: '../../assets/tpl/app.html'
              })
              
              //warehouse
              .state('manager.warehouse', {
                  abstract: true,
                  url: '/warehouse',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager/warehouse/controller.js']);
                      }]
                    }
              })
              .state('manager.warehouse.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager/warehouse/list.html',
              })

              //staff
              .state('manager.staff', {
                  abstract: true,
                  url: '/manager',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager/staff/controller.js']);
                      }]
                    }
              })
              .state('manager.staff.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager/staff/list.html',
              })
              .state('manager.staff.users_list', {
                  url: '/users_list',
                  templateUrl: '../../apps/manager/staff/users_list.html',
              })
              .state('manager.staff.add', {
                  url: '/add',
                  templateUrl: '../../apps/manager/staff/add.html',

              })
              .state('manager.staff.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/manager/staff/edit.html',

              })
              .state('manager.staff.users_edit', {
                  url: '/users_edit/:id',
                  templateUrl: '../../apps/manager/staff/users_edit.html',

              })
              
              //product_type
              .state('manager.product_type', {
                  abstract: true,
                  url: '/product_type',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager/product_type/controller.js']);
                      }]
                    }
              })
              .state('manager.product_type.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager/product_type/list.html',
              })
              .state('manager.product_type.add', {
                  url: '/add',
                  templateUrl: '../../apps/manager/product_type/add.html',
              })
               .state('manager.product_type.edit', {
                  url: '/edit/:id',
                  templateUrl: '../../apps/manager/product_type/edit.html',

              })
              
              //raw_limit
              .state('manager.raw_limit', {
                  abstract: true,
                  url: '/raw_limit',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager/raw_limit/controller.js']);
                      }]
                    }
              })
              .state('manager.raw_limit.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager/raw_limit/list.html',
              })
              
              //balance
              .state('manager.balance', {
                  abstract: true,
                  url: '/balance',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager/balance/controller.js']);
                      }]
                    }
              })
              .state('manager.balance.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager/balance/list.html',
              })
              .state('manager.balance.bycategory', {
                  url: '/list',
                  templateUrl: '../../apps/manager/balance/bycategory.html',
              })
              
              /*//balance_category
              .state('manager.balance_category', {
                  abstract: true,
                  url: '/balance_category',
                  template: '<ui-view/>',
                  resolve: {
                      deps: ['$ocLazyLoad',
                        function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['../../apps/manager/balance_category/controller.js']);
                      }]
                    }
              })
              .state('manager.balance_category.list', {
                  url: '/list',
                  templateUrl: '../../apps/manager/balance_category/list.html',
              })*/
               
              
      }
    ]
  );
