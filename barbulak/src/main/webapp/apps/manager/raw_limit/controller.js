'use strict';

/* Controllers */
  // signin controller


app.controller('RawLimitListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', function($scope, restService, ngTableParams, Utils, $modal) {
	

	
	restService.all("place").customPOST({startIndex: 0, resultQuantity: 1000}, "list").then(function(data){
		$scope.places = data.originalElement.resultList;
		$scope.place= {"id" : $scope.places[0].id};
	});
	
	
	$scope.$watch('place.id',function(newValue, oldValue) {
		$scope.getTable(newValue);
	});
	
	$scope.getTable = function(id){
		if(id == null){return false;}
		/*restService.all("product_place").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: 
		{"place.id": id}}, "list").then(function(data){
			$scope.products = data.originalElement.resultList;
		});*/
		
		restService.all("raw_place").customPOST( {"startIndex": 0, resultQuantity: 1000, searchParameters: 
		{"place.id": id}}, "list").then(function(data){
			$scope.raw_places = data.originalElement.resultList;
		});
	}
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };
  }]);

