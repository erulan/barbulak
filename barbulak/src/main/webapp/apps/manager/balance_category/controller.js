
'use strict';

/* Controllers */
  // signin controller


app.controller('BalanceListCtrl', ['$scope', 'restService', 'ngTableParams', 'Utils',  '$modal', function($scope, restService, ngTableParams, Utils, $modal) {
	
	$scope.datepicker = {};
	$scope.datepicker2 = {};
	$scope.categories = {};
	$scope.bycategories = [];
	
    $scope.getTable = function(){
    	restService.all("transaction").customPOST({"fromDate": $scope.fromDate, "toDate": $scope.toDate}, "get_balance_by_category").then(function(data){
    		$scope.bycategories = data;
    		$scope.categories = $scope.bycategories[0].debitCreditBalanceList;
    	});
    }
	$scope.exportData = function () {
		var table= document.getElementById("exportable");
		var html = table.outerHTML;
		window.open('data:application/vnd.ms-excel,' + '\uFEFF' + encodeURIComponent(html));
    };

	
	$scope.datepicker.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker.opened = true;
	  };
	 
	$scope.datepicker2.open = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.datepicker2.opened = true;
	  };
  }]);

